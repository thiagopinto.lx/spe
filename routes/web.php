<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$resources = [
    ['notificacao' => 'dengue', 'controller' => 'DengueController'],
    ['notificacao' => 'chik', 'controller' => 'ChikController'],
    ['notificacao' => 'zika', 'controller' => 'ZikaController'],
    ['notificacao' => 'leishvisc', 'controller' => 'LeishViscController'],
    ['notificacao' => 'leishteg', 'controller' => 'LeishTegController'],
    ['notificacao' => 'meningite', 'controller' => 'MeningiteController'],
    ['notificacao' => 'obito', 'controller' => 'ObitoController']
];

Auth::routes();

Route::get('/', 'HomeController@index')->name('index');
Route::get('/home', 'HomeController@index')->name('home');

///Rotas liberasdas temporariamente

Route::get('sindrome-gripal', 'SindromeGripalController@index')->name('sindrome-gripal');
Route::get('sindrome-gripal/charts', 'SindromeGripalController@charts')->name('sindrome-gripal.charts');

Route::get('srag/detalhamentos', 'SragController@detalhamento')->name('srag.detalhamentos');
Route::get('srag/getdatabar', 'SragController@getDataBar')->name('srag.getdatabar');
Route::get('srag/getdataline', 'SragController@getDataLine')->name('srag.getdataline');
Route::get('srag/getdatalinebysem', 'SragController@getDataLineBySem')->name('srag.getdatalinebysem');
Route::get('srag/getdataclassificacao', 'SragController@getDataClassificacao')->name('srag.getdataclassificacao');
Route::get('srag/getdataevolucao', 'SragController@getDataEvolucao')->name('srag.getdataevolucao');
Route::get('srag/getdataripsa', 'SragController@getDataRipsa')->name('srag.getdataripsa');
Route::get('srag/getdataoutra', 'SragController@getDataOutra')->name('srag.getdataoutra');
Route::get('srag/getdatasexo', 'SragController@getDataSexo')->name('srag.getdatasexo');
Route::get('srag/getdatabairro', 'SragController@getDataBairro')->name('srag.getdatabairro');
Route::get('srag/getsemepdem', 'SragController@getSemEpdem')->name('srag.getsemepdem');
Route::get('srag/getdatasemepdem', 'SragController@getDataSemEpdem')->name('srag.getdatasemepdem');
Route::get('srag/heatmap/{tabela}', 'SragController@heatMap')->name('srag.heatmap');
Route::get('srag/charts/{tabela?}', 'SragController@charts')->name('srag.charts');
Route::get('srag/getdataheatmap/{tabela}', 'SragController@getDataHeatMap')->name('srag.getdataheatmap');
Route::get('srag/clustersmaps', 'SragController@clustersMaps')->name('srag.clustersmaps');
Route::get('srag/getgeocodes', 'SragController@getGeoCodes')->name('srag.getgeocodes');
Route::resource('/srag', 'SragController');

Route::get('sragfull/detalhamentos', 'SragFullController@detalhamento')->name('sragfull.detalhamentos');
Route::get('sragfull/getdatabar', 'SragFullController@getDataBar')->name('sragfull.getdatabar');
Route::get('sragfull/getdataline', 'SragFullController@getDataLine')->name('sragfull.getdataline');
Route::get('sragfull/getdatalinebysem', 'SragFullController@getDataLineBySem')->name('sragfull.getdatalinebysem');
Route::get('sragfull/getdataclassificacao', 'SragFullController@getDataClassificacao')
    ->name('sragfull.getdataclassificacao');
Route::get('sragfull/getdataevolucao', 'SragFullController@getDataEvolucao')->name('sragfull.getdataevolucao');
Route::get('sragfull/getdataripsa', 'SragFullController@getDataRipsa')->name('sragfull.getdataripsa');
Route::get('sragfull/getdataoutra', 'SragFullController@getDataOutra')->name('sragfull.getdataoutra');
Route::get('sragfull/getdatasexo', 'SragFullController@getDataSexo')->name('sragfull.getdatasexo');
Route::get('sragfull/getdatabairro', 'SragFullController@getDataBairro')->name('sragfull.getdatabairro');
Route::get('sragfull/getsemepdem', 'SragFullController@getSemEpdem')->name('sragfull.getsemepdem');
Route::get('sragfull/getdatasemepdem', 'SragFullController@getDataSemEpdem')->name('sragfull.getdatasemepdem');
Route::get('sragfull/heatmap/{tabela}', 'SragFullController@heatMap')->name('sragfull.heatmap');
Route::get('sragfull/charts/{tabela?}', 'SragFullController@charts')->name('sragfull.charts');
Route::get('sragfull/getdataheatmap/{tabela}', 'SragFullController@getDataHeatMap')->name('sragfull.getdataheatmap');
Route::get('sragfull/clustersmaps', 'SragFullController@clustersMaps')->name('sragfull.clustersmaps');
Route::get('sragfull/getgeocodes', 'SragFullController@getGeoCodes')->name('sragfull.getgeocodes');
Route::resource('/sragfull', 'SragFullController');

Route::get('casoscovid/', 'CasosCovidController@index')->name('casoscovid.index');
Route::get('casoscovid/serieabsoluta/{id?}', 'CasosCovidController@serieAbsoluta')
    ->name('casoscovid.serieabsoluta');
Route::get('casoscovid/serieincidencia/{id?}', 'CasosCovidController@serieIncidencia')
    ->name('casoscovid.serieincidencia');
    Route::get('casoscovid/obito', 'CasosCovidController@obito')->name('casoscovid.obito');
Route::get('casoscovid/serieobitoabsoluta/{id?}', 'CasosCovidController@serieObitoAbsoluta')
    ->name('casoscovid.serieobitoabsoluta');
Route::get('casoscovid/serieobitoincidencia/{id?}', 'CasosCovidController@serieObitoIncidencia')
    ->name('casoscovid.serieobitoincidencia');

Route::get('casoscovid/seriecasosdias/{id?}', 'CasosCovidController@serieCasosDias')
    ->name('casoscovid.seriecasosdias');
Route::get('casoscovid/seriemediasdias/{id?}', 'CasosCovidController@serieMediasDias')
    ->name('casoscovid.seriemediasdias');
Route::get('casoscovid/seriemediasincidencia/{id?}', 'CasosCovidController@serieMediasIncidencia')
    ->name('casoscovid.seriemediasincidencia');

Route::get('casoscovid/serieobitosdias/{id?}', 'CasosCovidController@serieObitoDias')
    ->name('casoscovid.serieobitosdias');
Route::get('casoscovid/seriemediasobitosdias/{id?}', 'CasosCovidController@serieMediasObitosDias')
    ->name('casoscovid.seriemediasobitosdias');
Route::get('casoscovid/seriemediasobitosincidencia/{id?}', 'CasosCovidController@serieMediasObitosIncidencias')
    ->name('casoscovid.seriemediasobitosincidencia');

Route::get('casoscovid/evolucao-casos/{cidadeId?}', 'CasosCovidController@evolucaoCasos')
    ->name('casoscovid.evolucao-casos');
Route::get('casoscovid/evolucao-obitos/{cidadeId?}', 'CasosCovidController@evolucaoObitos')
    ->name('casoscovid.evolucao-obitos');
Route::get('casoscovid/map-evolucao', 'CasosCovidController@mapEvolucao')
    ->name('casoscovid.map-evolucao');
Route::get('casoscovid/map-data/{data?}/{cidadeId?}', 'CasosCovidController@mapData')
    ->name('casoscovid.map-data');

Route::get('casoscovidbr/', 'CasosCovidBrController@index')->name('casoscovidbr.index');
Route::get('casoscovidbr/serieabsoluta/{id?}', 'CasosCovidBrController@serieAbsoluta')
    ->name('casoscovidbr.serieabsoluta');
Route::get('casoscovidbr/serieincidencia/{id?}', 'CasosCovidBrController@serieIncidencia')
    ->name('casoscovidbr.serieincidencia');
Route::get('casoscovidbr/obito', 'CasosCovidBrController@obito')->name('casoscovidbr.obito');
Route::get('casoscovidbr/serieobitoabsoluta/{id?}', 'CasosCovidBrController@serieObitoAbsoluta')
    ->name('casoscovidbr.serieobitoabsoluta');
Route::get('casoscovidbr/serieobitoincidencia/{id?}', 'CasosCovidBrController@serieObitoIncidencia')
    ->name('casoscovidbr.serieobitoincidencia');
Route::get('casoscovidbr/seriecasosdias/{id?}', 'CasosCovidBrController@serieCasosDias')
    ->name('casoscovidbr.seriecasosdias');

Route::get('casoscovidbr/seriemediasdias/{id?}', 'CasosCovidBrController@serieMediasDias')
    ->name('casoscovidbr.seriemediasdias');
Route::get('casoscovidbr/seriemediaincidencia/{id?}', 'CasosCovidBrController@serieMediasIncidencia')
    ->name('casoscovidbr.seriemediaincidencia');

Route::get('casoscovidbr/serieobitosdias/{id?}', 'CasosCovidBrController@serieObitosDias')
    ->name('casoscovidbr.serieobitosdias');
Route::get('casoscovidbr/seriemediasobitosdias/{id?}', 'CasosCovidBrController@serieMediasObitosDias')
    ->name('casoscovidbr.seriemediasobitosdias');
Route::get('casoscovidbr/seriemediasobitosincidencia/{id?}', 'CasosCovidBrController@serieMediasObitosIncidencia')
    ->name('casoscovidbr.seriemediasobitosincidencia');

Route::get('casoscovidbr/evolucao-casos/{estadoId?}', 'CasosCovidBrController@evolucaoCasos')
    ->name('casoscovidbr.evolucao-casos');
Route::get('casoscovidbr/evolucao-obitos/{estadoId?}', 'CasosCovidBrController@evolucaoObitos')
    ->name('casoscovidbr.evolucao-obitos');
Route::get('casoscovidbr/map-evolucao', 'CasosCovidBrController@mapEvolucao')
    ->name('casoscovidbr.map-evolucao');
Route::get('casoscovidbr/map-data/{data?}/{estadoId?}', 'CasosCovidBrController@mapData')
    ->name('casoscovidbr.map-data');

Route::get('casoscovidbr-capitais/', 'CasosCovidBrController@capitais')->name('casoscovidbr.capitais');
Route::get('casoscovidbr-capitais/serieabsoluta/{id?}', 'CasosCovidBrController@serieAbsolutaCapitais')
        ->name('casoscovidbr-capitais.serieabsoluta');
Route::get('casoscovidbr-capitais/serieincidencia/{id?}', 'CasosCovidBrController@serieIncidenciaCapitais')
        ->name('casoscovidbr-capitais.serieincidencia');

Route::get('casoscovidbr-capitais/obito', 'CasosCovidBrController@obitoCapitais')->name('casoscovidbr.obitocapitais');
Route::get('casoscovidbr-capitais/serieobitoabsoluta/{id?}', 'CasosCovidBrController@serieObitoAbsolutaCapitais')
    ->name('casoscovidbr-capitais.serieobitoabsoluta');
Route::get('casoscovidbr-capitais/serieobitoincidencia/{id?}', 'CasosCovidBrController@serieObitoIncidenciaCapitais')
    ->name('casoscovidbr-capitais.serieobitoincidencia');

Route::get('casoscovidbr-capitais/seriecasosdias/{id?}', 'CasosCovidBrController@serieCasosDiasCapitais')
    ->name('casoscovidbr-capitais.seriecasosdias');

Route::get('casoscovidbr-capitais/seriemediasdias/{id?}', 'CasosCovidBrController@serieMediasDiasCapitais')
    ->name('casoscovidbr-capitais.seriemediasdias');
Route::get('casoscovidbr-capitais/seriemediaincidencia/{id?}', 'CasosCovidBrController@serieMediasIncidenciaCapitais')
    ->name('casoscovidbr-capitais.seriemediaincidencia');

Route::get('casoscovidbr-capitais/serieobitosdias/{id?}', 'CasosCovidBrController@serieObitosDiasCapitais')
    ->name('casoscovidbr-capitais.serieobitosdias');

Route::get('casoscovidbr-capitais/seriemediasobitosdias/{id?}', 'CasosCovidBrController@serieMediasObitosDiasCapitais')
    ->name('casoscovidbr-capitais.seriemediasobitosdias');
Route::get('casoscovidbr-capitais/seriemediasobitosincidencia/{id?}', 'CasosCovidBrController@serieMediasObitosIncidenciaCapitais')
    ->name('casoscovidbr-capitais.seriemediasobitosincidencia');

Route::get('casoscovidbr-capitais/evolucao-casos/{estadoId?}', 'CasosCovidBrController@evolucaoCapitaisCasos')
    ->name('casoscovidbr-capitais.evolucao-casos');
Route::get('casoscovidbr-capitais/evolucao-obitos/{estadoId?}', 'CasosCovidBrController@evolucaoCapitaisObitos')
    ->name('casoscovidbr-capitais.evolucao-obitos');
Route::get('casoscovidbr-capitais/map-evolucao', 'CasosCovidBrController@mapEvolucaoCapitais')
    ->name('casoscovidbr-capitais.map-evolucao');
Route::get('casoscovidbr-capitais/map-data/{data?}', 'CasosCovidBrController@mapData')
    ->name('casoscovidbr-capitais.map-data');

Route::get('covid-the', 'CovidTheController@index')->name('covid-the.index');
Route::get('covid-the/obito', 'CovidTheController@obito')->name('covid-the.obito');
Route::get('covid-the/sintomatologia', 'CovidTheController@sintomatologia')->name('covid-the.sintomatologia');
Route::get('covid-the/map-evolucao', 'CovidTheController@mapEvolucao')
    ->name('covid-the.map-evolucao');
Route::get('covid-the/map-data-casos/{data?}/{bairroId?}', 'CovidTheController@mapDataCasos')
    ->name('covid-the.map-data-casos');
Route::get('covid-the/map-data-obitos/{data?}/{bairroId?}', 'CovidTheController@mapDataObitos')
    ->name('covid-the.map-data-obitos');
Route::get('covid-the/map-data-incidencias/{data?}/{bairroId?}', 'CovidTheController@mapDataIncidencias')
    ->name('covid-the.map-data-incidencias');
Route::get('covid-the/map-data-table/{data?}/{typeMap?}', 'CovidTheController@mapDataTable')
    ->name('covid-the.map-data-table');
Route::get('covid-the/outras-info', 'CovidTheController@outrasInfo')->name('covid-the.outras-info');

Route::group(['middleware' => 'auth'], function () use ($resources) {
    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('arbovirose', 'ArboviroseController@index')->name('arbovirose');
    Route::get('arbovirose/getgeocodes', 'ArboviroseController@getGeoCodes')->name('arbovirose.getgeocodes');
    Route::get('meningite/getdatacontatoagravo', 'MeningiteController@getDataContatoAgravo')->name('meningite.getdatacontatoagravo');
    Route::get('meningite/getdatasintomashemorragico', 'MeningiteController@getDataSintomasHemorragico')->name('meningite.getdatasintomashemorragico');
    Route::get('meningite/getdatatipo', 'MeningiteController@getDataTipo')->name('meningite.getdatatipo');
    Route::get('meningite/getdatacriterioconfirmacao', 'MeningiteController@getDataCriterioConfirmacao')->name('meningite.getdatacriterioconfirmacao');
    Route::get('meningite/getdatanotificacaounidade', 'MeningiteController@getDataNotifiacaoUnidade')->name('meningite.getdatanotificacaounidade');

    Route::get('meningite/detalhamentos/{unidade}', 'MeningiteController@detalhamentoUnidade')->name('meningite.detalhamentoUnidade');


    Route::get('obitoinfantil', 'ObitoInfantilController@index')->name('obitoinfantil.index');
    Route::get('obitoinfantil/detalhamentos', 'ObitoInfantilController@detalhamento')->name('obitoinfantil.detalhamentos');
    Route::get('obitoinfantil/heatmap/{tabela}', 'ObitoInfantilController@heatMap')->name('obitoinfantil.heatmap');
    Route::get('obitoinfantil/clustersmaps', 'ObitoInfantilController@clustersMaps')->name('obitoinfantil.clustersmaps');
    Route::get('obitoinfantil/getgeocodes', 'ObitoInfantilController@getGeoCodes')->name('obitoinfantil.getgeocodes');
    Route::get('obitoinfantil/getdatabairro', 'ObitoInfantilController@getDataBairro')->name('obitoinfantil.getdatabairro');
    Route::get('obitoinfantil/getdataobitocid', 'ObitoInfantilController@getDataObitoCid')->name('obitoinfantil.getdataobitocid');
    Route::get('obitoinfantil/getdatanotificacaounidade', 'ObitoInfantilController@getDataNotifiacaoUnidade')->name('obitoinfantil.getdatanotificacaounidade');

    Route::get('obitoperinatal', 'ObitoPerinatalController@index')->name('obitoperinatal.index');
    Route::get('obitoperinatalobitoperinatalobitoperinatal/detalhamentos', 'ObitoPerinatalController@detalhamento')->name('obitoperinatal.detalhamentos');
    Route::get('obitoperinatalobitoperinatal/heatmap/{tabela}', 'ObitoPerinatalController@heatMap')->name('obitoperinatal.heatmap');
    Route::get('obitoperinatal/clustersmaps', 'ObitoPerinatalController@clustersMaps')->name('obitoperinatal.clustersmaps');
    Route::get('obitoperinatal/getgeocodes', 'ObitoPerinatalController@getGeoCodes')->name('obitoperinatal.getgeocodes');
    Route::get('obitoperinatal/getdatabairro', 'ObitoPerinatalController@getDataBairro')->name('obitoperinatal.getdatabairro');
    Route::get('obitoperinatal/getdataobitocid', 'ObitoPerinatalController@getDataObitoCid')->name('obitoperinatal.getdataobitocid');
    Route::get('obitoperinatal/getdatanotificacaounidade', 'ObitoPerinatalController@getDataNotifiacaoUnidade')->name('obitoperinatal.getdatanotificacaounidade');

    Route::get('ciclos/getdataciclo/{ano}', 'CicloController@getDataCiclo')->name('ciclos.getdataciclo');

    Route::get('imunizacao', 'ImunizacaoController@index')->name('imunizacao.index');
    Route::get('imunizacao/imuno/{id}', 'ImunizacaoController@imuno')->name('imunizacao.imuno');
    Route::get('imunizacao/estabelecimentos/{idImuno?}/{ano?}', 'ImunizacaoController@estabelecimentos')->name('imunizacao.estabelecimentos');
    Route::get('imunizacao/estabelecimento/search/{id}', 'ImunizacaoController@estabelecimentoSearch')->name('imunizacao.estabelecimento.search');
    Route::get('imunizacao/estabelecimento/autocomplete/{id}', 'ImunizacaoController@estabelecimentoAutoComplete')->name('imunizacao.estabelecimento.autocomplete');
    Route::get('imunizacao/estabelecimento/{idImuno?}/{ano?}/{idEstabelecimento?}', 'ImunizacaoController@estabelecimento')->name('imunizacao.estabelecimento');

    //Route::get('sindrome-gripal', 'SindromeGripalController@index')->name('sindrome-gripal');
    //Route::get('sindrome-gripal/charts', 'SindromeGripalController@charts')->name('sindrome-gripal.charts');



    foreach ($resources as $resource) {
        Route::get($resource['notificacao'] . '/detalhamentos', $resource['controller'] . '@detalhamento')->name($resource['notificacao'] . '.detalhamentos');
        Route::get($resource['notificacao'] . '/getdatabar', $resource['controller'] . '@getDataBar')->name($resource['notificacao'] . '.getdatabar');
        Route::get($resource['notificacao'] . '/getdataline', $resource['controller'] . '@getDataLine')->name($resource['notificacao'] . '.getdataline');
        Route::get($resource['notificacao'] . '/getdatalinebysem', $resource['controller'] . '@getDataLineBySem')->name($resource['notificacao'] . '.getdatalinebysem');
        Route::get($resource['notificacao'] . '/getdataclassificacao', $resource['controller'] . '@getDataClassificacao')->name($resource['notificacao'] . '.getdataclassificacao');
        Route::get($resource['notificacao'] . '/getdataevolucao', $resource['controller'] . '@getDataEvolucao')->name($resource['notificacao'] . '.getdataevolucao');
        Route::get($resource['notificacao'] . '/getdataripsa', $resource['controller'] . '@getDataRipsa')->name($resource['notificacao'] . '.getdataripsa');
        Route::get($resource['notificacao'] . '/getdatasexo', $resource['controller'] . '@getDataSexo')->name($resource['notificacao'] . '.getdatasexo');
        Route::get($resource['notificacao'] . '/getdatabairro', $resource['controller'] . '@getDataBairro')->name($resource['notificacao'] . '.getdatabairro');
        Route::get($resource['notificacao'] . '/getsemepdem', $resource['controller'] . '@getSemEpdem')->name($resource['notificacao'] . '.getsemepdem');
        Route::get($resource['notificacao'] . '/getdatasemepdem', $resource['controller'] . '@getDataSemEpdem')->name($resource['notificacao'] . '.getdatasemepdem');
        Route::get($resource['notificacao'] . '/heatmap/{tabela}', $resource['controller'] . '@heatMap')->name($resource['notificacao'] . '.heatmap');
        Route::get($resource['notificacao'] . '/getdataheatmap/{tabela}', $resource['controller'] . '@getDataHeatMap')->name($resource['notificacao'] . '.getdataheatmap');
        Route::get($resource['notificacao'] . '/clustersmaps', $resource['controller'] . '@clustersMaps')->name($resource['notificacao'] . '.clustersmaps');
        Route::get($resource['notificacao'] . '/getgeocodes', $resource['controller'] . '@getGeoCodes')->name($resource['notificacao'] . '.getgeocodes');
        Route::resource('/' . $resource['notificacao'], $resource['controller']);
    }

    /*
    | Panel admin
    */
    Route::group(['namespace' => 'Painel', 'prefix' => 'painel', 'as' => 'painel.'], function () use ($resources) {
        Route::get('/', 'HomeController@index')->name('home');

        Route::get('users', 'UsersController@index')->name('users');
        Route::post('users/getindex', 'UsersController@getIndex')->name('users.getindex');
        Route::post('users/destroy', 'UsersController@destroy')->name('users.destroy');

        $resources[] = ['notificacao' => 'srag', 'controller' => 'SragController'];
        $resources[] = ['notificacao' => 'sragfull', 'controller' => 'SragFullController'];
        $resources[] = ['notificacao' => 'sg', 'controller' => 'SgController'];
        $resources[] = ['notificacao' => 'sgfull', 'controller' => 'SgFullController'];


        foreach ($resources as $resource) {
            Route::get($resource['notificacao'], $resource['controller'] . '@index')->name($resource['notificacao']);
            Route::post($resource['notificacao'] . '/store', $resource['controller'] . '@store')->name($resource['notificacao'] . '.store');
            Route::post($resource['notificacao'] . '/getindex', $resource['controller'] . '@getIndex')->name($resource['notificacao'] . '.getindex');
            Route::post($resource['notificacao'] . '/storeColor', $resource['controller'] . '@storeColor')->name($resource['notificacao'] . '.storecolor');
        }

        Route::post('notificacao/getgps/{tabela}', 'NotificacaoController@getGps')->name('notificacao.getgps');
        Route::post('notificacao/getindex/{tabela}', 'NotificacaoController@getIndex')->name('notificacao.getindex');
        Route::get('notificacao/{tabela}', 'NotificacaoController@index')->name('notificacao.index');
        Route::get('notificacao/editgeocode/{tabela}/{agravo}', 'NotificacaoController@editGeocode')->name('notificacao.editgeocode');
        Route::match(['put', 'patch'], 'notificacao/updategeocode/{tabela}', 'NotificacaoController@updateGeocode')->name('notificacao.updategeocode');
        Route::match(['put', 'patch'], 'notificacao/updategeocodes/{tabela}', 'NotificacaoController@updateGeocodes')->name('notificacao.updategeocodes');
        Route::get('notificacao/editgeocodes/{tabela}', 'NotificacaoController@editGeocodes')->name('notificacao.editgeocodes');
        Route::get('notificacao/getdatageocodes/{tabela}', 'NotificacaoController@getDataGeocodes')->name('notificacao.getdatageocodes');

        Route::post('notificacao-obito/getgps/{tabela}', 'NotificacaoObitoController@getGps')->name('notificacao-obito.getgps');
        Route::post('notificacao-obito/getindex/{tabela}', 'NotificacaoObitoController@getIndex')->name('notificacao-obito.getindex');
        Route::get('notificacao-obito/{tabela}', 'NotificacaoObitoController@index')->name('notificacao-obito.index');
        Route::get('notificacao-obito/editgeocode/{tabela}/{agravo}', 'NotificacaoObitoController@editGeocode')->name('notificacao-obito.editgeocode');
        Route::match(['put', 'patch'], 'notificacao-obito/updategeocode/{tabela}', 'NotificacaoObitoController@updateGeocode')->name('notificacao-obito.updategeocode');
        Route::match(['put', 'patch'], 'notificacao-obito/updategeocodes/{tabela}', 'NotificacaoObitoController@updateGeocodes')->name('notificacao-obito.updategeocodes');
        Route::get('notificacao-obito/editgeocodes/{tabela}', 'NotificacaoObitoController@editGeocodes')->name('notificacao-obito.editgeocodes');
        Route::get('notificacao-obito/getdatageocodes/{tabela}', 'NotificacaoObitoController@getDataGeocodes')->name('notificacao-obito.getdatageocodes');

        Route::post('notificacao-srag/getgps/{tabela}', 'NotificacaoSragController@getGps')
            ->name('notificacao-srag.getgps');
        Route::post('notificacao-srag/getindex/{tabela}', 'NotificacaoSragController@getIndex')
            ->name('notificacao-srag.getindex');
        Route::get('notificacao-srag/{tabela}', 'NotificacaoSragController@index')
            ->name('notificacao-srag.index');
        Route::get('notificacao-srag/editgeocode/{tabela}/{agravo}', 'NotificacaoSragController@editGeocode')
            ->name('notificacao-srag.editgeocode');
        Route::match(['put', 'patch'], 'notificacao-srag/updategeocode/{tabela}', 'NotificacaoSragController@updateGeocode')
            ->name('notificacao-srag.updategeocode');
        Route::match(['put', 'patch'], 'notificacao-srag/updategeocodes/{tabela}', 'NotificacaoSragController@updateGeocodes')
            ->name('notificacao-srag.updategeocodes');
        Route::get('notificacao-srag/editgeocodes/{tabela}', 'NotificacaoSragController@editGeocodes')
            ->name('notificacao-srag.editgeocodes');
        Route::get('notificacao-srag/getdatageocodes/{tabela}', 'NotificacaoSragController@getDataGeocodes')
            ->name('notificacao-srag.getdatageocodes');

        Route::post('notificacao-sg/getgps/{tabela}', 'NotificacaoSgController@getGps')
            ->name('notificacao-sg.getgps');
        Route::post('notificacao-sg/getindex/{tabela}', 'NotificacaoSgController@getIndex')
            ->name('notificacao-sg.getindex');
        Route::get('notificacao-sg/{tabela}', 'NotificacaoSgController@index')
            ->name('notificacao-sg.index');
        Route::get('notificacao-sg/editgeocode/{tabela}/{agravo}', 'NotificacaoSgController@editGeocode')
            ->name('notificacao-sg.editgeocode');
        Route::match(['put', 'patch'], 'notificacao-sg/updategeocode/{tabela}', 'NotificacaoSgController@updateGeocode')
            ->name('notificacao-sg.updategeocode');
        Route::match(['put', 'patch'], 'notificacao-sg/updategeocodes/{tabela}', 'NotificacaoSgController@updateGeocodes')
            ->name('notificacao-sg.updategeocodes');
        Route::get('notificacao-sg/editgeocodes/{tabela}', 'NotificacaoSgController@editGeocodes')
            ->name('notificacao-sg.editgeocodes');
        Route::get('notificacao-sg/getdatageocodes/{tabela}', 'NotificacaoSgController@getDataGeocodes')
            ->name('notificacao-sg.getdatageocodes');

        Route::post('unidadesnotificadoras/getindex', 'UnidadeNotificadoraController@getIndex')->name('unidadesnotificadoras.getindex');
        Route::get('unidadesnotificadoras', 'UnidadeNotificadoraController@index')->name('unidadesnotificadoras');
        Route::post('unidadesnotificadoras/store', 'UnidadeNotificadoraController@store')->name('unidadesnotificadoras.store');

        Route::post('cids/getindex', 'CidController@getIndex')->name('cids.getindex');
        Route::get('cids', 'CidController@index')->name('cids');
        Route::post('cids/store', 'CidController@store')->name('cids.store');

        Route::post('bairros/getindex', 'BairroController@getIndex')->name('bairros.getindex');
        Route::get('bairros', 'BairroController@index')->name('bairros');
        Route::post('bairros/store', 'BairroController@store')->name('bairros.store');
        Route::get('bairros/map/{id}', 'BairroController@map')->name('bairros.map');
        Route::get('bairros/autocomplete', 'BairroController@autocomplete')->name('bairros.autocomplete');

        Route::post('cidades/getindex', 'CidadeController@getIndex')->name('cidades.getindex');
        Route::get('cidades', 'CidadeController@index')->name('cidades');
        Route::post('cidades/store', 'CidadeController@store')->name('cidades.store');
        Route::get('cidades/map/{id}', 'CidadeController@map')->name('cidades.map');
        Route::get('cidades/autocomplete', 'CidadeController@autocomplete')->name('cidades.autocomplete');

        Route::post('estados/getindex', 'EstadoController@getIndex')->name('estados.getindex');
        Route::get('estados', 'EstadoController@index')->name('estados');
        Route::post('estados/store', 'EstadoController@store')->name('estados.store');
        Route::get('estados/map/{id}', 'EstadoController@map')->name('estados.map');
        Route::get('estados/autocomplete', 'EstadoController@autocomplete')->name('estados.autocomplete');

        Route::post('regionais/getindex', 'RegionalController@getIndex')->name('regionais.getindex');
        Route::get('regionais', 'RegionalController@index')->name('regionais');
        Route::post('regionais/store', 'RegionalController@store')->name('regionais.store');
        Route::get('regionais/map/{id}', 'RegionalController@map')->name('regionais.map');

        Route::get('ciclos', 'CicloController@index')->name('ciclos');
        Route::post('ciclos/getindex', 'CicloController@getIndex')->name('ciclos.getindex');
        Route::post('ciclos/store', 'CicloController@store')->name('ciclos.store');
        Route::get('ciclos/bairros/{id}', 'CicloController@bairros')->name('ciclos.bairros');
        Route::post('ciclos/getindexbairros/{id}', 'CicloController@getIndexBairros')->name('ciclos.getindexbairros');
        Route::post('ciclos/storebairrociclo/{id}', 'CicloController@storeBairroCiclo')->name('ciclos.storebairrociclo');
        Route::delete('ciclos/bairro/destroy/{id}', 'CicloController@destroyBairros')->name('ciclos.bairro.destroy');

        Route::get('estabelecimentos-cnes', 'EstabelecimentoCnesController@index')
            ->name('estabelecimentos-cnes');
        Route::post('estabelecimentos-cnes/getindex', 'EstabelecimentoCnesController@getIndex')
            ->name('estabelecimentos-cnes.getindex');
        Route::post('estabelecimentos-cnes/store', 'EstabelecimentoCnesController@store')
            ->name('estabelecimentos-cnes.store');
        Route::get('estabelecimentos-cnes/editgeocodes', 'EstabelecimentoCnesController@editGeocodes')
            ->name('estabelecimentos-cnes.editgeocodes');
        Route::get('estabelecimentos-cnes/getdatageocodes', 'EstabelecimentoCnesController@getDataGeocodes')
            ->name('estabelecimentos-cnes.getdatageocodes');
        Route::match(['put', 'patch'], 'notificacao-obito/updategeocodes', 'EstabelecimentoCnesController@updateGeocodes')
            ->name('estabelecimentos-cnes.updategeocodes');
        Route::get('estabelecimentos-cnes/editgeocode/{co_cnes}', 'EstabelecimentoCnesController@editGeocode')
            ->name('estabelecimentos-cnes.editgeocode');
        Route::match(['put', 'patch'], 'notificacao-obito/updategeocode', 'EstabelecimentoCnesController@updateGeocode')
            ->name('estabelecimentos-cnes.updategeocode');
        Route::post('estabelecimentos-cnes/getgps', 'EstabelecimentoCnesController@getGps')
            ->name('estabelecimentos-cnes.getgps');
        Route::get(
            'estabelecimentos-cnes/autocomplete-fantasia',
            'EstabelecimentoCnesController@autocompleteFantasia'
        )->name('estabelecimentos-cnes.autocomplete-fantasia');
        Route::get(
            'estabelecimentos-cnes/autocomplete-razaosocial',
            'EstabelecimentoCnesController@autocompleteRazaoSocial'
        )->name('estabelecimentos-cnes.autocomplete-razaosocial');

        Route::get('load-sies', 'LoadSiesController@index')
            ->name('load-sies');
        Route::post('load-sies/getindex', 'LoadSiesController@getIndex')
            ->name('load-sies.getindex');
        Route::post('load-sies/store', 'LoadSiesController@store')
            ->name('load-sies.store');

        Route::get('movimento-sies/{load_sies_id}', 'MovimentoSiesController@index')
            ->name('movimento-sies');
        Route::post('movimento-sies/getindex/{load_sies_id}', 'MovimentoSiesController@getIndex')
            ->name('movimento-sies.getindex');

        Route::get('estabelecimentos-sies', 'EstabelecimentoSiesController@index')
            ->name('estabelecimentos-sies');
        Route::post('estabelecimentos-sies/getindex', 'EstabelecimentoSiesController@getIndex')
            ->name('estabelecimentos-sies.getindex');
        Route::post('estabelecimentos-sies/store', 'EstabelecimentoSiesController@store')
            ->name('estabelecimentos-sies.store');
        Route::get('estabelecimentos-sies/relationship/{idSies}/{idCnes}', 'EstabelecimentoSiesController@relationship')
            ->name('estabelecimentos-sies.relationship');

        Route::get('load-sipni', 'LoadSipniController@index')
            ->name('load-sipni');
        Route::post('load-sipni/getindex', 'LoadSipniController@getIndex')
            ->name('load-sipni.getindex');
        Route::post('load-sipni/store', 'LoadSipniController@store')
            ->name('load-sipni.store');
        Route::post('load-sipni/storeColor', 'LoadSipniController@storeColor')->name('load-sipni.storecolor');


        Route::get('doses-aplicadas-sipni/{load_sipni_id}', 'DosesAplicadasSipniController@index')
            ->name('doses-aplicadas-sipni');
        Route::post('doses-aplicadas-sipni/getindex/{load_sipni_id}', 'DosesAplicadasSipniController@getIndex')
            ->name('doses-aplicadas-sipni.getindex');

        Route::get('imuno-sies', 'ImunoSiesController@index')
            ->name('imuno-sies');
        Route::post('imuno-sies/getindex', 'ImunoSiesController@getIndex')
            ->name('imuno-sies.getindex');
        Route::get('imuno-sies/relationship/{idSies}/{idSipni}', 'ImunoSiesController@relationship')
            ->name('imuno-sies.relationship');
        Route::get('imuno-sies/percatecnica/{idImunoSies}/{perdaImuno}', 'ImunoSiesController@percatecnica')
            ->name('imuno-sies.percatecnica');

        Route::get('imuno-sipni', 'ImunoSipniController@index')
            ->name('imuno-sipni');
        Route::post('imuno-sipni/getindex', 'ImunoSipniController@getIndex')
            ->name('imuno-sipni.getindex');
        Route::get(
            'imuno-sipni/autocomplete-nome',
            'ImunoSipniController@autocompleteNome'
        )->name('imuno-sipni.autocomplete-nome');
        Route::get(
            'imuno-sipni/autocomplete-sigla',
            'ImunoSipniController@autocompleteSigla'
        )->name('imuno-sipni.autocomplete-sigla');

        Route::get('/imunizacao', 'ImunizacaoController@index')->name('imunizacao');

        Route::get('load-inmet', 'LoadInmetController@index')
            ->name('load-inmet');
        Route::post('load-inmet/getindex', 'LoadInmetController@getIndex')
            ->name('load-inmet.getindex');
        Route::post('load-inmet/store', 'LoadInmetController@store')
            ->name('load-inmet.store');
        Route::post('load-inmet/storeColor', 'LoadInmetController@storeColor')->name('load-inmet.storecolor');

        Route::get('dados-inmet/{load_inmet_id}', 'DadosInmetController@index')
            ->name('dados-inmet');
        Route::post('dados-inmet/getindex/{load_inmet_id}', 'DadosInmetController@getIndex')
            ->name('dados-inmet.getindex');

        Route::get('load-trend', 'LoadTrendController@index')
            ->name('load-trend');
        Route::post('load-trend/getindex', 'LoadTrendController@getIndex')
            ->name('load-trend.getindex');
        Route::post('load-trend/store', 'LoadTrendController@store')
            ->name('load-trend.store');
        Route::post('load-trend/storeColor', 'LoadTrendController@storeColor')->name('load-trend.storecolor');

        Route::get('dados-trend/{load_trend_id}', 'DadosTrendController@index')
            ->name('dados-trend');
        Route::post('dados-trend/getindex/{load_trend_id}', 'DadosTrendController@getIndex')
            ->name('dados-trend.getindex');

        Route::get('casoscovid', 'CasosCovidController@index')
            ->name('casoscovid');
        Route::post('casoscovid/getindex', 'CasosCovidController@getIndex')
            ->name('casoscovid.getindex');
        Route::post('casoscovid/store', 'CasosCovidController@store')
            ->name('casoscovid.store');
        Route::get('casoscovid/update_by_api', 'CasosCovidController@updateByApi')
            ->name('casoscovid.update_by_api');

        Route::get('covid-datasus', 'CovidDatasusController@index')
            ->name('covid-datasus');
        Route::post('covid-datasus/getindex', 'CovidDatasusController@getIndex')
            ->name('covid-datasus.getindex');
        Route::get('covid-datasus/update/{dateInicio?}/{dateFim?}', 'CovidDatasusController@update')
            ->name('covid-datasus.update');

        Route::get('bairro-alias', 'CovidDatasusController@indexBairro')
            ->name('bairro-alias');
        Route::post('bairro-alias/getindex', 'CovidDatasusController@getIndexBairro')
            ->name('bairro-alias.getindex');
        Route::get(
            'bairro-alias/relationship/{idBairroAlias}/{idBairro}',
            'CovidDatasusController@relationship'
        )->name('bairro-alias.relationship');
        Route::post('bairro-alias/getgps', 'CovidDatasusController@getGps')->name('bairro-alias.getgps');
    });
});
