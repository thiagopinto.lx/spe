@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <div class="d-sm-flex align-items-right right-content-between mb-4 d-print-none">
        <a href="{{$urls['index']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Casos confirmados
        </a>
        <a href="{{$urls['obito']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Óbito confirmados
        </a>
        <a href="{{$urls['sintomatologia']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Sintomatologia
        </a>
        <a href="{{$urls['map-evolucao']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Mapa da evolução dos casos
        </a>
        <a href="{{$urls['outras-info']}}" style="margin-right: 10px;"
        class="d-none d-sm-inline-block btn btn-sm
        @if($options['area'] == 'outras-info')
            btn-outline-primary
        @else
            btn-primary
        @endif
        shadow-sm">
            <i class="fas fa-tachometer-alt
            @if($options['area'] == 'outras-info')
                text-primary-50
            @else
                text-white-50
            @endif
            "></i> Outras Informações
        </a>
      </div>
    </div>

    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Unidades com mais notificação E-SUS VE</h6>
                </div>
                <div class="card-body">
                    <div id="unidadesNotificadoras">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Distribuição de teste com resultado E-SUS VE</h6>
                </div>
                <div class="card-body">
                    <div id="tiposDeTeste">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Distribuição de teste por CBO com resultado E-SUS VE RT-PCR</h6>
                </div>
                <div class="card-body">
                    <div id="cbo-rt-pcr">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Distribuição de teste por CBO com resultado E-SUS VE TESTE RÁPIDO - ANTICORPO</h6>
                </div>
                <div class="card-body">
                    <div id="cbo-anticorpo">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Distribuição de teste por CBO com resultado E-SUS VE TESTE RÁPIDO - ANTÍGENO</h6>
                </div>
                <div class="card-body">
                    <div id="cbo-antigeno">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Distribuição de teste por CBO com resultado E-SUS VE</h6>
                </div>
                <div class="card-body">
                    <div id="cbo">
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
@section('scripts')
<!-- Page level plugins -->
<script src="{{ mix('/js/apexcharts.min.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";

        var optionsUnidadesNotificadoras = {
          series: [{
          data: {!! json_encode($unidadesNotificadoras) !!}
        }],
          chart: {
          type: 'bar',
          height: 500,
        },
        plotOptions: {
          bar: {
            barHeight: '100%',
            distributed: true,
            horizontal: true,
            dataLabels: {
              position: 'bottom'
            }
          }
        },
        colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e',
          '#f48024', '#69d2e7'
        ],
        dataLabels: {
          enabled: true,
          textAnchor: 'start',
          style: {
            fontWeight: '400',
            colors: ['#444']
          },
          formatter: function (val, opt) {
            return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val
          },
          offsetX: 0,
          dropShadow: {
            enabled: true
          }
        },
        stroke: {
          width: 1,
          colors: ['#fff']
        },
        xaxis: {
            type: 'category'
        },
        yaxis: {
          labels: {
            show: false,
            formatter: function (val) {
              return val;
            }
          }
        },
        tooltip: {
          theme: 'dark',
          x: {
            show: false
          },
          y: {
            title: {
              formatter: function () {
                return ''
              }
            }
          }
        }
        };

        var optionstiposDeTeste = {
          series: [{
            name: {!! json_encode($resultados->negativos->name) !!},
            data: {!! json_encode($resultados->negativos->data) !!}

          },
          {
              name: {!! json_encode($resultados->positivos->name) !!},
              data: {!! json_encode($resultados->positivos->data) !!}
          }
          ],
            chart: {
            type: 'bar',
            height: 440,
            stacked: true
          },
          colors: ['#00A100', '#FF0000'],
          plotOptions: {
            bar: {
              horizontal: true,
              barHeight: '80%',

            },
          },
          dataLabels: {
            enabled: true,
            formatter: function (val) {
                return Math.abs(val)
            }
          },
          stroke: {
            width: 1,
            colors: ["#fff"]
          },

          grid: {
            xaxis: {
              lines: {
                show: true
              }
            }
          },
          yaxis: {
            title: {
              // text: 'Age',
            },
          },
          tooltip: {
            shared: false,
            x: {
              formatter: function (val) {
                return val
              }
            },
            y: {
              formatter: function (val) {
                return Math.abs(val)
              }
            }
          },
          xaxis: {
            categories: {!! json_encode($resultados->categorias) !!},
            labels: {
              formatter: function (val) {
                return Math.abs(Math.round(val))
              }
            }
          },
        };

        /*
        *CBO
        */
        var optionsCbo = {
            series: [{
              name: {!! json_encode($cbo->negativos->name) !!},
              data: {!! json_encode($cbo->negativos->data) !!}

          },
          {
              name: {!! json_encode($cbo->positivos->name) !!},
              data: {!! json_encode($cbo->positivos->data) !!}
          }
          ],
            chart: {
            type: 'bar',
            height: 880,
            stacked: true
          },
          colors: ['#00A100', '#FF0000'],
          plotOptions: {
            bar: {
              horizontal: true,
              barHeight: '80%',

            },
          },
          dataLabels: {
            enabled: true,
            formatter: function (val) {
                return Math.abs(val)
            }
          },
          stroke: {
            width: 1,
            colors: ["#fff"]
          },

          grid: {
            xaxis: {
              lines: {
                show: true
              }
            }
          },
          yaxis: {
            title: {
              // text: 'Age',
            },
          },
          tooltip: {
            shared: false,
            x: {
              formatter: function (val) {
                return val
              }
            },
            y: {
              formatter: function (val) {
                return Math.abs(val)
              }
            }
          },
          xaxis: {
            categories: {!! json_encode($cbo->categorias) !!},
            labels: {
              formatter: function (val) {
                return Math.abs(Math.round(val))
              }
            }
          }
        };

        /*
        *CBO RT-PCR
        */
        var optionsCboRtPcr = {
            series: [{
              name: {!! json_encode($cboRtPcr->negativos->name) !!},
              data: {!! json_encode($cboRtPcr->negativos->data) !!}

          },
          {
              name: {!! json_encode($cboRtPcr->positivos->name) !!},
              data: {!! json_encode($cboRtPcr->positivos->data) !!}
          }
          ],
            chart: {
            type: 'bar',
            height: 880,
            stacked: true
          },
          colors: ['#00A100', '#FF0000'],
          plotOptions: {
            bar: {
              horizontal: true,
              barHeight: '80%',

            },
          },
          dataLabels: {
            enabled: true,
            formatter: function (val) {
                return Math.abs(val)
            }
          },
          stroke: {
            width: 1,
            colors: ["#fff"]
          },

          grid: {
            xaxis: {
              lines: {
                show: true
              }
            }
          },
          yaxis: {
            title: {
              // text: 'Age',
            },
          },
          tooltip: {
            shared: false,
            x: {
              formatter: function (val) {
                return val
              }
            },
            y: {
              formatter: function (val) {
                return Math.abs(val)
              }
            }
          },
          xaxis: {
            categories: {!! json_encode($cboRtPcr->categorias) !!},
            labels: {
              formatter: function (val) {
                return Math.abs(Math.round(val))
              }
            }
          }
        };


        /*
        *CBO TESTE RÁPIDO - ANTICORPO
        */
        var optionsCboAnticorpo = {
            series: [{
              name: {!! json_encode($cboAnticorpo->negativos->name) !!},
              data: {!! json_encode($cboAnticorpo->negativos->data) !!}

          },
          {
              name: {!! json_encode($cboAnticorpo->positivos->name) !!},
              data: {!! json_encode($cboAnticorpo->positivos->data) !!}
          }
          ],
            chart: {
            type: 'bar',
            height: 880,
            stacked: true
          },
          colors: ['#00A100', '#FF0000'],
          plotOptions: {
            bar: {
              horizontal: true,
              barHeight: '80%',

            },
          },
          dataLabels: {
            enabled: true,
            formatter: function (val) {
                return Math.abs(val)
            }
          },
          stroke: {
            width: 1,
            colors: ["#fff"]
          },

          grid: {
            xaxis: {
              lines: {
                show: true
              }
            }
          },
          yaxis: {
            title: {
              // text: 'Age',
            },
          },
          tooltip: {
            shared: false,
            x: {
              formatter: function (val) {
                return val
              }
            },
            y: {
              formatter: function (val) {
                return Math.abs(val)
              }
            }
          },
          xaxis: {
            categories: {!! json_encode($cboAnticorpo->categorias) !!},
            labels: {
              formatter: function (val) {
                return Math.abs(Math.round(val))
              }
            }
          }
        };

                /*
        *CBO TESTE RÁPIDO - ANTÍGENO
        */
        var optionsCboAntigeno = {
            series: [{
              name: {!! json_encode($cboAntigeno->negativos->name) !!},
              data: {!! json_encode($cboAntigeno->negativos->data) !!}

          },
          {
              name: {!! json_encode($cboAntigeno->positivos->name) !!},
              data: {!! json_encode($cboAntigeno->positivos->data) !!}
          }
          ],
            chart: {
            type: 'bar',
            height: 880,
            stacked: true
          },
          colors: ['#00A100', '#FF0000'],
          plotOptions: {
            bar: {
              horizontal: true,
              barHeight: '80%',

            },
          },
          dataLabels: {
            enabled: true,
            formatter: function (val) {
                return Math.abs(val)
            }
          },
          stroke: {
            width: 1,
            colors: ["#fff"]
          },

          grid: {
            xaxis: {
              lines: {
                show: true
              }
            }
          },
          yaxis: {
            title: {
              // text: 'Age',
            },
          },
          tooltip: {
            shared: false,
            x: {
              formatter: function (val) {
                return val
              }
            },
            y: {
              formatter: function (val) {
                return Math.abs(val)
              }
            }
          },
          xaxis: {
            categories: {!! json_encode($cboAntigeno->categorias) !!},
            labels: {
              formatter: function (val) {
                return Math.abs(Math.round(val))
              }
            }
          }
        };

    $(document).ready(() => {

        var unidadesNotificadoras = new ApexCharts(document.querySelector("#unidadesNotificadoras"), optionsUnidadesNotificadoras);
        unidadesNotificadoras.render();

        var tiposDeTeste = new ApexCharts(document.querySelector("#tiposDeTeste"), optionstiposDeTeste);
        tiposDeTeste.render();

        var cboRtPcr = new ApexCharts(document.querySelector("#cbo-rt-pcr"), optionsCboRtPcr);
        cboRtPcr.render();

        var cboAnticorpo = new ApexCharts(document.querySelector("#cbo-anticorpo"), optionsCboAnticorpo);
        cboAnticorpo.render();

        var cboAntigeno = new ApexCharts(document.querySelector("#cbo-antigeno"), optionsCboAntigeno);
        cboAntigeno.render();

        var cbo = new ApexCharts(document.querySelector("#cbo"), optionsCbo);
        cbo.render();



    });
</script>
@endsection
