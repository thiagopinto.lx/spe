@extends('layouts.home')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-header py-3">
                <h2 class="m-0 font-weight-bold text-primary">Painel Endemiológico </h2>
              </div>
              <div class="card-body border-bottom-primary">
                <p>
                  Desde dezembro de 2019, a FMS através de funcionários da Gerência de Vigilância Epidemiológica, juntamente com membros do CIATEN (Centro de Inteligência de Agravos Tropicais Emergentes e Negligenciados), vem elaborando este painel epidemiológico para os diversos agravos de Teresina. Com o avanço da pandemia na região, decidimos incluir também os dados de COVID-19, em conjunto com a equipe da Sala de situação do grupo de saúde do Comitê Gestor de Crise da Universidade Federal do Piauí (UFPI).
                </p>
                <p>
                  Os gráficos, mapas e tabelas estão licenciados como Atribuição-NãoComercial CC BY-NC. Esta licença permite que outros remixem, adaptem e criem, desde que atribuam o devido crédito, e desde que para fins não comerciais.
                </p>
              </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-body border-bottom-primary">
                <h3 class="m-0 font-weight-bold text-primary">COVID-19</h3>
              </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('casoscovidbr.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>COVID-19 BR</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('casoscovid.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>COVID-19 PI</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('covid-the.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>COVID-19 THE</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('sragfull.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>SRAG Notificados THE</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('srag.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>SRAG Residentes THE</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
              <div class="card-body border-bottom-primary">
                <h3 class="m-0 font-weight-bold text-primary">Demais Agravos</h3>
              </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('arbovirose')}}">
                        <i class="icofont-chart-line"></i>
                        <span>Arboviroses</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('dengue.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>Dengue</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('chik.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>Chikungunya</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('zika.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>Zika</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('leishvisc.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>Leishmaniose Visceral</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('leishteg.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>Leishmaniose Tegumentar Americana</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('meningite.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>Meniginte</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('obitoinfantil.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>Óbitos Infantis</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('obitoperinatal.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>Óbitos Perinatal</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card shadow mb-4 h-75">
              <div class="card-body border-bottom-primary">
                <h5 class="m-0 font-weight-bold text-primary">
                    <a class="nav-link" href="{{route('imunizacao.index')}}">
                        <i class="icofont-chart-line"></i>
                        <span>Imunização</span>
                    </a>
                </h5>
              </div>
            </div>
        </div>

    </div>

</div>
@endsection

