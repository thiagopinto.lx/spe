@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <div class="d-sm-flex align-items-right right-content-between mb-4 d-print-none">
        <a href="{{$urls['index']}}" style="margin-right: 10px;"
          class="d-none d-sm-inline-block btn btn-sm
          @if($options['area'] == 'index')
              btn-outline-primary
          @else
              btn-primary
          @endif
          shadow-sm">
              <i class="fas fa-tachometer-alt
              @if($options['area'] == 'index')
                  text-primary-50
              @else
                  text-white-50
              @endif
              "></i> Casos confirmados
        </a>
        <a href="{{$urls['obito']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
          <i class="fas fa-tachometer-alt text-white-50"></i> Óbito confirmados
        </a>
        <a href="{{$urls['sintomatologia']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Sintomatologia
        </a>
        <a href="{{$urls['map-evolucao']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Mapa da evolução dos casos
        </a>
        <a href="{{$urls['outras-info']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Outras Informações
        </a>
      </div>
    </div>
    <div class="row">
      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
          <div class="card-body p-1">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    <h6><strong>Casos Confirmados</strong></h6>
                </div>
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    E-SUS:
                    <span class="text-gray-800">
                        {{$totalCasos['esus']}}
                    </span>
                </div>
                <div class="text-gray-600 text-xs">
                    <small>Atualizado: {{$options['updateEsus']}}</small>
                </div>
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    SIVEP SG:
                    <span class="text-gray-800">
                        {{$totalCasos['sg']}}
                    </span>
                </div>
                <div class="text-gray-600 text-xs">
                    <small>Atualizado: {{$options['updateSg']}}</small>
                </div>
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    SIVEP SRAG:
                    <span class="text-gray-800">
                        {{$totalCasos['srag']}}
                    </span>
                </div>
                <div class="text-gray-600 text-xs">
                    <small>Atualizado: {{$options['updateSrag']}}</small>
                </div>
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    <strong>
                        Total: {{$totalCasos['total']}}
                    </strong>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-viruses text-gray-300 fa-2x"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
          <div class="card-body p-1">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                    <h6><strong>Casos Recuperados</strong></h6>
                </div>
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                    E-SUS:
                    <span class="text-gray-800">
                        {{$totalCuras['esus']}}
                    </span>
                </div>
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                    SIVEP Sentinela:
                    <span class="text-gray-800">
                        {{$totalCuras['sg']}}
                    </span>
                </div>
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                    SIVEP SRAG:
                    <span class="text-gray-800">
                        {{$totalCuras['srag']}}
                    </span>
                </div>
                <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                    <strong>
                        Total: {{$totalCuras['total']}}
                    </strong>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-viruses text-gray-300 fa-2x"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
          <div class="card-body p-1">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                    <h6><strong>Incidência 1 mil hab.</strong></h6>
                </div>
                <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                    Incidência:
                    <span class="text-gray-800">
                        {{$incidencia}}
                    </span>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-viruses text-gray-300 fa-2x"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-danger shadow h-100 py-2">
          <div class="card-body p-1">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                    <h6><strong>Óbitos.</strong></h6>
                </div>
                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                    Óbitos:
                    <span class="text-gray-800">
                        {{$quantObito}}
                    </span>
                </div>
                <div class="text-xs font-weight-bold text-danger text-uppercase mb-1">
                    Mortalidade por 100mil:
                    <span class="text-gray-800">
                        {{$mortalidade}}
                    </span>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-viruses text-gray-300 fa-2x"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Evolução casos novos pelos primeiros sintomas</h6>
                </div>
                <div class="card-body">
                    <div id="chartSintomas">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Evolução casos novos pela data inserção</h6>
                </div>
                <div class="card-body">
                    <div id="chartDigitacao">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Evolução casos novos pela data notificação</h6>
                </div>
                <div class="card-body">
                    <div id="chartNotificacao">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<!-- Page level plugins -->
<script src="{{ mix('/js/apexcharts.min.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";


    var optionsChartSintomas = {
        series: [
            {
                name: "E-sus",
                type: 'column',
                data: {!! json_encode($chartSintomas->barEsus) !!},
            }, {
                name: "SRAG-SIVEP",
                type: 'column',
                data: {!! json_encode($chartSintomas->barSrag) !!}
            }, {
                name: 'SG-SIVEP',
                type: 'column',
                data: {!! json_encode($chartSintomas->barSg) !!},
            }, {
                name: 'Casos acumulado COVID-19',
                type: 'area',
                data: {!! json_encode($chartSintomas->area) !!},
                stroke: {
                curve: 'smooth'
                }
            }
        ],
        chart: {
            type: 'area',
            height: 400,
            width: "100%",
            stacked: true
        },
        stroke: {
          width: [1, 1, 1, 1]
        },
        plotOptions: {
            bar: {
              columnWidth: '50%',
            },
            dataLabels: {
              position: 'top',
              hideOverflowingLabels: true
            }
        },
        fill: {
          opacity: [1, 1, 1, 0.50],
          type: ['solid', 'solid', 'gradient'],
          gradient: {
            inverseColors: false,
            shade: 'light',
            type: "vertical",
            opacityFrom: 0.85,
            opacityTo: 0.55,
            stops: [0, 100, 100, 100]
          }
        },
        dataLabels: {
            enabled: false,
            offsetY: 0,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'top',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        xaxis: {
            type: "datetime",
            categories: {!! json_encode($chartSintomas->datas) !!},
            labels: {
              hideOverlappingLabels: false,
              showDuplicates: true,
              format: 'dd-MM',
                datetimeFormatter: {
                    year: 'yyyy',
                    month: 'MM-yyyy',
                    day: 'dd-MM',
                    hour: 'HH:mm'
                },
              axisTicks: {
                show: true,
                borderType: 'solid'
              }
            },
            title: {
                text: '',
                offsetY: 10
            }
        },
        yaxis: [
          {
            min: 0,
            max: {!! json_encode($chartSintomas->yaxis0) !!},
                tooltip: {
                enabled: true
            },
            title: {
                text: "Nº casos por dia"
            }
          },
          {
            min: 0,
            max: {!! json_encode($chartSintomas->yaxis0) !!},
            show: false,
            tooltip: {
            enabled: true
            }
          },
          {
            min: 0,
            max: {!! json_encode($chartSintomas->yaxis0) !!},
            show: false,
            tooltip: {
            enabled: true
            }
          },
          {
            max: {!! json_encode($chartSintomas->yaxis1) !!},
            opposite: true,
            seriesName: 'Casos acumulado',
            tooltip: {
                enabled: true
            },
            title: {
                text: "Casos acumulado"
            }
          }
        ],
        tooltip: {
          shared: false,
          intersect: true,
          inverseOrder: true,
          markers: {
              size: 0
          },
          fixed: {
            enabled: false,
            position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
            offsetY: 30,
            offsetX: 60
          },
          x: {
              show: true,
              format: 'dd-MM'
          },

        },
        legend: {
          horizontalAlign: 'left',
          offsetX: 40
        }

    };



    var optionschartDigitacao = {
        series: [
            {
                name: "E-sus",
                type: 'column',
                data: {!! json_encode($chartDigitacao->barEsus) !!},
            }, {
                name: "SRAG-SIVEP",
                type: 'column',
                data: {!! json_encode($chartDigitacao->barSrag) !!}
            }, {
                name: 'SG-SIVEP',
                type: 'column',
                data: {!! json_encode($chartDigitacao->barSg) !!},
            }, {
                name: 'Casos acumulado COVID-19',
                type: 'area',
                data: {!! json_encode($chartDigitacao->area) !!},
                stroke: {
                curve: 'smooth'
                }
            }
        ],
        chart: {
            type: 'area',
            height: 400,
            width: "100%",
            stacked: true
        },
        stroke: {
          width: [1, 1, 1, 1]
        },
        plotOptions: {
            bar: {
              columnWidth: '50%',
            },
            dataLabels: {
              position: 'top',
              hideOverflowingLabels: true
            }
        },
        fill: {
          opacity: [1, 1, 1, 0.50],
          type: ['solid', 'solid', 'gradient'],
          gradient: {
            inverseColors: false,
            shade: 'light',
            type: "vertical",
            opacityFrom: 0.85,
            opacityTo: 0.55,
            stops: [0, 100, 100, 100]
          }
        },
        dataLabels: {
            enabled: false,
            offsetY: 0,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'top',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        xaxis: {
            type: "datetime",
            categories: {!! json_encode($chartDigitacao->datas) !!},
            labels: {
              hideOverlappingLabels: false,
              showDuplicates: true,
              format: 'dd-MM',
                datetimeFormatter: {
                    year: 'yyyy',
                    month: 'MM-yyyy',
                    day: 'dd-MM',
                    hour: 'HH:mm'
                },
              axisTicks: {
                show: true,
                borderType: 'solid'
              }
            },
            title: {
                text: '',
                offsetY: 10
            }
        },
        yaxis: [
          {
            min: 0,
            max: {!! json_encode($chartDigitacao->yaxis0) !!},
                tooltip: {
                enabled: true
            },
            title: {
                text: "Nº casos por dia"
            }
          },
          {
            min: 0,
            max: {!! json_encode($chartDigitacao->yaxis0) !!},
            show: false,
            tooltip: {
            enabled: true
            }
          },
          {
            min: 0,
            max: {!! json_encode($chartDigitacao->yaxis0) !!},
            show: false,
            tooltip: {
            enabled: true
            }
          },
          {
            max: {!! json_encode($chartDigitacao->yaxis1) !!},
            opposite: true,
            seriesName: 'Casos acumulado',
            tooltip: {
                enabled: true
            },
            title: {
                text: "Casos acumulado"
            }
          }
        ],
        tooltip: {
          shared: false,
          intersect: true,
          inverseOrder: true,
          markers: {
              size: 0
          },
          fixed: {
            enabled: false,
            position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
            offsetY: 30,
            offsetX: 60
          },
          x: {
              show: true,
              format: 'dd-MM'
          },

        },
        legend: {
          horizontalAlign: 'left',
          offsetX: 40
        }

    };



    var optionsChartNotificacao = {
        series: [
            {
                name: "E-sus",
                type: 'column',
                data: {!! json_encode($chartNotificacao->barEsus) !!},
            }, {
                name: "SRAG-SIVEP",
                type: 'column',
                data: {!! json_encode($chartNotificacao->barSrag) !!}
            }, {
                name: 'SG-SIVEP',
                type: 'column',
                data: {!! json_encode($chartNotificacao->barSg) !!},
            }, {
                name: 'Casos acumulado COVID-19',
                type: 'area',
                data: {!! json_encode($chartNotificacao->area) !!},
                stroke: {
                curve: 'smooth'
                }
            }
        ],
        chart: {
            type: 'area',
            height: 400,
            width: "100%",
            stacked: true
        },
        stroke: {
          width: [1, 1, 1, 1]
        },
        plotOptions: {
            bar: {
              columnWidth: '50%',
            },
            dataLabels: {
              position: 'top',
              hideOverflowingLabels: true
            }
        },
        fill: {
          opacity: [1, 1, 1, 0.50],
          type: ['solid', 'solid', 'gradient'],
          gradient: {
            inverseColors: false,
            shade: 'light',
            type: "vertical",
            opacityFrom: 0.85,
            opacityTo: 0.55,
            stops: [0, 100, 100, 100]
          }
        },
        dataLabels: {
            enabled: false,
            offsetY: 0,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'top',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        xaxis: {
            type: "datetime",
            categories: {!! json_encode($chartNotificacao->datas) !!},
            labels: {
              hideOverlappingLabels: false,
              showDuplicates: true,
              format: 'dd-MM',
                datetimeFormatter: {
                    year: 'yyyy',
                    month: 'MM-yyyy',
                    day: 'dd-MM',
                    hour: 'HH:mm'
                },
              axisTicks: {
                show: true,
                borderType: 'solid'
              }
            },
            title: {
                text: '',
                offsetY: 10
            }
        },
        yaxis: [
          {
            min: 0,
            max: {!! json_encode($chartNotificacao->yaxis0) !!},
                tooltip: {
                enabled: true
            },
            title: {
                text: "Nº casos por dia"
            }
          },
          {
            min: 0,
            max: {!! json_encode($chartNotificacao->yaxis0) !!},
            show: false,
            tooltip: {
            enabled: true
            }
          },
          {
            min: 0,
            max: {!! json_encode($chartNotificacao->yaxis0) !!},
            show: false,
            tooltip: {
            enabled: true
            }
          },
          {
            max: {!! json_encode($chartNotificacao->yaxis1) !!},
            opposite: true,
            seriesName: 'Casos acumulado COVID-19',
            tooltip: {
                enabled: true
            },
            title: {
                text: "Casos acumulado"
            }
          }
        ],
        tooltip: {
          shared: false,
          intersect: true,
          inverseOrder: true,
          markers: {
              size: 0
          },
          fixed: {
            enabled: false,
            position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
            offsetY: 30,
            offsetX: 60
          },
          x: {
              show: true,
              format: 'dd-MM'
          },

        },
        legend: {
          horizontalAlign: 'left',
          offsetX: 40
        }

    };




    $(document).ready(() => {
        var chartSintomas = new ApexCharts(document.querySelector("#chartSintomas"), optionsChartSintomas);
        var chartDigitacao = new ApexCharts(document.querySelector("#chartDigitacao"), optionschartDigitacao);
        var chartNotificacao = new ApexCharts(document.querySelector("#chartNotificacao"), optionsChartNotificacao);
        chartSintomas.render();
        chartDigitacao.render();
        chartNotificacao.render();
    });
</script>
@endsection
