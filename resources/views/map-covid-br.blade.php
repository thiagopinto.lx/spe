@extends('layouts.app')
@section('css')

@endsection
@section('content')
    <div class="d-sm-flex align-items-right right-content-between mb-4">
        <a href="{{$urls['index']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Casos confirmados
        </a>
        <a href="{{$urls['obito']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Óbito confirmados
        </a>
        <a href="{{$urls['capitais']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Casos confirmados nas capitais
        </a>
        <a href="{{$urls['obitocapitais']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Óbito confirmados nas capitais
        </a>
        <a href="{{$urls['map-evolucao']}}" style="margin-right: 10px;"
        class="d-none d-sm-inline-block btn btn-sm
        @if($options['area'] == 'map-evolucao')
            btn-outline-primary
        @else
            btn-primary
        @endif
        shadow-sm">
            <i class="fas fa-tachometer-alt
            @if($options['area'] == 'map-evolucao')
                text-primary-50
            @else
                text-white-50
            @endif
            "></i> Mapa da evolução dos casos
        </a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Mapa da evolução dos casos</h6>
                </div>
                <div class="card-body">
                    <div class="form-group mb-3">
                        <div class="col-12">
                          <input
                            class="form-control"
                            type="date"
                            class="form-control"
                            aria-label="date"
                            min="{{$datas[0]}}"
                            max="{{$datas[count($datas)-1]}}"
                            value="{{$datas[count($datas)-1]}}"
                            id="input-data">
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input
                            type="range"
                            id="range-data"
                            class="custom-range"
                            data-toggle="tooltip"
                            title="{{$datas[count($datas)-1]}}" min="0" max="{{count($datas)-1}}" value="{{count($datas)-1}}">
                    </div>

                    <div class="row">
                      <div class="col-lg-12" id="map" style="height: 900px">

                      </div>
                      <div class="card shadow mb-4 col-lg-2 p-0 d-none" id="legend">
                        <div class="card-header py-1 m-0">
                          <h6 class="m-0 font-weight-bold text-primary text-center">Legendas</h6>
                        </div>


                      </div>
                    </div>

                </div>
              </div>
        </div>
    </div>

@endsection
@section('scripts')
<script type="text/javascript">
  var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
  var token = "{{csrf_token()}}";
  var arrayDatas = {!! json_encode($datas) !!};
  //arrayDatas.pop();
  //console.log(arrayDatas)
  var estados = {!! json_encode($estados) !!};
  var url = "{{$urls['map-data']}}";
  var colorClass = [
      '#f0f0f0',
      '#ffffd9',
      '#edf8b1',
      '#c7e9b4',
      '#7fcdbb',
      '#41b6c4',
      '#1d91c0',
      '#225ea8',
      '#253494',
      '#081d58',
      '#210558',
      '#3E0258'

      ];
  var inter = [
      [0, 200],
      [200, 400],
      [400, 800],
      [800, 1200],
      [1200, 2400],
      [2400, 4800],
      [4800, 9600],
      [9600, 19200],
      [19200, 38400],
      [38400, 76800]

  ];
  var polygonEstados = [];
  var windowEstados = [];
  function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 5,
      center: {lat: -14.9265047, lng: -55.3092274},
      zoomControl: true,
      scaleControl: true
    });

    for (let i = 0; i < estados.length; i++) {
      let coordinates = [];
      for (let j = 0; j < estados[i].geocodes.length; j++) {
          coordinates.push(
              {
                  lat: parseFloat(estados[i].geocodes[j].lat),
                  lng: parseFloat(estados[i].geocodes[j].lng)
              }
          );
      }

      polygonEstados[estados[i].id] = new google.maps.Polygon({
          paths: coordinates,
          strokeColor: '#FFF',
          strokeOpacity: 0.9,
          strokeWeight: 3,
          fillColor: '#858796',
          fillOpacity: 0.8
      });

      let templateWindow =
      `<div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">${estados[i].nome}</h6>
        </div>
        <div class="card-body">
          População: ${estados[i].populacao} </br>
          Capital: ${estados[i].nome_capital} </br>
          População Capital: ${estados[i].populacao_capital} </br>
        </div>
      </div>`;

      windowEstados[estados[i].id] = new google.maps.InfoWindow({
        content: templateWindow,
        position: {
                    lat: parseFloat(estados[i].latitude_capital),
                    lng: parseFloat(estados[i].longitude_capital)
                  }
      });

      google.maps.event.addListener(polygonEstados[estados[i].id], 'click', function() {
        windowEstados[estados[i].id].open(map);
      });

      polygonEstados[estados[i].id].setMap(map);
    }



    for (let index = 0; index < colorClass.length; index++) {

        if (index == 0) {
            templateCard =
            `<div class="text-dark font-weight-bold shadow" style="background-color:${colorClass[0]};">
                <div class="text-center p-0">
                    0 Caso
                </div>
            </div>`;
            div = document.createElement('div');
            div.className += "card p-0 m-1";
            div.innerHTML = templateCard;
            legend.appendChild(div);
            continue;
        }
        if (index == colorClass.length-1) {
            templateCard =
            `<div class="text-white font-weight-bold shadow" style="background-color:${colorClass[index]};">
                <div class="text-center p-0">
                    ${inter[inter.length-1][1]}+ Casos
                </div>
            </div>`;
            div = document.createElement('div');
            div.className += "card p-0 m-1";
            div.innerHTML = templateCard;
            legend.appendChild(div);
            continue;
        }
        if(index > 0 && index <= inter.length) {
            if (index > 4) {
                textColor = "text-white";
            } else {
                textColor = "text-dark";
            }
            templateCard =
            `<div class="${textColor} font-weight-bold shadow" style="background-color:${colorClass[index]};">
                <div class="text-center p-0">
                    ${inter[index-1][0]} - ${inter[index-1][1]} Casos
                </div>
            </div>`;
            div = document.createElement('div');
            div.className += "card p-0 m-1";
            div.innerHTML = templateCard;
            legend.appendChild(div);
            continue;
        }

    }
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(legend);
    legend.classList.remove("d-none");

    changeMap(arrayDatas[arrayDatas.length -1]);
  }

  function changeMap(data) {
    estados.forEach(estado => {

        $.ajax({
            url : `${url}/${data}/${estado.id}`,
            contentType: 'application/x-www-form-urlencoded',
            cache: false
        }).done(function(estado){

          for (let index = 0; index < colorClass.length; index++) {
            if (estado.total_casos == 0) {
                console.log(estado.total_casos);
              polygonEstados[estado.id].setOptions({fillColor: colorClass[0]});
              polygonEstados[estado.id].setOptions({fillOpacity: 0.8});
              break;
            }
            if (estado.total_casos > inter[inter.length-1][1]) {
                console.log(estado.total_casos);
              polygonEstados[estado.id].setOptions({fillColor: colorClass[colorClass.length-1]});
              polygonEstados[estado.id].setOptions({fillOpacity: 0.8});
              break;
            }
            if(index > 0 && index <= inter.length) {
              if (estado.total_casos > inter[index-1][0] && estado.total_casos <= inter[index-1][1]) {
                polygonEstados[estado.id].setOptions({fillColor: colorClass[index]});
                polygonEstados[estado.id].setOptions({fillOpacity: 0.8});
                break;
              }
            }

          }

          let templateWindow =
          `<div class="card shadow mb-4">
              <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">${estado.nome}</h6>
              </div>
              <div class="card-body">
              População: ${estado.populacao} </br>
              Capital: ${estado.nome_capital} </br>
              População Capital: ${estado.populacao_capital} </br>
              Casos do dia: ${estado.casos_dia} </br>
              Total de casos: ${estado.total_casos} </br>
              </div>
          </div>`;

          windowEstados[estado.id].setContent(templateWindow);
        }).fail(function(response){
        console.log(response);
        }).always(function(response){
        });

    });
  }

  $(document).ready(function() {

    $('#range-data').mousemove(()=>{
        let indice = $('#range-data').val();
        $('#range-data').attr('title', arrayDatas[indice]);
        $('#input-data').val(arrayDatas[indice]);
        console.log(indice);
    });

    $('#range-data').change(()=>{
        let indice = $('#range-data').val();
        changeMap(arrayDatas[indice]);
    });

    $('#input-data').change(()=>{
        let data = $('#input-data').val();
        indice = arrayDatas.indexOf(data);
        $('#range-data').val(indice);
        changeMap(data);
    });



  });

</script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&libraries=visualization&callback=initMap">
    //google.maps.event.addDomListener(window, 'load', initialize);
</script>


@endsection
