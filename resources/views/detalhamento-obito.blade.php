@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')

    <div class="row">
        @include('components.bar-list')
        @isset($total)
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-2 col-md-4 mb-2">
                <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid {{$total->color}}">
                    <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:{{$total->color}} ">{{$total->titulo}}</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{$total->media}}</div>
                        </div>
                        <div class="col-auto">
                        <i class="fas fa-stethoscope fa-2x text-gray-400"></i>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        @endisset
    </div>

    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary"> Notificações de {{$fullName}} por estabelecimento de Saúde</h6>
                </div>
                <div class="card-body">
                    <table class="table-striped compact table-bordered" id="dataTabelaNotificacaoUnidade" width="100%" cellspacing="0">
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row" id="incidencia-cids">

    </div>
    <div class="row" id="incidencia-bairros">

    </div>
@endsection
@section('scripts')
<!-- Page level plugins -->
<script src="{{ mix('/js/Chart.min.js') }}"></script>
<script src="{{ mix('/js/datatables.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";
    var get_data_obito_cid = "{{$urls['getdataobitocid']}}";
    var get_data_notificacao_unidade = "{{$urls['getdatanotificacaounidade']}}";

    var markerClusters;
    var ano;
    var tableBairro;
    var tableCid;
    var dataTabelaObitoCid;
    var fields;
    var pieCids = new Array();
    var pieChart = new Array();
    function createDataTableCid(idDiv, fields){
        let ano = [fields];
        tableBairro = $("#"+idDiv).DataTable( {
        serverSide: true,
        processing: true,
        ajax:{
          url: get_data_obito_cid+'?'+decodeURIComponent( $.param( ano )),
          dataType: "json",
          dataFilter: function(response){
            let localResponse = JSON.parse(response);

            pieCids[idDiv] = document.getElementById('chat-'+idDiv).getContext('2d');
            console.log(pieCids[idDiv]);
            pieChart[idDiv] = new Chart(pieCids[idDiv], {
                type: 'pie',
                data: {},
                options:  {
                    				responsive: true
                    			}
            });
            pieChart[idDiv].data = localResponse;
            pieChart[idDiv].data.datasets[0].backgroundColor = [
                'rgba(48, 63, 159, 0.7)',
                'rgba(255, 22, 10, 0.7)',
                'rgba(255, 114, 75, 0.7)',
                'rgba(133, 135, 150, 0.7)',
                'rgba(75, 192, 192, 0.7)',
                'rgba(153, 102, 255, 0.7)',
                'rgba(255, 159, 64, 0.7)',
                'rgba(194, 24, 91, 0.7)',
                'rgba(4, 108, 70, 0.7)',
                'rgba(250, 206, 0, 0.7)'
            ];
            pieChart[idDiv].data.datasets[0].borderColor = [
              'rgba(48, 63, 159, 0.2)',
              'rgba(255, 22, 10, 0.2)',
              'rgba(255, 114, 75, 0.2)',
              'rgba(133, 135, 150, 0.2)',
              'rgba(75, 192, 192, 0.2)',
              'rgba(153, 102, 255, 0.2)',
              'rgba(255, 159, 64, 0.2)',
              'rgba(194, 24, 91, 0.2)',
              'rgba(4, 108, 70, 0.2)',
              'rgba(250, 206, 0, 0.2)'
            ];
            pieChart[idDiv].update();
            return response
          },
          error: function(error) {
            // to see what the error is
           console.log(error);
          }
        },
        info: false,
        paging: false,
        searching: false,
        ordering: false,
        columns: [{title: fields.value}, {title: "Qnt"}]
        } );

    }

    function createTable(fields){

        var columnsName = [];
        var columnsNameAnos = [];
        columnsName.push({ title: "Name" });

        for (i = 0; i < fields.length; i++) {
            columnsName.push({ title: fields[i].value });
            columnsNameAnos.push({ title: fields[i].value });
            columnsNameAnos.push({ title: "Quant" });
        }

        dataTabelaNotificacaoUnidade = $('#dataTabelaNotificacaoUnidade').DataTable( {
            serverSide: true,
            processing: true,
            ajax:{
                url: get_data_notificacao_unidade+'?'+decodeURIComponent( $.param( fields )),
                dataType: "json"
            },
            info: false,
            paging: false,
            searching: false,
            ordering: false,
            columns: columnsName
        } );

        for (i = 0; i < fields.length; i++) {

            $('#incidencia-cids').append(
            `<div class="col-lg-6 mb-4">
               <div class="card shadow mb-4">
                 <div class="card-header py-3">
                   <h6 class="m-0 font-weight-bold text-primary">Notificações por Cid</h6>
                 </div>
                 <div class="card-body">
                   <canvas width="400" height="400" id="chat-cids${fields[i].value}">
                   </canvas>
                   <table class="table-striped compact table-bordered" id="cids${fields[i].value}" width="100%" cellspacing="0">
                   </table>
                 </div>
              </div>
            </div>`
            );
            createDataTableCid("cids"+fields[i].value, fields[i]);
        }

    }

    function updateTable(fields, oldFields){

        $('#dataTabelaNotificacaoUnidade').DataTable().destroy();
        $('#dataTabelaNotificacaoUnidade').empty();

        for (i = 0; i < oldFields.length; i++) {
            $("#cids"+oldFields[i].value).DataTable().destroy();
            $("#cids"+oldFields[i].value).empty();
        }
        $('#incidencia-cids').empty();

        createTable(fields);
    }

    $(document).ready(function(){

      fields = $( ".anos:checked" ).serializeArray();
      createTable(fields);

      $('.anos').change(function(){

          oldFields = fields;

          fields = $( ".anos:checked" ).serializeArray();
          console.log(fields);
          updateTable(fields, oldFields);
      });

    });


</script>
@endsection
