@extends('layouts.chart-app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 mb-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tabela de {{$tableName}} de Teresina em: {{$data}}</h6>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <thead class="thead-dark">
                      <tr>
                        <th scope="col">Classificação</th>
                        <th scope="col">Nome</th>
                        <th scope="col">População</th>
                        <th scope="col">Valor</th>
                      </tr>
                    </thead>
                    <tbody>
                        @forelse ($bairros as $bairro)
                           <tr>
                               <td>{{ $loop->index+1 }}</td>
                               <td>{{ $bairro->name }}</td>
                               <td>{{ $bairro->populacao }}</td>
                               <td>{{ $bairro->casos }}</td>
                            </tr>
                        @empty
                            <p>No data</p>
                        @endforelse
                    </tbody>
                  </table>

            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')

@endsection
