@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <div class="d-sm-flex align-items-right right-content-between mb-4 d-print-none">
        <a href="{{$urls['index']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Casos confirmados
        </a>
        <a href="{{$urls['obito']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Óbito confirmados
        </a>
        <a href="{{$urls['sintomatologia']}}" style="margin-right: 10px;"
        class="d-none d-sm-inline-block btn btn-sm
        @if($options['area'] == 'sintomatologia')
            btn-outline-primary
        @else
            btn-primary
        @endif
        shadow-sm">
            <i class="fas fa-tachometer-alt
            @if($options['area'] == 'sintomatologia')
                text-primary-50
            @else
                text-white-50
            @endif
            "></i> Sintomatologia
        </a>
        <a href="{{$urls['map-evolucao']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Mapa da evolução dos casos
        </a>
        <a href="{{$urls['outras-info']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Outras Informações
        </a>
      </div>
    </div>

    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Sintomatologia COVID-19 Casos Leves</h6>
                </div>
                <div class="card-body">
                    <div id="sintomasLeves">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Sintomatologia COVID-19 Casos Graves</h6>
                </div>
                <div class="card-body">
                    <div id="sintomasGraves">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
<!-- Page level plugins -->
<script src="{{ mix('/js/apexcharts.min.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";

        var optionsSintomasLeves = {
          series: [{
          data: {!! json_encode($dataChartSintomasLeves) !!}
        }],
          chart: {
          type: 'bar',
          height: 380,
        },
        plotOptions: {
          bar: {
            barHeight: '100%',
            distributed: true,
            horizontal: true,
            dataLabels: {
              position: 'bottom'
            }
          }
        },
        colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e',
          '#f48024', '#69d2e7'
        ],
        dataLabels: {
          enabled: true,
          textAnchor: 'start',
          style: {
            fontWeight: '400',
            colors: ['#444']
          },
          formatter: function (val, opt) {
            return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val + " %"
          },
          offsetX: 0,
          dropShadow: {
            enabled: true
          }
        },
        stroke: {
          width: 1,
          colors: ['#fff']
        },
        xaxis: {
            type: 'category'
        },
        yaxis: {
          labels: {
            show: false,
            formatter: function (val) {
              return val + " %";
            }
          }
        },
        tooltip: {
          theme: 'dark',
          x: {
            show: false
          },
          y: {
            title: {
              formatter: function () {
                return ''
              }
            }
          }
        }
        };

        var optionsSintomasGraves = {
          series: [{
          data: {!! json_encode($dataChartSintomasGraves) !!}
        }],
          chart: {
          type: 'bar',
          height: 380,
        },
        plotOptions: {
          bar: {
            barHeight: '100%',
            distributed: true,
            horizontal: true,
            dataLabels: {
              position: 'bottom'
            }
          }
        },
        colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e',
          '#f48024', '#69d2e7'
        ],
        dataLabels: {
          enabled: true,
          textAnchor: 'start',
          style: {
            fontWeight: '400',
            colors: ['#444']
          },
          formatter: function (val, opt) {
            return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val + " %"
          },
          offsetX: 0,
          dropShadow: {
            enabled: true
          }
        },
        stroke: {
          width: 1,
          colors: ['#fff']
        },
        xaxis: {
            type: 'category'
        },
        yaxis: {
          labels: {
            show: false,
            formatter: function (val) {
              return val + " %";
            }
          }
        },
        tooltip: {
          theme: 'dark',
          x: {
            show: false
          },
          y: {
            title: {
              formatter: function () {
                return ''
              }
            }
          }
        }
        };


    $(document).ready(() => {

        var sintomasGraves = new ApexCharts(document.querySelector("#sintomasGraves"), optionsSintomasGraves);
        sintomasGraves.render();

        var sintomasLeves = new ApexCharts(document.querySelector("#sintomasLeves"), optionsSintomasLeves);
        sintomasLeves.render();

    });
</script>
@endsection
