@forelse ($newLoadSipnis as $loadData)

<div class="col-xl-2 col-md-4 mb-2">
    <div class="card shadow h-100 py-2" style="border-left: 0.25rem solid {{$loadData->color}}">
        <div class="card-body">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <div class="input-group-text" style="background-color:{{$loadData->color}}; padding: 0.50rem 0.45rem;">
                    <input type="checkbox" class="anos zikas" name="anos[]"  autocomplete="off" value="{{$loadData->ano}}">
                    </div>
                </div>
                <input type="text" class="form-control" aria-label="Input text com checkbox" value="{{$loadData->ano}}" style="padding: 1px;" disabled>
            </div>
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">

                <div class="h5 mb-0 font-weight-bold text-gray-800">
                    {{$loadData->count}}
                </div>
              </div>
          @isset($urls['heatmap'])
            <div class="">
              <a href="{{route($urls['heatmap'], ['tabela' => $loadData->tabela])}}"><i class="far fa-map fa-2x text-gray-500"></i></a>
            </div>
          @endisset
        </div>
        <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:{{$loadData->color}}">
          {{$loadData->imuno()->nome}} Sipni:
        </div>
        <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:{{$loadData->color}}">
          Doses Aplicadas: <u>{{$loadData->dosesAplicadas}}</u>
        </div>
        <div class="text-xs font-weight-bold text-uppercase mb-1" >
            Atualizado em: {{$loadData->updated}}
        </div>
        @forelse ($loadData->loadSies as $loadSies)
          <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:{{$loadData->color}}">
            {{$loadSies->imuno()->nome}} Sies:
          </div>
          <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:{{$loadData->color}}">
            Doses Distribuidas: <u>{{$loadSies->dosesDistribuidas}}</u>
          </div>
          <div class="text-xs font-weight-bold text-uppercase mb-1" >
              Atualizado em: {{$loadSies->updated}}
          </div>
        @empty
        <p>No data</p>
        @endforelse
        
        <div class="text-xs font-weight-bold text-uppercase mb-1" style="color:{{$loadData->color}}">
          Total Doses Distribuidas: <u>{{$loadData->dosesDistribuidas}}</u>
        </div>

        </div>
    </div>
</div>
@empty
<p>No data</p>
@endforelse
