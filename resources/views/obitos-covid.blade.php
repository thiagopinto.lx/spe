@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
      <div class="d-sm-flex align-items-right right-content-between mb-4 d-print-none">
        <a href="{{$urls['index']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Casos confirmados
        </a>
        <a href="{{$urls['obito']}}" style="margin-right: 10px;"
        class="d-none d-sm-inline-block btn btn-sm
        @if($options['area'] == 'obito')
            btn-outline-primary
        @else
            btn-primary
        @endif
        shadow-sm">
            <i class="fas fa-tachometer-alt
            @if($options['area'] == 'obito')
                text-primary-50
            @else
                text-white-50
            @endif
            "></i> Óbito confirmados
        </a>
        <a href="{{$urls['sintomatologia']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Sintomatologia
        </a>
        <a href="{{$urls['map-evolucao']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Mapa da evolução dos casos
        </a>
        <a href="{{$urls['outras-info']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Outras Informações
        </a>
      </div>
    </div>

    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Evolução pela data do óbito residentes</h6>
                </div>
                <div class="card-body">
                    <div id="chartObitos">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Evolução pela data do óbito residentes e não residentes</h6>
                </div>
                <div class="card-body">
                    <div id="chartObitosFull">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Fatores de risco números absolutos</h6>
                </div>
                <div class="card-body">
                    <div id="obitoDetalhamento">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Fatores de risco com percentagem</h6>
                </div>
                <div class="card-body">
                    <div id="obitoDetalhamentoPercent">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-12">
            <!-- Line Chart -->
            <div class="card shadow mb-12">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Raça/Cor com percentagem</h6>
                </div>
                <div class="card-body">
                    <div id="obitoCor">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
<!-- Page level plugins -->
<script src="{{ mix('/js/apexcharts.min.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";


    var optionsChartObitos = {
        series: [
            {
                name: "Óbitos por dia COVID-19",
                type: 'column',
                data: {!! json_encode($dataChartObitoEvolucao->bar) !!},
            }, {
                name: 'Óbitos acumulado COVID-19',
                type: 'area',
                data: {!! json_encode($dataChartObitoEvolucao->area) !!},
                stroke: {
                curve: 'smooth'
                }
            }
        ],
        chart: {
            type: 'area',
            height: 400,
            width: "100%",
            stacked: true
        },
        stroke: {
          width: [1, 1, 1, 1]
        },
        plotOptions: {
            bar: {
              columnWidth: '50%',
            },
            dataLabels: {
              position: 'top',
              hideOverflowingLabels: true
            }
        },
        fill: {
          opacity: [1, 0.50],
          type: ['solid', 'solid', 'gradient'],
          gradient: {
            inverseColors: false,
            shade: 'light',
            type: "vertical",
            opacityFrom: 0.85,
            opacityTo: 0.55,
            stops: [0, 100, 100, 100]
          }
        },
        dataLabels: {
            enabled: false,
            offsetY: 0,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'top',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        xaxis: {
            type: "datetime",
            categories: {!! json_encode($dataChartObitoEvolucao->datas) !!},
            labels: {
              hideOverlappingLabels: false,
              showDuplicates: true,
              format: 'dd-MM',
                datetimeFormatter: {
                    year: 'yyyy',
                    month: 'MM-yyyy',
                    day: 'dd-MM',
                    hour: 'HH:mm'
                },
              axisTicks: {
                show: true,
                borderType: 'solid'
              }
            },
            title: {
                text: '',
                offsetY: 10
            }
        },
        yaxis: [
          {
            min: 0,
            max: {!! json_encode($dataChartObitoEvolucao->yaxis0) !!},
                tooltip: {
                enabled: true
            },
            title: {
                text: "Óbitos por dia COVID-19"
            }
          },
          {
            max: {!! json_encode($dataChartObitoEvolucao->yaxis1) !!},
            opposite: true,
            seriesName: 'Óbitos acumulado COVID-19',
            tooltip: {
                enabled: true
            },
            title: {
                text: "Óbitos acumulado COVID-19"
            }
          }
        ],
        tooltip: {
          shared: false,
          intersect: true,
          inverseOrder: true,
          markers: {
              size: 0
          },
          fixed: {
            enabled: false,
            position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
            offsetY: 30,
            offsetX: 60
          },
          x: {
              show: true,
              format: 'dd-MM'
          },

        },
        legend: {
          horizontalAlign: 'left',
          offsetX: 40
        }

    };

    var optionsChartObitosFull = {
        series: [
            {
                name: "Óbitos por dia COVID-19",
                type: 'column',
                data: {!! json_encode($dataChartObitoEvolucaoFull->bar) !!},
            }, {
                name: 'Óbitos acumulado COVID-19',
                type: 'area',
                data: {!! json_encode($dataChartObitoEvolucaoFull->area) !!},
                stroke: {
                curve: 'smooth'
                }
            }
        ],
        chart: {
            type: 'area',
            height: 400,
            width: "100%",
            stacked: true
        },
        stroke: {
          width: [1, 1, 1, 1]
        },
        plotOptions: {
            bar: {
              columnWidth: '50%',
            },
            dataLabels: {
              position: 'top',
              hideOverflowingLabels: true
            }
        },
        fill: {
          opacity: [1, 0.50],
          type: ['solid', 'solid', 'gradient'],
          gradient: {
            inverseColors: false,
            shade: 'light',
            type: "vertical",
            opacityFrom: 0.85,
            opacityTo: 0.55,
            stops: [0, 100, 100, 100]
          }
        },
        dataLabels: {
            enabled: false,
            offsetY: 0,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'top',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        xaxis: {
            type: "datetime",
            categories: {!! json_encode($dataChartObitoEvolucaoFull->datas) !!},
            labels: {
              hideOverlappingLabels: false,
              showDuplicates: true,
              format: 'dd-MM',
                datetimeFormatter: {
                    year: 'yyyy',
                    month: 'MM-yyyy',
                    day: 'dd-MM',
                    hour: 'HH:mm'
                },
              axisTicks: {
                show: true,
                borderType: 'solid'
              }
            },
            title: {
                text: '',
                offsetY: 10
            }
        },
        yaxis: [
          {
            min: 0,
            max: {!! json_encode($dataChartObitoEvolucaoFull->yaxis0) !!},
                tooltip: {
                enabled: true
            },
            title: {
                text: "Óbitos por dia COVID-19"
            }
          },
          {
            max: {!! json_encode($dataChartObitoEvolucaoFull->yaxis1) !!},
            opposite: true,
            seriesName: 'Óbitos acumulado COVID-19',
            tooltip: {
                enabled: true
            },
            title: {
                text: "Óbitos acumulado COVID-19"
            }
          }
        ],
        tooltip: {
          shared: false,
          intersect: true,
          inverseOrder: true,
          markers: {
              size: 0
          },
          fixed: {
            enabled: false,
            position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
            offsetY: 30,
            offsetX: 60
          },
          x: {
              show: true,
              format: 'dd-MM'
          },

        },
        legend: {
          horizontalAlign: 'left',
          offsetX: 40
        }

    };

    var optionsObitoDetalhamento = {
          series: [{
          data: {!! json_encode($dataChartObitoDetalhamento->absoluto) !!}
        }],
          chart: {
          type: 'bar',
          height: 380
        },
        plotOptions: {
          bar: {
            barHeight: '100%',
            distributed: true,
            horizontal: true,
            dataLabels: {
              position: 'bottom'
            },
          }
        },
        colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e',
          '#f48024', '#69d2e7'
        ],
        dataLabels: {
          enabled: true,
          textAnchor: 'start',
          style: {
            fontWeight: '400',
            colors: ['#444']
          },
          formatter: function (val, opt) {
            return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val
          },
          offsetX: 0,
          dropShadow: {
            enabled: true
          }
        },
        stroke: {
          width: 1,
          colors: ['#fff']
        },
        xaxis: {
            type: 'category'
        },
        yaxis: {
          labels: {
            show: false
          }
        },
        tooltip: {
          theme: 'dark',
          x: {
            show: false
          },
          y: {
            title: {
              formatter: function () {
                return ''
              }
            }
          }
        }
    };

    var optionsObitoDetalhamentoPercent = {
          series: [{
          data: {!! json_encode($dataChartObitoDetalhamento->percent) !!}
        }],
          chart: {
          type: 'bar',
          height: 380,
        },
        plotOptions: {
          bar: {
            barHeight: '100%',
            distributed: true,
            horizontal: true,
            dataLabels: {
              position: 'bottom'
            }
          }
        },
        colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e',
          '#f48024', '#69d2e7'
        ],
        dataLabels: {
          enabled: true,
          textAnchor: 'start',
          style: {
            fontWeight: '400',
            colors: ['#444']
          },
          formatter: function (val, opt) {
            return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val + " %"
          },
          offsetX: 0,
          dropShadow: {
            enabled: true
          }
        },
        stroke: {
          width: 1,
          colors: ['#fff']
        },
        xaxis: {
            type: 'category'
        },
        yaxis: {
          labels: {
            show: false,
            formatter: function (val) {
              return val + " %";
            }
          }
        },
        tooltip: {
          theme: 'dark',
          x: {
            show: false
          },
          y: {
            title: {
              formatter: function () {
                return ''
              }
            }
          }
        }
    };

    var optionsObitoCor = {
          series: [{
          data: {!! json_encode($dataChartObitoCor) !!}
        }],
          chart: {
          type: 'bar',
          height: 380,
        },
        plotOptions: {
          bar: {
            barHeight: '100%',
            distributed: true,
            horizontal: true,
            dataLabels: {
              position: 'bottom'
            }
          }
        },
        colors: ['#33b2df', '#546E7A', '#d4526e', '#13d8aa', '#A5978B', '#2b908f', '#f9a3a4', '#90ee7e',
          '#f48024', '#69d2e7'
        ],
        dataLabels: {
          enabled: true,
          textAnchor: 'start',
          style: {
            fontWeight: '400',
            colors: ['#444']
          },
          formatter: function (val, opt) {
            return opt.w.globals.labels[opt.dataPointIndex] + ":  " + val + " %"
          },
          offsetX: 0,
          dropShadow: {
            enabled: true
          }
        },
        stroke: {
          width: 1,
          colors: ['#fff']
        },
        xaxis: {
            type: 'category'
        },
        yaxis: {
          labels: {
            show: false,
            formatter: function (val) {
              return val + " %";
            }
          }
        },
        tooltip: {
          theme: 'dark',
          x: {
            show: false
          },
          y: {
            title: {
              formatter: function () {
                return ''
              }
            }
          }
        }
    };


    $(document).ready(() => {
        var chartObitos = new ApexCharts(document.querySelector("#chartObitos"), optionsChartObitos);
        chartObitos.render();

        var chartObitosFull = new ApexCharts(document.querySelector("#chartObitosFull"), optionsChartObitosFull);
        chartObitosFull.render();

        var obitoDetalhamento = new ApexCharts(document.querySelector("#obitoDetalhamento"), optionsObitoDetalhamento);
        obitoDetalhamento.render();

        var obitoDetalhamentoPercent = new ApexCharts(document.querySelector("#obitoDetalhamentoPercent"), optionsObitoDetalhamentoPercent);
        obitoDetalhamentoPercent.render();

        var obitoCor = new ApexCharts(document.querySelector("#obitoCor"), optionsObitoCor);
        obitoCor.render();
    });
</script>
@endsection
