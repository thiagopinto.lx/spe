@extends('layouts.app')
@section('css')
<style>

</style>


@endsection
@section('content')
    <div class="d-sm-flex align-items-right right-content-between mb-4">
        <a href="{{$urls['index']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Casos confirmados
        </a>
        <a href="{{$urls['obito']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Óbito confirmados
        </a>
        <a href="{{$urls['sintomatologia']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Sintomatologia
        </a>
        <a href="{{$urls['map-evolucao']}}" style="margin-right: 10px;"
        class="d-none d-sm-inline-block btn btn-sm
        @if($options['area'] == 'map-evolucao')
            btn-outline-primary
        @else
            btn-primary
        @endif
        shadow-sm">
            <i class="fas fa-tachometer-alt
            @if($options['area'] == 'map-evolucao')
                text-primary-50
            @else
                text-white-50
            @endif
            "></i> Mapa da evolução dos casos
        </a>
        <a href="{{$urls['outras-info']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Outras Informações
        </a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Mapa da evolução dos casos</h6>
                  <div class="row">
                      <div class="col-lg-3">
                          <div class="custom-control custom-switch is-casos">
                              <input type="radio" name="type-map" class="custom-control-input" id="is-casos" value="is-casos" checked>
                              <label class="custom-control-label" for="is-casos">Mapa de casos</label>
                          </div>
                      </div>
                      <div class="col-lg-3">
                          <div class="custom-control custom-switch is-obitos">
                              <input type="radio" name="type-map" class="custom-control-input" id="is-obitos" value="is-obitos" >
                              <label class="custom-control-label" for="is-obitos">Mapa de óbitos</label>
                          </div>
                      </div>
                      <div class="col-lg-3">
                          <div class="custom-control custom-switch is-incidencias">
                              <input type="radio" name="type-map" class="custom-control-input" id="is-incidencias" value="is-incidencias">
                              <label class="custom-control-label" for="is-incidencias">Mapa de Incidência/1mil hab.</label>
                          </div>
                      </div>
                  </div>
                </div>
                <div class="card-body">
                    <div class="form-group mb-3">
                        <div class="col-12">
                          <input
                            class="form-control"
                            type="date"
                            class="form-control"
                            aria-label="date"
                            min="{{$datas[0]}}"
                            max="{{$datas[count($datas)-1]}}"
                            value="{{$datas[count($datas)-1]}}"
                            id="input-data">
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input
                            type="range"
                            id="range-data"
                            class="custom-range"
                            data-toggle="tooltip"
                            title="{{$datas[count($datas)-1]}}" min="0" max="{{count($datas)-1}}" value="{{count($datas)-1}}">
                    </div>

                    <div class="row">
                      <div class="col-lg-12" id="map" style="height: 800px">

                      </div>
                      <div class="card shadow mb-4 col-lg-2 p-0 d-none" id="legend">
                        <div class="card-header py-1 m-0">
                          <div class="row">
                            <h6 class="clol-12 m-0 font-weight-bold text-primary text-center">Legendas</h6>
                          </div>
                          <div class="row">
                            <button type="button" id="gerar-tabela" class="col-12 btn btn-secondary btn-sm">Gerar Tabela</button>
                          </div>

                        </div>


                      </div>
                    </div>

                </div>
              </div>
        </div>
    </div>

@endsection
@section('scripts')
<script type="text/javascript">
  var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
  var token = "{{csrf_token()}}";
  var arrayDatas = {!! json_encode($datas) !!};
  var bairros = {!! json_encode($bairros) !!};
  var urlCasos = "{{$urls['map-data-casos']}}";
  var urlObitos = "{{$urls['map-data-obitos']}}";
  var urlIncidencia = "{{$urls['map-data-incidencias']}}";
  var url = "{{$urls['map-data-casos']}}";
  var currentTypeMap = null;
  var map;
  var legend = document.getElementById('legend');

  var colorClassCasos = [
      '#f0f0f0',
      '#ffffd9',
      '#edf8b1',
      '#c7e9b4',
      '#7fcdbb',
      '#41b6c4',
      '#1d91c0',
      '#225ea8',
      '#253494',
      '#081d58'
      ];
    var colorClassObitos = [
      '#f0f0f0',
      '#ffffb2',
      '#fecc5c',
      '#fd8d3c',
      '#f03b20',
      '#bd0026'
    ];
    var colorClassIncidencia = [
      '#f0f0f0',
      '#ffffe5',
      '#f7fcb9',
      '#d9f0a3',
      '#addd8e',
      '#78c679',
      '#41ab5d',
      '#238443',
      '#006837',
      '#004529'
    ];
  var colorClass = colorClassCasos;

  var interCasos = [
      [0, 5],
      [5, 15],
      [15, 35],
      [35, 65],
      [65, 105],
      [105, 195],
      [195, 355],
      [355, 675]
  ];

  var interObitos = [
      [0, 1],
      [1, 5],
      [5, 10],
      [10, 20]
  ];

  var interIncidencia = [
      [0, 5],
      [5, 15],
      [15, 20],
      [20, 25],
      [25, 30],
      [30, 35],
      [35, 40],
      [40, 45]
  ];

  var inter = interCasos;

  var polygonBairros = [];
  var windowbairros = [];

  function getTypeMap() {
    var typeMap = $("input[name='type-map']:checked").val();
    if (typeMap != currentTypeMap) {
        if (typeMap === "is-casos") {
            url = urlCasos;
            colorClass = colorClassCasos;
            inter = interCasos;
        } else if (typeMap === "is-obitos") {
            url = urlObitos;
            colorClass = colorClassObitos;
            inter = interObitos;
        } else {
            url = urlIncidencia;
            colorClass = colorClassIncidencia;
            inter = interIncidencia;
        }
        currentTypeMap = typeMap;
        createLegend();
    }
  }

  function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: {lat: -5.082324, lng: -42.796397},
        scaleControl: true
    });

    for (let i = 0; i < bairros.length; i++) {
      let coordinates = [];
      for (let j = 0; j < bairros[i].geocodes.length; j++) {
          coordinates.push(
              {
                  lat: parseFloat(bairros[i].geocodes[j].lat),
                  lng: parseFloat(bairros[i].geocodes[j].lng)
              }
          );
      }

      polygonBairros[bairros[i].id] = new google.maps.Polygon({
          paths: coordinates,
          strokeColor: '#FFF',
          strokeOpacity: 0.9,
          strokeWeight: 2,
          fillColor: '#858796',
          fillOpacity: 0
      });

      let templateWindow =
      `<div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">${bairros[i].name}</h6>
        </div>
        <div class="card-body">
          População: ${bairros[i].populacao} </br>
        </div>
      </div>`;

      windowbairros[bairros[i].id] = new google.maps.InfoWindow({
        content: templateWindow,
        position: {
                    lat: coordinates[0].lat,
                    lng: coordinates[0].lng
                  }
      });

      google.maps.event.addListener(polygonBairros[bairros[i].id], 'click', function() {
        windowbairros[bairros[i].id].open(map);
      });

      polygonBairros[bairros[i].id].setMap(map);
    }

    createLegend();
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(legend);
    changeMap(arrayDatas[arrayDatas.length -1]);
  }

  function createLegend() {

    legend.classList.add("d-none");
    $('.card-inter').remove();

    for (let index = 0; index < colorClass.length; index++) {

        if (index == 0) {
            templateCard =
            `<div class="text-dark font-weight-bold shadow" style="background-color:${colorClass[0]};">
                <div class="text-center p-0">
                    0
                </div>
            </div>`;
            div = document.createElement('div');
            div.className += "card card-inter p-0 m-1";
            div.innerHTML = templateCard;
            legend.appendChild(div);
            continue;
        }
        if (index == colorClass.length-1) {
            templateCard =
            `<div class="text-white font-weight-bold shadow" style="background-color:${colorClass[index]};">
                <div class="text-center p-0">
                    ${inter[inter.length-1][1]}+
                </div>
            </div>`;
            div = document.createElement('div');
            div.className += "card card-inter p-0 m-1";
            div.innerHTML = templateCard;
            legend.appendChild(div);
            continue;
        }
        if(index > 0 && index <= inter.length) {
            if (index > 4) {
                textColor = "text-white";
            } else {
                textColor = "text-dark";
            }
            templateCard =
            `<div class="${textColor} font-weight-bold shadow" style="background-color:${colorClass[index]};">
                <div class="text-center p-0">
                    ${inter[index-1][0]} - ${inter[index-1][1]}
                </div>
            </div>`;
            div = document.createElement('div');
            div.className += "card card-inter p-0 m-1";
            div.innerHTML = templateCard;
            legend.appendChild(div);
            continue;
        }

    }

    legend.classList.remove("d-none");
  }

  function createTemplateWindow(bairro, dataBairro) {
    let templateWindow = null;
        if (currentTypeMap === "is-casos") {
            templateWindow =
            `<div class="card shadow mb-4">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">${bairro.name}</h6>
                </div>
                <div class="card-body">
                População: ${bairro.populacao} </br>
                Casos por dia: ${dataBairro.casos_dia} </br>
                Total de casos: ${dataBairro.casos} </br>
                </div>
            </div>`;
        } else if (currentTypeMap === "is-obitos") {
            templateWindow =
            `<div class="card shadow mb-4">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">${bairro.name}</h6>
                </div>
                <div class="card-body">
                População: ${bairro.populacao} </br>
                Óbitos por dia: ${dataBairro.casos_dia} </br>
                Total de óbitos: ${dataBairro.casos} </br>
                </div>
            </div>`;
        } else {
            templateWindow =
            `<div class="card shadow mb-4">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">${bairro.name}</h6>
                </div>
                <div class="card-body">
                População: ${bairro.populacao} </br>
                Incidência: ${dataBairro.casos} </br>
                </div>
            </div>`;
        }

    return templateWindow;
  }

  function changeMap(data) {
    getTypeMap();
    bairros.forEach(bairro => {

      $.ajax({
        url : `${url}/${data}/${bairro.id}`,
        contentType: 'application/x-www-form-urlencoded',
        cache: true
    	}).done(function(dataBairro){

          for (let index = 0; index < colorClass.length; index++) {
            if (dataBairro.casos == 0) {
              polygonBairros[dataBairro.id].setOptions({fillColor: colorClass[0]});
              polygonBairros[dataBairro.id].setOptions({fillOpacity: 0.8});
              break;
            }
            if (dataBairro.casos > inter[inter.length-1][1]) {
              polygonBairros[dataBairro.id].setOptions({fillColor: colorClass[colorClass.length-1]});
              polygonBairros[dataBairro.id].setOptions({fillOpacity: 0.8});
              break;
            }
            if(index > 0 && index <= inter.length) {
              if (dataBairro.casos > inter[index-1][0] && dataBairro.casos <= inter[index-1][1]) {
                polygonBairros[dataBairro.id].setOptions({fillColor: colorClass[index]});
                polygonBairros[dataBairro.id].setOptions({fillOpacity: 0.8});
                break;
              }
            }

          }

          let templateWindow = createTemplateWindow(bairro, dataBairro);

          windowbairros[dataBairro.id].setContent(templateWindow);
    	}).fail(function(response){
      	    console.log(response);
    	}).always(function(response){

    	});

    });

  }

  $(document).ready(function() {

    $('#range-data').mousemove(()=>{
        let indice = $('#range-data').val();
        $('#range-data').attr('title', arrayDatas[indice]);
        $('#input-data').val(arrayDatas[indice]);
    });

    $('#range-data').change(()=>{
        let indice = $('#range-data').val();
        changeMap(arrayDatas[indice]);
    });

    $('#input-data').change(()=>{
        let data = $('#input-data').val();
        indice = arrayDatas.indexOf(data);
        $('#range-data').val(indice);
        changeMap(data);
    });

    $("input[name='type-map']").change(()=>{
        let indice = $('#range-data').val();
        changeMap(arrayDatas[indice]);
    });

    $("#gerar-tabela").click(()=>{
        console.log(currentTypeMap);
        let data = $('#input-data').val();
        indice = arrayDatas.indexOf(data);

        if (currentTypeMap === "is-casos") {

        } else if (currentTypeMap === "is-obitos") {

        } else {

        }
        window.open(`{{route('covid-the.map-data-table')}}/${data}/${currentTypeMap}`,
                                      'newwindow',
                                      'width=800,height=600');

    });


  });

</script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&libraries=visualization&callback=initMap">
    //google.maps.event.addDomListener(window, 'load', initialize);
</script>


@endsection
