@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-sm-flex align-items-right right-content-between mb-4">
            <a href="{{$urls['detalhamentos']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-tachometer-alt text-white-50"></i> Geral
            </a>
            <a href="{{$urls['detalhamentos']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-info-circle fa-sm text-white-50"></i> Detalhamento
            </a>
            <a href="{{$urls['clustersmaps']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-globe-americas fa-sm text-white-50"></i>Mapas
            </a>
            <a href="{{$urls['getsemepdem']}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="far fa-calendar-alt fa-sm text-white-50"></i> Semanas Epidemiológicas
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Série linha do tempo {{$fullName}}</h6>
                </div>
                <div class="card-body">
                    <div id="chartMix">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Série linha do tempo {{$fullName}} por COVID-19</h6>
                </div>
                <div class="card-body">
                    <div id="chartMixCovid">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">SRAG hospitalizado {{$fullName}}</h6>
                </div>
                <div class="card-body">
                    <div id="chartFaixaEtaria">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">SRAG hospitalizado {{$fullName}} por COVID-19</h6>
                </div>
                <div class="card-body">
                    <div id="chartFaixaEtariaCovid">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')

<script src="{{ mix('/js/apexcharts.min.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";

    var optionsChartMix = {
        series: [
            {
                name: "Óbitos novos",
                type: 'column',
                data: {!! json_encode($dataChatBarDeath) !!},
            }, {
                name: "Casos novos",
                type: 'column',
                data: {!! json_encode($dataChatBarNew) !!}
            }, {
                name: 'Casos acumulados',
                type: 'area',
                data: {!! json_encode($dataChatArea) !!},
                stroke: {
                curve: 'smooth'
                }
            }
        ],
        chart: {
            type: 'area',
            height: 400,
            width: "100%",
            stacked: true
        },
        stroke: {
          width: [1, 1, 4]
        },
        plotOptions: {
            bar: {
              columnWidth: '50%',
            },
            dataLabels: {
              position: 'top',
              hideOverflowingLabels: true
            }
        },
        fill: {
          opacity: [1, 1, 0.50],
          type: ['solid', 'solid', 'gradient'],
          gradient: {
            inverseColors: false,
            shade: 'light',
            type: "vertical",
            opacityFrom: 0.85,
            opacityTo: 0.55,
            stops: [0, 100, 100, 100]
          }
        },
        dataLabels: {
            enabled: false,
            offsetY: 0,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'top',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        xaxis: {
            type: "datetime",
            categories: {!! json_encode($dataDates) !!},
            labels: {
              hideOverlappingLabels: false,
              showDuplicates: true,
              format: 'dd-MM',
                datetimeFormatter: {
                    year: 'yyyy',
                    month: 'MM-yyyy',
                    day: 'dd-MM',
                    hour: 'HH:mm'
                },
              axisTicks: {
                show: true,
                borderType: 'solid'
              }
            },
            title: {
                text: 'Atualizado em: {{$loadData->updated}}',
                offsetY: 10
            }
        },
        yaxis: [
          {
            min: 0,
            max: 100,
                tooltip: {
                enabled: true
            },
            title: {
                text: "Nº casos por dia"
            }
          },
          {
            min: 0,
            max: 100,
            show: false,
            tooltip: {
            enabled: true
          }
          },
          {
            opposite: true,
            seriesName: 'Casos acumulado',
            tooltip: {
                enabled: true
            },
            title: {
                text: "Casos acumulado"
            }
          }
        ],
        tooltip: {
          fixed: {
            enabled: true,
            position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
            offsetY: 30,
            offsetX: 60
          },
        },
        legend: {
          horizontalAlign: 'left',
          offsetX: 40
        }

    };

    var chartMix = new ApexCharts(document.querySelector("#chartMix"), optionsChartMix);
    chartMix.render();


    var optionsChartMixCovid = {
      series: [
            {
                name: "Nº óbitos casos",
                type: 'column',
                data: {!! json_encode($dataChatBarDeathCovid) !!},
            }, {
                name: "Nº novos casos",
                type: 'column',
                data: {!! json_encode($dataChatBarNewCovid) !!}
            }, {
                name: 'Casos acumulado',
                type: 'area',
                data: {!! json_encode($dataChatAreaCovid) !!},
                stroke: {
                curve: 'smooth'
                }
            }
        ],
        chart: {
            type: 'area',
            height: 400,
            width: "100%",
            stacked: true
        },
        stroke: {
          width: [1, 1, 4]
        },
        plotOptions: {
            bar: {
              columnWidth: '50%',
            },
            dataLabels: {
              position: 'top',
              hideOverflowingLabels: true
            }
        },
        fill: {
          opacity: [1, 1, 0.50],
          type: ['solid', 'solid', 'gradient'],
          gradient: {
            inverseColors: false,
            shade: 'light',
            type: "vertical",
            opacityFrom: 0.85,
            opacityTo: 0.55,
            stops: [0, 100, 100, 100]
          }
        },
        dataLabels: {
            enabled: false,
            offsetY: 0,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'top',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        xaxis: {
            type: "datetime",
            categories: {!! json_encode($dataDatesCovid) !!},
            labels: {
              hideOverlappingLabels: true,
              showDuplicates: true,
              format: 'dd-MM',
                datetimeFormatter: {
                    year: 'yyyy',
                    month: 'MM-yyyy',
                    day: 'dd-MM',
                    hour: 'HH:mm'
                }
            },
            title: {
                text: 'Atualizado em: {{$loadData->updated}}',
                offsetY: 10
            }
        },
        yaxis: [
          {
            min: 0,
            max: 100,
                tooltip: {
                enabled: true
            },
            title: {
                text: "Nº casos por dia"
            }
          },
          {
            min: 0,
            max: 100,
            show: false,
            tooltip: {
            enabled: true
          }
          },
          {
            opposite: true,
            seriesName: 'Casos acumulado',
            tooltip: {
                enabled: true
            },
            title: {
                text: "Casos acumulado"
            }
          }
        ],
        tooltip: {
          fixed: {
            enabled: true,
            position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
            offsetY: 30,
            offsetX: 60
          },
        },
        legend: {
          horizontalAlign: 'left',
          offsetX: 40
        }
    };

    var chartMixCovid = new ApexCharts(document.querySelector("#chartMixCovid"), optionsChartMixCovid);
    chartMixCovid.render();


    var optionsFaixaEtaria = {
          series: [
            {
                name: 'Feminino',
                data: {!! json_encode($dataFaixaEtaria['feminino']) !!}
            }, {
                name: 'Masculino',
                data: {!! json_encode($dataFaixaEtaria['masculino']) !!}
            }
        ],
          chart: {
          type: 'bar',
          height: 440,
          stacked: true
        },
        colors: ['#FF4560', '#008FFB'],
        plotOptions: {
          bar: {
            horizontal: true,
            barHeight: '80%',

          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          width: 1,
          colors: ["#fff"]
        },

        grid: {
          xaxis: {
            lines: {
              show: false
            }
          }
        },
        yaxis: {
          min: -{!! json_encode($dataFaixaEtaria['maxValue']) !!},
          max: {!! json_encode($dataFaixaEtaria['maxValue']) !!},
          title: {
            // text: 'Age',
          },
        },
        tooltip: {
          shared: false,
          x: {
            formatter: function (val) {
              return val
            }
          },
          y: {
            formatter: function (val) {
              return Math.abs(val) + "%"
            }
          }
        },
        xaxis: {
          categories: {!! json_encode($dataFaixaEtaria['categories']) !!},
          title: {
            text: 'Atualizado em: {{$loadData->updated}}',
          },
          labels: {
            formatter: function (val) {
              return Math.abs(Math.round(val)) + "%"
            }
          }
        },
        };

        var chartFaixaEtaria = new ApexCharts(document.querySelector("#chartFaixaEtaria"), optionsFaixaEtaria);
        chartFaixaEtaria.render();

        var optionsFaixaEtariaCovid = {
          series: [
            {
                name: 'Feminino',
                data: {!! json_encode($dataFaixaEtariaCovid['feminino']) !!}
            },
            {
                name: 'Masculino',
                data: {!! json_encode($dataFaixaEtariaCovid['masculino']) !!}
            }
        ],
          chart: {
          type: 'bar',
          height: 440,
          stacked: true
        },
        colors: ['#FF4560', '#008FFB'],
        plotOptions: {
          bar: {
            horizontal: true,
            barHeight: '80%',

          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          width: 1,
          colors: ["#fff"]
        },

        grid: {
          xaxis: {
            lines: {
              show: false
            }
          }
        },
        yaxis: {
          min: -{!! json_encode($dataFaixaEtariaCovid['maxValue']) !!},
          max: {!! json_encode($dataFaixaEtariaCovid['maxValue']) !!},
          title: {
            // text: 'Age',
          },
        },
        tooltip: {
          shared: false,
          x: {
            formatter: function (val) {
              return val
            }
          },
          y: {
            formatter: function (val) {
              return Math.abs(val) + "%"
            }
          }
        },
        xaxis: {
          categories: {!! json_encode($dataFaixaEtariaCovid['categories']) !!},
          title: {
            text: 'Atualizado em: {{$loadData->updated}}'
          },
          labels: {
            formatter: function (val) {
              return Math.abs(Math.round(val)) + "%"
            }
          }
        },
        };

        var chartFaixaEtariaCovid = new ApexCharts(document.querySelector("#chartFaixaEtariaCovid"), optionsFaixaEtariaCovid);
        chartFaixaEtariaCovid.render();

</script>
@endsection
