@extends('layouts.chart-app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="row">
        <div class="col-lx-12 col-lg-12 mb-2">
            <canvas id="myChart" width="800" height="600"></canvas>
        </div>
    </div>

@endsection
@section('scripts')
<script src="{{ mix('/js/moment.min.js') }}"></script>
<script src="{{ mix('/js/Chart.min.js') }}"></script>


<script>
    var timeFormat = 'YYYY-MM-DD';
    var timeFormatBR = 'DD-MM-YYYY';
    var dataLabels = {!! json_encode($labels) !!}

    function newDateString(days) {
            let it_time = moment().add(days, 'd').format(timeFormat);
            console.log(it_time);
			return it_time;
    }

    function labelsDates(labels){
        let newLabels = [];

        labels.forEach(element => {
            newLabels.push(moment(element));
        });
        return newLabels;
    }

    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        data: {
            labels: labelsDates(dataLabels),
            datasets: {!! json_encode($datasets) !!}
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'time',
                    //distribution: 'series',
                    distribution: 'linear',
					offset: true,
                    ticks: {
                        major: {
                            enabled: true,
                            fontStyle: 'bold'
                        },
                        source: 'labels',
                        autoSkip: true,
                        autoSkipPadding: 30,
                        maxRotation: 0,
                        sampleSize: 100
                    },
					//display: true,
                    time: {
                        parser: 'MM-YYYY',
                        tooltipFormat: 'DD-MM-YYYY',
                        unit: 'month',
                        unitStepSize: 1,
                        displayFormats: {
                        'month': 'MM-YYYY'
                        }
                    }
				}],
                yAxes: {!! json_encode($yAxes) !!}
            }
        }
    });

</script>


@endsection
