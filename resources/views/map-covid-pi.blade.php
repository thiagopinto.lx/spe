@extends('layouts.app')
@section('css')

@endsection
@section('content')
    <div class="d-sm-flex align-items-right right-content-between mb-4">
        <a href="{{$urls['index']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Casos confirmados
        </a>
        <a href="{{$urls['obito']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-tachometer-alt text-white-50"></i> Óbito confirmados
        </a>
        <a href="{{$urls['map-evolucao']}}" style="margin-right: 10px;"
        class="d-none d-sm-inline-block btn btn-sm
        @if($options['area'] == 'map-evolucao')
            btn-outline-primary
        @else
            btn-primary
        @endif
        shadow-sm">
            <i class="fas fa-tachometer-alt
            @if($options['area'] == 'map-evolucao')
                text-primary-50
            @else
                text-white-50
            @endif
            "></i> Mapa da evolução dos casos
        </a>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">Mapa da evolução dos casos</h6>
                </div>
                <div class="card-body">
                    <div class="form-group mb-3">
                        <div class="col-12">
                          <input
                            class="form-control"
                            type="date"
                            class="form-control"
                            aria-label="date"
                            min="{{$datas[0]}}"
                            max="{{$datas[count($datas)-1]}}"
                            value="{{$datas[count($datas)-1]}}"
                            id="input-data">
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input
                            type="range"
                            id="range-data"
                            class="custom-range"
                            data-toggle="tooltip"
                            title="{{$datas[count($datas)-1]}}" min="0" max="{{count($datas)-1}}" value="{{count($datas)-1}}">
                    </div>

                    <div class="row">
                      <div class="col-lg-12" id="map" style="height: 800px">

                      </div>
                      <div class="card shadow mb-4 col-lg-2 p-0 d-none" id="legend">
                        <div class="card-header py-1 m-0">
                          <h6 class="m-0 font-weight-bold text-primary text-center">Legendas</h6>
                        </div>


                      </div>
                    </div>

                </div>
              </div>
        </div>
    </div>

@endsection
@section('scripts')
<script type="text/javascript">
  var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
  var token = "{{csrf_token()}}";
  var arrayDatas = {!! json_encode($datas) !!};
  var estado = {!! json_encode($estado) !!};
  var cidades = {!! json_encode($cidades) !!};
  var url = "{{$urls['map-data']}}";
  var colorClass = [
      '#f0f0f0',
      '#ffffd9',
      '#edf8b1',
      '#c7e9b4',
      '#7fcdbb',
      '#41b6c4',
      '#1d91c0',
      '#225ea8',
      '#253494',
      '#081d58',
      '#210558',
      '#3E0258'

      ];
  var inter = [
      [0, 10],
      [10, 20],
      [20, 40],
      [40, 80],
      [80, 160],
      [160, 320],
      [320, 640],
      [640, 1280],
      [1280, 2560],
      [2560, 5120]

  ];
  var polygonCidades = [];
  var windowCidades = [];
  function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 7,
      center: {lat: -6.9265047, lng: -43.3092274},
      scaleControl: true
    });
    var legend = document.getElementById('legend');

    for (let i = 0; i < cidades.length; i++) {
      let coordinates = [];
      for (let j = 0; j < cidades[i].geocodes.length; j++) {
          coordinates.push(
              {
                  lat: parseFloat(cidades[i].geocodes[j].lat),
                  lng: parseFloat(cidades[i].geocodes[j].lng)
              }
          );
      }

      polygonCidades[cidades[i].id] = new google.maps.Polygon({
          paths: coordinates,
          strokeColor: '#FFF',
          strokeOpacity: 0.9,
          strokeWeight: 2,
          fillColor: '#858796',
          fillOpacity: 0
      });

      let templateWindow =
      `<div class="card shadow mb-4">
        <div class="card-header py-3">
          <h6 class="m-0 font-weight-bold text-primary">${cidades[i].nome}</h6>
        </div>
        <div class="card-body">
          População: ${cidades[i].populacao} </br>
        </div>
      </div>`;

      windowCidades[cidades[i].id] = new google.maps.InfoWindow({
        content: templateWindow,
        position: {
                    lat: coordinates[0].lat,
                    lng: coordinates[0].lng
                  }
      });

      google.maps.event.addListener(polygonCidades[cidades[i].id], 'click', function() {
        windowCidades[cidades[i].id].open(map);
      });

      polygonCidades[cidades[i].id].setMap(map);
    }
    let coordinatesEstado = [];
    for (let j = 0; j < estado.geocodes.length; j++) {
        coordinatesEstado.push(
            {
                lat: parseFloat(estado.geocodes[j].lat),
                lng: parseFloat(estado.geocodes[j].lng)
            }
        );
    }

    polygonEstado = new google.maps.Polygon({
          paths: [coordinatesEstado, coordinatesEstado],
          strokeColor: '#FFF',
          strokeOpacity: 0.5,
          strokeWeight: 3,
          fillColor: 'FFF',
          fillOpacity: 0.2
      });


    for (let index = 0; index < colorClass.length; index++) {

        if (index == 0) {
            templateCard =
            `<div class="text-dark font-weight-bold shadow" style="background-color:${colorClass[0]};">
                <div class="text-center p-0">
                    0 Caso
                </div>
            </div>`;
            div = document.createElement('div');
            div.className += "card p-0 m-1";
            div.innerHTML = templateCard;
            legend.appendChild(div);
            continue;
        }
        if (index == colorClass.length-1) {
            templateCard =
            `<div class="text-white font-weight-bold shadow" style="background-color:${colorClass[index]};">
                <div class="text-center p-0">
                    ${inter[inter.length-1][1]}+ Casos
                </div>
            </div>`;
            div = document.createElement('div');
            div.className += "card p-0 m-1";
            div.innerHTML = templateCard;
            legend.appendChild(div);
            continue;
        }
        if(index > 0 && index <= inter.length) {
            if (index > 4) {
                textColor = "text-white";
            } else {
                textColor = "text-dark";
            }
            templateCard =
            `<div class="${textColor} font-weight-bold shadow" style="background-color:${colorClass[index]};">
                <div class="text-center p-0">
                    ${inter[index-1][0]} - ${inter[index-1][1]} Casos
                </div>
            </div>`;
            div = document.createElement('div');
            div.className += "card p-0 m-1";
            div.innerHTML = templateCard;
            legend.appendChild(div);
            continue;
        }

    }
    map.controls[google.maps.ControlPosition.RIGHT_TOP].push(legend);
    legend.classList.remove("d-none");

    polygonEstado.setMap(map);
    changeMap(arrayDatas[arrayDatas.length -1]);
  }

  function changeMap(data) {
    cidades.forEach(cidade => {
        $.ajax({
        url : `${url}/${data}/${cidade.id}`,
        contentType: 'application/x-www-form-urlencoded',
        cache: false
        }).done(function(cidade){

          for (let index = 0; index < colorClass.length; index++) {
            if (cidade.total_casos == 0) {
                console.log(cidade.total_casos);
              polygonCidades[cidade.id].setOptions({fillColor: colorClass[0]});
              polygonCidades[cidade.id].setOptions({fillOpacity: 0.8});
              break;
            }
            if (cidade.total_casos > inter[inter.length-1][1]) {
                console.log(cidade.total_casos);
              polygonCidades[cidade.id].setOptions({fillColor: colorClass[colorClass.length-1]});
              polygonCidades[cidade.id].setOptions({fillOpacity: 0.8});
              break;
            }
            if(index > 0 && index <= inter.length) {
              if (cidade.total_casos > inter[index-1][0] && cidade.total_casos <= inter[index-1][1]) {
                polygonCidades[cidade.id].setOptions({fillColor: colorClass[index]});
                polygonCidades[cidade.id].setOptions({fillOpacity: 0.8});
                break;
              }
            }

          }

            let templateWindow =
            `<div class="card shadow mb-4">
                <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">${cidade.nome}</h6>
                </div>
                <div class="card-body">
                População: ${cidade.populacao} </br>
                Casos do dia: ${cidade.casos_dia} </br>
                Total de casos: ${cidade.total_casos} </br>
                </div>
            </div>`;

            windowCidades[cidade.id].setContent(templateWindow);
        }).fail(function(response){
        console.log(response);
        }).always(function(response){
        });
    });
  }

  $(document).ready(function() {

    $('#range-data').mousemove(()=>{
        let indice = $('#range-data').val();
        $('#range-data').attr('title', arrayDatas[indice]);
        $('#input-data').val(arrayDatas[indice]);
    });

    $('#range-data').change(()=>{
        let indice = $('#range-data').val();
        changeMap(arrayDatas[indice]);
    });

    $('#input-data').change(()=>{
        let data = $('#input-data').val();
        indice = arrayDatas.indexOf(data);
        $('#range-data').val(indice);
        changeMap(data);
    });


  });

</script>

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&libraries=visualization&callback=initMap">
    //google.maps.event.addDomListener(window, 'load', initialize);
</script>


@endsection
