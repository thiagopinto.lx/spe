@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">

@endsection
@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="d-sm-flex align-items-right right-content-between mb-4 d-print-none">
            <a href="{{$urls['index']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-tachometer-alt text-white-50"></i> Casos confirmados
            </a>
            <a href="{{$urls['obito']}}" style="margin-right: 10px;"
            class="d-none d-sm-inline-block btn btn-sm
            @if($options['area'] == 'obito')
                btn-outline-primary
            @else
                btn-primary
            @endif
            shadow-sm">
                <i class="fas fa-tachometer-alt
                @if($options['area'] == 'obito')
                    text-primary-50
                @else
                    text-white-50
                @endif
                "></i> Óbito confirmados
            </a>
            <a href="{{$urls['map-evolucao']}}" style="margin-right: 10px;" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
                <i class="fas fa-tachometer-alt text-white-50"></i> Mapa da evolução dos casos
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 mb-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col-lg-3">
                            <h6 class="m-0 font-weight-bold text-primary">Progressão {{$fullName}}</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="custom-control custom-switch is-casos-dias">
                                <input type="checkbox" class="custom-control-input" id="is-casos-dias">
                                <label class="custom-control-label" for="is-casos-dias">Óbitos por dia</label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="custom-control custom-switch is-medias-dias">
                                <input type="checkbox" class="custom-control-input" id="is-medias-dias">
                                <label class="custom-control-label" for="is-medias-dias">Média rolante de 7 dias</label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="custom-control custom-switch is-faixa-crecimento">
                                <input type="checkbox" class="custom-control-input" id="is-faixa-crecimento">
                                <label class="custom-control-label" for="is-faixa-crecimento">Faixas de crescimento</label>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="custom-control custom-switch is-incidencia">
                                <input type="checkbox" class="custom-control-input" id="is-incidencia">
                                <label class="custom-control-label" for="is-incidencia">Mortalidade/100mil hab.</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div id="chart">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 mb-2">
            <a href="#" class="btn btn-light btn-icon-split">
                <span class="icon text-gray-600">
                <i class="fas fa-arrow-right"></i>
                </span>
                <span class="text mb-0 text-gray-800">Cidades do Piauí</span>
            </a>
        </div>
        <div class="col-lg-3 mb-2">
            <button type="button" id="limpar" class="btn btn-warning btn-block">Limpar</button>
        </div>
    </div>
    <div class="row">
    @isset($cidades)
        @php
            $j = 0;
        @endphp
        @for ($i = 0; $i < 3; $i++)
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-4 mb-4 no-gutters">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body" style="padding: 0.25rem;">
                <div class="row no-gutters">
                    <div class="btn-group-vertical" style="width: 100%;">
                        @while ($j < $totalCidades)
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <div class="input-group-text" style="padding: 0.50rem 0.45rem;">
                                    <input
                                        type="checkbox" class="cidade_{{$cidades[$j]->cidade_id}} cidade"
                                        name="cidades[]"
                                        value="{{$cidades[$j]->cidade_id}}"
                                        @if ($j < 5)
                                        checked
                                        @endif
                                        >
                                </div>
                            </div>
                            <label class="form-control" aria-label="Input text com checkbox" style="padding: auto auto;">
                                {{$cidades[$j]->nome}}
                                <a class="btn-sm btn-light"
                                    data-toggle="tooltip"
                                    data-placement="top"
                                    title="Gráficos de Evolução" style="margin: -5px;"
                                    href="{{route('casoscovid.evolucao-obitos')}}/{{$cidades[$j]->cidade_id}}"
                                    onclick="window.open('{{route('casoscovid.evolucao-obitos')}}/{{$cidades[$j]->cidade_id}}',
                                                'newwindow',
                                                'width=800,height=600');
                                    return false;"
                                    >
                                    Evolução <i class="fas fa-chart-line fa-1x text-gray-600"></i>
                                </a>
                            </label>
                        </div>
                        @php
                            $j++;
                        @endphp
                        @if (($j % $limiteColunas) == 0)
                            @break
                        @endif

                        @endwhile

                    </div>
                </div>
                </div>
            </div>
        </div>
        @endfor
    @endisset
    </div>

@endsection
@section('scripts')

<script src="{{ mix('/js/apexcharts.min.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";
    var urlSeries = "{{route('casoscovid.serieobitoabsoluta')}}";
    var urlIncidencia = "{{route('casoscovid.serieobitoincidencia')}}";
    var urlCasosDias = "{{route('casoscovid.serieobitosdias')}}";
    var urlMediasDias = "{{route('casoscovid.seriemediasobitosdias')}}";
    var urlMediasIncidencia = "{{route('casoscovid.seriemediasobitosincidencia')}}";
    var latestUpdate = {!! json_encode($latestUpdate) !!};
    const indiceAbsolutos = {!! json_encode($indiceAbsolutos) !!};
    const indiceIncidencia = {!! json_encode($indiceIncidencia) !!};
    const categories = {!! json_encode($datas) !!};
    var titlesY = [
        "Óbitos acumulados",
        "Óbitos por dia",
        "Média rolante de 7 dias",
        "Média rolante de 7 dias por Mortalidade/100mil hab.",
        "Incidência/100mil hab."
    ];
    var is_incidencia = false;
    var is_casos_dias = false;
    var is_faixa_crecimento = false;
    var is_medias_dias = false;
    var chart;
    var options;
    var maxY = 1;
    var url = urlSeries;
</script>
<script src="{{ mix('/js/charts-covid.js') }}"></script>
@endsection
