@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  @endsection

  @section('content')

  <!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Notificação: {{$notificacao}} da tabela {{$tabela}} </h1>
  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <div class="row">
        <div class="col-md-12">
            <form method="POST" action="{{ route('painel.notificacao.updategeocode', $tabela) }}" class="form-inline">
                @method('PATCH')
                @csrf
              <div class="form-row">
                <input type="hidden" name="NU_NOTIFIC" value="{{$options['notificacao']->NU_NOTIFIC}}">
                <div class="col">
                    <input type="text" disabled class="form-control" name="ID_GEO1" id="lat" value="{{$options['notificacao']->ID_GEO1}}">
                </div>
                <div class="col">
                    <input type="text" disabled class="form-control" name="ID_GEO2" id="lng" value="{{$options['notificacao']->ID_GEO2}}">
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
    <div class="card-body">
      <div id="map" style="height: 750px;">

      </div>
    </div>
  </div>


  @endsection
  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";
    </script>
    <script>

      var divContent = document.createElement('div');
      var divSiteNotice = document.createElement('div');
      var divBodyContent = document.createElement('div');
      var h4SiteNotice = document.createElement('h4');
      divContent.className = 'content';
      divSiteNotice.className = 'siteNotice';
      divBodyContent.className = 'divBodyContent';
      var titulo = document.createTextNode('Notificação: {{$options['notificacao']->NU_NOTIFIC}}');
      h4SiteNotice.appendChild(titulo);
      divSiteNotice.appendChild(h4SiteNotice);
      var endereco = document.createTextNode(
                 'Notificação: {{$options['notificacao']->NU_NOTIFIC}},'+
                 'Endereço: {{$options['notificacao']->NM_LOGRADO}} '+
                 '{{$options['notificacao']->NU_NUMERO}} '+
                 'Bairro: {{$options['notificacao']->NM_BAIRRO}} '+
                 'Referência: {{$options['notificacao']->NM_COMPLEM}}'
                 );
      divBodyContent.appendChild(endereco);
      divContent.appendChild(divSiteNotice);
      divContent.appendChild(divBodyContent);

      var map;
      var markers = new Array();
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: {{$options['notificacao']->ID_GEO1}}, lng: {{$options['notificacao']->ID_GEO2}}},
        zoom: 16
        });

        var infowindow = new google.maps.InfoWindow({
        content: divContent.outerHTML
        });

        markers[0] = new google.maps.Marker({
        position: {lat: {{$options['notificacao']->ID_GEO1}}, lng: {{$options['notificacao']->ID_GEO2}}},
        map: map,
        title: 'Notificação: {{$options['notificacao']->NU_NOTIFIC}},'+
                'Endereço: {{$options['notificacao']->NM_LOGRADO}} '+
                '{{$options['notificacao']->NU_NUMERO}} '+
                'Bairro: {{$options['notificacao']->NM_BAIRRO}} '+
                'Referência: {{$options['notificacao']->NM_COMPLEM}}',
        draggable: true
        });
        markers[0].addListener('click', function() {
        infowindow.open(map, markers[0]);
        });


        markers[0].addListener('dragend', function(){
        document.getElementById('lat').value = markers[0].getPosition().lat();
        document.getElementById('lng').value = markers[0].getPosition().lng();
        });

      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_KEY')}}&callback=initMap">
    //google.maps.event.addDomListener(window, 'load', initialize);
    </script>

  @endsection
