@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  <style>

    .multiple-datasets .league-name {
      width: 80%;
      font-size: 1rem;
      margin: 5px 20px 5px 20px;
      padding: 3px 0;
      border-bottom: 1px solid #ccc;
      background-color: #fff;
    }

    .multiple-datasets .typeahead {
      width: 450px;
    }
  </style>
  @endsection

  @section('content')
  <div class="row">

  </div>
  <div class="row">
    <div class="col-lg-12 mb-4">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <div class="row">
              <div class="col-md-6">
                <h6 class="font-weight-bold text-primary">{{$options['descricao']}}</h6>
              </div>
              <div class="col-md-6 text-right">
                <a class="ali btn btn-primary" id="checked-all" href="#" role="button">Marcar Todos</a>
                <a class="ali btn btn-primary button-gps" href="#" role="button">
                  <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span>
                  <span class="fas fa-globe-americas" role="status" aria-hidden="true"></span>
                </a>
              </div>
            </div>

          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" cellspacing="0">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Bairro Data SUS</th>
                    <th>Atualização</th>
                    <th>Bairro Teresina</th>
                    <th><i class="fas fa-globe-americas"></i></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Id</th>
                    <th>Bairro Data SUS</th>
                    <th>Atualização</th>
                    <th>Bairro Teresina</th>
                    <th><i class="fas fa-globe-americas"></i></th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
    </div>
  </div>

  @endsection
  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
      var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
      var getindex = "{{$options['getindex']}}";
      var mylanguage = "{{ asset('/js/Portuguese-Brasil.json') }}";
      var token = "{{csrf_token()}}";
      var no_bairro_url = "{{route('painel.bairros.autocomplete')}}";
      var relationship_url = "{{route('painel.bairro-alias')}}";
      var getgps = "{{ route('painel.bairro-alias.getgps') }}"
    </script>
    <script src="{{ mix('/js/bairro-alias.js') }}"></script>

  @endsection
