@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ mix('/css/bootstrap-colorselector.css') }}" rel="stylesheet" type="text/css">
  @endsection

  @section('content')
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#carregar-dbf">
      Cadastra Cilco
    </button>

    <div class="modal fade" id="carregar-dbf" tabindex="-1" role="dialog" aria-labelledby="carregarModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="carregarModalLabel">Formulario de cadastro de Ciclos do CSV</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <div id="divFormCarregar">
            <form class="addForm" action="{{$options['store']}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="ano">Ano</label>
                <input type="number" class="form-control" id="ano" maxlength="4" min="1900" max="2100" name="ano" required>
              </div>
              <div class="form-group">
                <label for="nu_ciclo">Ciclo</label>
                <input type="number" class="form-control" id="nu_ciclo" name="nu_ciclo" maxlength="1" min="1" max="5" required>
              </div>
              <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" id="is_cabecalho" name="is_cabecalho">
                <label class="form-check-label" for="is_cabecalho">Arqui tem cabeçalho?</label>
              </div>
              <div class="form-group">
                <label for="fileCSV">Selecione arquivo CSV</label>
                <input type="file" class="form-control-file" id="fileCSV" name="fileCSV">
              </div>
              <div class="loadDbf d-none">
                <div class="spinner-border" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>

              <button type="submit" class="carregar btn btn-primary">Cadastro de Ciclo</button>
            </form>
          </div>

          <div class="message">

          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">{{$options['descricao']}}</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Id</th>
              <th>Ano</th>
              <th>Ciclo</th>
              <th>View</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Id</th>
              <th>Ano</th>
              <th>Ciclo</th>
              <th>View</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
  @endsection
  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
      var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
      var getindex = "{{$options['getindex']}}";
      var mylanguage = "{{ asset('/js/Portuguese-Brasil.json') }}";
      var token = "{{csrf_token()}}";
    </script>

    <script src="{{ mix('/js/ciclos.js') }}"></script>
  @endsection
