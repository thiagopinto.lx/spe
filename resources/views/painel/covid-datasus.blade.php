@extends('layouts.app')
  @section('css')
  <link href="{{ mix('/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css">
  <link href="{{ mix('/css/bootstrap-colorselector.css') }}" rel="stylesheet" type="text/css">
  @endsection

  @section('content')
  <div class="row mb-2">
    <div class="col-md-6  align-self-center">
        <button class="btn btn-primary get-datasus" type="button">
            <span class="spinner-border spinner-border-sm spinner-loading d-none" role="status" aria-hidden="true"></span>
            <span class="text-loading d-none">Carregando dados da API Datasus</span>
            <span class="spinner-border-sm fas fa-database icon-not-loading"></span>
            <span class="text-not-loading">Carregar dados da API Datasus</span>
        </button>
    <a class="btn btn-primary" href="{{ route('painel.bairro-alias') }}" role="button">
            Bairros Data SUS <i class="fas fa-arrow-right"></i> Bairros Teresina
        </a>
    </div>
    <div class="col-md-6">
        <fieldset class="form-group row">
            <div class="form-group mx-sm-3 mb-2">
              <label for="dataInicio">Última atualização: </label>
              <input type="date" class="form-control" name="updateDate" value="{{$lastDateUpdate}}" id="updateDate" required>
            </div>
        </fieldset>
    </div>
  </div>
  <div class="row">
      <!-- DataTales Example -->
    <div class="col-lg-12 mb-4">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <div class="row">
              <div class="col-md-6">
                <h6 class="font-weight-bold text-primary">{{$options['descricao']}}</h6>
              </div>
              <div class="col-md-6 text-right">

              </div>
            </div>

          </div>

          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" cellspacing="0">
                <thead>
                  <tr>
                    <th>Data da notificação</th>
                    <th>Bairro</th>
                    <th>Tipo do teste</th>
                    <th>Sintomas</th>
                    <th>Atualização</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>Data da notificação</th>
                    <th>Bairro</th>
                    <th>Tipo do teste</th>
                    <th>Sintomas</th>
                    <th>Atualização</th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
    </div>
  </div>

  @endsection
  @section('scripts')

    <!-- Page level plugins -->
    <script type="text/javascript">
      var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
      var getindex = "{{$options['getindex']}}";
      var mylanguage = "{{ asset('/js/Portuguese-Brasil.json') }}";
      var token = "{{csrf_token()}}";
      var getgps = "{{ route('painel.estabelecimentos-cnes.getgps')}}"
      var urlUpdateNotificacoes = "{{ route('painel.covid-datasus.update') }}";
    </script>

    <script src="{{ mix('/js/covid-datasus.js') }}"></script>
  @endsection
