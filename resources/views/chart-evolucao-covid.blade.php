@extends('layouts.chart-app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12 mb-4">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Linha do tempo de {{$fullName}}</h6>
            </div>
            <div class="card-body">
                <div id="chartMix">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')

<script src="{{ mix('/js/apexcharts.min.js') }}"></script>
<script type="text/javascript">
    var xcsrftoken = $('meta[name="csrf-token"]').attr('content');
    var token = "{{csrf_token()}}";

    var optionsChartMix = {
        series: [
            {
                name: "{!! $serieNameBar !!}",
                type: 'column',
                data: {!! json_encode($dataChatBar) !!},
            }, {
                name: "{!! $serieNameArea !!}",
                type: 'area',
                data: {!! json_encode($dataChatArea) !!},
                stroke: {
                curve: 'smooth'
                }
            }
        ],
        chart: {
            type: 'area',
            height: 400,
            width: "100%",
            stacked: true
        },
        stroke: {
          width: [1, 1, 4]
        },
        plotOptions: {
            bar: {
              columnWidth: '50%',
            },
            dataLabels: {
              position: 'top',
              hideOverflowingLabels: true
            }
        },
        fill: {
          opacity: [1, 1, 0.50],
          type: ['solid', 'solid', 'gradient'],
          gradient: {
            inverseColors: false,
            shade: 'light',
            type: "vertical",
            opacityFrom: 0.85,
            opacityTo: 0.55,
            stops: [0, 100, 100, 100]
          }
        },
        dataLabels: {
            enabled: false,
            offsetY: 0,
            style: {
                fontSize: '12px',
                colors: ["#304758"]
            }
        },
        responsive: [{
          breakpoint: 480,
          options: {
            legend: {
              position: 'top',
              offsetX: -10,
              offsetY: 0
            }
          }
        }],
        xaxis: {
            type: "datetime",
            categories: {!! json_encode($dataDates) !!},
            labels: {
              hideOverlappingLabels: false,
              showDuplicates: true,
              format: 'dd-MM',
                datetimeFormatter: {
                    year: 'yyyy',
                    month: 'MM-yyyy',
                    day: 'dd-MM',
                    hour: 'HH:mm'
                },
              axisTicks: {
                show: true,
                borderType: 'solid'
              }
            },
            title: {
                text: '',
                offsetY: 10
            }
        },
        yaxis: [
          {
            min: 0,
            max: {!! json_encode($yaxis0) !!},
            seriesName: "{!! $serieNameBar !!}",
            tooltip: {
                enabled: true
            },
            title: {
                text: "{!! $serieNameBar !!}"
            }
          },
          {
            max: {!! json_encode($yaxis1) !!},
            opposite: true,
            seriesName: "{!! $serieNameArea !!}",
            tooltip: {
                enabled: true
            },
            title: {
                text: "{!! $serieNameBar !!}"
            }
          }
        ],
        tooltip: {
          shared: false,
          intersect: true,
          inverseOrder: true,
          markers: {
              size: 0
          },
          fixed: {
            enabled: false,
            position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
            offsetY: 30,
            offsetX: 60
          },
          x: {
              show: true,
              format: 'dd-MM'
          },

        },
        legend: {
          horizontalAlign: 'left',
          offsetX: 40
        }

    };

    var chartMix = new ApexCharts(document.querySelector("#chartMix"), optionsChartMix);
    chartMix.render();

</script>


@endsection
