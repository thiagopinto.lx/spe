@extends('layouts.site')
@section('css')

@endsection
@section('content')
<main role="main">


<div id="myCarousel" class="carousel slide bg-blue-fms" data-ride="carousel">
        <ol class="carousel-indicators bg-blue-fms">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner bg-blue-fms">
          <div class="carousel-item active">
            <div class="container">
              <div class="carousel-caption text-left">
                <h1>Título 01</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Leia Mais...</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item bg-blue-fms">
            <div class="container">
              <div class="carousel-caption">
                <h1>Título 02</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Leia Mais...</a></p>
              </div>
            </div>
          </div>
          <div class="carousel-item bg-blue-fms">
            <div class="container">
              <div class="carousel-caption text-right">
                <h1>Título 3</h1>
                <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Leia Mais...</a></p>
              </div>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Próximo</span>
        </a>
      </div>


  <div class="row">
    <div class="col-md-8">

      <div class="row">
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-2 text-primary">World</strong>
              <h3 class="mb-0">
                <a class="text-dark" href="#">Título</a>
              </h3>
              <div class="mb-1 text-muted">Nov 12</div>
              <p class="card-text mb-auto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eget dui vel tortor suscipit semper.</p>
              <a href="#">Continue lendo...</a>
            </div>
            <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="Card image cap">
          </div>
        </div>
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-2 text-success">Design</strong>
              <h3 class="mb-0">
                <a class="text-dark" href="#">Título</a>
              </h3>
              <div class="mb-1 text-muted">Nov 11</div>
              <p class="card-text mb-auto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eget dui vel tortor suscipit semper.</p>
              <a href="#">Continue lendo...</a>
            </div>
            <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="Card image cap">
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-2 text-primary">World</strong>
              <h3 class="mb-0">
                <a class="text-dark" href="#">Título</a>
              </h3>
              <div class="mb-1 text-muted">Nov 12</div>
              <p class="card-text mb-auto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eget dui vel tortor suscipit semper.</p>
              <a href="#">Continue lendo...</a>
            </div>
            <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="Card image cap">
          </div>
        </div>
        <div class="col-md-6">
          <div class="card flex-md-row mb-4 box-shadow h-md-250">
            <div class="card-body d-flex flex-column align-items-start">
              <strong class="d-inline-block mb-2 text-success">Design</strong>
              <h3 class="mb-0">
                <a class="text-dark" href="#">Título</a>
              </h3>
              <div class="mb-1 text-muted">Nov 11</div>
              <p class="card-text mb-auto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean eget dui vel tortor suscipit semper.</p>
              <a href="#">Continue lendo...</a>
            </div>
            <img class="card-img-right flex-auto d-none d-md-block" data-src="holder.js/200x250?theme=thumb" alt="Card image cap">
          </div>
        </div>
      </div>

    </div>

    <aside class="col-md-4 blog-sidebar">
      <div class="p-3 mb-3 rounded bg-grey">
        <h4 class="font-italic">Sobre</h4>
        <p class="mb-0">
          Realizamos as ações de vigilância, prevenção e controle de doenças transmissíveis, 
          verificação de fatores de risco para o desenvolvimento de doenças crônicas não transmissíveis, 
          saúde ambiental e do trabalhador para a análise de situação de saúde da população Teresina.
        </p>
      </div>

      <div class="p-3 mb-3 rounded bg-grey">
        <h4 class="font-italic">Postes</h4>
        <ol class="list-unstyled mb-0">
          <li><a href="#">Março 2020</a></li>
          <li><a href="#">Fevereiro 2020</a></li>
          <li><a href="#">Janeiro 2020</a></li>
          <li><a href="#">Dezembro 2019</a></li>
          <li><a href="#">Novembro 2019</a></li>
          <li><a href="#">Outubro 2019</a></li>
          <li><a href="#">Setembro 2019</a></li>
          <li><a href="#">Agosto 2019</a></li>
          <li><a href="#">Julho 2019</a></li>
          <li><a href="#">Junho 2019</a></li>
          <li><a href="#">Maio 2019</a></li>
          <li><a href="#">Abril 2019</a></li>
        </ol>
      </div>

      <div class="p-3 mb-3 rounded bg-grey">
        <h4 class="font-italic">Redes Sociais</h4>
        <ol class="list-unstyled">
          <li><a href="#">GitHub</a></li>
          <li><a href="#">Twitter</a></li>
          <li><a href="#">Facebook</a></li>
        </ol>
      </div>
    </aside><!-- /.blog-sidebar -->

  </div>
</main>
@endsection
@section('scripts')

@endsection