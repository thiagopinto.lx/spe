@extends('layouts.app')
@section('css')
<link href="{{ mix('/css/Chart.min.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('content')
    <div class="row justify-content-end">
        <div class="col-lg-2 mb-2">
            <a href="#" id="make-chart" class="btn btn-danger btn-icon-split">
                <span class="icon">
                <i class="far fa-chart-bar"></i>
                </span>
                <span class="text mb-0">Gerar Grafico</span>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mb-2">
            <a href="#" class="btn btn-light btn-icon-split">
                <span class="icon text-gray-600">
                <i class="fas fa-arrow-right"></i>
                </span>
                <span class="text mb-0 text-gray-800">Dados INMET</span>
            </a>
        </div>
    </div>

    <div class="row">
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-2 col-md-6 mb-4 no-gutters">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                <div class="row no-gutters">
                    <div class="btn-group-vertical">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" aria-label="Input text com checkbox" value="Precipitação" style="padding: 1px;" disabled>
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" aria-label="Input text com checkbox" value="Temperatura" style="padding: 1px;" disabled>
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" aria-label="Input text com checkbox" value="Umidade do ar" style="padding: 1px;" disabled>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    @isset($loadInmets)
        @for ($i = 0; $i < 5; $i++)
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-2 col-md-4 mb-4 no-gutters">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                <div class="row no-gutters">
                    <div class="btn-group-vertical" style="width: 100%;">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <div class="input-group-text" style="background-color:{{$loadInmets[$i]->color}}; padding: 0.50rem 0.45rem;">
                                    <input type="checkbox" class="anos chuva inmet" name="chuva[]"  autocomplete="off" value="{{$loadInmets[$i]->id}}">
                                </div>
                            </div>
                            <input type="text" class="form-control" aria-label="Input text com checkbox" value="{{$loadInmets[$i]->ano}}" style="padding: 1px;" disabled>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <div class="input-group-text" style="background-color:{{$loadInmets[$i]->color}}; padding: 0.50rem 0.45rem;">
                                    <input type="checkbox" class="anos temp inmet" name="temp[]"  autocomplete="off" value="{{$loadInmets[$i]->id}}">
                                </div>
                            </div>
                            <input type="text" class="form-control inmet_{{$loadInmets[$i]->id}}" aria-label="Input text com checkbox" value="{{$loadInmets[$i]->ano}}" style="padding: 1px;" disabled>
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <div class="input-group-text" style="background-color:{{$loadInmets[$i]->color}}; padding: 0.50rem 0.45rem;">
                                    <input type="checkbox" class="anos ar inmet" name="ar[]"  autocomplete="off" value="{{$loadInmets[$i]->id}}">
                                </div>
                            </div>
                            <input type="text" class="form-control inmet_{{$loadInmets[$i]->id}}" aria-label="Input text com checkbox" value="{{$loadInmets[$i]->ano}}" style="padding: 1px;" disabled>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        @endfor
    @endisset
    </div>

    <div class="row">
        <div class="col-lg-12 mb-2">
            <a href="#" class="btn btn-light btn-icon-split">
                <span class="icon text-gray-600">
                <i class="fas fa-arrow-right"></i>
                </span>
                <span class="text mb-0 text-gray-800">Dados menções feitas na internet</span>
            </a>
        </div>
    </div>

    <div class="row">
        @isset($loadTrends)
        <div class="col-xl-2 col-md-6 mb-4 no-gutters">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                <div class="row no-gutters">
                    <div class="btn-group-vertical">
                        @foreach ($loadTrends as $key => $item)
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" aria-label="Input text com checkbox" value="{{$key}}" style="padding: 1px;" disabled>
                        </div>
                        @endforeach
                    </div>
                </div>
                </div>
            </div>
        </div>
        @endisset
    @isset($loadInmets)
        @for ($i = 0; $i < 5; $i++)
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-2 col-md-4 mb-4 no-gutters">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                <div class="row no-gutters">
                    <div class="btn-group-vertical" style="width: 100%;">
                        @foreach ($loadTrends as $item)
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <div class="input-group-text" style="background-color:{{$item[$i]->color}}; padding: 0.50rem 0.45rem;">
                                    <input type="checkbox" class="anos_{{$item[$i]->ano}} trend" name="trend[]"  autocomplete="off" value="{{$item[$i]->id}}" disabled>
                                </div>
                            </div>
                            <input type="text" class="form-control" aria-label="Input text com checkbox" value="{{$item[$i]->ano}}" style="padding: 1px;" disabled>
                        </div>
                        @endforeach

                    </div>
                </div>
                </div>
            </div>
        </div>
        @endfor
    @endisset
    </div>

@endsection
@section('scripts')
<script>
    let urlCharts = "{{$urlCharts}}";
    let parameters = "";
$(document).ready(function() {

    $(".inmet").change(function(event) {
        if(this.checked) {
            $(".inmet").attr("disabled", true);
            $(this).removeAttr("disabled");
            let ano = $(this).parent().parent().next().val();
            //console.log(ano);
            $('.anos_'+ ano).removeAttr("disabled");
        }
        if(!this.checked) {
            $(".inmet").removeAttr("disabled");
            $(".trend").attr("disabled", true);
            $(".trend").prop("checked", false);
        }

    });

    $("a#make-chart").click(function(event) {
        event.preventDefault();
        let chuva_ids = $( ".chuva:checked" ).serializeArray();
        let temp_ids = $( ".temp:checked" ).serializeArray();
        let ar_ids = $( ".ar:checked" ).serializeArray();
        let trend_ids = $( ".trend:checked" ).serializeArray();

        if (chuva_ids.length >= 1 ) {

            chuva_ids.forEach(element => {
                if(parameters.length != 0){
                    parameters = parameters.concat("&");
                }
                parameters = parameters.concat(`chuva_id=${element.value}`);
            });
            //console.log(parameters);
        }

        if (temp_ids.length >= 1 ) {
            temp_ids.forEach(element => {
                if(parameters.length != 0){
                    parameters = parameters.concat("&");

                }
                parameters = parameters.concat(`temp_id=${element.value}`);
            });
        }

        if (ar_ids.length >= 1 ) {
            ar_ids.forEach(element => {
                if(parameters.length != 0){
                    parameters = parameters.concat("&");
                }
                parameters = parameters.concat(`ar_id=${element.value}`);
            });
        }

        if (trend_ids.length >= 1 ) {
            trend_ids.forEach(element => {
                if(parameters.length != 0){
                    parameters = parameters.concat("&");
                }
                parameters = parameters.concat(`trend_ids[]=${element.value}`);
            });
        }
        let is_checkedInmet = $( ".inmet:checked" ).serializeArray();
        let is_checkedTrend = $( ".trend:checked" ).serializeArray();

        console.log(is_checkedInmet);
        console.log(is_checkedTrend);

        if (is_checkedInmet.length >= 1 &&  is_checkedTrend.length >= 1) {
            var myWindow = window.open(`${urlCharts}?${parameters}`, "_blank", "width=800,height=600");
            parameters = "";
        }

    });
});

</script>


@endsection
