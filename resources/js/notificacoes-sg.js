window.$ = window.jQuery = require('jquery');
require('datatables.net');
require('datatables.net-bs4');
require('bootstrap-colorselector');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': xcsrftoken
    }
});

// Call the dataTables jQuery plugin
$(document).ready(function() {

  var table = $('#dataTable').DataTable({
      serverSide: true,
      processing: true,
      ajax:{
             "url": getindex,
             "dataType": "json",
             "type": "POST",
             "data":{ _token: token}
           },
      columns: [
          {data: 'NUM_REGIST', name: 'NUM_REGIST'},
          {data: 'DT_PREENC', name: 'DT_PREENC'},
          {data: 'NOM_BAIRRO', name: 'NOM_BAIRRO'},
          {data: 'NOM_LOGRAD', name: 'NOM_LOGRAD'},
          {data: 'NUM_LOGRAD', name: 'NUM_LOGRAD'},
          {data: 'ID_GEO1', name: 'ID_GEO1'},
          {data: 'ID_GEO2', name: 'ID_GEO2'},
          {data: 'checkbox', name: 'checkbox', searchable: false, orderable: false},
          {data: 'edit', name: 'edit', searchable: false, orderable: false}

      ],
      "language": {
          "url": mylanguage
      }

  });

  $('.button-gps').click(function() {
    spinner = $('.button-gps').find('span.spinner-border');
    fas = $('.button-gps').find('span.fas');
    spinner.removeClass('d-none');
    fas.addClass('d-none');
    button =
    $.ajax({
            url : getgps,
            type: "POST",
            dataType: "json",
            data : $("input[type=checkbox][name='checkboxs[]']:checked").serializeArray(),
            cache: false
          }).done(function(response){
                console.log(response);
          }).fail(function(response){
                console.log(response);
          }).always(function(response){
            spinner.addClass('d-none');
            fas.removeClass('d-none');
            table.ajax.reload(null, false);
          });
  });

  $('#checked-all').click(function() {
    $("input[type=checkbox][name='checkboxs[]']:checked").attr('checked', false);
    $("input[type=checkbox][name='checkboxs[]']").attr('checked', true);
    return false;
  });

});
