window.$ = window.jQuery = require("jquery");
require("datatables.net");
require("datatables.net-bs4");
var moment = require('moment');

$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": xcsrftoken
  },
  timeout: 600000
});

// Call the dataTables jQuery plugin
$(document).ready(function () {
  function postForm(seletor, callback) {
    $(seletor).submit(function (event) {
      event.preventDefault();
      button = $(event.target).find(".carregar");
      load = $(event.target).find(".loadCvs");
      button.addClass("d-none");
      load.addClass("d-flex justify-content-center");
      load.removeClass("d-none");
      var post_url = $(event.target).attr("action"); //get form action url
      var request_method = $(event.target).attr("method"); //get form GET/POST method
      var form_data = new FormData(event.target); //Encode form elements for submission

      $.ajax({
          url: post_url,
          type: request_method,
          data: form_data,
          timeout: 600000,
          contentType: false,
          processData: false,
          cache: false
        })
        .done(function (response) {
          console.log(response);
          if (typeof callback != "undefined") {
            callback();
          }
        })
        .fail(function (response) {
          console.log(response);
        })
        .always(function (response) {
          load.removeClass("d-flex justify-content-center");
          load.addClass("d-none");
          button.removeClass("d-none");
          $(".message").html(response);
        });
    });
  }

  var table = $("#dataTable").DataTable({
    serverSide: true,
    processing: true,
    ajax: {
      url: getindex,
      dataType: "json",
      type: "POST",
      data: {
        _token: token
      }
    },
    columns: [{
        data: "data",
        name: "data"
      },
      {
        data: "cidade_id",
        name: "cidade_id"
      },
      {
        data: "casos",
        name: "casos"
      },
      {
        data: "obitos",
        name: "obitos"
      },
      {
        data: "updated_at",
        name: "updated_at"
      }
    ],
    language: {
      url: mylanguage
    },
    drawCallback: function (settings) {}
  });

  postForm(".addForm", function () {
    table.ajax.reload(null, false);
  });

  $("#update-by-api").on('click', () => {

    /*
    current = new Date();
    d1 = new Date(lastUpdate);
    dm1 = moment('2020-11-10');
    console.log(dm1);
    */

    let current = moment(new Date());

    let dateLastUpdate = moment(lastUpdate);
    let duration = moment.duration(current.diff(dateLastUpdate));

    let dias = parseInt(duration.asDays());
    current.subtract(dias, 'days');

    for (let index = 0; index <= dias; index++) {
      current.add(1, 'days');
      console.log(current.format("YYYY-MM-DD"));
      getRegisterBrasilIO(current.format("YYYY-MM-DD"));

    }
    //getRegisterBrasilIO(current.day(-33).format("YYYY-MM-DD"));


  });

  async function getRegisterBrasilIO(date) {

    $.ajax({
        url: `${url_update_api}/?date=${date}`,
        type: 'GET',
        timeout: 600000,
        contentType: false,
        processData: false,
        cache: false
      })
      .done(function (response) {
        return {
          'date': this.data,
          'status': true
        }
      })
      .fail(function (response) {
        return {
          'date': this.data,
          'status': false
        }
      })
  }
});
