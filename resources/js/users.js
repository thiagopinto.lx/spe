window.$ = window.jQuery = require('jquery');
require('datatables.net');
require('datatables.net-bs4');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': xcsrftoken
    }
});

function deleteUser(seletor, callback){
    $(seletor).submit(function(event){
        event.preventDefault();
        var post_url = $(event.target ).attr("action"); //get form action url
        var request_method = $(event.target ).attr("method"); //get form GET/POST method
        var form_data = new FormData(event.target ); //Encode form elements for submission

          $.ajax({
            url : post_url,
            type: request_method,
            data : form_data,
            contentType: false,
            processData: false,
            cache: false
          }).done(function(response){
              console.log(response);
          }).fail(function(response){
              console.log(response);
          }).always(function(response){
            if (typeof(callback) !="undefined"){
              callback();
            }
          });
    });
  }

// Call the dataTables jQuery plugin
$(document).ready(function() {

  var table = $('#dataTable').DataTable({
      serverSide: true,
      processing: true,
      ajax:{
             "url": getindex,
             "dataType": "json",
             "type": "POST",
             "data":{ _token: token}
           },
      columns: [
          {data: 'id', name: 'id'},
          {data: 'name', name: 'name'},
          {data: 'email', name: 'email'},
          {data: 'delete', name: 'delete', searchable: false, orderable: false},
          {data: 'reset', name: 'reset', searchable: false, orderable: false}
      ],
      "language": {
          "url": mylanguage
      },
      "drawCallback": function(settings) {
        deleteUser('.deleteUser', function(){
          table.ajax.reload(null, false);
        });
      }

  }).on( 'draw.dt', function(){

  });

});
