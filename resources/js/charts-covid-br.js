window.$ = window.jQuery = require('jquery');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': xcsrftoken
    }
});

$(document).ready(function() {
    options = {
        series: [],
        chart: {
            height: 500,
            type: 'line',
            stacked: false,
        },
        annotations: {
            points: []
        },
        colors: [
            '#d63031',
            '#0984e3',
            '#00b894',
            '#fdcb6e',
            '#e84393',
            '#6c5ce7',
            '#00cec9',
            '#e17055',
            '#fd79a8',
            '#a29bfe',
            '#81ecec',
            '#fab1a0',
            '#ff7675',
            '#74b9ff',
            '#55efc4',
            '#ffeaa7'
        ],
        dataLabels: {
            enabled: false
        },
        stroke: {
            width: [],
            curve: 'smooth'
        },
        markers: {
            size: 2,
            strokeOpacity: 0.8,
            strokeWidth: 0.5,
            fillOpacity: 0.8
        },
        fill: {
        opacity: [],
            gradient: {
                inverseColors: false,
                shade: 'light',
                type: "vertical",
                opacityFrom: 0.85,
                opacityTo: 0.55,
                stops: [0, 100, 100, 100]
            }
        },
        yaxis: {
            labels: {
                formatter: function (value) {
                    return value.toFixed(0);
                }
            },
            title: {
                text: titlesY[0],
            },
        },
        xaxis: {
            type: "datetime",
            categories: categories,
            labels: {
                hideOverlappingLabels: true,
                showDuplicates: true,
                format: 'dd-MM',
                datetimeFormatter: {
                    year: 'yyyy',
                    month: 'MM-yyyy',
                    day: 'dd-MM',
                    hour: 'HH:mm'
                }
            }
        },
        title: {
          text: 'FMS | UFPI | CIATEN Atualizado em:'+latestUpdate,
          align: 'right',
          margin: 0,
          offsetX: 0,
          offsetY: 25,
          floating: false,
          style: {
            fontSize:  '10px',
            fontWeight:  'bold',
            fontFamily:  undefined,
            color:  '#858796'
          },
        },
        tooltip: {
            x: {
            format: 'dd/MM/yy'
            },
        }
    };
    chart = new ApexCharts(document.querySelector("#chart"), options);

    function getSeries (){
        chart.render();
        maxY = 0.001;
        let estados_ids = $( ".estado:checked" ).serializeArray();

        options.series = [];
        options.stroke.width = [];
        options.stroke.dashArray = [];
        options.fill.opacity = [];
        options.annotations.points = [];
        options.yaxis[0].labels.formatter = function (value) { return value.toFixed(0)};
        options.yaxis[0].title.text = titlesY[0];

        if(is_casos_dias) {
            options.yaxis[0].labels.formatter = function (value) { return value.toFixed(0)};
            options.yaxis[0].title.text = titlesY[1];
            url = urlCasosDias;
        } else if (is_medias_dias) {
            if (is_incidencia) {
                options.yaxis[0].labels.formatter = function (value) { return value.toFixed(3)};
                options.yaxis[0].title.text = titlesY[3];
                url = urlMediasIncidencia;
            } else {
                options.yaxis[0].labels.formatter = function (value) { return value.toFixed(3)};
                options.yaxis[0].title.text = titlesY[2];
                url = urlMediasDias;
            }
        } else {
            if (is_faixa_crecimento) {

                if (is_incidencia) {
                    options.yaxis[0].labels.formatter = function (value) { return value.toFixed(3)};
                    options.series = JSON.parse(JSON.stringify(indiceIncidencia));
                    options.yaxis[0].title.text = titlesY[4];
                    url = urlIncidencia;
                } else {
                    options.yaxis[0].labels.formatter = function (value) { return value.toFixed(2)};
                    options.series = JSON.parse(JSON.stringify(indiceAbsolutos));
                    url = urlSeries;
                }
                options.stroke.width = [1, 1, 1, 1, 1];
                options.stroke.dashArray = [1, 1, 1, 1, 1];
                options.fill.opacity = [0.01, 0.01, 0.01, 0.01, 0.01];
            } else {
                if (is_incidencia) {
                    options.yaxis[0].labels.formatter = function (value) { return value.toFixed(3)};
                    url = urlIncidencia;
                    options.yaxis[0].title.text = titlesY[4];
                } else {
                    options.yaxis[0].labels.formatter = function (value) { return value.toFixed(0)};
                    url = urlSeries;
                }
            }
        }

        if (estados_ids.length > 0) {
            for (let i = 0; i < estados_ids.length; i++) {
                $.ajax({
                    url : `${url}/${estados_ids[i].value}`,
                    contentType: 'application/x-www-form-urlencoded',
                    cache: false
                }).done(function(response){
                    numSerie = chart.getSeriesTotalXRange().length;
                    options.stroke.width.push(1);
                    options.fill.opacity.push(1);
                    options.series.push(response.serie);
                    let end = (response.serie.data.length - 1);
                    options.annotations.points.push({
                        x: new Date(response.serie.data[end].x).getTime(),
                        y: response.serie.data[end].y,
                        marker: {
                            size: 5,
                            strokeColor: '#FF4560',
                            fillColor: '#FF4560',
                            strokeOpacity: 0.5
                          },
                          label: {
                            offsetX: -25,
                            offsetY: 20,
                            style: {
                              fontSize: '10px',
                              color: '#333',
                              background: 'rgba(255, 255, 255, 0.5)',
                            },
                            text: response.serie.name
                        }
                    });
                    if(response.options.yaxis[0].max > maxY) {
                        options.yaxis[0].max = response.options.yaxis[0].max
                        maxY = response.options.yaxis[0].max;
                    }
                    chart.updateOptions(options);
                    //chart.appendSeries(response.serie);

                }).fail(function(response){
                    console.log(response);
                }).always(function(response){
                });

            }
        } else {
            options.series = [];
            options.stroke.width = [];
            options.stroke.dashArray = [];
            options.fill.opacity = [];

            if (!is_casos_dias) {
                if (is_faixa_crecimento) {
                    if (is_incidencia) {
                        options.series = JSON.parse(JSON.stringify(indiceIncidencia));
                    } else {
                        options.series = JSON.parse(JSON.stringify(indiceAbsolutos));
                    }
                    options.stroke.width = [1, 1, 1, 1, 1];
                    options.stroke.dashArray = [1, 1, 1, 1, 1];
                    options.fill.opacity = [0.01, 0.01, 0.01, 0.01, 0.01];
                }
            }

            chart.destroy();
            chart = new ApexCharts(document.querySelector("#chart"), options);
            chart.render();
        }
    }

    $(".estado").change(function(event) {
        chart.destroy();
        chart = new ApexCharts(document.querySelector("#chart"), options);
        getSeries();
    });

    $("#is-incidencia").change(function(event) {
        if ($('#is-incidencia').is(":checked"))
        {
            is_incidencia = true;
        } else {
            is_incidencia = false;
        }
        chart.destroy();
        chart = new ApexCharts(document.querySelector("#chart"), options);
        getSeries();
    });

    $("#is-faixa-crecimento").change(function(event) {
        if ($('#is-faixa-crecimento').is(":checked"))
        {
            is_faixa_crecimento = true;
        } else {
            is_faixa_crecimento = false;
        }
        chart.destroy();
        chart = new ApexCharts(document.querySelector("#chart"), options);
        getSeries();
    });

    $("#is-casos-dias").change(function(event) {
        if ($('#is-casos-dias').is(":checked"))
        {
            $('.is-incidencia').addClass('d-none');
            $('.is-faixa-crecimento').addClass('d-none');
            $('.is-medias-dias').addClass('d-none');
            is_casos_dias = true;
        } else {
            $('.is-incidencia').removeClass('d-none');
            $('.is-faixa-crecimento').removeClass('d-none');
            $('.is-medias-dias').removeClass('d-none');
            is_casos_dias = false;
        }
        chart.destroy();
        chart = new ApexCharts(document.querySelector("#chart"), options);
        getSeries();
    });

    $("#is-medias-dias").change(function(event) {
        if ($('#is-medias-dias').is(":checked"))
        {
            $('.is-faixa-crecimento').addClass('d-none');
            $('.is-casos-dias').addClass('d-none');
            is_medias_dias = true;
        } else {
            $('.is-faixa-crecimento').removeClass('d-none');
            $('.is-casos-dias').removeClass('d-none');
            is_medias_dias = false;
        }
        chart.destroy();
        chart = new ApexCharts(document.querySelector("#chart"), options);
        getSeries();
    });

    $('#limpar').click(function() {
        $("input[type=checkbox][name='estados[]']").prop('checked', false);
        $("input[type=checkbox][name='estados[]']").removeAttr('checked');
        // $(".estado:checked").attr('checked', false);
        //$(".estado").prop('checked', false);
        options.series = [];
        options.stroke.width = [];
        options.stroke.dashArray = [];
        options.fill.opacity = [];
        options.annotations.points = [];
        if (!is_casos_dias) {
            if (is_faixa_crecimento) {
                if (is_incidencia) {
                    options.series = JSON.parse(JSON.stringify(indiceIncidencia));
                } else {
                    options.series = JSON.parse(JSON.stringify(indiceAbsolutos));
                }
                options.stroke.width = [1, 1, 1, 1, 1];
                options.stroke.dashArray = [1, 1, 1, 1, 1];
                options.fill.opacity = [0.01, 0.01, 0.01, 0.01, 0.01];
            }
        }

        chart.destroy();
        chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
        return false;
    });

    getSeries();
});
