window.$ = window.jQuery = require('jquery');
require('datatables.net');
require('datatables.net-bs4');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': xcsrftoken
    }
});

// Call the dataTables jQuery plugin
$(document).ready(function() {

  function postForm(seletor, callback){
    $(seletor).submit(function(event){
        event.preventDefault();
        button = $(event.target).find(".carregar");
        load = $(event.target).find(".loadCvs");
        button.addClass("d-none");
        load.addClass("d-flex justify-content-center");
        load.removeClass("d-none");
        var post_url = $(event.target ).attr("action"); //get form action url
        var request_method = $(event.target ).attr("method"); //get form GET/POST method
        var form_data = new FormData(event.target ); //Encode form elements for submission

          $.ajax({
            url : post_url,
            type: request_method,
            data : form_data,
            contentType: false,
            processData: false,
            cache: false
          }).done(function(response){
                console.log(response);
                if (typeof(callback) !="undefined"){
                  callback();
                }
          }).fail(function(response){
                console.log(response);
          }).always(function(response){
            load.removeClass("d-flex justify-content-center");
            load.addClass("d-none");
            button.removeClass("d-none");
            $(".message").html(response);
          });
    });
  }

  var table = $('#dataTable').DataTable({
      serverSide: true,
      processing: true,
      ajax:{
             "url": getindex,
             "dataType": "json",
             "type": "POST",
             "data":{ _token: token}
           },
      columns: [
          {data: 'co_cnes', name: 'co_cnes'},
          {data: 'no_fantasia', name: 'no_fantasia'},
          {data: 'no_razao_social',   name: 'no_razao_social'},
          {data: 'nu_latitude',      name: 'nu_latitude'},
          {data: 'nu_longitude', name: 'nu_longitude'},
          {data: 'updated_at', name: 'updated_at'},
          {data: 'checkbox', name: 'checkbox', searchable: false, orderable: false},
          {data: 'edit', name: 'edit', searchable: false, orderable: false}
      ],
      "language": {
          "url": mylanguage
      }

  });

  postForm('.addForm', function(){
    table.ajax.reload(null, false);
  });

  $('.button-gps').click(function(event) {
    event.preventDefault();
    spinner = $('.button-gps').find('span.spinner-border');
    fas = $('.button-gps').find('span.fas');
    spinner.removeClass('d-none');
    fas.addClass('d-none');
    button =
    $.ajax({
            url : getgps,
            type: "POST",
            dataType: "json",
            data : $("input[type=checkbox][name='checkboxs[]']:checked").serializeArray(),
            cache: false
          }).done(function(response){
                console.log(response);
          }).fail(function(response){
                console.log(response);
          }).always(function(response){
            spinner.addClass('d-none');
            fas.removeClass('d-none');
            table.ajax.reload(null, false);
          });
  });

  $('#checked-all').click(function() {
    $("input[type=checkbox][name='checkboxs[]']:checked").attr('checked', false);
    $("input[type=checkbox][name='checkboxs[]']").attr('checked', true);
    return false;
  });

});
