window.$ = window.jQuery = require('jquery');
require('datatables.net');
require('datatables.net-bs4');
require('typeahead.js');
var Bloodhound = require('typeahead.js/dist/bloodhound');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': xcsrftoken
    }
});


var no_bairro = new Bloodhound({
  remote: {
      url: `${no_bairro_url}?q=%QUERY%`,
      wildcard: '%QUERY%'
  },
  datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
});

function initTypeahead(){
    $('.typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 1,
  }, {
      name: 'name',
      display: 'name',
      source: no_bairro,
      limit: 10,
      templates: {
        empty: [
            '<div class="list-group search-results-dropdown"><div class="list-group-item">Não encontrado.</div></div>'
        ],
        header: [
            '<h3 class="league-name text-danger">Nome Fantasia</h3><div class="list-group search-results-dropdown">'
        ],
        suggestion: function (data) {
            console.log(data);
            return '<a href="" onclick="return false;" class="list-group-item">' + data.name + '</a>'
        }
      }
  }).bind('typeahead:select', function(event, data){
    let idbairroAlias = $(event.target).parent().next().val();
    let idBairro = data.id;
    $.ajax({
      url : `${relationship_url}/relationship/${idbairroAlias}/${idBairro}`,
      type: "GET"
    }).done(function(response){
          console.log(response);
          if (typeof(callback) !="undefined"){
            callback();
          }
    }).fail(function(response){
          console.log(response);
    }).always(function(response){

    });
  });
}


// Call the dataTables jQuery plugin
$(document).ready(function() {

  var table = $('#dataTable').DataTable({
      serverSide: true,
      processing: true,
      ajax:{
             "url": getindex,
             "dataType": "json",
             "type": "POST",
             "data":{ _token: token}
           },
      columns: [
          {data: 'id', name: 'id'},
          {data: 'nome', name: 'nome'},
          {data: 'updated_at', name: 'updated_at'},
          {data: 'bairro', name: 'bairro', searchable: false, orderable: false},
          {data: 'checkbox', name: 'checkbox', searchable: false, orderable: false}
      ],
      "language": {
          "url": mylanguage
      },
      "drawCallback": function(settings) {
        initTypeahead();
      }

  });

  $('.button-gps').click(function() {
    spinner = $('.button-gps').find('span.spinner-border');
    fas = $('.button-gps').find('span.fas');
    spinner.removeClass('d-none');
    fas.addClass('d-none');
    button =
    $.ajax({
            url : getgps,
            type: "POST",
            dataType: "json",
            data : $("input[type=checkbox][name='checkboxs[]']:checked").serializeArray(),
            cache: false
          }).done(function(response){
                console.log(response);
          }).fail(function(response){
                console.log(response);
          }).always(function(response){
            spinner.addClass('d-none');
            fas.removeClass('d-none');
            table.ajax.reload(null, false);
          });
  });

  $('#checked-all').click(function() {
    $("input[type=checkbox][name='checkboxs[]']:checked").attr('checked', false);
    $("input[type=checkbox][name='checkboxs[]']").attr('checked', true);
    return false;
  });

});
