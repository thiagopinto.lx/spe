window.$ = window.jQuery = require("jquery");
require("datatables.net");
require("datatables.net-bs4");
var moment = require("moment");

$.ajaxSetup({
  headers: {
    "X-CSRF-TOKEN": xcsrftoken
  }
});

function getRegisterDataSus(initialDate, finalDate) {
  let iconNotLoading = $(".icon-not-loading");
  let textNotLoading = $(".text-not-loading");

  let spinnerLoading = $(".spinner-loading");
  let textLoading = $(".text-loading");

  $.ajax({
    url: `${urlUpdateNotificacoes}/${initialDate}/${finalDate}`,
    type: "GET",
    contentType: "application/x-www-form-urlencoded",
    cache: false,
    timeout: 6000000
  })
    .done(function(response) {
      console.log(response);
      if (typeof callback != "undefined") {
        callback();
      }
    })
    .fail(function(response) {
      console.log(response);
    })
    .always(function(response) {
      spinnerLoading.addClass("d-none");
      textLoading.addClass("d-none");

      iconNotLoading.removeClass("d-none");
      textNotLoading.removeClass("d-none");
      table.ajax.reload(null, false);
    });
}

// Call the dataTables jQuery plugin
$(document).ready(function() {
  var table = $("#dataTable").DataTable({
    serverSide: true,
    processing: true,
    ajax: {
      url: getindex,
      dataType: "json",
      type: "POST",
      data: { _token: token }
    },
    columns: [
      { data: "dataNotificacao", name: "dataNotificacao" },
      { data: "bairroalias_id", name: "bairroalias_id" },
      { data: "tipoTeste", name: "tipoTeste" },
      { data: "sintomas", name: "sintomas" },
      { data: "updated_at", name: "updated_at" }
    ],
    language: {
      url: mylanguage
    },
    drawCallback: function(settings) {}
  });

  $("button.get-datasus").click(event => {
    let iconNotLoading = $(".icon-not-loading");
    let textNotLoading = $(".text-not-loading");

    iconNotLoading.addClass("d-none");
    textNotLoading.addClass("d-none");

    let spinnerLoading = $(".spinner-loading");
    let textLoading = $(".text-loading");

    spinnerLoading.removeClass("d-none");
    textLoading.removeClass("d-none");

    //let date = $("#updateDate").val();

    let current = moment(new Date());

    let dateLastUpdate = moment($("#updateDate").val());

    let duration = moment.duration(current.diff(dateLastUpdate));

    let dias = parseInt(duration.asDays());
    current.subtract(dias, "days");

    for (let index = 0; index <= dias; index = index + 7) {
      console.log(current.format("YYYY-MM-DD"));
      getRegisterDataSus(
        current.format("YYYY-MM-DD"),
        current.add(7, "days").format("YYYY-MM-DD")
      );
    }

    //load = $(event.target).find(".loadHtml");
    //alert("oi");
  });
});
