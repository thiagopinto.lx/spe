<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMicroregioesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('microregioes', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->unsignedInteger('regiao_id')->nullable();
            $table->foreign('regiao_id')->references('id')->on('regioes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('microregioes');
    }
}
