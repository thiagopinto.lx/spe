<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estados', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('uf');
            $table->bigInteger('co_ibge');
            $table->bigInteger('populacao')->nullable();
            $table->string('nome_capital');
            $table->bigInteger('co_ibge_capital');
            $table->double('latitude_capital');
            $table->double('longitude_capital');
            $table->bigInteger('populacao_capital')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estados');
    }
}
