<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCovidDatasusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coviddatasus', function (Blueprint $table) {
            $table->id();
            $table->dateTime('dataNascimento')->nullable();
            $table->dateTime('dataNotificacao')->nullable();
            $table->dateTime('dataInicioSintomas')->nullable();
            $table->dateTime('dataTeste')->nullable();
            $table->string('_p_usuario')->nullable();
            $table->string('estrangeiro')->nullable();
            $table->string('profissionalSaude')->nullable();
            $table->string('profissionalSeguranca')->nullable();
            $table->string('cbo')->nullable();
            $table->string('cpf')->nullable();
            $table->string('cns')->nullable();
            $table->string('nomeCompleto')->nullable();
            $table->string('nomeMae')->nullable();
            $table->string('sexo')->nullable();
            $table->string('racaCor')->nullable();
            $table->string('passaporte')->nullable();
            $table->string('cep')->nullable();
            $table->string('logradouro')->nullable();
            $table->string('numero')->nullable();
            $table->string('complemento')->nullable();
            $table->string('bairro')->nullable();
            $table->unsignedInteger('bairroalias_id')->nullable();
            $table->string('estado')->nullable();
            $table->string('municipio')->nullable();
            $table->string('telefoneContato')->nullable();
            $table->string('telefone')->nullable();
            $table->string('sintomas')->nullable();
            $table->string('outrosSintomas')->nullable();
            $table->string('condicoes')->nullable();
            $table->string('estadoTeste')->nullable();
            $table->string('tipoTeste')->nullable();
            $table->string('resultadoTeste')->nullable();
            $table->string('numeroNotificacao')->nullable();
            $table->string('cnes')->nullable();
            $table->string('estadoNotificacao')->nullable();
            $table->string('municipioNotificacao')->nullable();
            $table->string('dataEncerramento')->nullable();
            $table->string('evolucaoCaso')->nullable();
            $table->string('classificacaoFinal')->nullable();
            $table->string('paisOrigem')->nullable();
            $table->string('origem')->nullable();
            $table->string('excluido')->nullable();
            $table->dateTime('_created_at_datasus')->nullable();
            $table->dateTime('_updated_at_datasus')->nullable();
            $table->boolean('desnormalizarNome')->nullable();
            $table->string('nomeCompletoDesnormalizado')->nullable();
            $table->string('estadoIBGE')->nullable();
            $table->string('estadoNotificacaoIBGE')->nullable();
            $table->string('municipioIBGE')->nullable();
            $table->boolean('municipioCapital')->nullable();
            $table->string('municipioNotificacaoIBGE')->nullable();
            $table->boolean('municipioNotificacaoCapital')->nullable();
            $table->string('source_id')->nullable();
            $table->string('idade')->nullable();
            $table->foreign('bairroalias_id')->references('id')->on('bairroalias');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coviddatasus');
    }
}
