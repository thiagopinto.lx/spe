<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstabelecimentoSiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estabelecimento_sies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_fantasia')->nullable();
            $table->unsignedInteger('estabelecimento_cnes_id')->nullable();
            $table->foreign('estabelecimento_cnes_id')->references('id')->on('estabelecimento_cnes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estabelecimento_sies');
    }
}
