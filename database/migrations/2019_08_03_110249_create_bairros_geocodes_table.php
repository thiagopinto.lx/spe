<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBairrosGeocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bairro_geocodes', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('bairro_id');
          $table->double('lat');
          $table->double('lng');
          $table->foreign('bairro_id')->references('id')->on('bairros');
          $table->timestamps();
        });
        DB::statement('CREATE EXTENSION IF NOT EXISTS unaccent');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bairros_geocodes');
    }
}
