<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraToCasoscovidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('casoscovid', function (Blueprint $table) {
            $table->unsignedInteger('estado_id')->nullable();
            $table->string('semana_epidemiologica')->nullable();
            $table->string('tipo_de_lugar')->nullable();
            $table->boolean('capital')->nullable();
            $table->string('estado')->nullable();
            $table->foreign('estado_id')->references('id')->on('estados');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('casoscovid', function (Blueprint $table) {
            $table->dropColumn(
                [
                    'estado_id',
                    'semana_epidemiologica',
                    'tipo_de_lugar',
                    'capital',
                    'estado'
                ]
            );
        });
    }
}
