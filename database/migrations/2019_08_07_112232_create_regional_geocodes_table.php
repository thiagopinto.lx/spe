<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegionalGeocodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regional_geocodes', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('regional_id');
          $table->double('lat');
          $table->double('lng');
          $table->foreign('regional_id')->references('id')->on('regionals');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regional_geocodes');
    }
}
