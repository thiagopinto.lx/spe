<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCasoscovidTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('casoscovid', function (Blueprint $table) {
            $table->id();
            $table->date('data');
            $table->unsignedInteger('cidade_id')->nullable();
            $table->integer('casos')->nullable();
            $table->integer('obitos')->nullable();
            $table->foreign('cidade_id')->references('id')->on('cidades');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('casoscovid');
    }
}
