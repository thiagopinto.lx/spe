<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cidades', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->bigInteger('co_ibge');
            $table->unsignedInteger('microregiao_id')->nullable();
            $table->unsignedInteger('regiao_id')->nullable();
            $table->foreign('regiao_id')->references('id')->on('regioes');
            $table->foreign('microregiao_id')->references('id')->on('microregioes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cidades');
    }
}
