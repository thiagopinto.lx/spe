<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCicloBairrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ciclo_bairros', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('ciclo_id');
            $table->unsignedInteger('bairro_id');
            $table->double('ipla');
            $table->foreign('ciclo_id')->references('id')->on('ciclos');
            $table->foreign('bairro_id')->references('id')->on('bairros');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ciclo_bairros');
    }
}
