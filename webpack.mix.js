const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/js/app.js', 'public/js').sourceMaps()
   .js('resources/js/site.js', 'public/js').sourceMaps()
   .js('resources/js/datatables.js', 'public/js')
   .js('resources/js/users.js', 'public/js')
   .js('resources/js/notificacoes.js', 'public/js')
   .js('resources/js/notificacoes-obito.js', 'public/js')
   .js('resources/js/notificacoes-sg.js', 'public/js')
   .js('resources/js/agravos.js', 'public/js')
   .js('resources/js/unidades-notificadoras.js', 'public/js')
   .js('resources/js/estabelecimento-cnes.js', 'public/js')
   .js('resources/js/load-sies.js', 'public/js')
   .js('resources/js/load-sipni.js', 'public/js')
   .js('resources/js/load-inmet.js', 'public/js')
   .js('resources/js/load-trend.js', 'public/js')
   .js('resources/js/estabelecimento-sies.js', 'public/js')
   .js('resources/js/imuno-sies.js', 'public/js')
   .js('resources/js/imuno-sipni.js', 'public/js')
   .js('resources/js/movimento-sies.js', 'public/js')
   .js('resources/js/doses-aplicadas-sipni.js', 'public/js')
   .js('resources/js/dados-inmet.js', 'public/js')
   .js('resources/js/dados-trend.js', 'public/js')
   .js('resources/js/cids.js', 'public/js')
   .js('resources/js/cidades.js', 'public/js')
   .js('resources/js/casoscovid.js', 'public/js')
   .js('resources/js/covid-datasus.js', 'public/js')
   .js('resources/js/bairro-alias.js', 'public/js')
   .js('resources/js/bairros.js', 'public/js')
   .js('resources/js/ciclos.js', 'public/js')
   .js('resources/js/ciclobairros.js', 'public/js')
   .js('resources/js/chartjs-chart-financial.js', 'public/js')
   .js('resources/js/charts-covid-br', 'public/js')
   .js('resources/js/charts-covid', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .styles('resources/css/site.css', 'public/css/site.css')
   .styles('node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css', 'public/css/dataTables.bootstrap4.css')
   .copy('resources/js/Portuguese-Brasil.json', 'public/js')
   .copy('node_modules/@fortawesome/fontawesome-free/webfonts/', 'public/fonts')
   .copy('node_modules/bootstrap4-paginator/build/bootstrap-paginator.min.js', 'public/js/bootstrap4-paginator.js')
   .copy('node_modules/bootstrap-colorselector/dist/bootstrap-colorselector.min.js', 'public/js/bootstrap-colorselector.js')
   .copy('resources/css/bootstrap-colorselector.css', 'public/css/bootstrap-colorselector.css')
   .copy('resources/css/checkbox-buttons.css', 'public/css/checkbox-buttons.css')
   .copy('node_modules/chart.js/dist/Chart.min.css', 'public/css/Chart.min.css')
   .copy('node_modules/chart.js/dist/Chart.min.js', 'public/js/Chart.min.js')
   .copy('node_modules/moment/min/moment.min.js', 'public/js/moment.min.js')
   .copy('node_modules/hammerjs/hammer.min.js', 'public/js/hammer.min.js')
   .copy('node_modules/hammerjs/hammer.min.js.map', 'public/js/hammer.min.js.map')
   .copy('node_modules/apexcharts/dist/apexcharts.min.js', 'public/js/apexcharts.min.js')
   .copy('node_modules/apexcharts/dist/apexcharts.css', 'public/css/apexcharts.css')
   .copy('node_modules/chartjs-plugin-zoom/chartjs-plugin-zoom.min.js', 'public/js/chartjs-plugin-zoom.min.js')
   .copy('resources/js/markerclusterer.js', 'public/js/markerclusterer.js')
   .copy('node_modules/typeahead.js/dist/typeahead.bundle.min.js', 'public/js/typeahead.min.js')
   .copy('node_modules/jquery-mask-plugin/dist/jquery.mask.min.js', 'public/js/jquery.mask.min.js')
   .copy('resources/fonts/icofont/fonts/', 'public/fonts/icofont/fonts')
   .copy('resources/fonts/icofont/icofont.min.css', 'public/fonts/icofont/icofont.min.css')
   .copy('node_modules/bootstrap-slider/dist/bootstrap-slider.min.js', 'public/js/bootstrap-slider.min.js')
   .copy('node_modules/bootstrap-slider/dist/css/bootstrap-slider.min.css', 'public/css/bootstrap-slider.min.css')
   .copy('node_modules/bootstrap-slider/dependencies/js/highlight.min.js', 'public/js/highlight.min.js')
   .copy('node_modules/bootstrap-slider/dependencies/css/highlightjs-github-theme.css', 'public/css/highlightjs-github-theme.css')
   .copy('node_modules/bootstrap-slider/dependencies/js/modernizr.js', 'public/js/modernizr.js')
   .version();
//.extract(['jquery', 'bootstrap', 'lodash', 'popper.js', 'vue'])
/*mix.autoload({
    jquery: ['$', 'window.jQuery']
});
*/
