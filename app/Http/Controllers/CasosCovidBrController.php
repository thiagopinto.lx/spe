<?php

namespace App\Http\Controllers;

use App\Models\CasosCovid;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Estado;

class CasosCovidBrController extends Controller
{
    public static $resouce = 'casoscovidbr';
    public static $resouceFullName = 'Casos de COVID-19 BR';
    public static $resouceShortName = 'COVID-19 BR';
    public static $routeBefore = ['index', 'obito', 'capitais', 'obitocapitais', 'map-evolucao'];
    public static $routeAfter = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = array();
        $casosDatas = CasosCovid::select('data')
            ->groupBy('data')
            ->orderBy('data')
            ->get();
        $piaui = CasosCovid::select(
            DB::raw('estado_id, estados.nome AS nome, SUM(casos) AS total')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state'],
                    ['estados.uf', 'PI']
                ]
            )
            ->join('estados', 'estado_id', '=', 'estados.id')
            ->groupBy('estado_id', 'estados.nome')
            ->orderByDesc('total')
            ->get();
        $estados = CasosCovid::select(
            DB::raw('estado_id, estados.nome AS nome, SUM(casos) AS total')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state'],
                    ['estados.uf', '<>', 'PI']
                ]
            )
            ->join('estados', 'estado_id', '=', 'estados.id')
            ->groupBy('estado_id', 'estados.nome')
            ->orderByDesc('total')
            ->get();

        $allItems = new \Illuminate\Database\Eloquent\Collection;
        foreach ($piaui as $pi) {
            $allItems->add($pi);
        }
        foreach ($estados as $estado) {
            $allItems->add($estado);
        }

        $latestcaso = CasosCovid::select('updated_at')->latest('updated_at')->first();
        $latestUpdate = date_format($latestcaso->updated_at, 'd-m-Y');

        $estados = $allItems;
        $totalEstados = count($estados);
        $limiteColunas = intdiv($totalEstados, 3);
        $maxCasosPorEstado = $estados[0]->total;

        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }
        $indiceAbsolutos = array();
        $indiceIncidencia = array();

        for ($i=5; $i < 26; $i=$i+5) {
            $indiceAbsolutos[] = CasosCovidBrController::indiceAbsoluto($i)->serie;
            $indiceIncidencia[] = CasosCovidBrController::indiceIncidencia($i)
                ->serie;

        }

        $options['titulo'] = self::$resouceFullName;
        $fullName = self::$resouceFullName;
        $options['area'] = 'index';
        $routeBefore = array_merge(self::$routeBefore);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('charts-covid-br', compact(
            'options',
            'fullName',
            'datas',
            'maxCasosPorEstado',
            'estados',
            'totalEstados',
            'limiteColunas',
            'indiceAbsolutos',
            'indiceIncidencia',
            'urls',
            'latestUpdate'
        ));
    }

    public function obito()
    {
        $datas = array();
        $casosDatas = CasosCovid::select('data')
            ->groupBy('data')
            ->orderBy('data')
            ->get();

        $piaui = CasosCovid::select(
            DB::raw('estado_id, estados.nome AS nome, SUM(obitos) AS total')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state'],
                    ['estados.uf', 'PI',]
                ]
            )
            ->join('estados', 'estado_id', '=', 'estados.id')
            ->groupBy('estado_id', 'estados.nome')
            ->orderByDesc('total')
            ->get();

        $estados = CasosCovid::select(
            DB::raw('estado_id, estados.nome AS nome, SUM(obitos) AS total')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state'],
                    ['estados.uf', '<>', 'PI']
                ]
            )
            ->join('estados', 'estado_id', '=', 'estados.id')
            ->groupBy('estado_id', 'estados.nome')
            ->orderByDesc('total')
            ->get();

        $allItems = new \Illuminate\Database\Eloquent\Collection;
        foreach ($piaui as $pi) {
            $allItems->add($pi);
        }
        foreach ($estados as $estado) {
            $allItems->add($estado);
        }

        $latestcaso = CasosCovid::select('updated_at')->latest('updated_at')->first();
        $latestUpdate = date_format($latestcaso->updated_at, 'd-m-Y');

        $estados = $allItems;
        $totalEstados = count($estados);
        $limiteColunas = intdiv($totalEstados, 3);
        $maxCasosPorEstado = $estados[0]->total;

        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }
        $indiceAbsolutos = array();
        $indiceIncidencia = array();

        for ($i=5; $i < 26; $i=$i+5) {
            $indiceAbsolutos[] = CasosCovidBrController::indiceAbsoluto($i)->serie;
            $indiceIncidencia[] = CasosCovidBrController::indiceIncidencia($i)
                ->serie;

        }

        $options['titulo'] = 'Óbitos por COVID-19 BR';
        $fullName = 'Óbitos por COVID-19 BR';
        $options['area'] = 'obito';
        $routeBefore = array_merge(self::$routeBefore);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('charts-covid-obitos-br', compact(
            'options',
            'fullName',
            'datas',
            'maxCasosPorEstado',
            'estados',
            'totalEstados',
            'limiteColunas',
            'indiceAbsolutos',
            'indiceIncidencia',
            'urls',
            'latestUpdate'
        ));
    }

    public function indiceAbsoluto($indice)
    {
        $datas = array();
        $casosCovidDatas = CasosCovid::select('data')
            ->groupBy('data')
            ->orderBy('data')
            ->get();
        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosCovidDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;
        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $serie = new \stdClass;
        $serie->name = "Faixa de crecimento {$indice}%";
        $serie->type = "area";
        $serie->data = array();
        $count = count($datas);
        for ($i = 0; $i < $count; $i++) {
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x  = $datas[$i];

            if ($i == 0) {
                $serie->data[$i]->y = 1;
            } else {
                $serie->data[$i]->y = round((($serie->data[$i - 1]->y / 100) * $indice) + $serie->data[$i - 1]->y, 3);
            }
        }

        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;
        return $response;
    }

    public function indiceIncidencia($indice)
    {
        $datas = array();
        $casosCovidDatas = CasosCovid::select('data')
            ->groupBy('data')
            ->orderBy('data')
            ->get();
        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosCovidDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;
        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $serie = new \stdClass;
        $serie->name = "Faixa de crecimento {$indice}%";
        $serie->type = "area";
        $serie->data = array();
        $count = count($datas);
        $populacao = Estado::sum('populacao');
        for ($i = 0; $i < $count; $i++) {
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x  = $datas[$i];

            if ($i == 0) {
                $serie->data[$i]->y = round((1/$populacao)*100000, 5);
            } else {
                $serie->data[$i]->y = round((($serie->data[$i - 1]->y / 100) * $indice) + $serie->data[$i - 1]->y, 5);
            }
        }

        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;
        return $response;
    }

    public function serieAbsoluta($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = $casos[$i]->sub;
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;


        return response()->json($response);
    }

    public function serieCasosDias($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        $maxCasosDia = 0;
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = $casos[$i]->casos;

            if ($maxCasosDia < $casos[$i]->casos) {
                $maxCasosDia = $casos[$i]->casos;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxCasosDia;


        return response()->json($response);
    }

    public function serieIncidencia($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = round(($casos[$i]->sub/$estado->populacao)*100000, 3);
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;


        return response()->json($response);
    }

    public function serieMediasDias($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        $maxMedias = 0;
        for ($i = 6; $i < $count; $i++) {
            $quant = 0;
            $total = 0;
            for ($j=$i; $j >= $i-6; $j--) {
                if ($casos[$j]->casos > 0) {
                    $quant++;
                    $total += $casos[$j]->casos;
                }
            }
            $serie->data[$i-6] = new \stdClass;
            $serie->data[$i-6]->x = $casos[$i]->data;
            if ($total > 0) {
                $serie->data[$i-6]->y = $total/7;
                if ($maxMedias < $serie->data[$i-6]->y) {
                    $maxMedias = $serie->data[$i-6]->y;
                }
            } else {
                $serie->data[$i-6]->y = 0;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxMedias;

        return response()->json($response);
    }

    public function serieObitoAbsoluta($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = $casos[$i]->sub;
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;


        return response()->json($response);
    }

    public function serieObitoIncidencia($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = round(($casos[$i]->sub/$estado->populacao)*100000, 3);
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;


        return response()->json($response);
    }

    public function serieObitosDias($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        $maxObitosDia = 0;
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = $casos[$i]->obitos;

            if ($maxObitosDia < $casos[$i]->obitos) {
                $maxObitosDia = $casos[$i]->obitos;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxObitosDia;

        return response()->json($response);
    }

    public function serieMediasObitosDias($id)
    {
        $estado = Estado::find($id);

        $obitos = $estado->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($obitos);
        $maxMedias = 0;
        for ($i = 6; $i < $count; $i++) {
            $quant = 0;
            $total = 0;
            for ($j=$i; $j >= $i-6; $j--) {
                if ($obitos[$j]->obitos > 0) {
                    $quant++;
                    $total += $obitos[$j]->obitos;
                }
            }
            $serie->data[$i-6] = new \stdClass;
            $serie->data[$i-6]->x = $obitos[$i]->data;
            if ($total > 0) {
                $serie->data[$i-6]->y = $total/7;
                if ($maxMedias < $serie->data[$i-6]->y) {
                    $maxMedias = $serie->data[$i-6]->y;
                }
            } else {
                $serie->data[$i-6]->y = 0;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxMedias;

        return response()->json($response);
    }

    public function evolucaoCasos($estadoId)
    {
        $casosDatas = CasosCovid::select('data')
            ->where(
                [
                    ['estado_id', $estadoId],
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data')
            ->orderBy('data')
            ->get();

        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $dataDates[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $estado = Estado::find($estadoId);

        $casos = $estado->casos()->select(
            DB::raw('data, casos, obitos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['estado_id', $estadoId],
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data', 'casos', 'obitos')->orderBy('data')->get();

        $dataChatArea = array();
        $count = count($casos);
        $yaxis1 = $casos[$count - 1]->sub;

        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $dataChatArea[$i] = new \stdClass;
            $dataChatArea[$i]->x = $casos[$i]->data;
            $dataChatArea[$i]->y = $casos[$i]->sub;
        }

        $maxCasos = 0;
        $dataChatBar = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $dataChatBar[$i] = new \stdClass;
            $dataChatBar[$i]->x = $casos[$i]->data;
            $dataChatBar[$i]->y = $casos[$i]->casos;
            if ($maxCasos < $casos[$i]->casos) {
                $maxCasos = $casos[$i]->casos;
            }
        }

        $yaxis0 = $maxCasos;
        $fullName = $estado->nome;
        $serieNameBar = "Casos";
        $serieNameArea = "Acumulado de casos";

        return view('chart-evolucao-covid',
            compact(
                'fullName',
                'dataDates',
                'dataChatBar',
                'dataChatArea',
                'serieNameBar',
                'serieNameArea',
                'yaxis0',
                'yaxis1'
            )
        );
    }

    public function evolucaoObitos($estadoId)
    {
        $casosDatas = CasosCovid::select('data')
            ->where(
                [
                    ['estado_id', $estadoId],
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data')
            ->orderBy('data')
            ->get();

        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $dataDates[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $estado = Estado::find($estadoId);

        $obitos = $estado->casos()->select(
            DB::raw('data, casos, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['estado_id', $estadoId],
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data', 'casos', 'obitos')->orderBy('data')->get();

        $dataChatArea = array();
        $count = count($obitos);
        $yaxis1 = $obitos[$count - 1]->sub;

        for ($i = 0; $i < $count; $i++) {
            $dataChatArea[$i] = new \stdClass;
            $dataChatArea[$i]->x = $obitos[$i]->data;
            $dataChatArea[$i]->y = $obitos[$i]->sub;
        }

        $maxObitos =0;
        $dataChatBar = array();
        $count = count($obitos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $dataChatBar[$i] = new \stdClass;
            $dataChatBar[$i]->x = $obitos[$i]->data;
            $dataChatBar[$i]->y = $obitos[$i]->obitos;
            if ($maxObitos < $obitos[$i]->obitos) {
                $maxObitos = $obitos[$i]->obitos;
            }
        }
        $yaxis0 = $maxObitos;
        $fullName = $estado->nome;
        $serieNameBar = "Óbitos";
        $serieNameArea = "Acumulado de òbitos";

        return view('chart-evolucao-covid',
            compact(
                'fullName',
                'dataDates',
                'dataChatBar',
                'dataChatArea',
                'serieNameBar',
                'serieNameArea',
                'yaxis0',
                'yaxis1'
            )
        );
    }

    public function capitais()
    {
        $datas = array();
        $casosDatas = CasosCovid::select('data')
            ->groupBy('data')
            ->orderBy('data')
            ->get();

        $piaui = CasosCovid::select(
            DB::raw('estado_id, estados.nome_capital AS nome, SUM(casos) AS total')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true],
                    ['estados.uf', 'PI']
                ]
            )
            ->join('estados', 'estado_id', '=', 'estados.id')
            ->groupBy('estado_id', 'estados.nome_capital')
            ->orderByDesc('total')
            ->get();

        $estados = CasosCovid::select(
            DB::raw('estado_id, estados.nome_capital AS nome, SUM(casos) AS total')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true],
                    ['estados.uf', '<>', 'PI']
                ]
            )
            ->join('estados', 'estado_id', '=', 'estados.id')
            ->groupBy('estado_id', 'estados.nome_capital')
            ->orderByDesc('total')
            ->get();

        $allItems = new \Illuminate\Database\Eloquent\Collection;
        foreach ($piaui as $pi) {
            $allItems->add($pi);
        }
        foreach ($estados as $estado) {
            $allItems->add($estado);
        }

        $latestcaso = CasosCovid::select('updated_at')->latest('updated_at')->first();
        $latestUpdate = date_format($latestcaso->updated_at, 'd-m-Y');

        $estados = $allItems;

        $totalEstados = count($estados);
        $limiteColunas = intdiv($totalEstados, 3);
        $maxCasosPorEstado = $estados[0]->total;

        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }
        $indiceAbsolutos = array();
        $indiceIncidencia = array();

        for ($i=5; $i < 26; $i=$i+5) {
            $indiceAbsolutos[] = CasosCovidBrController::indiceAbsoluto($i)->serie;
            $indiceIncidencia[] = CasosCovidBrController::indiceIncidencia($i)
            ->serie;

        }

        $options['titulo'] = self::$resouceFullName;
        $fullName = self::$resouceFullName;
        $options['area'] = 'capitais';
        $routeBefore = array_merge(self::$routeBefore);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('charts-covid-br-capital', compact(
            'options',
            'fullName',
            'datas',
            'maxCasosPorEstado',
            'estados',
            'totalEstados',
            'limiteColunas',
            'indiceAbsolutos',
            'indiceIncidencia',
            'urls',
            'latestUpdate'
        ));
    }

    public function serieAbsolutaCapitais($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome_capital;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = $casos[$i]->sub;
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;


        return response()->json($response);
    }

    public function serieCasosDiasCapitais($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome_capital;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        $maxCasosDia = 0;
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = $casos[$i]->casos;

            if ($maxCasosDia < $casos[$i]->casos) {
                $maxCasosDia = $casos[$i]->casos;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxCasosDia;


        return response()->json($response);
    }

    public function serieMediasDiasCapitais($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome_capital;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        $maxMedias = 0;
        for ($i = 6; $i < $count; $i++) {
            $quant = 0;
            $total = 0;
            for ($j=$i; $j >= $i-6; $j--) {
                if ($casos[$j]->casos > 0) {
                    $quant++;
                    $total += $casos[$j]->casos;
                }
            }
            $serie->data[$i-6] = new \stdClass;
            $serie->data[$i-6]->x = $casos[$i]->data;
            if ($total > 0) {
                $serie->data[$i-6]->y = $total/7;
                if ($maxMedias < $serie->data[$i-6]->y) {
                    $maxMedias = $serie->data[$i-6]->y;
                }
            } else {
                $serie->data[$i-6]->y = 0;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxMedias;

        return response()->json($response);
    }

    public function serieIncidenciaCapitais($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome_capital;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = round(($casos[$i]->sub/$estado->populacao_capital)*100000, 3);
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;


        return response()->json($response);
    }

    public function serieObitoAbsolutaCapitais($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome_capital;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = $casos[$i]->sub;
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;


        return response()->json($response);
    }

    public function serieObitosDiasCapitais($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome_capital;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        $maxObitosDia = 0;
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = $casos[$i]->obitos;

            if ($maxObitosDia < $casos[$i]->obitos) {
                $maxObitosDia = $casos[$i]->obitos;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxObitosDia;


        return response()->json($response);
    }

    public function serieMediasObitosDiasCapitais($id)
    {
        $estado = Estado::find($id);

        $obitos = $estado->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome_capital;
        $serie->type = "line";
        $serie->data = array();
        $count = count($obitos);
        $maxMedias = 0;
        for ($i = 6; $i < $count; $i++) {
            $quant = 0;
            $total = 0;
            for ($j=$i; $j >= $i-6; $j--) {
                if ($obitos[$j]->obitos > 0) {
                    $quant++;
                    $total += $obitos[$j]->obitos;
                }
            }
            $serie->data[$i-6] = new \stdClass;
            $serie->data[$i-6]->x = $obitos[$i]->data;
            if ($total > 0) {
                $serie->data[$i-6]->y = $total/7;
                if ($maxMedias < $serie->data[$i-6]->y) {
                    $maxMedias = $serie->data[$i-6]->y;
                }
            } else {
                $serie->data[$i-6]->y = 0;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxMedias;

        return response()->json($response);
    }

    public function serieObitoIncidenciaCapitais($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome_capital;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = round(($casos[$i]->sub/$estado->populacao_capital)*100000, 3);
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;


        return response()->json($response);
    }

    public function obitoCapitais()
    {
        $datas = array();
        $casosDatas = CasosCovid::select('data')
            ->groupBy('data')
            ->orderBy('data')
            ->get();
        $piaui = CasosCovid::select(
            DB::raw('estado_id, estados.nome_capital AS nome, SUM(obitos) AS total')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true],
                    ['estados.uf', 'PI']
                ]
            )
            ->join('estados', 'estado_id', '=', 'estados.id')
            ->groupBy('estado_id', 'estados.nome_capital')
            ->orderByDesc('total')
            ->get();

        $estados = CasosCovid::select(
            DB::raw('estado_id, estados.nome_capital AS nome, SUM(obitos) AS total')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true],
                    ['estados.uf', '<>', 'PI']
                ]
            )
            ->join('estados', 'estado_id', '=', 'estados.id')
            ->groupBy('estado_id', 'estados.nome_capital')
            ->orderByDesc('total')
            ->get();

        $allItems = new \Illuminate\Database\Eloquent\Collection;
        foreach ($piaui as $pi) {
            $allItems->add($pi);
        }
        foreach ($estados as $estado) {
            $allItems->add($estado);
        }

        $latestcaso = CasosCovid::select('updated_at')->latest('updated_at')->first();
        $latestUpdate = date_format($latestcaso->updated_at, 'd-m-Y');

        $estados = $allItems;

        $totalEstados = count($estados);
        $limiteColunas = intdiv($totalEstados, 3);
        $maxCasosPorEstado = $estados[0]->total;

        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }
        $indiceAbsolutos = array();
        $indiceIncidencia = array();

        for ($i=5; $i < 26; $i=$i+5) {
            $indiceAbsolutos[] = CasosCovidBrController::indiceAbsoluto($i)->serie;
            $indiceIncidencia[] = CasosCovidBrController::indiceIncidencia($i)
                ->serie;

        }

        $options['titulo'] = 'Óbitos por COVID-19 BR';
        $fullName = 'Óbitos por COVID-19 BR';
        $options['area'] = 'obitocapitais';
        $routeBefore = array_merge(self::$routeBefore);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('charts-covid-obitos-br-capital', compact(
            'options',
            'fullName',
            'datas',
            'maxCasosPorEstado',
            'estados',
            'totalEstados',
            'limiteColunas',
            'indiceAbsolutos',
            'indiceIncidencia',
            'urls',
            'latestUpdate'
        ));
    }

    public function evolucaoCapitaisCasos($estadoId)
    {
        $casosDatas = CasosCovid::select('data')
            ->where(
                [
                    ['estado_id', $estadoId],
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data')
            ->orderBy('data')
            ->get();

        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $dataDates[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $estado = Estado::find($estadoId);

        $casos = $estado->casos()->select(
            DB::raw('data, casos, obitos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['estado_id', $estadoId],
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data', 'casos', 'obitos')->orderBy('data')->get();

        $dataChatArea = array();
        $count = count($casos);
        $yaxis1 = $casos[$count - 1]->sub;

        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $dataChatArea[$i] = new \stdClass;
            $dataChatArea[$i]->x = $casos[$i]->data;
            $dataChatArea[$i]->y = $casos[$i]->sub;
        }

        $maxCasos = 0;
        $dataChatBar = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $dataChatBar[$i] = new \stdClass;
            $dataChatBar[$i]->x = $casos[$i]->data;
            $dataChatBar[$i]->y = $casos[$i]->casos;
            if ($maxCasos < $casos[$i]->casos) {
                $maxCasos = $casos[$i]->casos;
            }
        }

        $yaxis0 = $maxCasos;
        $fullName = $estado->nome_capital;
        $serieNameBar = "Casos";
        $serieNameArea = "Acumulado de casos";

        return view('chart-evolucao-covid',
            compact(
                'fullName',
                'dataDates',
                'dataChatBar',
                'dataChatArea',
                'serieNameBar',
                'serieNameArea',
                'yaxis0',
                'yaxis1'
            )
        );
    }

    public function evolucaoCapitaisObitos($estadoId)
    {
        $casosDatas = CasosCovid::select('data')
            ->where(
                [
                    ['estado_id', $estadoId],
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data')
            ->orderBy('data')
            ->get();

        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $dataDates[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $estado = Estado::find($estadoId);

        $obitos = $estado->casos()->select(
            DB::raw('data, casos, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['estado_id', $estadoId],
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data', 'casos', 'obitos')->orderBy('data')->get();

        $dataChatArea = array();
        $count = count($obitos);
        $yaxis1 = $obitos[$count - 1]->sub;

        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($obitos[$i]->data, $datas);
            $dataChatArea[$i] = new \stdClass;
            $dataChatArea[$i]->x = $obitos[$i]->data;
            $dataChatArea[$i]->y = $obitos[$i]->sub;
        }

        $maxObitos =0;
        $dataChatBar = array();
        $count = count($obitos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($obitos[$i]->data, $datas);
            $dataChatBar[$i] = new \stdClass;
            $dataChatBar[$i]->x = $obitos[$i]->data;
            $dataChatBar[$i]->y = $obitos[$i]->obitos;
            if ($maxObitos < $obitos[$i]->obitos) {
                $maxObitos = $obitos[$i]->obitos;
            }
        }
        $yaxis0 = $maxObitos;
        $fullName = $estado->nome_capital;
        $serieNameBar = "Óbitos";
        $serieNameArea = "Acumulado de òbitos";

        return view('chart-evolucao-covid',
            compact(
                'fullName',
                'dataDates',
                'dataChatBar',
                'dataChatArea',
                'serieNameBar',
                'serieNameArea',
                'yaxis0',
                'yaxis1'
            )
        );
    }
    /**
     * Undocumented function
     *
     * @author Thiago Pinto Dias <email@email.com>
     * @return view
     */
    public function mapEvolucao()
    {
        $casosDatas = CasosCovid::select('data')
            ->groupBy('data')
            ->orderBy('data')
            ->get();
        foreach ($casosDatas as  $value) {
            $datas[] = $value->data;
        }
        $estados = Estado::with('geocodes')->get();
        $options['titulo'] = 'Mapa evolução casos COVID-19 BR';
        $fullName = 'Mapa evolução casos COVID-19 BR';
        $options['area'] = 'map-evolucao';
        $routeBefore = array_merge(self::$routeBefore, ['map-data']);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('map-covid-br',
            compact(
                'datas',
                'estados',
                'options',
                'fullName',
                'urls'
            )
        );
    }

    /**
     * Undocumented function
     *
     * @author Thiago Pinto Dias <email@email.com>
     * @param [date] $data
     * @return json
     */
    public function mapData($data, $estadoId)
    {
        $estado = Estado::find($estadoId);

        $casos_dia = $estado->casos()->select('casos')->where(
            [
                ['tipo_de_lugar', 'state'],
                ['data', $data]
            ]
        )->first();

        $total_casos = $estado->casos()->select(DB::raw('SUM(casos) AS total'))->where(
            [
                ['tipo_de_lugar', 'state'],
                ['data', '<=', $data]
            ]
        )->first();
        if (isset($casos_dia['casos'])) {
            $estado->casos_dia = $casos_dia['casos'];
        } else {
            $estado->casos_dia = 0;
        }

        if (isset($total_casos['total'])) {
            $estado->total_casos = $total_casos['total'];
        } else {
            $estado->total_casos = 0;
        }

        return response()->json($estado);
    }

    public function serieMediasIncidencia($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        $maxMedias = 0;
        for ($i = 6; $i < $count; $i++) {
            $quant = 0;
            $total = 0;
            for ($j=$i; $j >= $i-6; $j--) {
                if ($casos[$j]->casos > 0) {
                    $quant++;
                    $total += $casos[$j]->casos;
                }
            }
            $serie->data[$i-6] = new \stdClass;
            $serie->data[$i-6]->x = $casos[$i]->data;
            if ($total > 0) {
                $serie->data[$i-6]->y = round((($total/7)/$estado->populacao)*100000, 3);
                if ($maxMedias < $serie->data[$i-6]->y) {
                    $maxMedias = $serie->data[$i-6]->y;
                }
            } else {
                $serie->data[$i-6]->y = 0;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxMedias;

        return response()->json($response);
    }


    public function serieMediasIncidenciaCapitais($id)
    {
        $estado = Estado::find($id);

        $casos = $estado->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome_capital;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        $maxMedias = 0;
        for ($i = 6; $i < $count; $i++) {
            $quant = 0;
            $total = 0;
            for ($j=$i; $j >= $i-6; $j--) {
                if ($casos[$j]->casos > 0) {
                    $quant++;
                    $total += $casos[$j]->casos;
                }
            }
            $serie->data[$i-6] = new \stdClass;
            $serie->data[$i-6]->x = $casos[$i]->data;
            if ($total > 0) {
                $serie->data[$i-6]->y = round((($total/7)/$estado->populacao_capital)*100000, 3);
                if ($maxMedias < $serie->data[$i-6]->y) {
                    $maxMedias = $serie->data[$i-6]->y;
                }
            } else {
                $serie->data[$i-6]->y = 0;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxMedias;

        return response()->json($response);
    }

    public function serieMediasObitosIncidenciaCapitais($id)
    {
        $estado = Estado::find($id);

        $obitos = $estado->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'city'],
                    ['capital', true]
                ]
            )
            ->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome_capital;
        $serie->type = "line";
        $serie->data = array();
        $count = count($obitos);
        $maxMedias = 0;
        for ($i = 6; $i < $count; $i++) {
            $quant = 0;
            $total = 0;
            for ($j=$i; $j >= $i-6; $j--) {
                if ($obitos[$j]->obitos > 0) {
                    $quant++;
                    $total += $obitos[$j]->obitos;
                }
            }
            $serie->data[$i-6] = new \stdClass;
            $serie->data[$i-6]->x = $obitos[$i]->data;
            if ($total > 0) {
                $serie->data[$i-6]->y = round((($total/7)/$estado->populacao_capital)*100000, 3);
                if ($maxMedias < $serie->data[$i-6]->y) {
                    $maxMedias = $serie->data[$i-6]->y;
                }
            } else {
                $serie->data[$i-6]->y = 0;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxMedias;

        return response()->json($response);
    }

    public function serieMediasObitosIncidencia($id)
    {
        $estado = Estado::find($id);

        $obitos = $estado->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )
            ->where(
                [
                    ['tipo_de_lugar', 'state']
                ]
            )
            ->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $estado->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($obitos);
        $maxMedias = 0;
        for ($i = 6; $i < $count; $i++) {
            $quant = 0;
            $total = 0;
            for ($j=$i; $j >= $i-6; $j--) {
                if ($obitos[$j]->obitos > 0) {
                    $quant++;
                    $total += $obitos[$j]->obitos;
                }
            }
            $serie->data[$i-6] = new \stdClass;
            $serie->data[$i-6]->x = $obitos[$i]->data;
            if ($total > 0) {
                $serie->data[$i-6]->y = round((($total/7)/$estado->populacao)*100000, 3);
                if ($maxMedias < $serie->data[$i-6]->y) {
                    $maxMedias = $serie->data[$i-6]->y;
                }
            } else {
                $serie->data[$i-6]->y = 0;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxMedias;

        return response()->json($response);
    }


}
