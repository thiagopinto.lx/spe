<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LoadInmet;
use App\Models\LoadTrend;
use App\Utilities\Helper;

class SindromeGripalController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $options['titulo'] = "Analise Sindrome Sripal";
        $loadTrends = array();

        $loadInmets = LoadInmet::orderBy('ano', 'desc')
            ->limit(5)
            ->get();

        $loadTrendTermos = LoadTrend::select('termo')
            ->groupBy('termo', 'ano')
            ->orderBy('ano', 'desc')
            ->limit(5)
            ->get();

        foreach ($loadTrendTermos as $loadTrendTermo) {
            $loadTrends[$loadTrendTermo->termo] = LoadTrend::where('termo', $loadTrendTermo->termo)
                ->orderBy('ano', 'desc')
                ->limit(5)
                ->get();
        }
        $urlCharts = route('sindrome-gripal.charts');
        //dd($loadTrends);
        return view('sindrome-gripal', compact('options', 'loadInmets', 'loadTrends', 'urlCharts'));
    }

    public function charts(Request $request)
    {
        $labels = array();
        $termoInmet = null;
        $typeChart = null;

        if ($request->has('chuva_id')) {
            $chuva_id = $request->get('chuva_id');
            $termoInmet = "precipitacao";
            $displayTermoInmet = "Precipitacao";

            $loadInmet = LoadInmet::find($chuva_id);
            $loadImmetColor = $loadInmet->color;
            $ano = $loadInmet->ano;
            $loadInmet->datas =
                DB::table('inmet_' . $ano)
                ->select('data', 'precipitacao')
                ->whereNotNull('precipitacao')
                ->orderBy('data')
                ->get();

            for ($j = 0; $j < count($loadInmet->datas); $j++) {
                $labels[$j] = $loadInmet->datas[$j]->data;
            }

            //dd($loadInmet->datas);
            //dd($labels);
            //dd($chuva_ids);
        }

        if ($request->has('temp_id')) {
            $temp_id = $request->get('temp_id');
            $termoInmet = "temperatura";
            $displayTermoInmet = "Temperatura";

            $loadInmet = LoadInmet::find($temp_id);
            $loadImmetColor = $loadInmet->color;

            $ano = $loadInmet->ano;
            $loadInmet->datas =
                DB::table('inmet_' . $ano)
                ->select(DB::raw('
                data,
                AVG(tempmaxima) AS maxima,
                AVG(tempminima) AS minima
                '))
                ->where(function ($q) {
                    $q->whereNotNull('tempmaxima');
                })
                ->orWhere(function ($q) {
                    $q->whereNotNull('tempminima');
                })
                ->groupBy('data')
                ->orderBy('data')
                ->get();

            for ($j = 0; $j < count($loadInmet->datas); $j++) {
                $labels[$j] = $loadInmet->datas[$j]->data;
            }

            //dd($loadInmet->datas);
            //dd($temp_ids);
        }

        if ($request->has('ar_id')) {
            $ar_id = $request->get('ar_id');
            $termoInmet = "umidade_relativa_media";
            $displayTermoInmet = "Umidade";

            $loadInmet = LoadInmet::find($ar_id);
            $loadImmetColor = $loadInmet->color;

            $ano = $loadInmet->ano;
            $loadInmet->datas =
                DB::table('inmet_' . $ano)
                ->select('data', 'umidade_relativa_media')
                ->whereNotNull('umidade_relativa_media')
                ->orderBy('data')
                ->get();

            for ($j = 0; $j < count($loadInmet->datas); $j++) {
                $labels[$j] = $loadInmet->datas[$j]->data;
            }

            //dd($loadInmet->datas);
            //dd($labels);
            //dd($chuva_ids);
        }

        if ($request->has('trend_ids')) {
            $trend_ids = $request->get('trend_ids');

            for ($i = 0; $i < count($trend_ids); $i++) {
                $loadTrends[$i] = LoadTrend::find($trend_ids[$i]);

                $loadTrends[$i]->datas = DB::table('trend_' . $ano)
                    ->select('data', 'ocorrencia')
                    ->where('load_trend_id', $loadTrends[$i]->id)
                    ->orderBy('data')
                    ->get();

                for ($j = 0; $j < count($loadTrends[$i]->datas); $j++) {
                    $datasTrends[$j] = $loadTrends[$i]->datas[$j]->data;
                }

                $labels = array_unique(array_merge($labels, $datasTrends));

                sort($labels);
            }

            $datasets = array();
            $yAxes = array();
            if (strcmp($termoInmet, "temperatura") == 0) {
                $datasets[0] = new \stdClass;
                $datasets[1] = new \stdClass;
                $datasets[0]->type = "line";
                $datasets[1]->type = "line";
                $datasets[0]->data = $labels;

                for ($i = 0; $i < count($datasets[0]->data); $i++) {
                    $datasets[0]->data[$i] = null;
                }

                $datasets[1]->data = $datasets[0]->data;

                $datasets[0]->label = "Temperatura Máxima";
                $datasets[1]->label = "Temperatura Mínima";

                for ($i = 0; $i < count($loadInmet->datas); $i++) {

                    $index = array_search($loadInmet->datas[$i]->data, $labels);

                    $datasets[0]->data[$index] = $loadInmet->datas[$i]->maxima;
                    $datasets[1]->data[$index] = $loadInmet->datas[$i]->minima;
                }
                $datasets[0]->backgroundColor = Helper::hex2rgba("#f44336", 0.5);
                $datasets[0]->borderColor = "#f44336";
                $datasets[0]->borderWidth = 2;
                $datasets[0]->spanGaps = true;
                $datasets[0]->lineTension = 0;
                $datasets[0]->pointRadius = 0.5;
                $datasets[0]->fill =  false;
                $datasets[0]->yAxisID = 'y-axis-0';

                $datasets[1]->backgroundColor = Helper::hex2rgba("#2196F3", 0.5);
                $datasets[1]->borderColor = "#2196F3";
                $datasets[1]->borderWidth = 2;
                $datasets[1]->spanGaps = true;
                $datasets[1]->lineTension = 0;
                $datasets[1]->pointRadius = 0.5;
                $datasets[1]->fill =  false;
                $datasets[1]->yAxisID = 'y-axis-0';
                //dd($datasets);
                $datasetsIndex = 2;
            } else {
                $datasets[0] = new \stdClass;
                $datasets[0]->type = "line";
                $datasets[0]->data = $labels;
                for ($i = 0; $i < count($datasets[0]->data); $i++) {
                    $datasets[0]->data[$i] = null;
                }
                $datasets[0]->label = $displayTermoInmet;

                for ($i = 0; $i < count($loadInmet->datas); $i++) {

                    $index = array_search($loadInmet->datas[$i]->data, $labels);

                    if ($loadInmet->datas[$i]->$termoInmet == 0) {
                        $datasets[0]->data[$index] = null;
                    } else {
                        $datasets[0]->data[$index] = $loadInmet->datas[$i]->$termoInmet;
                    }
                    $datasets[0]->fill = false;
                }
                $datasets[0]->backgroundColor = Helper::hex2rgba($loadInmet->color, 0.5);
                $datasets[0]->borderColor = $loadInmet->color;
                $datasets[0]->borderWidth = 2;
                $datasets[0]->spanGaps = true;
                $datasets[0]->lineTension = 0;
                $datasets[0]->fill =  false;
                $datasets[0]->yAxisID = 'y-axis-0';
                $datasetsIndex = 1;
                //dd($datasets);
            }


            $yAxes[0] = new \stdClass;
            $yAxes[0]->id = 'y-axis-0';
            $yAxes[0]->position = 'left';
            $yAxes[0]->ticks = new \stdClass;
            $yAxes[0]->ticks->beginAtZero = false;

            for ($i = 0; $i < count($loadTrends); $i++) {

                $l = $i + $datasetsIndex;
                $datasets[$l] = new \stdClass;
                $datasets[$l]->type = 'line';
                $datasets[$l]->label = $loadTrends[$i]->termo;

                $datasets[$l]->data = $labels;
                for ($x = 0; $x < count($datasets[$l]->data); $x++) {
                    $datasets[$l]->data[$x] = null;
                }

                for ($j = 0; $j < count($loadTrends[$i]->datas); $j++) {
                    $index = array_search($loadTrends[$i]->datas[$j]->data, $labels);

                    if ($loadTrends[$i]->datas[$j]->ocorrencia == null) {
                        $datasets[$l]->data[$index] = null;
                    } else {
                        $datasets[$l]->data[$index] = $loadTrends[$i]->datas[$j]->ocorrencia;
                    }
                }

                $datasets[$l]->backgroundColor = $loadTrends[$i]->color;
                $datasets[$l]->borderColor = $loadTrends[$i]->color;
                $datasets[$l]->borderWidth = 2;
                //$datasets[$l]->pointRadius = 0;
                $datasets[$l]->spanGaps = true;
                $datasets[$l]->lineTension = 0;
                $datasets[$l]->fill =  false;
                $datasets[$l]->yAxisID = "y-axis-1";

                $yAxes[1] = new \stdClass;
                $yAxes[1]->id = "y-axis-1";
                $yAxes[1]->position = 'right';
                $yAxes[1]->ticks = new \stdClass;
                $yAxes[1]->ticks->beginAtZero = false;
                $yAxes[1]->gridLines = new \stdClass;
                $yAxes[1]->gridLines->drawBorder = false;
                $yAxes[1]->scaleLabel = new \stdClass;
                $yAxes[1]->scaleLabel->display = true;
            }
        }

        //dd($arrayDataInmet);


        return view('chart-sindrome-gripal', compact('labels', 'datasets', 'yAxes'));
    }
}
