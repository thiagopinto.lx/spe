<?php

namespace App\Http\Controllers;

use App\Models\CasosCovid;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Cidade;
use App\Models\Estado;

class CasosCovidController extends Controller
{
    public static $resouce = 'casoscovid';
    public static $resouceFullName = 'Casos de COVID-19 PI';
    public static $resouceShortName = 'COVID-19 PI';
    public static $routeBefore = ['index', 'obito', 'map-evolucao'];
    public static $routeAfter = [];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = array();
        $estado = Estado::where('uf', 'PI')->first();
        $casosDatas = CasosCovid::select('data')
            ->groupBy('data')
            ->orderBy('data')
            ->get();
        $cidades = CasosCovid::select(
            DB::raw(
                'cidade_id, cidades.nome AS nome, SUM(casos) AS total,
                		( 
                    select avg(casos) avg_casos 
                      from 
                      ( select casos 
                        from 
                        "casoscovid" as "lest_casos" 
                        where "lest_casos"."cidade_id" = "casoscovid"."cidade_id"
                        order by "lest_casos"."data" desc 
                        limit 7
                      ) as lest_casos
                      
                    ) as avg'
            )
        )
            ->join('cidades', 'cidade_id', '=', 'cidades.id')
            ->where('cidades.estado_id', $estado->id)
            ->groupBy('cidade_id', 'cidades.nome')
            ->orderByDesc('avg')
            ->get();

        /*        foreach ($cidades as $cidade) {
                   $lastCases = $cidade->orderByDesc('data')->limit(7)->get();
                   $quantCases = count($lastCases);
                   $media = 0;
                   $soma = 0;
                   for ($i=0; $i < $quantCases; $i++) {
                       $soma += $lastCases[$i]->casos;
                   }
                   if ($soma > 0) {
                       $media = $soma/$quantCases;
                   } else {
                       $media = $soma;
                   }
                   $cidade->media = $media;
               }

               $cidades = $cidades->sortByDesc('media'); */
        //dd($cidades[0]);
        /*         usort(
                    $cidades,
                    function ($a, $b) {
                        if ($a['media'] == $b['media']) {
                            return 0;
                        }
                        return (($a['media'] > $b['media']) ? -1 : 1);
                    }
                );
         */

        $totalCidades = count($cidades);
        $limiteColunas = intdiv($totalCidades, 3);
        $maxCasosPorCidade = $cidades[0]->total;

        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }
        $indiceAbsolutos = array();
        $indiceIncidencia = array();

        for ($i=5; $i < 26; $i=$i+5) {
            $indiceAbsolutos[] = CasosCovidController::indiceAbsoluto($i)->serie;
            $indiceIncidencia[] = CasosCovidController::indiceIncidencia($i, 'PI')
                ->serie;
        }

        $latestcaso = CasosCovid::select('updated_at')->latest('updated_at')->first();
        $latestUpdate = date_format($latestcaso->updated_at, 'd-m-Y');

        $options['titulo'] = self::$resouceFullName;
        $fullName = self::$resouceFullName;
        $options['area'] = 'index';
        $routeBefore = array_merge(self::$routeBefore);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('charts-covid', compact(
            'options',
            'fullName',
            'datas',
            'maxCasosPorCidade',
            'cidades',
            'totalCidades',
            'limiteColunas',
            'indiceAbsolutos',
            'indiceIncidencia',
            'urls',
            'latestUpdate'
        ));
    }

    public function obito()
    {
        $datas = array();
        $estado = Estado::where('uf', 'PI')->first();
        $casosDatas = CasosCovid::select('data')
            ->groupBy('data')
            ->orderBy('data')
            ->get();
        $cidades = CasosCovid::select(
            DB::raw('cidade_id, cidades.nome AS nome, SUM(obitos) AS total')
        )
            ->join('cidades', 'cidade_id', '=', 'cidades.id')
            ->where('cidades.estado_id', $estado->id)
            ->groupBy('cidade_id', 'cidades.nome')
            ->orderByDesc('total')
            ->get();

        $totalCidades = count($cidades);
        $limiteColunas = intdiv($totalCidades, 3);
        $maxCasosPorCidade = $cidades[0]->total;

        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }
        $indiceAbsolutos = array();
        $indiceIncidencia = array();

        for ($i=5; $i < 26; $i=$i+5) {
            $indiceAbsolutos[] = CasosCovidController::indiceAbsoluto($i)->serie;
            $indiceIncidencia[] = CasosCovidController::indiceIncidencia($i, 'PI')
                ->serie;
        }

        $latestcaso = CasosCovid::select('updated_at')->latest('updated_at')->first();
        $latestUpdate = date_format($latestcaso->updated_at, 'd-m-Y');

        $options['titulo'] = 'Óbitos por COVID-19 PI';
        $fullName = 'Óbitos por COVID-19 PI';
        $options['area'] = 'obito';
        $routeBefore = array_merge(self::$routeBefore);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('charts-covid-obitos', compact(
            'options',
            'fullName',
            'datas',
            'maxCasosPorCidade',
            'cidades',
            'totalCidades',
            'limiteColunas',
            'indiceAbsolutos',
            'indiceIncidencia',
            'urls',
            'latestUpdate'
        ));
    }

    public function indiceAbsoluto($indice)
    {
        $datas = array();
        $casosCovidDatas = CasosCovid::select('data')
            ->groupBy('data')
            ->orderBy('data')
            ->get();
        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosCovidDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;
        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $serie = new \stdClass;
        $serie->name = "Faixa de crecimento {$indice}%";
        $serie->type = "area";
        $serie->data = array();
        $count = count($datas);
        for ($i = 0; $i < $count; $i++) {
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x  = $datas[$i];

            if ($i == 0) {
                $serie->data[$i]->y = 1;
            } else {
                $serie->data[$i]->y = round((($serie->data[$i - 1]->y / 100) * $indice) + $serie->data[$i - 1]->y, 3);
            }
        }

        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;
        return $response;
    }

    public function indiceIncidencia($indice, $uf)
    {
        $datas = array();
        $casosCovidDatas = CasosCovid::select('data')
            ->groupBy('data')
            ->orderBy('data')
            ->get();
        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosCovidDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;
        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $serie = new \stdClass;
        $serie->name = "Faixa de crecimento {$indice}%";
        $serie->type = "area";
        $serie->data = array();
        $count = count($datas);
        $estado = Estado::where('uf', $uf)->first();
        for ($i = 0; $i < $count; $i++) {
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x  = $datas[$i];

            if ($i == 0) {
                $serie->data[$i]->y = round((1/$estado->populacao)*100000, 3);
            } else {
                $serie->data[$i]->y = round((($serie->data[$i - 1]->y / 100) * $indice) + $serie->data[$i - 1]->y, 3);
            }
        }

        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;
        return $response;
    }

    public function serieAbsoluta($id)
    {
        $cidade = Cidade::find($id);

        $casos = $cidade->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $cidade->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = $casos[$i]->sub;
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;


        return response()->json($response);
    }

    public function serieCasosDias($id)
    {
        $cidade = Cidade::find($id);

        $casos = $cidade->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $cidade->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        $maxCasos = 0;
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = $casos[$i]->casos;

            if ($maxCasos < $casos[$i]->casos) {
                $maxCasos = $casos[$i]->casos;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxCasos;


        return response()->json($response);
    }

    public function serieMediasDias($id)
    {
        $cidade = Cidade::find($id);

        $casos = $cidade->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $cidade->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        $maxMedias = 0;
        for ($i = 6; $i < $count; $i++) {
            $quant = 0;
            $total = 0;
            for ($j=$i; $j >= $i-6; $j--) {
                if ($casos[$j]->casos > 0) {
                    $quant++;
                    $total += $casos[$j]->casos;
                }
            }
            $serie->data[$i-6] = new \stdClass;
            $serie->data[$i-6]->x = $casos[$i]->data;
            if ($total > 0) {
                $serie->data[$i-6]->y = $total/7;
                if ($maxMedias < $serie->data[$i-6]->y) {
                    $maxMedias = $serie->data[$i-6]->y;
                }
            } else {
                $serie->data[$i-6]->y = 0;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxMedias;

        return response()->json($response);
    }

    public function serieIncidencia($id)
    {
        $cidade = Cidade::find($id);

        $casos = $cidade->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $cidade->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = round(($casos[$i]->sub/$cidade->populacao)*100000, 3);
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;


        return response()->json($response);
    }

    public function serieObitoAbsoluta($id)
    {
        $cidade = Cidade::find($id);

        $casos = $cidade->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $cidade->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = $casos[$i]->sub;
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;


        return response()->json($response);
    }

    public function serieObitoDias($id)
    {
        $cidade = Cidade::find($id);

        $casos = $cidade->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $cidade->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        $maxObitos = 0;
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = $casos[$i]->obitos;

            if ($maxObitos < $casos[$i]->obitos) {
                $maxObitos = $casos[$i]->obitos;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxObitos;


        return response()->json($response);
    }

    public function serieMediasObitosDias($id)
    {
        $cidade = Cidade::find($id);

        $obitos = $cidade->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $cidade->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($obitos);
        $maxMedias = 0;
        for ($i = 6; $i < $count; $i++) {
            $quant = 0;
            $total = 0;
            for ($j=$i; $j >= $i-6; $j--) {
                if ($obitos[$j]->obitos > 0) {
                    $quant++;
                    $total += $obitos[$j]->obitos;
                }
            }
            $serie->data[$i-6] = new \stdClass;
            $serie->data[$i-6]->x = $obitos[$i]->data;
            if ($total > 0) {
                $serie->data[$i-6]->y = $total/7;
                if ($maxMedias < $serie->data[$i-6]->y) {
                    $maxMedias = $serie->data[$i-6]->y;
                }
            } else {
                $serie->data[$i-6]->y = 0;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxMedias;

        return response()->json($response);
    }

    public function serieObitoIncidencia($id)
    {
        $cidade = Cidade::find($id);

        $casos = $cidade->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $cidade->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $serie->data[$i] = new \stdClass;
            $serie->data[$i]->x = $casos[$i]->data;
            $serie->data[$i]->y = round(($casos[$i]->sub/$cidade->populacao)*100000, 3);
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $serie->data[$count - 1]->y;


        return response()->json($response);
    }

    public function evolucaoCasos($cidadeId)
    {
        $casosDatas = CasosCovid::select('data')
            ->where('cidade_id', $cidadeId)
            ->groupBy('data')
            ->orderBy('data')
            ->get();

        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $dataDates[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $cidade = Cidade::find($cidadeId);

        $casos = $cidade->casos()->select(
            DB::raw('data, casos, obitos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )->groupBy('data', 'casos', 'obitos')->orderBy('data')->get();

        $dataChatArea = array();
        $count = count($casos);
        $yaxis1 = $casos[$count - 1]->sub;

        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $dataChatArea[$i] = new \stdClass;
            $dataChatArea[$i]->x = $casos[$i]->data;
            $dataChatArea[$i]->y = $casos[$i]->sub;
        }

        $maxCasos = 0;
        $dataChatBar = array();
        $count = count($casos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $dataChatBar[$i] = new \stdClass;
            $dataChatBar[$i]->x = $casos[$i]->data;
            $dataChatBar[$i]->y = $casos[$i]->casos;
            if ($maxCasos < $casos[$i]->casos) {
                $maxCasos = $casos[$i]->casos;
            }
        }

        $yaxis0 = $maxCasos;
        $fullName = $cidade->nome;
        $serieNameBar = "Casos";
        $serieNameArea = "Acumulado de casos";

        return view(
            'chart-evolucao-covid',
            compact(
                'fullName',
                'dataDates',
                'dataChatBar',
                'dataChatArea',
                'serieNameBar',
                'serieNameArea',
                'yaxis0',
                'yaxis1'
            )
        );
    }

    public function evolucaoObitos($cidadeId)
    {
        $casosDatas = CasosCovid::select('data')
            ->where('cidade_id', $cidadeId)
            ->groupBy('data')
            ->orderBy('data')
            ->get();

        $dataInitial = \DateTime::createFromFormat('Y-m-d', $casosDatas[0]->data);
        $dataFinal = new \DateTime();

        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $dataDates[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $cidade = Cidade::find($cidadeId);

        $obitos = $cidade->casos()->select(
            DB::raw('data, casos, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )->groupBy('data', 'casos', 'obitos')->orderBy('data')->get();

        $dataChatArea = array();
        $count = count($obitos);
        $yaxis1 = $obitos[$count - 1]->sub;

        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($casos[$i]->data, $datas);
            $dataChatArea[$i] = new \stdClass;
            $dataChatArea[$i]->x = $obitos[$i]->data;
            $dataChatArea[$i]->y = $obitos[$i]->sub;
        }

        $maxObitos =0;
        $dataChatBar = array();
        $count = count($obitos);
        for ($i = 0; $i < $count; $i++) {
            //$index = array_search($obitos[$i]->data, $datas);
            $dataChatBar[$i] = new \stdClass;
            $dataChatBar[$i]->x = $obitos[$i]->data;
            $dataChatBar[$i]->y = $obitos[$i]->obitos;
            if ($maxObitos < $obitos[$i]->obitos) {
                $maxObitos = $obitos[$i]->obitos;
            }
        }
        $yaxis0 = $maxObitos;
        $fullName = $cidade->nome;
        $serieNameBar = "Óbitos";
        $serieNameArea = "Acumulado de óbitos";


        return view(
            'chart-evolucao-covid',
            compact(
                'fullName',
                'dataDates',
                'dataChatBar',
                'dataChatArea',
                'serieNameBar',
                'serieNameArea',
                'yaxis0',
                'yaxis1'
            )
        );
    }

    /**
     * Undocumented function
     *
     * @author Thiago Pinto Dias <email@email.com>
     * @return view
     */
    public function mapEvolucao()
    {
        $casosDatas = CasosCovid::select('data')
            ->groupBy('data')
            ->orderBy('data')
            ->get();
        foreach ($casosDatas as  $value) {
            $datas[] = $value->data;
        }
        $estado = Estado::where('uf', 'PI')->with('geocodes')->first();

        $cidades = Cidade::select('id', 'co_ibge', 'nome', 'populacao')
            ->where('estado_id', $estado->id)->with('geocodes')->get();

        $options['titulo'] = 'Mapa evolução casos COVID-19 BR';
        $fullName = 'Mapa evolução casos COVID-19 BR';
        $options['area'] = 'map-evolucao';
        $routeBefore = array_merge(self::$routeBefore, ['map-data']);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view(
            'map-covid-pi',
            compact(
                'datas',
                'estado',
                'cidades',
                'options',
                'fullName',
                'urls'
            )
        );
    }

    /**
     * Undocumented function
     *
     * @author Thiago Pinto Dias <email@email.com>
     * @param [date] $data
     * @return json
     */
    public function mapData($data, $cidadeId)
    {
        $cidade = Cidade::select('id', 'nome', 'populacao')->find($cidadeId);

        $casos_dia = $cidade->casos()->select('casos')->where(
            [
                ['tipo_de_lugar', 'city'],
                ['data', $data]
            ]
        )->first();

        $total_casos = $cidade->casos()->select(DB::raw('SUM(casos) AS total'))->where(
            [
                ['tipo_de_lugar', 'city'],
                ['data', '<=', $data]
            ]
        )->first();

        if (isset($casos_dia['casos'])) {
            $cidade->casos_dia = $casos_dia['casos'];
        } else {
            $cidade->casos_dia = 0;
        }

        if (isset($total_casos['total'])) {
            $cidade->total_casos = $total_casos['total'];
        } else {
            $cidade->total_casos = 0;
        }

        return response()->json($cidade);
    }

    public function serieMediasIncidencia($id)
    {
        $cidade = Cidade::find($id);

        $casos = $cidade->casos()->select(
            DB::raw('data, casos, SUM(casos) OVER (ORDER BY "data") AS sub')
        )->groupBy('data', 'casos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $cidade->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($casos);
        $maxMedias = 0;
        for ($i = 6; $i < $count; $i++) {
            $quant = 0;
            $total = 0;
            for ($j=$i; $j >= $i-6; $j--) {
                if ($casos[$j]->casos > 0) {
                    $quant++;
                    $total += $casos[$j]->casos;
                }
            }
            $serie->data[$i-6] = new \stdClass;
            $serie->data[$i-6]->x = $casos[$i]->data;
            if ($total > 0) {
                $serie->data[$i-6]->y = round((($total/7)/$cidade->populacao)*100000, 3);
                if ($maxMedias < $serie->data[$i-6]->y) {
                    $maxMedias = $serie->data[$i-6]->y;
                }
            } else {
                $serie->data[$i-6]->y = 0;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxMedias;

        return response()->json($response);
    }

    public function serieMediasObitosIncidencias($id)
    {
        $cidade = Cidade::find($id);

        $obitos = $cidade->casos()->select(
            DB::raw('data, obitos, SUM(obitos) OVER (ORDER BY "data") AS sub')
        )->groupBy('data', 'obitos')->orderBy('data')->get();

        $serie = new \stdClass;
        $serie->name = $cidade->nome;
        $serie->type = "line";
        $serie->data = array();
        $count = count($obitos);
        $maxMedias = 0;
        for ($i = 6; $i < $count; $i++) {
            $quant = 0;
            $total = 0;
            for ($j=$i; $j >= $i-6; $j--) {
                if ($obitos[$j]->obitos > 0) {
                    $quant++;
                    $total += $obitos[$j]->obitos;
                }
            }
            $serie->data[$i-6] = new \stdClass;
            $serie->data[$i-6]->x = $obitos[$i]->data;
            if ($total > 0) {
                $serie->data[$i-6]->y = round((($total/7)/$cidade->populacao)*100000, 3);
                if ($maxMedias < $serie->data[$i-6]->y) {
                    $maxMedias = $serie->data[$i-6]->y;
                }
            } else {
                $serie->data[$i-6]->y = 0;
            }
        }
        $response = new \stdClass;
        $response->serie = $serie;
        $response->options = new \stdClass;
        $response->options->yaxis[0] = new \stdClass;
        $response->options->yaxis[0]->max = $maxMedias;

        return response()->json($response);
    }
}
