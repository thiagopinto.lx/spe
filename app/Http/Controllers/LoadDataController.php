<?php

namespace App\Http\Controllers;

use App\Model\LoadDataModel;
use Illuminate\Http\Request;

class LoadDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LoadData  $loadData
     * @return \Illuminate\Http\Response
     */
    public function show(LoadData $loadData)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LoadData  $loadData
     * @return \Illuminate\Http\Response
     */
    public function edit(LoadData $loadData)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LoadData  $loadData
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LoadData $loadData)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LoadData  $loadData
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoadData $loadData)
    {
        //
    }
}
