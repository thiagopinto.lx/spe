<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LoadData;
use App\Models\Dengue;
use App\Utilities\Helper;

class ObitoController extends Controller
{
    public static $resouce = 'obito';
    public static $resouceFullName = 'Obito';
    public static $resouceShortName = 'Obito';
    public static $routeAfter = ['heatmap'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loadDatas = LoadData::where('table_name', self::$resouce)
        ->orderBy('ano', 'desc')
        ->limit(5)
        ->get();
        $newLoadDatas = array();
        $soma = 0;
        $countDatas = count($loadDatas);
        foreach($loadDatas as $loadData){
            $count = DB::table($loadData->table_name.$loadData->ano)->whereNull('IDADE')
            ->orwhere('IDADE', '<' ,'400')->count();
            $loadData->count = $count;
            $loadData->tabela = $loadData->table_name.$loadData->ano;
            $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
            $newLoadDatas[] = $loadData;
            $soma = $soma+$count;
        }
        if($soma > 0 && $countDatas >0){
            $total = new \stdClass;
            $media = $soma/$countDatas;
            $total->titulo = "Média".self::$resouceShortName;
            $total->media = $media;
            $total->color = '#FF0000';
        }

        $options['titulo'] = self::$resouceFullName;
        $fullName = self::$resouceFullName;
        $routeBefore = ['getgeocodes', 'getdatabairro'];
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('clustersmaps-obito', compact('newLoadDatas', 'total', 'urls', 'fullName', 'options'));
    }

    public function heatMap($tabela)
    {
      $ano = $rest = substr($tabela, -4);
      $options['inicio'] = $ano."-01-01";
      $options['fim'] = $ano."-12-31";
      $options['titulo'] = self::$resouceFullName;
      $routeAfter = array_merge(self::$routeAfter, ['getdataheatmap']);
      $urls = Controller::createUrl(self::$resouce, self::$routeBefore, $routeAfter);
      $urls['getdataheatmap'] =  route($urls['getdataheatmap'], ['tabela' => $tabela]);
      $fullName = self::$resouceFullName;
      return view('heatmap', compact('fullName', 'urls', 'options', 'ano'));
    }

    public function getDataHeatMap(Request $request, $tabela)
    {
      $inicio = $request->get('inicio');
      $fim = $request->get('fim');

      $notificacoes = DB::table($tabela)->select('ID_GEO1', 'ID_GEO2')->where(function($q) use ($inicio, $fim){
          $q->where('DTOBITO', '>=', $inicio);
          $q->where('DTOBITO', '<=', $fim);
          $q->whereNotNull('ID_GEO1');
          $q->whereNotNull('ID_GEO2');
      })
      ->get();

        return json_encode($notificacoes);
    }

    public function clustersMaps()
    {
        $loadDatas = LoadData::where('table_name', self::$resouce)
        ->orderBy('ano', 'desc')
        ->limit(5)
        ->get();
        $newLoadDatas = array();
        $soma = 0;
        $countDatas = count($loadDatas);
        foreach($loadDatas as $loadData){
            $count = DB::table($loadData->table_name.$loadData->ano)->count();
            $loadData->count = $count;
            $loadData->tabela = $loadData->table_name.$loadData->ano;
            $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
            $newLoadDatas[] = $loadData;
            $soma = $soma+$count;
        }
        if($soma > 0 && $countDatas >0){
            $total = new \stdClass;
            $media = $soma/$countDatas;
            $total->titulo = "Média".self::$resouceShortName;
            $total->media = $media;
            $total->color = '#FF0000';
        }

        $options['titulo'] = self::$resouceFullName;
        $fullName = self::$resouceFullName;
        $routeBefore = array_merge(self::$routeBefore, ['getgeocodes', 'getdatabairro', 'getdatalinebysem']);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('clustersmaps', compact('newLoadDatas', 'total', 'urls', 'fullName', 'options'));
    }


    public function getDataSexo(Request $request)
    {
        $anos = $request->get('anos');
        $countAnos = count($anos);
        $data = array();
        $datas = new \stdClass;


        if ($countAnos > 0) {
            $datas->data = Dengue::getBySexo(self::$resouce, $anos);
        }else{
            $datas->data = array();
        }

        return json_encode($datas);
    }



    public function getGeoCodestest(Request $request)
    {
        $ano = $request->get('ano');
        $datas = new \stdClass;
        $loadData = LoadData::where([
            ['table_name', '=', self::$resouce],
            ['ano', '=', $ano]
        ])
        ->first();
        $geocodes = DB::table(self::$resouce.$ano)->select('NU_NOTIFIC', 'NU_ANO','ID_GEO1', 'ID_GEO2')->where(function($q){
            $q->whereNotNull('ID_GEO1');
            $q->whereNotNull('ID_GEO2');
        })
        ->get();
        $datas->ano = $ano;
        $datas->geocodes = $geocodes;
        $datas->color = $loadData->color;


        return json_encode($datas);
    }

    public function getGeoCodes(Request $request)
    {
        $ano = $request->get('ano');
        $datas = new \stdClass;
        $loadData = LoadData::where([
            ['table_name', '=', self::$resouce],
            ['ano', '=', $ano]
        ])
        ->first();
        $geocodes = DB::table(self::$resouce.$ano)->select('NUMERODO', 'IDADE', 'NU_ANO','ID_GEO1', 'ID_GEO2')->where(function($q){
            $q->whereNotNull('ID_GEO1');
            $q->whereNotNull('ID_GEO2');
            $q->whereNull('IDADE');
        })->ORwhere(function($q){
            $q->whereNotNull('ID_GEO1');
            $q->whereNotNull('ID_GEO2');
            $q->where('IDADE', '<' ,'400');
        })
        ->get();
        $datas->ano = $ano;
        $datas->geocodes = $geocodes;
        $datas->color = $loadData->color;


        return json_encode($datas);
    }

    public function getSemEpdem(Request $request)
    {
        $loadDatas = LoadData::where('table_name', self::$resouce)
        ->orderBy('ano', 'desc')
        ->limit(5)
        ->get();
        $newLoadDatas = array();
        $soma = 0;
        $countDatas = count($loadDatas);
        foreach($loadDatas as $loadData){
            $count = DB::table($loadData->table_name.$loadData->ano)->count();
            $loadData->count = $count;
            $loadData->tabela = $loadData->table_name.$loadData->ano;
            $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
            $newLoadDatas[] = $loadData;
            $soma = $soma+$count;
        }
        if($soma > 0 && $countDatas >0){
            $total = new \stdClass;
            $media = $soma/$countDatas;
            $total->titulo = "Média Dengue";
            $total->media = $media;
            $total->color = '#FF0000';
        }

        $options['titulo'] = self::$resouceFullName;
        $fullName = self::$resouceFullName;
        $routeBefore = array_merge(self::$routeBefore, ['getdatasemepdem']);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('semanas-ep', compact('newLoadDatas', 'total', 'urls', 'fullName', 'options'));
    }

    public function getDataSemEpdem(Request $request)
    {
        $anos = $request->get('anos');
        $countAnos = count($anos);
        $data = array();
        $datas = new \stdClass;

        if ($countAnos > 0) {

            for ($i=0; $i < $countAnos; $i++) {
                $semanas[] = DB::table(self::$resouce.$anos[$i])
                        ->select(DB::raw('count(*)'))
                        ->groupBy('SEM_NOT')
                        ->get();
            }
            //dd($semanas);

            for ($i=0; $i < 52; $i++) {
                $semana[] = $i+1;
                for ($j=0; $j < $countAnos; $j++) {
                    if (isset($semanas[$j][$i])) {
                        $semana[] = $semanas[$j][$i]->count;
                    }else {
                        $semana[] = null;
                    }
                }
                $data[] = $semana;
                unset($semana);
            }
            $datas->data = $data;
        }else {
            $datas->data = [];
        }


        return json_encode($datas);

    }

    public function show($id)
    {
    }

    public function getDataBairro(Request $request)
    {
        $anos = $request->get('anos');
        $countAnos = count($anos);
        $data = array();
        $datas = new \stdClass;

        if ($countAnos > 0) {

            for ($i=0; $i < $countAnos; $i++) {
                $semanas[] = DB::table(self::$resouce.$anos[$i])
                        ->select(DB::raw('"BAIRES", count(*) AS "COUNTB"'))
                        ->whereNull('IDADE')
                        ->orwhere('IDADE', '<' ,'400')
                        ->whereNotNull('BAIRES')
                        ->groupBy('BAIRES')
                        ->orderBy('COUNTB', 'desc')
                        ->limit(10)
                        ->get();
            }

            //dd($semanas);

            for ($i=0; $i < 10; $i++) {

                for ($j=0; $j < $countAnos; $j++) {
                    if (isset($semanas[$j][$i])) {
                        $semana[] = "{$semanas[$j][$i]->BAIRES}";
                        $semana[] = "{$semanas[$j][$i]->COUNTB}";
                    }else {
                        $semana[] = null;
                        $semana[] = null;
                    }
                }
                $data[] = $semana;
                unset($semana);
            }
            $datas->data = $data;
        }else {
            $datas->data = [];
        }


        return json_encode($datas);

    }
}
