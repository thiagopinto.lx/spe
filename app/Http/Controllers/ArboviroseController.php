<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LoadData;
use App\Models\Zika;
use App\Utilities\Helper;

class ArboviroseController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $agravos = ['dengue', 'chik', 'zika'];
        $newLoadDatas = array();
        foreach ($agravos as $value) {
            $loadDatas[$value] = LoadData::where('table_name', $value)
            ->orderBy('ano', 'desc')
            ->limit(5)
            ->get();
            $soma[$value] = 0;
            $countDatas[$value] = count($loadDatas[$value]);
            foreach($loadDatas[$value] as $loadData){
                $loadData->count = DB::table($loadData->table_name.$loadData->ano)->count();
                $loadData->tabela = $loadData->table_name.$loadData->ano;
                $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
                $newLoadDatas[$value][] = $loadData;
                $soma[$value] = $soma[$value]+$loadData->count;
            }
            if($soma[$value] > 0 && $countDatas[$value] >0){
                $total[$value] = new \stdClass;
                $media = $soma[$value]/$countDatas[$value];
                $total[$value]->titulo = "Média ".$value;
                $total[$value]->media = $media;
                $total[$value]->color = '#FF0000';
            }
        }


        $options['agravo'] = 'Dengue, Zika, Chikung.';
        $options['titulo'] = 'Dengue, Zika, Chikung.';
        $route['dengue'] = route('dengue.getgeocodes');
        $route['zika'] = route('zika.getgeocodes');
        $route['chik'] = route('chik.getgeocodes');

       // new google.maps.LatLng(37.782551, -122.445368)
        return view('arbovirose-clustersmaps', compact('newLoadDatas', 'total', 'route', 'options'));
    }

    public function getDataEvolucao(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->agravo = "zika";

        $mydata->periodos = $request->get('anos');

        $mydata->criterios[] = ["Cura", function($tabela){
            return DB::table($tabela)->where([
                ['EVOLUCAO', '=', '1']
            ])->count();
        }];
        $mydata->criterios[] = ["Obito pelo Agravo", function($tabela){
            return DB::table($tabela)->where([
                ['EVOLUCAO', '=', '2']
            ])->count();
        }];
        $mydata->criterios[] = ["Obito por Outros", function($tabela){
            return DB::table($tabela)->where([
                ['EVOLUCAO', '=', '3']
            ])->count();
        }];
        $mydata->criterios[] = ["Branco", function($tabela){
            return DB::table($tabela)->where(
                DB::raw('"EVOLUCAO" = \'9\' OR "CLASSI_FIN" IS NULL')
                )->count();
        }];

        $datas = new \stdClass;
        $datas->data = Zika::createDataDetalhamento($mydata);
        return json_encode($datas);
    }

    public function getGeoCodes(Request $request)
    {
        $ano = $request->get('ano');
        $agravo = $request->get('agravo');
        $datas = new \stdClass;
        $loadData = LoadData::where([
            ['table_name', '=', $agravo],
            ['ano', '=', $ano]
        ])
        ->first();
        $geocodes = DB::table($agravo.$ano)->select('NU_NOTIFIC', 'NU_ANO','ID_GEO1', 'ID_GEO2')->where(function($q){
            $q->whereNotNull('ID_GEO1');
            $q->whereNotNull('ID_GEO2');
        })
        ->get();
        $datas->ano = $ano;
        $datas->agravo = $agravo;
        $datas->geocodes = $geocodes;
        $datas->color = $loadData->color;


        return json_encode($datas);
    }



}
