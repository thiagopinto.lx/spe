<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LoadData;
use App\Models\Srag;

class SragFullController extends Controller
{
    public static $resouce = 'sragfull';
    public static $resouceFullName = 'Srag Notificados em Teresina';
    public static $resouceShortName = 'Srag Notificados em Teresina';
    public static $routeBefore = ['index', 'detalhamentos', 'clustersmaps', 'getsemepdem'];
    public static $routeAfter = ['heatmap', 'charts'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $loadDatas = LoadData::where('table_name', self::$resouce)
            ->orderBy('ano', 'desc')
            ->limit(5)
            ->get();
        $newLoadDatas = array();
        $soma = 0;
        $countDatas = count($loadDatas);
        foreach ($loadDatas as $loadData) {
            $count = DB::table($loadData->table_name . $loadData->ano)->count();
            $loadData->count = $count;
            $loadData->tabela = $loadData->table_name . $loadData->ano;
            $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
            $newLoadDatas[] = $loadData;
            $soma = $soma + $count;
        }
        if ($soma > 0 && $countDatas > 0) {
            $total = new \stdClass;
            $media = $soma / $countDatas;
            $total->titulo = "Média" . self::$resouceShortName;
            $total->media = $media;
            $total->color = '#FF0000';
        }

        $options['titulo'] = self::$resouceFullName;
        $options['session'] = self::$resouce;
        $options['area'] = 'index';
        $routeBefore = array_merge(self::$routeBefore, ['getdatabar', 'getdataline', 'getdatalinebysem']);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('dashboard', compact('newLoadDatas', 'total', 'urls', 'options'));
    }

    public function getDataBar(Request $request)
    {
        $data = new \stdClass;
        $data->periodos =  $request->get('anos');

        $countAnos = count($data->periodos);

        if ($countAnos > 0) {
            $data->table_name = self::$resouce;
            $data->labels[] = "Casos notificados";
            $data->labels[] = "UTI";
            $data->labels[] = "Óbito";
            $data->labels[] = "Óbito COVID-19";

            $data->querys[] = function ($table) {
                return DB::table($table)->count();
            };
            $data->querys[] = function ($table) {
                return DB::table($table)->where([
                    ['UTI', '=', '1']
                ])->count();
            };
            $data->querys[] = function ($table) {
                return DB::table($table)->where([
                    ['EVOLUCAO', '=', '2']
                ])->count();
            };
            $data->querys[] = function ($table) {
                return DB::table($table)->where([
                    ['EVOLUCAO', '=', '2'],
                    ['CLASSI_FIN', '=', '5']
                ])->count();
            };

            $dataSet = Srag::createDataSetBar($data);
        }
        return json_encode($dataSet);
    }

    public function getDataLine(Request $request)
    {
        $data = new \stdClass;
        $data->table_name = self::$resouce;
        $data->periodos = $request->get('anos');
        $data->labels = self::$meses;
        $data->querys[] = function ($table) {
            return DB::table($table)
                ->select(DB::raw('count("DT_SIN_PRI")'))
                ->groupBy(DB::raw('DATE_TRUNC(\'month\', "DT_SIN_PRI")'))
                ->orderBy(DB::raw('DATE_TRUNC(\'month\', "DT_SIN_PRI")'))
                ->get();
        };
        $data->quantMedia = 12;
        $dataSet = Srag::createDataSetLine($data);

        return json_encode($dataSet);
    }

    public function getDataLineBySem(Request $request)
    {
        $data = new \stdClass;
        $data->table_name = self::$resouce;
        $data->periodos = $request->get('anos');
        for ($i = 0; $i < 52; $i++) {
            $data->labels[] = $i + 1;
        }
        $data->querys[] = function ($table) {
            return DB::table($table)
                ->select(DB::raw('count(*)'))
                ->groupBy('SEM_PRI')
                ->orderBy('SEM_PRI')
                ->get();
        };
        $data->quantMedia = 52;

        $data->optionLine = new \stdClass;
        $data->optionLine->steppedLine = true;
        $data->optionLine->pointRadius = 5;
        $data->optionLine->pointHoverRadius = 15;
        $data->optionLine->pointStyles = [
            'circle',
            'rectRot',
            'rect',
            'star',
            'triangle'
        ];

        $dataSet = Srag::createDataSetLine($data);
        return json_encode($dataSet);
    }

    public function heatMap($tabela)
    {
        $ano = $rest = substr($tabela, -4);
        //$ciclos = Ciclo::where('ano', $ano)->get();
        $options['inicio'] = $ano . "-01-01";
        $options['fim'] = $ano . "-12-31";
        $options['titulo'] = self::$resouceFullName;
        $routeAfter = array_merge(self::$routeAfter, ['getdataheatmap']);
        $urls = Controller::createUrl(self::$resouce, self::$routeBefore, $routeAfter);
        $urls['getdataheatmap'] =  route($urls['getdataheatmap'], ['tabela' => $tabela]);
        $urls['getdataciclo'] =  route('ciclos.getdataciclo', compact('ano'));
        $fullName = self::$resouceFullName;
        return view('heatmap', compact('fullName', 'urls', 'options', 'ano'));
    }

    public function charts($tabela)
    {
        $ano = $rest = substr($tabela, -4);
        //$ciclos = Ciclo::where('ano', $ano)->get();
        $options['inicio'] = $ano . "-01-01";
        $options['fim'] = $ano . "-12-31";

        $loadData = LoadData::where(
            [
                ['ano', $ano],
                ['table_name', self::$resouce]
            ]
        )->first();

        $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');

        $acumuladoDeCasos = DB::table($tabela)
            ->select(DB::raw(
                '"DT_SIN_PRI" AS date,
                COUNT("DT_SIN_PRI") AS new,
                COUNT("EVOLUCAO") FILTER (WHERE "EVOLUCAO" = \'2\') AS death,
                SUM(COUNT("DT_SIN_PRI")) OVER (ORDER BY "DT_SIN_PRI") AS sub,
                SUM(COUNT("EVOLUCAO") FILTER (WHERE "EVOLUCAO" = \'2\'))
                OVER
                (ORDER BY "DT_SIN_PRI") AS total_obitos'
            ))
            ->groupBy("DT_SIN_PRI")
            ->orderBy("DT_SIN_PRI")
            ->get();

        $acumuladoDeCasosCovid = DB::table($tabela)
            ->select(DB::raw(
                '"DT_SIN_PRI" AS date,
                COUNT("DT_SIN_PRI") AS new,
                COUNT("EVOLUCAO") FILTER (WHERE "EVOLUCAO" = \'2\') AS death,
                SUM(COUNT("DT_SIN_PRI")) OVER (ORDER BY "DT_SIN_PRI") AS sub,
                SUM(COUNT("EVOLUCAO") FILTER (WHERE "EVOLUCAO" = \'2\'))
                OVER
                (ORDER BY "DT_SIN_PRI") AS total_obitos'
            ))
            ->where("CLASSI_FIN", '5')
            ->groupBy("DT_SIN_PRI")
            ->orderBy("DT_SIN_PRI")
            ->get();

        $dataChatBarNew = array();
        $dataChatBarDeath = array();
        $dataChatArea = array();
        $dataDates = array();

        $dataChatBarNewCovid = array();
        $dataChatBarDeathCovid = array();
        $dataChatAreaCovid = array();
        $dataDatesCovid = array();

        for ($i = 0; $i < count($acumuladoDeCasos) - 1; $i++) {
            $new = new \stdClass;
            $new->x = $acumuladoDeCasos[$i]->date;
            $new->y = $acumuladoDeCasos[$i]->new;
            $dataChatBarNew[] = $new;

            $death = new \stdClass;
            $death->x = $acumuladoDeCasos[$i]->date;
            $death->y = $acumuladoDeCasos[$i]->death;
            $dataChatBarDeath[] = $death;

            $dataDates[] = $acumuladoDeCasos[$i]->date;

            $item = new \stdClass;
            $item->x = $acumuladoDeCasos[$i]->date;
            $item->y = [$acumuladoDeCasos[$i]->sub];
            $dataChatArea[] = $item;
        }

        for ($i = 0; $i < count($acumuladoDeCasosCovid) - 1; $i++) {
            $new = new \stdClass;
            $new->x = $acumuladoDeCasosCovid[$i]->date;
            $new->y = $acumuladoDeCasosCovid[$i]->new;
            $dataChatBarNewCovid[] = $new;

            $death = new \stdClass;
            $death->x = $acumuladoDeCasosCovid[$i]->date;
            $death->y = $acumuladoDeCasosCovid[$i]->death;
            $dataChatBarDeathCovid[] = $death;

            $dataDatesCovid[] = $acumuladoDeCasosCovid[$i]->date;

            $item = new \stdClass;
            $item->x = $acumuladoDeCasosCovid[$i]->date;
            $item->y = [$acumuladoDeCasosCovid[$i]->sub];
            $dataChatAreaCovid[] = $item;
        }

        $total = new \stdClass;
        $total->x = $acumuladoDeCasos[count($acumuladoDeCasos) - 1]->date;
        $total->y = [0, $acumuladoDeCasos[count($acumuladoDeCasos) - 1]->sub];

        $dataFaixaEtaria = SragFullController::chartFaixaEtaria($tabela);
        $dataFaixaEtariaCovid = SragFullController::chartFaixaEtariaCovid($tabela);

        $options['titulo'] = self::$resouceFullName;
        $routeAfter = array_merge(self::$routeAfter, ['getdataheatmap']);
        $urls = Controller::createUrl(self::$resouce, self::$routeBefore, $routeAfter);
        $urls['getdataheatmap'] =  route($urls['getdataheatmap'], ['tabela' => $tabela]);
        $urls['getdataciclo'] =  route('ciclos.getdataciclo', compact('ano'));
        $fullName = self::$resouceFullName;

        return view(
            'charts-srag',
            compact(
                'fullName',
                'urls',
                'options',
                'ano',
                'loadData',
                'dataChatArea',
                'dataChatBarNew',
                'dataChatBarDeath',
                'dataDates',
                'dataFaixaEtaria',
                'dataFaixaEtariaCovid',
                'dataChatAreaCovid',
                'dataChatBarNewCovid',
                'dataChatBarDeathCovid',
                'dataDatesCovid',
                'total'
            )
        );
    }

    public function clustersMaps()
    {
        $loadDatas = LoadData::where('table_name', self::$resouce)
            ->orderBy('ano', 'desc')
            ->limit(5)
            ->get();
        $newLoadDatas = array();
        $soma = 0;
        $countDatas = count($loadDatas);
        foreach ($loadDatas as $loadData) {
            $count = DB::table($loadData->table_name . $loadData->ano)->count();
            $loadData->count = $count;
            $loadData->tabela = $loadData->table_name . $loadData->ano;
            $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
            $newLoadDatas[] = $loadData;
            $soma = $soma + $count;
        }
        if ($soma > 0 && $countDatas > 0) {
            $total = new \stdClass;
            $media = $soma / $countDatas;
            $total->titulo = "Média" . self::$resouceShortName;
            $total->media = $media;
            $total->color = '#FF0000';
        }

        $options['titulo'] = self::$resouceFullName;
        $fullName = self::$resouceFullName;
        $options['session'] = self::$resouce;
        $options['area'] = 'clustersmaps';
        $routeBefore = array_merge(self::$routeBefore, ['getgeocodes', 'getdatabairro', 'getdatalinebysem']);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view(
            'clustersmaps',
            compact(
                'newLoadDatas',
                'total',
                'urls',
                'fullName',
                'options',
            )
        );
    }

    public function getDataClassificacao(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->table_name = self::$resouce;

        $mydata->periodos = $request->get('anos');

        $mydata->criterios[0] = ["SRAG por influenza", function ($tabela) {
            return DB::table($tabela)->where([
                ['CLASSI_FIN', '=', '1']
            ])->count();
        }];
        $mydata->criteriosOld[0] = $mydata->criterios[0];

        $mydata->criterios[1] = ["SRAG por outro vírus respiratório", function ($tabela) {
            return DB::table($tabela)->where([
                ['CLASSI_FIN', '=', '2']
            ])->count();
        }];
        $mydata->criteriosOld[1] = $mydata->criterios[1];

        $mydata->criterios[2] = ["SRAG por outro agente etiológico", function ($tabela) {
            return DB::table($tabela)->where([
                ['CLASSI_FIN', '=', '3']
            ])->count();
        }];
        $mydata->criteriosOld[2] = $mydata->criterios[2];

        $mydata->criterios[3] = ["SRAG não especificado", function ($tabela) {
            return DB::table($tabela)->where([
                ['CLASSI_FIN', '=', '4']
            ])->count();
        }];
        $mydata->criteriosOld[3] = $mydata->criterios[3];

        $mydata->criterios[4] = ["COVID-19", function ($tabela) {
            return DB::table($tabela)->where([
                ['CLASSI_FIN', '=', '5']
            ])->count();
        }];
        $mydata->criteriosOld[4] = $mydata->criterios[4];

        $mydata->criterios[5] = ["Branco", function ($tabela) {
            return DB::table($tabela)->where(
                DB::raw('"CLASSI_FIN" IS NULL OR "CLASSI_FIN" = \'\'')
            )->count();
        }];

        $mydata->criteriosOld[5] = ["Branco", function ($tabela) {
            return DB::table($tabela)->where(
                DB::raw('"CLASSI_FIN" IS NULL')
            )->count();
        }];

        $datas = new \stdClass;
        $datas->data = Srag::createDataDetalhamento($mydata);
        return json_encode($datas);
    }

    public function getDataEvolucao(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->table_name = self::$resouce;

        $mydata->periodos = $request->get('anos');

        $mydata->criterios[] = ["Letalidade", function ($tabela) {
            $total = DB::table($tabela)->count();
            $obito = DB::table($tabela)->where([
                ['EVOLUCAO', '=', '2']
            ])->count();

            return (round(($obito / $total) * 100, 2)) . "%";
        }];

        $mydata->criterios[0] = ["Cura", function ($tabela) {
            return DB::table($tabela)->where([
                ['EVOLUCAO', '=', '1']
            ])->count();
        }];
        $mydata->criteriosOld[0] = $mydata->criterios[0];

        $mydata->criterios[1] = ["Obito", function ($tabela) {
            return DB::table($tabela)->where([
                ['EVOLUCAO', '=', '2']
            ])->count();
        }];
        $mydata->criteriosOld[1] = $mydata->criterios[1];

        $mydata->criterios[2] = ["Branco", function ($tabela) {
            return DB::table($tabela)->where(
                DB::raw('"CLASSI_FIN" IS NULL OR "EVOLUCAO" = \'9\'')
            )->count();
        }];
        $mydata->criteriosOld[2] = $mydata->criterios[2];

        $datas = new \stdClass;
        $datas->data = Srag::createDataDetalhamento($mydata);
        return json_encode($datas);
    }

    public function getDataOutra(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->table_name = self::$resouce;

        $mydata->periodos = $request->get('anos');

        $mydata->criterios[0] = ["Média de tempo entre primeiros sistomas e internação em dias", function ($tabela) {
            $dias = DB::table($tabela)
                ->select(
                    DB::raw('"DT_INTERNA" - ("DT_SIN_PRI" -  INTERVAL \'1 DAY\') :: DATE  AS QUANT_DIAS')
                )
                ->get();

            $quant_dias = 0;
            $soma_dias = 0;

            for ($i = 0; $i < count($dias); $i++) {
                if ($dias[$i]->quant_dias != null) {
                    $quant_dias++;
                    $soma_dias = $soma_dias + $dias[$i]->quant_dias;
                }
            }


            return (round(($soma_dias / $quant_dias), 2)) . "d";
        }];

        $mydata->criteriosOld[0] = $mydata->criterios[0];

        $mydata->criterios[1] = ["Média de tempo entre internação, alta ou óbito em dias", function ($tabela) {
            $dias = DB::table($tabela)
                ->select(
                    DB::raw('"DT_EVOLUCA" - ("DT_INTERNA" -  INTERVAL \'1 DAY\') :: DATE  AS QUANT_DIAS')
                )
                ->get();

            $quant_dias = 0;
            $soma_dias = 0;

            for ($i = 0; $i < count($dias); $i++) {
                if ($dias[$i]->quant_dias != null) {
                    $quant_dias++;
                    $soma_dias = $soma_dias + $dias[$i]->quant_dias;
                }
            }


            return (round(($soma_dias / $quant_dias), 2)) . "d";
        }];

        $mydata->criteriosOld[1] =  false;

        $mydata->criterios[2] = ["Quantidade que precisou de UTI", function ($tabela) {
            return DB::table($tabela)->where([
                ['UTI', '=', '1']
            ])->count();
        }];
        $mydata->criteriosOld[2] = ["Quantidade que precisou de UTI", function ($tabela) {
            return DB::table($tabela)->where([
                ['UTI', '=', 1]
            ])->count();
        }];

        $mydata->criterios[3] = ["O paciente de UTI fez uso de suporte ventilatório invasivo", function ($tabela) {
            return DB::table($tabela)->where([
                ['UTI', '=', '1'],
                ['SUPORT_VEN', '=', '1']
            ])->count();
        }];
        $mydata->criteriosOld[3] = ["O paciente de UTI fez uso de suporte ventilatório invasivo", function ($tabela) {
            return DB::table($tabela)->where([
                ['UTI', '=', 1],
                ['SUPORT_VEN', '=', 1]
            ])->count();
        }];

        $mydata->criterios[4] = ["O paciente de UTI fez uso de suporte ventilatório não invasivo", function ($tabela) {
            return DB::table($tabela)->where([
                ['UTI', '=', '1'],
                ['SUPORT_VEN', '=', '2']
            ])->count();
        }];
        $mydata->criteriosOld[4] = ["O paciente de UTI fez uso de suporte ventilatório não invasivo", function ($tabela) {
            return DB::table($tabela)->where([
                ['UTI', '=', 1],
                ['SUPORT_VEN', '=', 2]
            ])->count();
        }];

        $datas = new \stdClass;
        $datas->data = Srag::createDataDetalhamento($mydata);
        return json_encode($datas);
    }

    public function getDataRipsa(Request $request)
    {
        $mydata = new \stdClass;
        $mydata->table_name = self::$resouce;

        $mydata->periodos = $request->get('anos');

        $mydata->criterios[0] = ["Menor 6 meses ", function ($tabela) {
            return DB::table($tabela)->where([
                ['COD_IDADE', '<', '2006']
            ])->count();
        }];
        $mydata->criteriosOld[0] = ["Menor 6 meses ", function ($tabela) {
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '<', '3006']
            ])->count();
        }];

        $mydata->criterios[1] = ["6-11 meses", function ($tabela) {
            return DB::table($tabela)->where([
                ['COD_IDADE', '>=', '2006'],
                ['COD_IDADE', '<=', '2011']
            ])->count();
        }];
        $mydata->criteriosOld[1] = ["6-11 meses", function ($tabela) {
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '3006'],
                ['NU_IDADE_N', '<=', '3011']
            ])->count();
        }];

        $mydata->criterios[2] = ["1-4 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3001'],
                ['COD_IDADE', '<=', '3004']
            ])->count();
        }];
        $mydata->criteriosOld[2] = ["1-4 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4001'],
                ['NU_IDADE_N', '<=', '4004']
            ])->count();
        }];

        $mydata->criterios[3] = ["5-9 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3005'],
                ['COD_IDADE', '<=', '3009']
            ])->count();
        }];
        $mydata->criteriosOld[3] = ["5-9 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4005'],
                ['NU_IDADE_N', '<=', '4009']
            ])->count();
        }];

        $mydata->criterios[4] = ["10-14 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3010'],
                ['COD_IDADE', '<=', '3014']
            ])->count();
        }];
        $mydata->criteriosOld[4] = ["10-14 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4010'],
                ['NU_IDADE_N', '<=', '4014']
            ])->count();
        }];

        $mydata->criterios[5] = ["15-19 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3015'],
                ['COD_IDADE', '<=', '3019']
            ])->count();
        }];
        $mydata->criteriosOld[5] = ["15-19 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4015'],
                ['NU_IDADE_N', '<=', '4019']
            ])->count();
        }];

        $mydata->criterios[6] = ["20-29 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3020'],
                ['COD_IDADE', '<=', '3029']
            ])->count();
        }];
        $mydata->criteriosOld[6] = ["20-29 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4020'],
                ['NU_IDADE_N', '<=', '4029']
            ])->count();
        }];

        $mydata->criterios[7] = ["30-39 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3030'],
                ['COD_IDADE', '<=', '3039']
            ])->count();
        }];
        $mydata->criteriosOld[7] = ["30-39 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4030'],
                ['NU_IDADE_N', '<=', '4039']
            ])->count();
        }];

        $mydata->criterios[8] = ["40-49 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3040'],
                ['COD_IDADE', '<=', '3049']
            ])->count();
        }];
        $mydata->criteriosOld[8] = ["40-49 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4040'],
                ['NU_IDADE_N', '<=', '4049']
            ])->count();
        }];

        $mydata->criterios[9] = ["50-59 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3050'],
                ['COD_IDADE', '<=', '3059']
            ])->count();
        }];
        $mydata->criteriosOld[9] = ["50-59 anos", function ($tabela) {
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4050'],
                ['NU_IDADE_N', '<=', '4059']
            ])->count();
        }];

        $mydata->criterios[10] = ["60 anos e mais ", function ($tabela) {
            return DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3060'],
                ['COD_IDADE', '<=', '3500']
            ])->count();
        }];
        $mydata->criteriosOld[10] = ["60 anos e mais ", function ($tabela) {
            return DB::table($tabela)->where([
                ['NU_IDADE_N', '>=', '4060'],
                ['NU_IDADE_N', '<=', '4500']
            ])->count();
        }];

        $datas = new \stdClass;
        $datas->data = Srag::createDataDetalhamento($mydata);
        return json_encode($datas);
    }

    public function chartFaixaEtaria($tabela)
    {
        $categories = [
            "Menor 6 meses",
            "6-11 meses",
            "1-4 anos",
            "5-9 anos",
            "10-14 anos",
            "15-19 anos",
            "20-29 anos",
            "30-39 anos",
            "40-49 anos",
            "50-59 anos",
            "60 anos e mais"
        ];

        $masculino = array();
        $feminino = array();

        $total_de_casos = DB::table($tabela)->count();

        if(true) {
            $masculino[] = DB::table($tabela)->where([
                ['COD_IDADE', '<', '2006'],
                ['CS_SEXO', '=', 'M']
            ])->count();

            $feminino[] = DB::table($tabela)->where([
                ['COD_IDADE', '<', '2006'],
                ['CS_SEXO', '=', 'F']
            ])->count();

            $masculino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '2006'],
                ['COD_IDADE', '<=', '2011'],
                ['CS_SEXO', '=', 'M']
            ])->count();

            $feminino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '2006'],
                ['COD_IDADE', '<=', '2011'],
                ['CS_SEXO', '=', 'F']
            ])->count();

            $masculino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3001'],
                ['COD_IDADE', '<=', '3004'],
                ['CS_SEXO', '=', 'M']
            ])->count();

            $feminino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3001'],
                ['COD_IDADE', '<=', '3004'],
                ['CS_SEXO', '=', 'F']
            ])->count();

            $masculino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3005'],
                ['COD_IDADE', '<=', '3009'],
                ['CS_SEXO', '=', 'M']
            ])->count();

            $feminino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3005'],
                ['COD_IDADE', '<=', '3009'],
                ['CS_SEXO', '=', 'F']
            ])->count();

            $masculino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3010'],
                ['COD_IDADE', '<=', '3014'],
                ['CS_SEXO', '=', 'M']
            ])->count();

            $feminino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3010'],
                ['COD_IDADE', '<=', '3014'],
                ['CS_SEXO', '=', 'F']
            ])->count();

            $masculino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3015'],
                ['COD_IDADE', '<=', '3019'],
                ['CS_SEXO', '=', 'M']
            ])->count();

            $feminino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3015'],
                ['COD_IDADE', '<=', '3019'],
                ['CS_SEXO', '=', 'F']
            ])->count();

            $masculino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3020'],
                ['COD_IDADE', '<=', '3029'],
                ['CS_SEXO', '=', 'M']
            ])->count();

            $feminino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3020'],
                ['COD_IDADE', '<=', '3029'],
                ['CS_SEXO', '=', 'F']
            ])->count();

            $masculino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3030'],
                ['COD_IDADE', '<=', '3039'],
                ['CS_SEXO', '=', 'M']
            ])->count();

            $feminino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3030'],
                ['COD_IDADE', '<=', '3039'],
                ['CS_SEXO', '=', 'F']
            ])->count();

            $masculino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3040'],
                ['COD_IDADE', '<=', '3049'],
                ['CS_SEXO', '=', 'M']
            ])->count();

            $feminino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3040'],
                ['COD_IDADE', '<=', '3049'],
                ['CS_SEXO', '=', 'F']
            ])->count();

            $masculino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3050'],
                ['COD_IDADE', '<=', '3059'],
                ['CS_SEXO', '=', 'M']
            ])->count();

            $feminino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3050'],
                ['COD_IDADE', '<=', '3059'],
                ['CS_SEXO', '=', 'F']
            ])->count();

            $masculino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3060'],
                ['COD_IDADE', '<=', '3500'],
                ['CS_SEXO', '=', 'M']
            ])->count();

            $feminino[] = DB::table($tabela)->where([
                ['COD_IDADE', '>=', '3060'],
                ['COD_IDADE', '<=', '3500'],
                ['CS_SEXO', '=', 'F']
            ])->count();
        }

        $maxValue = 0;

        for ($i = 0; $i < count($masculino); $i++) {
            if ($masculino[$i] > 0) {
                $masculino[$i] = $masculino[$i] / ($total_de_casos / 100);
            } else {
                $masculino[$i] = 0;
            }

            if ($maxValue < $masculino[$i]) {
                $maxValue = $masculino[$i];
            }
        }

        for ($i = 0; $i < count($feminino); $i++) {
            if ($feminino[$i] > 0) {
                $feminino[$i] = $feminino[$i] / ($total_de_casos / 100);
            } else {
                $feminino[$i] = 0;
            }

            if ($maxValue < $masculino[$i]) {
                $maxValue = $masculino[$i];
            }
            $feminino[$i] = ($feminino[$i]) * -1;
        }

        $dataFaixaEtaria['categories'] = $categories;
        $dataFaixaEtaria['masculino'] = $masculino;
        $dataFaixaEtaria['feminino'] = $feminino;
        $dataFaixaEtaria['maxValue'] = $maxValue;

        return $dataFaixaEtaria;
    }

    public function chartFaixaEtariaCovid($tabela)
    {
        $categories = [
            "Menor 6 meses",
            "6-11 meses",
            "1-4 anos",
            "5-9 anos",
            "10-14 anos",
            "15-19 anos",
            "20-29 anos",
            "30-39 anos",
            "40-49 anos",
            "50-59 anos",
            "60 anos e mais"
        ];

        $masculino = array();
        $feminino = array();

        $total_de_casos = DB::table($tabela)->where('CLASSI_FIN', '5')->count();

        $masculino[] = DB::table($tabela)->where([
            ['COD_IDADE', '<', '2006'],
            ['CS_SEXO', '=', 'M'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $feminino[] = DB::table($tabela)->where([
            ['COD_IDADE', '<', '2006'],
            ['CS_SEXO', '=', 'F'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $masculino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '2006'],
            ['COD_IDADE', '<=', '2011'],
            ['CS_SEXO', '=', 'M'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $feminino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '2006'],
            ['COD_IDADE', '<=', '2011'],
            ['CS_SEXO', '=', 'F'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $masculino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3001'],
            ['COD_IDADE', '<=', '3004'],
            ['CS_SEXO', '=', 'M'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $feminino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3001'],
            ['COD_IDADE', '<=', '3004'],
            ['CS_SEXO', '=', 'F'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $masculino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3005'],
            ['COD_IDADE', '<=', '3009'],
            ['CS_SEXO', '=', 'M'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $feminino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3005'],
            ['COD_IDADE', '<=', '3009'],
            ['CS_SEXO', '=', 'F'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $masculino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3010'],
            ['COD_IDADE', '<=', '3014'],
            ['CS_SEXO', '=', 'M'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $feminino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3010'],
            ['COD_IDADE', '<=', '3014'],
            ['CS_SEXO', '=', 'F'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $masculino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3015'],
            ['COD_IDADE', '<=', '3019'],
            ['CS_SEXO', '=', 'M'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $feminino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3015'],
            ['COD_IDADE', '<=', '3019'],
            ['CS_SEXO', '=', 'F'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $masculino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3020'],
            ['COD_IDADE', '<=', '3029'],
            ['CS_SEXO', '=', 'M'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $feminino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3020'],
            ['COD_IDADE', '<=', '3029'],
            ['CS_SEXO', '=', 'F'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $masculino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3030'],
            ['COD_IDADE', '<=', '3039'],
            ['CS_SEXO', '=', 'M'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $feminino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3030'],
            ['COD_IDADE', '<=', '3039'],
            ['CS_SEXO', '=', 'F'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $masculino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3040'],
            ['COD_IDADE', '<=', '3049'],
            ['CS_SEXO', '=', 'M'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $feminino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3040'],
            ['COD_IDADE', '<=', '3049'],
            ['CS_SEXO', '=', 'F'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $masculino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3050'],
            ['COD_IDADE', '<=', '3059'],
            ['CS_SEXO', '=', 'M'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $feminino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3050'],
            ['COD_IDADE', '<=', '3059'],
            ['CS_SEXO', '=', 'F'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $masculino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3060'],
            ['COD_IDADE', '<=', '3500'],
            ['CS_SEXO', '=', 'M'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $feminino[] = DB::table($tabela)->where([
            ['COD_IDADE', '>=', '3060'],
            ['COD_IDADE', '<=', '3500'],
            ['CS_SEXO', '=', 'F'],
            ['CLASSI_FIN', '=', '5']
        ])->count();

        $maxValue = 0;

        for ($i = 0; $i < count($masculino); $i++) {
            if ($masculino[$i] > 0) {
                $masculino[$i] = $masculino[$i] / ($total_de_casos / 100);
            } else {
                $masculino[$i] = 0;
            }

            if ($maxValue < $masculino[$i]) {
                $maxValue = $masculino[$i];
            }
        }

        for ($i = 0; $i < count($feminino); $i++) {
            if ($feminino[$i] > 0) {
                $feminino[$i] = $feminino[$i] / ($total_de_casos / 100);
            } else {
                $feminino[$i] = 0;
            }
            if ($maxValue < $masculino[$i]) {
                $maxValue = $masculino[$i];
            }
            $feminino[$i] = ($feminino[$i]) * -1;
        }

        $dataFaixaEtaria['categories'] = $categories;
        $dataFaixaEtaria['masculino'] = $masculino;
        $dataFaixaEtaria['feminino'] = $feminino;
        $dataFaixaEtaria['maxValue'] = $maxValue;

        return $dataFaixaEtaria;
    }

    public function getDataSexo(Request $request)
    {
        $anos = $request->get('anos');
        $countAnos = count($anos);
        $data = array();
        $datas = new \stdClass;


        if ($countAnos > 0) {
            $datas->data = Srag::getBySexo(self::$resouce, $anos);
        } else {
            $datas->data = array();
        }

        return json_encode($datas);
    }

    public function detalhamento(Request $request)
    {
        $loadDatas = LoadData::where('table_name', self::$resouce)
            ->orderBy('ano', 'desc')
            ->limit(5)
            ->get();
        $newLoadDatas = array();
        $soma = 0;
        $countDatas = count($loadDatas);
        foreach ($loadDatas as $loadData) {
            $count = DB::table($loadData->table_name . $loadData->ano)->count();
            $loadData->count = $count;
            $loadData->tabela = $loadData->table_name . $loadData->ano;
            $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
            $newLoadDatas[] = $loadData;
            $soma = $soma + $count;
        }
        if ($soma > 0 && $countDatas > 0) {
            $total = new \stdClass;
            $media = $soma / $countDatas;
            $total->titulo = "Média" . self::$resouceShortName;
            $total->media = $media;
            $total->color = '#FF0000';
        }

        $options['titulo'] = self::$resouceFullName;
        $fullName = self::$resouceFullName;
        $options['session'] = self::$resouce;
        $options['area'] = 'detalhamentos';
        $routeBefore = array_merge(
            self::$routeBefore,
            [
                'getdataclassificacao',
                'getdataevolucao',
                'getdataripsa',
                'getdataoutra',
                'getdatasexo',
                'getdatabairro'
            ]
        );

        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('detalhamento-srag', compact('newLoadDatas', 'total', 'urls', 'fullName', 'options'));
    }

    public function getGeoCodes(Request $request)
    {
        $ano = $request->get('ano');
        $datas = new \stdClass;
        $loadData = LoadData::where([
            ['table_name', '=', self::$resouce],
            ['ano', '=', $ano]
        ])
            ->first();
        $geocodes = DB::table(self::$resouce . $ano)->select('NU_NOTIFIC', 'ID_GEO1', 'ID_GEO2')->where(function ($q) {
            $q->whereNotNull('ID_GEO1');
            $q->whereNotNull('ID_GEO2');
        })
            ->get();
        $datas->ano = $ano;
        $datas->geocodes = $geocodes;
        $datas->color = $loadData->color;


        return json_encode($datas);
    }

    public function getSemEpdem(Request $request)
    {
        $loadDatas = LoadData::where('table_name', self::$resouce)
            ->orderBy('ano', 'desc')
            ->limit(5)
            ->get();
        $newLoadDatas = array();
        $soma = 0;
        $countDatas = count($loadDatas);
        foreach ($loadDatas as $loadData) {
            $count = DB::table($loadData->table_name . $loadData->ano)->count();
            $loadData->count = $count;
            $loadData->tabela = $loadData->table_name . $loadData->ano;
            $loadData->updated = date_format($loadData->updated_at, 'd-m-Y');
            $newLoadDatas[] = $loadData;
            $soma = $soma + $count;
        }
        if ($soma > 0 && $countDatas > 0) {
            $total = new \stdClass;
            $media = $soma / $countDatas;
            $total->titulo = "Média Srag";
            $total->media = $media;
            $total->color = '#FF0000';
        }

        $options['titulo'] = self::$resouceFullName;
        $fullName = self::$resouceFullName;
        $options['session'] = self::$resouce;
        $options['area'] = 'getsemepdem';
        $routeBefore = array_merge(self::$routeBefore, ['getdatasemepdem']);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('semanas-ep', compact('newLoadDatas', 'total', 'urls', 'fullName', 'options'));
    }

    public function getDataSemEpdem(Request $request)
    {
        $anos = $request->get('anos');
        $countAnos = count($anos);
        $data = array();
        $datas = new \stdClass;

        if ($countAnos > 0) {
            for ($i = 0; $i < $countAnos; $i++) {
                $semanas[] = DB::table(self::$resouce . $anos[$i])
                    ->select(DB::raw('count(*)'))
                    ->groupBy('SEM_NOT')
                    ->get();
            }
            //dd($semanas);

            for ($i = 0; $i < 52; $i++) {
                $semana[] = $i + 1;
                for ($j = 0; $j < $countAnos; $j++) {
                    if (isset($semanas[$j][$i])) {
                        $semana[] = $semanas[$j][$i]->count;
                    } else {
                        $semana[] = null;
                    }
                }
                $data[] = $semana;
                unset($semana);
            }
            $datas->data = $data;
        } else {
            $datas->data = [];
        }


        return json_encode($datas);
    }

    public function show($id)
    {
    }

    public function getDataBairro(Request $request)
    {
        $anos = $request->get('anos');
        $countAnos = count($anos);
        $data = array();
        $datas = new \stdClass;

        if ($countAnos > 0) {

            for ($i = 0; $i < $countAnos; $i++) {
                $semanas[] = DB::table(self::$resouce . $anos[$i])
                    ->select(DB::raw('"NM_BAIRRO", count(*) AS "COUNTB"'))
                    ->whereNotNull('NM_BAIRRO')
                    ->groupBy('NM_BAIRRO')
                    ->orderBy('COUNTB', 'desc')
                    ->limit(10)
                    ->get();
            }

            //dd($semanas);

            for ($i = 0; $i < 10; $i++) {

                for ($j = 0; $j < $countAnos; $j++) {
                    if (isset($semanas[$j][$i])) {
                        $semana[] = "{$semanas[$j][$i]->NM_BAIRRO}";
                        $semana[] = "{$semanas[$j][$i]->COUNTB}";
                    } else {
                        $semana[] = null;
                        $semana[] = null;
                    }
                }
                $data[] = $semana;
                unset($semana);
            }
            $datas->data = $data;
        } else {
            $datas->data = [];
        }


        return json_encode($datas);
    }
}
