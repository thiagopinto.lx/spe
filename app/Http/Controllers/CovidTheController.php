<?php
/**
 * Name expacei referenta a parte publica do site
 *
 * @author Thiago Pinto Dias <thiagopinto.lx@gmail.com>
 *
 */
namespace App\Http\Controllers;

use App\Models\CovidDatasus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\LoadData;
use App\Models\Srag;
use App\Models\Cidade;
use App\Models\Bairro;
use DateInterval;
use DateTime;
use stdClass;

class CovidTheController extends Controller
{
    public static $resouce = 'covid-the';
    public static $resouceFullName = 'Covid-19 residente em Teresina';
    public static $resouceShortName = 'Covid-19 residente em Teresina';
    public static $routeBefore = ['index', 'obito', 'sintomatologia', 'map-evolucao', 'outras-info'];
    public static $routeAfter = ['heatmap', 'charts'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hoje = new DateTime();
        $cidade = Cidade::whereRaw(
            'LEFT( CAST(co_ibge AS TEXT), 6) = ?', ['221100']
        )->first();

        $limiteReuperados = new DateTime();
        $quizeDias = new DateInterval('P15D');
        $limiteReuperados->sub($quizeDias);

        $quantCasosDataSus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo']
            ]
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->count();

        $quantCasosSg = DB::table('sg2020')->where('PCR_SARS2', 1)->count();
        //$quantCasosSrag = DB::table('srag2020')->where('PCR_SARS2', 1)->count();
        $quantCasosSrag = DB::table('srag2020')->where('CLASSI_FIN', 5)->count();

        $updateEsus = CovidDatasus::select('updated_at')
            ->orderBy('updated_at', 'desc')->first();
        $updateEsus = date_format($updateEsus->updated_at, 'd-m-Y');

        $updateSg = DB::table('sg2020')->select('updated_at')
            ->orderBy('updated_at', 'desc')->first();
        $updateSg = date_format(date_create($updateSg->updated_at), 'd-m-Y');

        $updateSrag = DB::table('srag2020')->select('updated_at')
            ->orderBy('updated_at', 'desc')->first();
        $updateSrag = date_format(date_create($updateSrag->updated_at), 'd-m-Y');

        $totalCasos['esus'] = $quantCasosDataSus;
        $totalCasos['sg'] = $quantCasosSg;
        $totalCasos['srag'] = $quantCasosSrag;
        $totalCasos['total'] = $quantCasosDataSus+$quantCasosSg+$quantCasosSrag;

        $quantCurasDataSus = CovidDatasus::where(
            [
              ['municipio', 'Teresina'],
              ['resultadoTeste', 'Positivo'],
              ['dataInicioSintomas','<', $limiteReuperados->format('Y-m-d H:i:s')]
            ]
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->count();

        $quantCurasSg = DB::table('sg2020')->where(
            [
              ['PCR_SARS2', 1],
              ['DT_PRISINT', '<', $limiteReuperados->format('Y-m-d H:i:s')]
            ]
        )->count();
        $quantCurasSrag = DB::table('srag2020')->where(
            [
              ['CLASSI_FIN', 5],
              ['EVOLUCAO', 1]
            ]
        )->count();
        $totalCuras['esus'] = $quantCurasDataSus;
        $totalCuras['sg'] = $quantCurasSg;
        $totalCuras['srag'] = $quantCurasSrag;
        $totalCuras['total'] = $quantCurasDataSus+$quantCurasSg+$quantCurasSrag;

        $quantObito = DB::table('srag2020')->where(
            [
              ['CLASSI_FIN', 5],
              ['EVOLUCAO', 2]
            ]
        )->count();

        $incidencia = round(($totalCasos['total']/$cidade->populacao)*1000, 2);
        $mortalidade = round(($quantObito/$cidade->populacao)*100000, 2);
        $chartSintomas = CovidTheController::dataChartSintomas();
        $chartDigitacao = CovidTheController::dataChartDigitacao();
        $chartNotificacao = CovidTheController::dataChartNotificacao();

        $options['titulo'] = self::$resouceFullName;
        $options['session'] = self::$resouce;
        $options['area'] = 'index';
        $options['updateEsus'] = $updateEsus;
        $options['updateSg'] = $updateSg;
        $options['updateSrag'] = $updateSrag;
        $routeBefore = array_merge(self::$routeBefore, []);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('casos-covid', compact(
            'totalCasos',
            'totalCuras',
            'incidencia',
            'quantObito',
            'mortalidade',
            'chartSintomas',
            'chartDigitacao',
            'chartNotificacao',
            'urls',
            'options'
        ));
    }

    public function obito()
    {

        $dataChartObitoEvolucao = CovidTheController::dataChartObitoEvolucao();
        $dataChartObitoEvolucaoFull = CovidTheController::dataChartObitoEvolucaoFull();
        $dataChartObitoDetalhamento = CovidTheController::dataChartObitoDetalhado();
        $dataChartObitoCor = CovidTheController::dataChartObitoCor();

        //dd($dataChartObitoDetalhamento, $dataChartObitoEvolucao);

        $options['titulo'] = self::$resouceFullName;
        $options['session'] = self::$resouce;
        $options['area'] = 'obito';
        $routeBefore = array_merge(self::$routeBefore, []);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('obitos-covid', compact(
            'dataChartObitoEvolucao',
            'dataChartObitoEvolucaoFull',
            'dataChartObitoDetalhamento',
            'dataChartObitoCor',
            'urls',
            'options'
        ));
    }

    public static function dataChartObitoEvolucao()
    {
        $obitosOcorrencia = DB::table('srag2020')->select(
            DB::raw(
                '"DT_EVOLUCA" as data,
                COUNT("DT_EVOLUCA") AS quantdia,
                SUM(COUNT("DT_EVOLUCA")) OVER (ORDER BY "DT_EVOLUCA") AS totaldia'
            )
        )->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2]
            ]
        )->groupBy(
            'data'
        )->orderBy('data')->get();

        $datas = array();
        $areaObitos = array();
        $barObitos = array();
        $maxObitos = 0;

        for ($i=0; $i < count($obitosOcorrencia); $i++) {
            if ($obitosOcorrencia[$i]->data != null) {
                $datas[$i] = $obitosOcorrencia[$i]->data;

                $areaObitos[$i] = new \stdClass;
                $areaObitos[$i]->x = $obitosOcorrencia[$i]->data;
                $areaObitos[$i]->y = $obitosOcorrencia[$i]->totaldia;

                $barObitos[$i] = new \stdClass;
                $barObitos[$i]->x = $obitosOcorrencia[$i]->data;
                $barObitos[$i]->y = $obitosOcorrencia[$i]->quantdia;

                if ($maxObitos < $obitosOcorrencia[$i]->quantdia) {
                    $maxObitos = $obitosOcorrencia[$i]->quantdia;
                }
            }
        }

        $yaxis0 = $maxObitos;
        $yaxis1 = $obitosOcorrencia[count($obitosOcorrencia) - 1]->totaldia;
        $chartObitos = new \stdClass;
        $chartObitos->yaxis0 = $yaxis0;
        $chartObitos->yaxis1 = $yaxis1;
        $chartObitos->area = $areaObitos;
        $chartObitos->bar = $barObitos;
        $chartObitos->datas = $datas;

        return $chartObitos;
    }

    public static function dataChartObitoEvolucaoFull()
    {
        $obitosOcorrencia = DB::table('sragfull2020')->select(
            DB::raw(
                '"DT_EVOLUCA" as data,
                COUNT("DT_EVOLUCA") AS quantdia,
                SUM(COUNT("DT_EVOLUCA")) OVER (ORDER BY "DT_EVOLUCA") AS totaldia'
            )
        )->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2]
            ]
        )->groupBy(
            'data'
        )->orderBy('data')->get();

        $datas = array();
        $areaObitos = array();
        $barObitos = array();
        $maxObitos = 0;

        for ($i=0; $i < count($obitosOcorrencia); $i++) {
            if ($obitosOcorrencia[$i]->data != null) {
                $datas[$i] = $obitosOcorrencia[$i]->data;

                $areaObitos[$i] = new \stdClass;
                $areaObitos[$i]->x = $obitosOcorrencia[$i]->data;
                $areaObitos[$i]->y = $obitosOcorrencia[$i]->totaldia;

                $barObitos[$i] = new \stdClass;
                $barObitos[$i]->x = $obitosOcorrencia[$i]->data;
                $barObitos[$i]->y = $obitosOcorrencia[$i]->quantdia;

                if ($maxObitos < $obitosOcorrencia[$i]->quantdia) {
                    $maxObitos = $obitosOcorrencia[$i]->quantdia;
                }
            }
        }

        $yaxis0 = $maxObitos;
        $yaxis1 = $obitosOcorrencia[count($obitosOcorrencia) - 1]->totaldia;
        $chartObitos = new \stdClass;
        $chartObitos->yaxis0 = $yaxis0;
        $chartObitos->yaxis1 = $yaxis1;
        $chartObitos->area = $areaObitos;
        $chartObitos->bar = $barObitos;
        $chartObitos->datas = $datas;

        return $chartObitos;
    }

    public static function dataChartObitoDetalhado()
    {
        //Quantidade de todos os obitos
        $quantObitos = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2]
            ]
        )->count();

        //Quantidade de todos os obitos com comorbidade
        $quantObitosComorbidade = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
            ]
        )->where(
            function ($q) {
                $q->orWhere('PUERPERA', 1)
                    ->orWhere('CARDIOPATI', 1)
                    ->orWhere('HEMATOLOGI', 1)
                    ->orWhere('SIND_DOWN', 1)
                    ->orWhere('HEPATICA', 1)
                    ->orWhere('ASMA', 1)
                    ->orWhere('DIABETES', 1)
                    ->orWhere('NEUROLOGIC', 1)
                    ->orWhere('PNEUMOPATI', 1)
                    ->orWhere('IMUNODEPRE', 1)
                    ->orWhere('RENAL', 1)
                    ->orWhere('OBESIDADE', 1)
                    ->orWhere('OBES_IMC', 1)
                    ->orWhere('OUT_MORBI', 1);
            }
        )->count();

        //Quantidade de todos os obitos de puerpera
        $quantObitosPuerpera = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['PUERPERA', 1]
            ]
        )->count();

        //Quantidade de todos os obitos de cardiopatas
        $quantObitosCardiopata = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['CARDIOPATI', 1]
            ]
        )->count();

        //Quantidade de todos os obitos de Hematológica Crônica
        $quantObitosHermato = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['HEMATOLOGI', 1]
            ]
        )->count();

        //Quantidade de todos os obitos de Síndrome de Down
        $quantObitosDown = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['SIND_DOWN', 1]
            ]
        )->count();

        //Quantidade de todos os obitos com Doença Hepática Crônica
        $quantObitosHepatica = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['HEPATICA', 1]
            ]
        )->count();

        //Quantidade de todos os obitos com Asma
        $quantObitosAsma = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['ASMA', 1]
            ]
        )->count();

        //Quantidade de todos os obitos com Diabetes
        $quantObitosDiabetes = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['DIABETES', 1]
            ]
        )->count();


        //Quantidade de todos os obitos com Doenças Neurológica Crônica
        $quantObitosNeuro = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['NEUROLOGIC', 1]
            ]
        )->count();

        //Quantidade de todos os obitos com Pneumatopatia Crônica
        $quantObitosPneumo = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['PNEUMOPATI', 1]
            ]
        )->count();

        //Quantidade de todos os obitos com Imunodeficêincia ou Imunodepressão
        $quantObitosImuno = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['IMUNODEPRE', 1]
            ]
        )->count();

        //Quantidade de todos os obitos com Doença Renal Crônica
        $quantObitosRenal = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['RENAL', 1]
            ]
        )->count();

        //Quantidade de todos os obitos com Obesidade
        $quantObitosObesidade = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['OBESIDADE', 1]
            ]
        )->count();

        //Quantidade de todos os obitos com Outras Comorbidades
        $quantObitosOutraComorbidade = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['OUT_MORBI', 1]
            ]
        )->count();

        $obitosDetalhados = array();
        $obitosDetalhados[0] = new stdClass;
        $obitosDetalhados[0]->x = "Total";
        $obitosDetalhados[0]->y = $quantObitos;

        $obitosDetalhados[1] = new stdClass;
        $obitosDetalhados[1]->x = "Pelo menos um fatores de risco";
        $obitosDetalhados[1]->y = $quantObitosComorbidade;

        $obitosDetalhados[2] = new stdClass;
        $obitosDetalhados[2]->x = "Puérpera";
        $obitosDetalhados[2]->y = $quantObitosPuerpera;

        $obitosDetalhados[3] = new stdClass;
        $obitosDetalhados[3]->x = "Doença Cardiovascular Crônica";
        $obitosDetalhados[3]->y = $quantObitosCardiopata;

        $obitosDetalhados[4] = new stdClass;
        $obitosDetalhados[4]->x = "Doença Hematológica Crônica";
        $obitosDetalhados[4]->y = $quantObitosHermato;

        $obitosDetalhados[5] = new stdClass;
        $obitosDetalhados[5]->x = "Síndrome de Down";
        $obitosDetalhados[5]->y = $quantObitosDown;

        $obitosDetalhados[6] = new stdClass;
        $obitosDetalhados[6]->x = "Doença Hepática Crônica";
        $obitosDetalhados[6]->y = $quantObitosHepatica;

        $obitosDetalhados[7] = new stdClass;
        $obitosDetalhados[7]->x = "Asma";
        $obitosDetalhados[7]->y = $quantObitosAsma;

        $obitosDetalhados[8] = new stdClass;
        $obitosDetalhados[8]->x = "Diabetes mellitus";
        $obitosDetalhados[8]->y = $quantObitosDiabetes;

        $obitosDetalhados[9] = new stdClass;
        $obitosDetalhados[9]->x = "Doença Neurológica Crônica";
        $obitosDetalhados[9]->y = $quantObitosNeuro;

        $obitosDetalhados[10] = new stdClass;
        $obitosDetalhados[10]->x = "Outra Pneumopatia Crônica";
        $obitosDetalhados[10]->y = $quantObitosPneumo;

        $obitosDetalhados[11] = new stdClass;
        $obitosDetalhados[11]->x = "Imunodeficiência/Imunodepressão";
        $obitosDetalhados[11]->y = $quantObitosImuno;

        $obitosDetalhados[12] = new stdClass;
        $obitosDetalhados[12]->x = "Doença Renal Crônica";
        $obitosDetalhados[12]->y = $quantObitosRenal;

        $obitosDetalhados[13] = new stdClass;
        $obitosDetalhados[13]->x = "Obesidade";
        $obitosDetalhados[13]->y = $quantObitosObesidade;

        $obitosDetalhados[14] = new stdClass;
        $obitosDetalhados[14]->x = "Outros";
        $obitosDetalhados[14]->y = $quantObitosOutraComorbidade;

        $indice = 0;

        if ($quantObitosComorbidade > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Pelo menos um fatores de risco";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosComorbidade/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosPuerpera > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Puérpera";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosPuerpera/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosCardiopata > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Doença Cardiovascular Crônica";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosCardiopata/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosHermato > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Doença Hematológica Crônica";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosHermato/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosDown > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Síndrome de Down";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosDown/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosHepatica > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Doença Hepática Crônica";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosHepatica/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosAsma > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Asma";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosAsma/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosDiabetes > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Diabetes mellitus";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosDiabetes/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosNeuro > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Doença Neurológica Crônica";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosNeuro/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosPneumo > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Outra Pneumopatia Crônica";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosPneumo/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosImuno > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Imunodeficiência/Imunodepressão";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosImuno/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosRenal > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Doença Renal Crônica";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosRenal/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosObesidade > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Obesidade";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosObesidade/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosOutraComorbidade > 0) {
            $obitosDetalhadosPorcent[$indice] = new stdClass;
            $obitosDetalhadosPorcent[$indice]->x = "Outros";
            $obitosDetalhadosPorcent[$indice]->y = round($quantObitosOutraComorbidade/($quantObitos/100), 2);
            $indice++;
        }

        $detalhados = new stdClass;
        $detalhados->absoluto = $obitosDetalhados;
        $detalhados->percent = $obitosDetalhadosPorcent;

        return $detalhados;
    }

    public static function dataChartObitoCor()
    {
        //Quantidade de todos os obitos
        $quantObitos = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2]
            ]
        )->count();

        //Quantidade de todos os obitos de brancos
        $quantObitosBrancos = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['CS_RACA', 1]
            ]
        )->count();

        //Quantidade de todos os obitos de pretos
        $quantObitosPretos = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['CS_RACA', 2]
            ]
        )->count();

        //Quantidade de todos os obitos de amarelos
        $quantObitosAmarelos = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['CS_RACA', 3]
            ]
        )->count();

        //Quantidade de todos os obitos de amarelos
        $quantObitosPardos = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['CS_RACA', 4]
            ]
        )->count();

        //Quantidade de todos os obitos de amarelos
        $quantObitosIndigenas = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['CS_RACA', 5]
            ]
        )->count();

        //Quantidade de todos os obitos de amarelos
        $quantObitosIgnorados = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['CS_RACA', 9]
            ]
        )->count();

        $indice = 0;

        if ($quantObitosBrancos > 0) {
            $obitoCor[$indice] = new stdClass;
            $obitoCor[$indice]->x = "Branca";
            $obitoCor[$indice]->y = round($quantObitosBrancos/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosPretos > 0) {
            $obitoCor[$indice] = new stdClass;
            $obitoCor[$indice]->x = "Preta";
            $obitoCor[$indice]->y = round($quantObitosPretos/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosAmarelos > 0) {
            $obitoCor[$indice] = new stdClass;
            $obitoCor[$indice]->x = "Amarela";
            $obitoCor[$indice]->y = round($quantObitosAmarelos/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosPardos > 0) {
            $obitoCor[$indice] = new stdClass;
            $obitoCor[$indice]->x = "Parda";
            $obitoCor[$indice]->y = round($quantObitosPardos/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosIndigenas > 0) {
            $obitoCor[$indice] = new stdClass;
            $obitoCor[$indice]->x = "Indígena";
            $obitoCor[$indice]->y = round($quantObitosIndigenas/($quantObitos/100), 2);
            $indice++;
        }

        if ($quantObitosIgnorados > 0) {
            $obitoCor[$indice] = new stdClass;
            $obitoCor[$indice]->x = "Não declarado";
            $obitoCor[$indice]->y = round($quantObitosIgnorados/($quantObitos/100), 2);
            $indice++;
        }


        return $obitoCor;
    }

    public static function dataChartSintomas()
    {
        $cidade = Cidade::whereRaw(
            'LEFT( CAST(co_ibge AS TEXT), 6) = ?', ['221100']
        )->first();

        $casosData = CovidDatasus::select(
            DB::raw('TO_CHAR("dataInicioSintomas" :: DATE, \'yyyy-mm-dd\') as data')
        )->where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo']
            ]
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->orderBy('data')->first();

        $casosSgData = DB::table('sg2020')->select(
            DB::raw('"DT_PRISINT" as data')
        )->where('PCR_SARS2', 1)
            ->orderBy('data')
            ->first();

        $casosSragData = DB::table('srag2020')->select(
            DB::raw('"DT_SIN_PRI" as data')
        )->where('CLASSI_FIN', 5)
            ->orderBy('data')
            ->first();

        $casosData = DateTime::createFromFormat('Y-m-d', $casosData->data);
        $casosSgData = DateTime::createFromFormat('Y-m-d', $casosSgData->data);
        $casosSragData = DateTime::createFromFormat('Y-m-d', $casosSragData->data);

        $dataInitial = min($casosData, $casosSgData, $casosSragData);

        $dataFinal = new DateTime();
        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $casosDatasus = CovidDatasus::select(
            DB::raw(
                'DATE_TRUNC(\'day\',"dataInicioSintomas") AS datetime,
                TO_CHAR("dataInicioSintomas" :: DATE, \'yyyy-mm-dd\') as data,
                COUNT(DATE_TRUNC(\'day\',"dataInicioSintomas")) quantdia,
                SUM(COUNT(DATE_TRUNC(\'day\',"dataInicioSintomas")))
                    OVER (ORDER BY DATE_TRUNC(\'day\',"dataInicioSintomas"))
                AS totaldia'
            )
        )->where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo']
            ]
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->groupBy(
            'datetime',
            'data'
        )->orderBy('datetime')->get();

        $casosSg = DB::table('sg2020')->select(
            DB::raw(
                '"DT_PRISINT" as data,
                COUNT("DT_PRISINT") AS quantdia,
                SUM(COUNT("DT_PRISINT")) OVER (ORDER BY "DT_PRISINT") AS totaldia'
            )
        )->where(
            'PCR_SARS2', 1
        )->groupBy(
            'data'
        )->orderBy('data')->get();

        $casosSrag = DB::table('srag2020')->select(
            DB::raw(
                '"DT_SIN_PRI" as data,
                COUNT("DT_SIN_PRI") AS quantdia,
                SUM(COUNT("DT_SIN_PRI")) OVER (ORDER BY "DT_SIN_PRI") AS totaldia'
            )
        )->where(
            'CLASSI_FIN', 5
        )->groupBy(
            'data'
        )->orderBy('data')->get();

        $dataChartArea = array();
        $barCasosDatasus = array();
        $barCasosSg = array();
        $barCasosSrag = array();
        //$count = count($casosDatasus);
        $datasCasosDatasus = array();
        $datasCasosSrag = array();
        $datasCasosSg = array();

        $maxCasosDataSus = 0;
        $maxCasosSg = 0;
        $maxCasosSrag = 0;

        for ($i = 0; $i < count($casosDatasus); $i++) {
            if ($casosDatasus[$i]->data == null) {
                continue;
            }
            $datasCasosDatasus[$i] = $casosDatasus[$i]->data;

            $areaCasosDatasus[$i] = new \stdClass;
            $areaCasosDatasus[$i]->x = $casosDatasus[$i]->data;
            $areaCasosDatasus[$i]->y = $casosDatasus[$i]->totaldia;

            $barCasosDatasus[$i] = new \stdClass;
            $barCasosDatasus[$i]->x = $casosDatasus[$i]->data;
            $barCasosDatasus[$i]->y = $casosDatasus[$i]->quantdia;

            if ($maxCasosDataSus < $casosDatasus[$i]->quantdia) {
                $maxCasosDataSus = $casosDatasus[$i]->quantdia;
            }
        }

        for ($i=0; $i < count($casosSg); $i++) {
            $datasCasosSg[$i] = $casosSg[$i]->data;

            $areaCasosSg[$i] = new \stdClass;
            $areaCasosSg[$i]->x = $casosSg[$i]->data;
            $areaCasosSg[$i]->y = $casosSg[$i]->totaldia;

            $barCasosSg[$i] = new \stdClass;
            $barCasosSg[$i]->x = $casosSg[$i]->data;
            $barCasosSg[$i]->y = $casosSg[$i]->quantdia;

            if ($maxCasosSg < $casosSg[$i]->quantdia) {
                $maxCasosSg = $casosSg[$i]->quantdia;
            }
        }

        for ($i=0; $i < count($casosSrag); $i++) {
            $datasCasosSrag[$i] = $casosSrag[$i]->data;

            $areaCasosSrag[$i] = new \stdClass;
            $areaCasosSrag[$i]->x = $casosSrag[$i]->data;
            $areaCasosSrag[$i]->y = $casosSrag[$i]->totaldia;

            $barCasosSrag[$i] = new \stdClass;
            $barCasosSrag[$i]->x = $casosSrag[$i]->data;
            $barCasosSrag[$i]->y = $casosSrag[$i]->quantdia;

            if ($maxCasosSrag < $casosSrag[$i]->quantdia) {
                $maxCasosSrag = $casosSrag[$i]->quantdia;
            }
        }

        $area = array();
        for ($i=0; $i < count($datas) ; $i++) {

            $area[$i] = new \stdClass;
            $area[$i]->x = $datas[$i];

            if ($i > 0) {
                $area[$i]->y = $area[$i-1]->y;
            } else {
                $area[$i]->y = 0;
            }

            $indexDs = array_search($datas[$i], $datasCasosDatasus);
            if ($indexDs !== false) {
                $area[$i]->y = $area[$i]->y + $casosDatasus[$indexDs]->quantdia;
            }

            $indexSg = array_search($datas[$i], $datasCasosSg);
            if ($indexSg !== false) {
                $area[$i]->y = $area[$i]->y + $casosSg[$indexSg]->quantdia;
            }

            $indexSrag = array_search($datas[$i], $datasCasosSrag);
            if ($indexSrag !== false) {
                $area[$i]->y = $area[$i]->y + $casosSrag[$indexSrag]->quantdia;
            }

        }
        //dd($area);

        $yaxis0 = $maxCasosDataSus+$maxCasosSg+$maxCasosSrag;
        $yaxis1 =
            $casosDatasus[count($casosDatasus) - 1]->totaldia +
            $casosSg[count($casosSg) - 1]->totaldia +
            $casosSrag[count($casosSrag) - 1]->totaldia;

        $chartCovid = new \stdClass;

        $chartCovid->yaxis0 = $yaxis0;
        $chartCovid->yaxis1 = $yaxis1;
        $chartCovid->area = $dataChartArea;
        $chartCovid->barEsus = $barCasosDatasus;
        $chartCovid->barSg = $barCasosSg;
        $chartCovid->barSrag = $barCasosSrag;
        $chartCovid->area = $area;
        //$chartCovid->areaEsus = $areaCasosDatasus;
        //$chartCovid->areaSg = $areaCasosSg;
        //$chartCovid->areaSrag = $areaCasosSrag;

        $chartCovid->datas = $datas;

        return $chartCovid;

    }

    public static function dataChartDigitacao()
    {
        $cidade = Cidade::whereRaw(
            'LEFT( CAST(co_ibge AS TEXT), 6) = ?', ['221100']
        )->first();

        $casosData = CovidDatasus::select(
            DB::raw('TO_CHAR("_created_at_datasus" :: DATE, \'yyyy-mm-dd\') as data')
        )->where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo']
            ]
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->orderBy('data')->first();

        $casosSgData = DB::table('sg2020')->select(
            DB::raw('"DT_DIGITA" as data')
        )->where('PCR_SARS2', 1)
            ->orderBy('data')
            ->first();

        $casosSragData = DB::table('srag2020')->select(
            DB::raw('"DT_DIGITA" as data')
        )->where('CLASSI_FIN', 5)
            ->orderBy('data')
            ->first();

        $casosData = DateTime::createFromFormat('Y-m-d', $casosData->data);
        $casosSgData = DateTime::createFromFormat('Y-m-d', $casosSgData->data);
        $casosSragData = DateTime::createFromFormat('Y-m-d', $casosSragData->data);

        $dataInitial = min($casosData, $casosSgData, $casosSragData);

        $dataFinal = new DateTime();
        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $casosDatasus = CovidDatasus::select(
            DB::raw(
                'DATE_TRUNC(\'day\',"_created_at_datasus") AS datetime,
                TO_CHAR("_created_at_datasus" :: DATE, \'yyyy-mm-dd\') as data,
                COUNT(DATE_TRUNC(\'day\',"_created_at_datasus")) quantdia,
                SUM(COUNT(DATE_TRUNC(\'day\',"_created_at_datasus")))
                    OVER (ORDER BY DATE_TRUNC(\'day\',"_created_at_datasus"))
                AS totaldia'
            )
        )->where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo']
            ]
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->groupBy(
            'datetime',
            'data'
        )->orderBy('datetime')->get();

        $casosSg = DB::table('sg2020')->select(
            DB::raw(
                '"DT_DIGITA" as data,
                COUNT("DT_DIGITA") AS quantdia,
                SUM(COUNT("DT_DIGITA")) OVER (ORDER BY "DT_DIGITA") AS totaldia'
            )
        )->where(
            'PCR_SARS2', 1
        )->groupBy(
            'data'
        )->orderBy('data')->get();

        $casosSrag = DB::table('srag2020')->select(
            DB::raw(
                '"DT_DIGITA" as data,
                COUNT("DT_DIGITA") AS quantdia,
                SUM(COUNT("DT_DIGITA")) OVER (ORDER BY "DT_DIGITA") AS totaldia'
            )
        )->where(
            'CLASSI_FIN', 5
        )->groupBy(
            'data'
        )->orderBy('data')->get();

        $dataChartArea = array();
        $barCasosDatasus = array();
        $barCasosSg = array();
        $barCasosSrag = array();
        //$count = count($casosDatasus);
        $datasCasosDatasus = array();
        $datasCasosSrag = array();
        $datasCasosSg = array();

        $maxCasosDataSus = 0;
        $maxCasosSg = 0;
        $maxCasosSrag = 0;

        for ($i = 0; $i < count($casosDatasus); $i++) {
            if ($casosDatasus[$i]->data == null) {
                continue;
            }
            $datasCasosDatasus[$i] = $casosDatasus[$i]->data;

            $areaCasosDatasus[$i] = new \stdClass;
            $areaCasosDatasus[$i]->x = $casosDatasus[$i]->data;
            $areaCasosDatasus[$i]->y = $casosDatasus[$i]->totaldia;

            $barCasosDatasus[$i] = new \stdClass;
            $barCasosDatasus[$i]->x = $casosDatasus[$i]->data;
            $barCasosDatasus[$i]->y = $casosDatasus[$i]->quantdia;

            if ($maxCasosDataSus < $casosDatasus[$i]->quantdia) {
                $maxCasosDataSus = $casosDatasus[$i]->quantdia;
            }
        }

        for ($i=0; $i < count($casosSg); $i++) {
            $datasCasosSg[$i] = $casosSg[$i]->data;

            $areaCasosSg[$i] = new \stdClass;
            $areaCasosSg[$i]->x = $casosSg[$i]->data;
            $areaCasosSg[$i]->y = $casosSg[$i]->totaldia;

            $barCasosSg[$i] = new \stdClass;
            $barCasosSg[$i]->x = $casosSg[$i]->data;
            $barCasosSg[$i]->y = $casosSg[$i]->quantdia;

            if ($maxCasosSg < $casosSg[$i]->quantdia) {
                $maxCasosSg = $casosSg[$i]->quantdia;
            }
        }

        for ($i=0; $i < count($casosSrag); $i++) {
            $datasCasosSrag[$i] = $casosSrag[$i]->data;

            $areaCasosSrag[$i] = new \stdClass;
            $areaCasosSrag[$i]->x = $casosSrag[$i]->data;
            $areaCasosSrag[$i]->y = $casosSrag[$i]->totaldia;

            $barCasosSrag[$i] = new \stdClass;
            $barCasosSrag[$i]->x = $casosSrag[$i]->data;
            $barCasosSrag[$i]->y = $casosSrag[$i]->quantdia;

            if ($maxCasosSrag < $casosSrag[$i]->quantdia) {
                $maxCasosSrag = $casosSrag[$i]->quantdia;
            }
        }

        $area = array();
        for ($i=0; $i < count($datas) ; $i++) {

            $area[$i] = new \stdClass;
            $area[$i]->x = $datas[$i];

            if ($i > 0) {
                $area[$i]->y = $area[$i-1]->y;
            } else {
                $area[$i]->y = 0;
            }

            $indexDs = array_search($datas[$i], $datasCasosDatasus);
            if ($indexDs !== false) {
                $area[$i]->y = $area[$i]->y + $casosDatasus[$indexDs]->quantdia;
            }

            $indexSg = array_search($datas[$i], $datasCasosSg);
            if ($indexSg !== false) {
                $area[$i]->y = $area[$i]->y + $casosSg[$indexSg]->quantdia;
            }

            $indexSrag = array_search($datas[$i], $datasCasosSrag);
            if ($indexSrag !== false) {
                $area[$i]->y = $area[$i]->y + $casosSrag[$indexSrag]->quantdia;
            }

        }
        //dd($area);

        $yaxis0 = $maxCasosDataSus+$maxCasosSg+$maxCasosSrag;
        $yaxis1 =
            $casosDatasus[count($casosDatasus) - 1]->totaldia +
            $casosSg[count($casosSg) - 1]->totaldia +
            $casosSrag[count($casosSrag) - 1]->totaldia;

        $chartCovid = new \stdClass;

        $chartCovid->yaxis0 = $yaxis0;
        $chartCovid->yaxis1 = $yaxis1;
        $chartCovid->area = $dataChartArea;
        $chartCovid->barEsus = $barCasosDatasus;
        $chartCovid->barSg = $barCasosSg;
        $chartCovid->barSrag = $barCasosSrag;
        $chartCovid->area = $area;
        //$chartCovid->areaEsus = $areaCasosDatasus;
        //$chartCovid->areaSg = $areaCasosSg;
        //$chartCovid->areaSrag = $areaCasosSrag;

        $chartCovid->datas = $datas;

        return $chartCovid;

    }

    public static function dataChartNotificacao()
    {
        $cidade = Cidade::whereRaw(
            'LEFT( CAST(co_ibge AS TEXT), 6) = ?', ['221100']
        )->first();

        $casosData = CovidDatasus::select(
            DB::raw('TO_CHAR("dataNotificacao" :: DATE, \'yyyy-mm-dd\') as data')
        )->where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo']
            ]
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->orderBy('data')->first();

        $casosSgData = DB::table('sg2020')->select(
            DB::raw('"DT_PREENC" as data')
        )->where('PCR_SARS2', 1)
            ->orderBy('data')
            ->first();

        $casosSragData = DB::table('srag2020')->select(
            DB::raw('"DT_NOTIFIC" as data')
        )->where('CLASSI_FIN', 5)
            ->orderBy('data')
            ->first();

        $casosData = DateTime::createFromFormat('Y-m-d', $casosData->data);
        $casosSgData = DateTime::createFromFormat('Y-m-d', $casosSgData->data);
        $casosSragData = DateTime::createFromFormat('Y-m-d', $casosSragData->data);

        $dataInitial = min($casosData, $casosSgData, $casosSragData);

        $dataFinal = new DateTime();
        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $casosDatasus = CovidDatasus::select(
            DB::raw(
                'DATE_TRUNC(\'day\',"dataNotificacao") AS datetime,
                TO_CHAR("dataNotificacao" :: DATE, \'yyyy-mm-dd\') as data,
                COUNT(DATE_TRUNC(\'day\',"dataNotificacao")) quantdia,
                SUM(COUNT(DATE_TRUNC(\'day\',"dataNotificacao")))
                    OVER (ORDER BY DATE_TRUNC(\'day\',"dataNotificacao"))
                AS totaldia'
            )
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo']
            ]
        )->groupBy(
            'datetime',
            'data'
        )->orderBy('datetime')->get();

        $casosSg = DB::table('sg2020')->select(
            DB::raw(
                '"DT_PREENC" as data,
                COUNT("DT_PREENC") AS quantdia,
                SUM(COUNT("DT_PREENC")) OVER (ORDER BY "DT_PREENC") AS totaldia'
            )
        )->where(
            'PCR_SARS2', 1
        )->groupBy(
            'data'
        )->orderBy('data')->get();

        $casosSrag = DB::table('srag2020')->select(
            DB::raw(
                '"DT_NOTIFIC" as data,
                COUNT("DT_NOTIFIC") AS quantdia,
                SUM(COUNT("DT_NOTIFIC")) OVER (ORDER BY "DT_NOTIFIC") AS totaldia'
            )
        )->where(
            'CLASSI_FIN', 5
        )->groupBy(
            'data'
        )->orderBy('data')->get();

        $dataChartArea = array();
        $barCasosDatasus = array();
        $barCasosSg = array();
        $barCasosSrag = array();
        //$count = count($casosDatasus);
        $datasCasosDatasus = array();
        $datasCasosSrag = array();
        $datasCasosSg = array();

        $maxCasosDataSus = 0;
        $maxCasosSg = 0;
        $maxCasosSrag = 0;

        for ($i = 0; $i < count($casosDatasus); $i++) {
            if ($casosDatasus[$i]->data == null) {
                continue;
            }
            $datasCasosDatasus[$i] = $casosDatasus[$i]->data;

            $areaCasosDatasus[$i] = new \stdClass;
            $areaCasosDatasus[$i]->x = $casosDatasus[$i]->data;
            $areaCasosDatasus[$i]->y = $casosDatasus[$i]->totaldia;

            $barCasosDatasus[$i] = new \stdClass;
            $barCasosDatasus[$i]->x = $casosDatasus[$i]->data;
            $barCasosDatasus[$i]->y = $casosDatasus[$i]->quantdia;

            if ($maxCasosDataSus < $casosDatasus[$i]->quantdia) {
                $maxCasosDataSus = $casosDatasus[$i]->quantdia;
            }
        }

        for ($i=0; $i < count($casosSg); $i++) {
            $datasCasosSg[$i] = $casosSg[$i]->data;

            $areaCasosSg[$i] = new \stdClass;
            $areaCasosSg[$i]->x = $casosSg[$i]->data;
            $areaCasosSg[$i]->y = $casosSg[$i]->totaldia;

            $barCasosSg[$i] = new \stdClass;
            $barCasosSg[$i]->x = $casosSg[$i]->data;
            $barCasosSg[$i]->y = $casosSg[$i]->quantdia;

            if ($maxCasosSg < $casosSg[$i]->quantdia) {
                $maxCasosSg = $casosSg[$i]->quantdia;
            }
        }

        for ($i=0; $i < count($casosSrag); $i++) {
            $datasCasosSrag[$i] = $casosSrag[$i]->data;

            $areaCasosSrag[$i] = new \stdClass;
            $areaCasosSrag[$i]->x = $casosSrag[$i]->data;
            $areaCasosSrag[$i]->y = $casosSrag[$i]->totaldia;

            $barCasosSrag[$i] = new \stdClass;
            $barCasosSrag[$i]->x = $casosSrag[$i]->data;
            $barCasosSrag[$i]->y = $casosSrag[$i]->quantdia;

            if ($maxCasosSrag < $casosSrag[$i]->quantdia) {
                $maxCasosSrag = $casosSrag[$i]->quantdia;
            }
        }

        $area = array();
        for ($i=0; $i < count($datas) ; $i++) {

            $area[$i] = new \stdClass;
            $area[$i]->x = $datas[$i];

            if ($i > 0) {
                $area[$i]->y = $area[$i-1]->y;
            } else {
                $area[$i]->y = 0;
            }

            $indexDs = array_search($datas[$i], $datasCasosDatasus);
            if ($indexDs !== false) {
                $area[$i]->y = $area[$i]->y + $casosDatasus[$indexDs]->quantdia;
            }

            $indexSg = array_search($datas[$i], $datasCasosSg);
            if ($indexSg !== false) {
                $area[$i]->y = $area[$i]->y + $casosSg[$indexSg]->quantdia;
            }

            $indexSrag = array_search($datas[$i], $datasCasosSrag);
            if ($indexSrag !== false) {
                $area[$i]->y = $area[$i]->y + $casosSrag[$indexSrag]->quantdia;
            }

        }
        //dd($area);

        $yaxis0 = $maxCasosDataSus+$maxCasosSg+$maxCasosSrag;
        $yaxis1 =
            $casosDatasus[count($casosDatasus) - 1]->totaldia +
            $casosSg[count($casosSg) - 1]->totaldia +
            $casosSrag[count($casosSrag) - 1]->totaldia;

        $chartCovid = new \stdClass;

        $chartCovid->yaxis0 = $yaxis0;
        $chartCovid->yaxis1 = $yaxis1;
        $chartCovid->area = $dataChartArea;
        $chartCovid->barEsus = $barCasosDatasus;
        $chartCovid->barSg = $barCasosSg;
        $chartCovid->barSrag = $barCasosSrag;
        $chartCovid->area = $area;
        //$chartCovid->areaEsus = $areaCasosDatasus;
        //$chartCovid->areaSg = $areaCasosSg;
        //$chartCovid->areaSrag = $areaCasosSrag;

        $chartCovid->datas = $datas;

        return $chartCovid;

    }

    public static function dataChartSintomasLeve()
    {
        $totalEsus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo']
            ]
        )->count();

        $febreEsus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['sintomas', 'LIKE', '%Febre%']
            ]
        )->count();

        $gargantaEsus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['sintomas', 'LIKE', '%Dor de Garganta%']
            ]
        )->count();

        $tosseEsus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['sintomas', 'LIKE', '%Tosse%']
            ]
        )->count();

        $dispneiaEsus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['sintomas', 'LIKE', '%Dispneia%']
            ]
        )->count();

        $outrosEsus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['sintomas', 'LIKE', '%Outros%']
            ]
        )->count();

        $diarreiaEsus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%Diarreia%']
            ]
        )->count();

        $diarreiaEsus += CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%DIARREIA%']
            ]
        )->count();

        $cefaleiaEsus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%CEFALEIA%']
            ]
        )->count();

        $cefaleiaEsus += CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%cefaleia%']
            ]
        )->count();

        $cefaleiaEsus += CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%CEFALÉIA%']
            ]
        )->count();

        $corizaEsus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%CORIZA%']
            ]
        )->count();

        $corizaEsus += CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%coriza%']
            ]
        )->count();

        $mialgiaEsus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%mialgia%']
            ]
        )->count();

        $mialgiaEsus += CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%MIALGIA%']
            ]
        )->count();

        $anosmiaEsus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%anosmia%']
            ]
        )->count();

        $anosmiaEsus += CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%ANOSMIA%']
            ]
        )->count();

        $vomitoEsus = CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%vomito%']
            ]
        )->count();

        $vomitoEsus += CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%VOMITO%']
            ]
        )->count();

        $vomitoEsus += CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%VÔMITO%']
            ]
        )->count();

        $vomitoEsus += CovidDatasus::where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['outrosSintomas', 'LIKE', '%vômito%']
            ]
        )->count();

        $totalSg = DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1]
            ]
        )->count();

        $febreSg = DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['FEBRE', 1]
            ]
        )->count();

        $tosseSg = DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['TOSSE', 1]
            ]
        )->count();

        $gargantaSg = DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['DOR_GARGAN', 1]
            ]
        )->count();

        $outrosSg = DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINT', 1]
            ]
        )->count();

        $diarreiaSg = DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%Diarreia%']
            ]
        )->count();

        $diarreiaSg += DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%DIARREIA%']
            ]
        )->count();

        $cefaleiaSg = DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%CEFALEIA%']
            ]
        )->count();

        $cefaleiaSg += DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%cefaleia%']
            ]
        )->count();

        $corizaSg = DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%CORIZA%']
            ]
        )->count();

        $corizaSg += DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%coriza%']
            ]
        )->count();

        $dispneiaSg = DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%Dispneia%']
            ]
        )->count();

        $dispneiaSg += DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%DISPNEIA%']
            ]
        )->count();

        $mialgiaSg = DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%mialgia%']
            ]
        )->count();

        $mialgiaSg += DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%MIALGIA%']
            ]
        )->count();

        $anosmiaSg = DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%anosmia%']
            ]
        )->count();

        $anosmiaSg += DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%ANOSMIA%']
            ]
        )->count();

        $vomitoSg = DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%vomito%']
            ]
        )->count();

        $vomitoSg += DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%VOMITO%']
            ]
        )->count();

        $vomitoSg += DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%VÔMITO%']
            ]
        )->count();

        $vomitoSg += DB::table('sg2020')->where(
            [
                ['PCR_SARS2', 1],
                ['OUT_SINTD', 'LIKE', '%vômito%']
            ]
        )->count();



        $totalLeves = $totalEsus + $totalSg;
        $febreLeves = $febreEsus + $febreSg;
        $tosseLeves = $tosseEsus + $tosseSg;
        $gargantaLeves = $gargantaEsus + $gargantaSg;
        $dispneiaLeves = $dispneiaEsus + $dispneiaSg;
        $outrosLeves = $outrosEsus + $outrosSg;
        $diarreiaLeves = $diarreiaEsus + $diarreiaSg;
        $cefaleiaLeves = $cefaleiaEsus + $cefaleiaSg;
        $corizaLeves = $corizaEsus + $corizaSg;
        $mialgiaLeves = $mialgiaEsus + $mialgiaSg;
        $anosmiaLeves = $anosmiaEsus + $anosmiaSg;
        $vomitoLeves = $vomitoEsus + $vomitoSg;

        $indice = 0;

        if ($febreLeves > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Febre";
            $sintomas[$indice]->y = round($febreLeves/($totalLeves/100), 2);
            $indice++;
        }

        if ($tosseLeves > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Tosse";
            $sintomas[$indice]->y = round($tosseLeves/($totalLeves/100), 2);
            $indice++;
        }

        if ($gargantaLeves > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Dor de Garganta";
            $sintomas[$indice]->y = round($gargantaLeves/($totalLeves/100), 2);
            $indice++;
        }

        if ($dispneiaLeves > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Dispneia";
            $sintomas[$indice]->y = round($dispneiaLeves/($totalLeves/100), 2);
            $indice++;
        }

        if ($diarreiaLeves > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Diarreia";
            $sintomas[$indice]->y = round($diarreiaLeves/($totalLeves/100), 2);
            $indice++;
        }

        if ($cefaleiaLeves > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Cefaleia";
            $sintomas[$indice]->y = round($cefaleiaLeves/($totalLeves/100), 2);
            $indice++;
        }

        if ($corizaLeves > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Coriza";
            $sintomas[$indice]->y = round($corizaLeves/($totalLeves/100), 2);
            $indice++;
        }

        if ($mialgiaLeves > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Mialgia";
            $sintomas[$indice]->y = round($mialgiaLeves/($totalLeves/100), 2);
            $indice++;
        }

        if ($anosmiaLeves > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Anosmia";
            $sintomas[$indice]->y = round($anosmiaLeves/($totalLeves/100), 2);
            $indice++;
        }

        if ($vomitoLeves > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Vômito";
            $sintomas[$indice]->y = round($vomitoLeves/($totalLeves/100), 2);
            $indice++;
        }

        usort(
            $sintomas,
            function ($a, $b) {
                if ($a->y == $b->y) return 0;
                return ( ( $a->y > $b->y ) ? -1 : 1 );
            }
        );

        return $sintomas;

    }

    public static function dataChartSintomasGraves()
    {
        $totalSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5]
            ]
        )->count();

        $febreSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['FEBRE', 1]
            ]
        )->count();

        $tosseSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['TOSSE', 1]
            ]
        )->count();

        $gargantaSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['GARGANTA', 1]
            ]
        )->count();

        $dispneiaSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['DISPNEIA', 1]
            ]
        )->count();

        $desconfortoSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['DESC_RESP', 1]
            ]
        )->count();

        $saturacaoSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['SATURACAO', 1]
            ]
        )->count();

        $diarreiaSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['DIARREIA', 1]
            ]
        )->count();

        $vomitoSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['VOMITO', 1]
            ]
        )->count();

        $outrosSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['OUTRO_SIN', 1]
            ]
        )->count();

        $cefaleiaSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['OUTRO_DES', 'LIKE', '%CEFALEIA%']
            ]
        )->count();

        $cefaleiaSrag += DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['OUTRO_DES', 'LIKE', '%cefaleia%']
            ]
        )->count();

        $mialgiaSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['OUTRO_DES', 'LIKE', '%MIALGIA%']
            ]
        )->count();

        $mialgiaSrag += DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['OUTRO_DES', 'LIKE', '%mialgia%']
            ]
        )->count();

        $corizaSrag = DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['OUTRO_DES', 'LIKE', '%CORIZA%']
            ]
        )->count();

        $corizaSrag += DB::table('srag2020')->where(
            [
                ['CLASSI_FIN', 5],
                ['OUTRO_DES', 'LIKE', '%coriza%']
            ]
        )->count();


        $indice = 0;

        if ($febreSrag > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Febre";
            $sintomas[$indice]->y = round($febreSrag/($totalSrag/100), 2);
            $indice++;
        }

        if ($tosseSrag > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Tosse";
            $sintomas[$indice]->y = round($tosseSrag/($totalSrag/100), 2);
            $indice++;
        }

        if ($gargantaSrag > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Dor de Garganta";
            $sintomas[$indice]->y = round($gargantaSrag/($totalSrag/100), 2);
            $indice++;
        }

        if ($dispneiaSrag > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Dispneia";
            $sintomas[$indice]->y = round($dispneiaSrag/($totalSrag/100), 2);
            $indice++;
        }

        if ($desconfortoSrag > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Desconforto Respiratório";
            $sintomas[$indice]->y = round($desconfortoSrag/($totalSrag/100), 2);
            $indice++;
        }

        if ($saturacaoSrag > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Saturação O2 < 95%";
            $sintomas[$indice]->y = round($saturacaoSrag/($totalSrag/100), 2);
            $indice++;
        }

        if ($diarreiaSrag > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Diarreia";
            $sintomas[$indice]->y = round($diarreiaSrag/($totalSrag/100), 2);
            $indice++;
        }

        if ($vomitoSrag > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Vômito";
            $sintomas[$indice]->y = round($vomitoSrag/($totalSrag/100), 2);
            $indice++;
        }

        if ($cefaleiaSrag > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Cefaleia";
            $sintomas[$indice]->y = round($cefaleiaSrag/($totalSrag/100), 2);
            $indice++;
        }

        if ($mialgiaSrag > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Mialgia";
            $sintomas[$indice]->y = round($mialgiaSrag/($totalSrag/100), 2);
            $indice++;
        }

        if ($corizaSrag > 0) {
            $sintomas[$indice] = new stdClass;
            $sintomas[$indice]->x = "Coriza";
            $sintomas[$indice]->y = round($corizaSrag/($totalSrag/100), 2);
            $indice++;
        }

        usort(
            $sintomas,
            function ($a, $b) {
                if ($a->y == $b->y) return 0;
                return ( ( $a->y > $b->y ) ? -1 : 1 );
            }
        );

        return $sintomas;

    }

    public function sintomatologia()
    {

        $dataChartSintomasLeves = CovidTheController::dataChartSintomasLeve();
        $dataChartSintomasGraves = CovidTheController::dataChartSintomasGraves();

        //dd($dataChartObitoDetalhamento, $dataChartObitoEvolucao);

        $options['titulo'] = self::$resouceFullName;
        $options['session'] = self::$resouce;
        $options['area'] = 'sintomatologia';
        $routeBefore = array_merge(self::$routeBefore, []);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('sintomatologia-covid', compact(
            'dataChartSintomasLeves',
            'dataChartSintomasGraves',
            'urls',
            'options'
        ));
    }

    /**
     * Undocumented function
     *
     * @author Thiago Pinto Dias <email@email.com>
     * @return view
     */
    public function mapEvolucao()
    {
        $casosData = CovidDatasus::select(
            DB::raw('TO_CHAR("dataInicioSintomas" :: DATE, \'yyyy-mm-dd\') as data')
        )->where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo']
            ]
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->orderBy('data')->first();

        $casosSgData = DB::table('sg2020')->select(
            DB::raw('"DT_PRISINT" as data')
        )->where('PCR_SARS2', 1)
            ->orderBy('data')
            ->first();

        $casosSragData = DB::table('srag2020')->select(
            DB::raw('"DT_SIN_PRI" as data')
        )->where('CLASSI_FIN', 5)
            ->orderBy('data')
            ->first();

        $casosData = DateTime::createFromFormat('Y-m-d', $casosData->data);
        $casosSgData = DateTime::createFromFormat('Y-m-d', $casosSgData->data);
        $casosSragData = DateTime::createFromFormat('Y-m-d', $casosSragData->data);

        $dataInitial = min($casosData, $casosSgData, $casosSragData);

        $dataFinal = new DateTime();
        $dataCurrent = $dataInitial;

        while ($dataCurrent <= $dataFinal) {
            $datas[] = $dataCurrent->format('Y-m-d');
            $dataCurrent->modify('+1 day');
        }

        $bairros = Bairro::where('gid', '>', '0')->with('geocodes')->get();
        $options['titulo'] = 'Mapa evolução casos COVID-19 Teresina';
        $fullName = 'Mapa evolução casos COVID-19 Teresina';
        $options['area'] = 'map-evolucao';
        $routeBefore = array_merge(self::$routeBefore,
            [
                'map-data-casos',
                'map-data-obitos',
                'map-data-incidencias'
            ]
        );
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('map-covid',
            compact(
                'datas',
                'bairros',
                'options',
                'fullName',
                'urls'
            )
        );
    }

    /**
     * Undocumented function
     *
     * @author Thiago Pinto Dias <email@email.com>
     * @param [date] $data
     * @return json
     */
    public function mapDataCasos($data, $bairroId)
    {
        $dataLimite = new DateTime($data);
        $bairro = new stdClass;
        $bairro->id = intval($bairroId);
        $bairro->casos = CovidDatasus::join(
            'bairroalias', 'coviddatasus.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['dataInicioSintomas','<=', $dataLimite->format('Y-m-d H:i:s')],
                ['bairros.id', $bairroId]
            ]
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->count();

        $bairro->casos_dia = CovidDatasus::join(
            'bairroalias', 'coviddatasus.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['dataInicioSintomas','=', $dataLimite->format('Y-m-d H:i:s')],
                ['bairros.id', $bairroId]
            ]
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->count();


        $bairro->casos += DB::table('sg2020')->join(
            'bairroalias', 'sg2020.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['PCR_SARS2', 1],
                ['DT_PRISINT','<=', $dataLimite->format('Y-m-d')],
                ['bairros.id', $bairroId]
            ]
        )->count();

        $bairro->casos_dia += DB::table('sg2020')->join(
            'bairroalias', 'sg2020.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['PCR_SARS2', 1],
                ['DT_PRISINT','=', $dataLimite->format('Y-m-d')],
                ['bairros.id', $bairroId]
            ]
        )->count();

        $bairro->casos += DB::table('srag2020')->join(
            'bairroalias', 'srag2020.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['CLASSI_FIN', 5],
                ['DT_SIN_PRI','<=', $dataLimite->format('Y-m-d')],
                ['bairros.id', $bairroId]
            ]
        )->count();

        $bairro->casos_dia += DB::table('srag2020')->join(
            'bairroalias', 'srag2020.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['CLASSI_FIN', 5],
                ['DT_SIN_PRI','=', $dataLimite->format('Y-m-d')],
                ['bairros.id', $bairroId]
            ]
        )->count();


        return response()->json($bairro);

    }

    public function mapDataObitos($data, $bairroId)
    {
        $dataLimite = new DateTime($data);
        $bairro = new stdClass;
        $bairro->id = intval($bairroId);

        $bairro->casos = DB::table('srag2020')->join(
            'bairroalias', 'srag2020.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['DT_EVOLUCA','<=', $dataLimite->format('Y-m-d')],
                ['bairros.id', $bairroId]
            ]
        )->count();

        $bairro->casos_dia = DB::table('srag2020')->join(
            'bairroalias', 'srag2020.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['CLASSI_FIN', 5],
                ['EVOLUCAO', 2],
                ['DT_EVOLUCA','=', $dataLimite->format('Y-m-d')],
                ['bairros.id', $bairroId]
            ]
        )->count();


        return response()->json($bairro);

    }

    public function mapDataIncidencias($data, $bairroId)
    {
        $dataLimite = new DateTime($data);
        $bairro = Bairro::find($bairroId);
        $bairro->casos = CovidDatasus::join(
            'bairroalias', 'coviddatasus.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['dataInicioSintomas','<=', $dataLimite->format('Y-m-d H:i:s')],
                ['bairros.id', $bairroId]
            ]
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->count();

        $bairro->casos_dia = CovidDatasus::join(
            'bairroalias', 'coviddatasus.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['municipio', 'Teresina'],
                ['resultadoTeste', 'Positivo'],
                ['dataInicioSintomas','=', $dataLimite->format('Y-m-d H:i:s')],
                ['bairros.id', $bairroId]
            ]
        )->where(function ($q) {
            $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
        })->where(function ($q) {
            $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
        })->count();


        $bairro->casos += DB::table('sg2020')->join(
            'bairroalias', 'sg2020.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['PCR_SARS2', 1],
                ['DT_PRISINT','<=', $dataLimite->format('Y-m-d')],
                ['bairros.id', $bairroId]
            ]
        )->count();

        $bairro->casos_dia += DB::table('sg2020')->join(
            'bairroalias', 'sg2020.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['PCR_SARS2', 1],
                ['DT_PRISINT','=', $dataLimite->format('Y-m-d')],
                ['bairros.id', $bairroId]
            ]
        )->count();

        $bairro->casos += DB::table('srag2020')->join(
            'bairroalias', 'srag2020.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['CLASSI_FIN', 5],
                ['DT_SIN_PRI','<=', $dataLimite->format('Y-m-d')],
                ['bairros.id', $bairroId]
            ]
        )->count();

        $bairro->casos_dia += DB::table('srag2020')->join(
            'bairroalias', 'srag2020.bairroalias_id', '=', 'bairroalias.id'
        )->join(
            'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
        )->where(
            [
                ['CLASSI_FIN', 5],
                ['DT_SIN_PRI','=', $dataLimite->format('Y-m-d')],
                ['bairros.id', $bairroId]
            ]
        )->count();

        $bairro->casos = round(($bairro->casos/$bairro->populacao)*1000, 3);
        $bairro->casos_dia = round(($bairro->casos_dia/$bairro->populacao)*1000, 3);

        return response()->json($bairro);

    }

    public static function dataChartUnidadesNotificadoras()
    {
        $estabelecimentos = CovidDatasus::join(
            'estabelecimento_cnes',
            DB::raw('CAST (coviddatasus.cnes AS BIGINT)'),
            '=',
            'estabelecimento_cnes.co_cnes'
        )->select(
            DB::raw('estabelecimento_cnes.no_fantasia AS nome, cnes, COUNT(cnes) AS quant')
        )->whereRaw(
            'CHAR_LENGTH(coviddatasus.cnes) < 8'
        )->groupBy(
            'cnes', 'estabelecimento_cnes.no_fantasia'
        )->orderBy('quant', 'desc')->limit(10)->get();

        for ($i=0; $i < count($estabelecimentos); $i++) {
            $estabelecimentosListe[$i] = new stdClass;
            $estabelecimentosListe[$i]->x = $estabelecimentos[$i]->nome;
            $estabelecimentosListe[$i]->y = $estabelecimentos[$i]->quant;
        }

        return $estabelecimentosListe;

    }

    public static function dataChartTiposTeste()
    {
        $testes = CovidDatasus::select(
            DB::raw('"tipoTeste", "resultadoTeste", COUNT("tipoTeste") AS quant')
        )->whereRaw(
            '"tipoTeste" IS NOT NULL'
        )->groupBy(
            'tipoTeste', 'resultadoTeste'
        )->orderBy('tipoTeste')->orderBy('resultadoTeste')->get();

        $testePositivos = new stdClass;
        $testePositivos->name = 'Positivo';

        $testeNegativos = new stdClass;
        $testeNegativos->name = 'Negativo';

        $categories =  array();

        for ($i=0; $i < count($testes); $i++) {
            if (strcmp($testes[$i]->resultadoTeste, "Negativo") == 0 ) {
                $categories[] = $testes[$i]->tipoTeste;
                $testeNegativos->data[] = ($testes[$i]->quant)*-1;
            } else {
                $testePositivos->data[] = $testes[$i]->quant;
            }
        }

        $resultados = new stdClass;
        $resultados->positivos = $testePositivos;
        $resultados->negativos = $testeNegativos;
        $resultados->categorias = $categories;

        return $resultados;

    }

    public static function dataChartCbo()
    {
        $cbos = CovidDatasus::select(
            DB::raw('"cbo" AS name')
        )->whereRaw(
            '"cbo" IS NOT NULL
            AND
            cbo <> \'\''
        )->groupBy(
            'cbo'
        )->orderBy('cbo')->get();

        $testePositivos = new stdClass;
        $testePositivos->name = 'Positivo';

        $testeNegativos = new stdClass;
        $testeNegativos->name = 'Negativo';

        $categories =  array();

        for ($i=0; $i < count($cbos); $i++) {
            if (strcmp($cbos[$i]->name, '2237 - Nutrisionista') == 0) {
                continue;
            }
            $categories[] = $cbos[$i]->name;
            if (strcmp($cbos[$i]->name, '2237 - Nutricionista') == 0) {
                $negativo = CovidDatasus::whereRaw(
                    "(cbo LIKE '%Nutricionista%'
                    OR
                    cbo LIKE '%Nutrisionista%')
                    AND
                    \"resultadoTeste\" LIKE 'Negativo'"
                )->count();
                $testeNegativos->data[] = $negativo * -1;
                $testePositivos->data[] = CovidDatasus::whereRaw(
                    "(cbo LIKE '%Nutricionista%'
                    OR
                    cbo LIKE '%Nutrisionista%')
                    AND
                    \"resultadoTeste\" LIKE 'Positivo'"
                )->count();
            } else {
                $negativo = CovidDatasus::whereRaw(
                    "(cbo LIKE '%{$cbos[$i]->name}%')
                    AND
                    \"resultadoTeste\" LIKE 'Negativo'"
                )->count();
                $testeNegativos->data[] = $negativo * -1;
                $testePositivos->data[] = CovidDatasus::whereRaw(
                    "(cbo LIKE '%{$cbos[$i]->name}%')
                    AND
                    \"resultadoTeste\" LIKE 'Positivo'"
                )->count();
            }

        }

        $resultados = new stdClass;
        $resultados->positivos = $testePositivos;
        $resultados->negativos = $testeNegativos;
        $resultados->categorias = $categories;

        return $resultados;

    }

    public static function dataChartCboRtPcr()
    {
        $cbos = CovidDatasus::select(
            DB::raw('"cbo" AS name')
        )->whereRaw(
            '"cbo" IS NOT NULL
            AND
            cbo <> \'\''
        )->groupBy(
            'cbo'
        )->orderBy('cbo')->get();

        $testePositivos = new stdClass;
        $testePositivos->name = 'Positivo';

        $testeNegativos = new stdClass;
        $testeNegativos->name = 'Negativo';

        $categories =  array();

        for ($i=0; $i < count($cbos); $i++) {
            if (strcmp($cbos[$i]->name, '2237 - Nutrisionista') == 0) {
                continue;
            }
            $categories[] = $cbos[$i]->name;
            if (strcmp($cbos[$i]->name, '2237 - Nutricionista') == 0) {
                $negativo = CovidDatasus::whereRaw(
                    "(cbo LIKE '%Nutricionista%'
                    OR
                    cbo LIKE '%Nutrisionista%')
                    AND
                    \"resultadoTeste\" LIKE 'Negativo'
                    AND
                    \"tipoTeste\" LIKE '%RT-PCR%'"
                )->count();
                $testeNegativos->data[] = $negativo * -1;
                $testePositivos->data[] = CovidDatasus::whereRaw(
                    "(cbo LIKE '%Nutricionista%'
                    OR
                    cbo LIKE '%Nutrisionista%')
                    AND
                    \"resultadoTeste\" LIKE 'Positivo'
                    AND
                    \"tipoTeste\" LIKE '%RT-PCR%'"
                )->count();
            } else {
                $negativo = CovidDatasus::whereRaw(
                    "(cbo LIKE '%{$cbos[$i]->name}%')
                    AND
                    \"resultadoTeste\" LIKE 'Negativo'
                    AND
                    \"tipoTeste\" LIKE '%RT-PCR%'"
                )->count();
                $testeNegativos->data[] = $negativo * -1;
                $testePositivos->data[] = CovidDatasus::whereRaw(
                    "(cbo LIKE '%{$cbos[$i]->name}%')
                    AND
                    \"resultadoTeste\" LIKE 'Positivo'
                    AND
                    \"tipoTeste\" LIKE '%RT-PCR%'"
                )->count();
            }

        }

        $resultados = new stdClass;
        $resultados->positivos = $testePositivos;
        $resultados->negativos = $testeNegativos;
        $resultados->categorias = $categories;

        return $resultados;

    }

    public static function dataChartCboAnticorpo()
    {
        $cbos = CovidDatasus::select(
            DB::raw('"cbo" AS name')
        )->whereRaw(
            '"cbo" IS NOT NULL
            AND
            cbo <> \'\''
        )->groupBy(
            'cbo'
        )->orderBy('cbo')->get();

        $testePositivos = new stdClass;
        $testePositivos->name = 'Positivo';

        $testeNegativos = new stdClass;
        $testeNegativos->name = 'Negativo';

        $categories =  array();

        for ($i=0; $i < count($cbos); $i++) {
            if (strcmp($cbos[$i]->name, '2237 - Nutrisionista') == 0) {
                continue;
            }
            $categories[] = $cbos[$i]->name;
            if (strcmp($cbos[$i]->name, '2237 - Nutricionista') == 0) {
                $negativo = CovidDatasus::whereRaw(
                    "(cbo LIKE '%Nutricionista%'
                    OR
                    cbo LIKE '%Nutrisionista%')
                    AND
                    \"resultadoTeste\" LIKE 'Negativo'
                    AND
                    \"tipoTeste\" LIKE '%TESTE RÁPIDO - ANTICORPO%'"
                )->count();
                $testeNegativos->data[] = $negativo * -1;
                $testePositivos->data[] = CovidDatasus::whereRaw(
                    "(cbo LIKE '%Nutricionista%'
                    OR
                    cbo LIKE '%Nutrisionista%')
                    AND
                    \"resultadoTeste\" LIKE 'Positivo'
                    AND
                    \"tipoTeste\" LIKE '%TESTE RÁPIDO - ANTICORPO%'"
                )->count();
            } else {
                $negativo = CovidDatasus::whereRaw(
                    "(cbo LIKE '%{$cbos[$i]->name}%')
                    AND
                    \"resultadoTeste\" LIKE 'Negativo'
                    AND
                    \"tipoTeste\" LIKE '%TESTE RÁPIDO - ANTICORPO%'"
                )->count();
                $testeNegativos->data[] = $negativo * -1;
                $testePositivos->data[] = CovidDatasus::whereRaw(
                    "(cbo LIKE '%{$cbos[$i]->name}%')
                    AND
                    \"resultadoTeste\" LIKE 'Positivo'
                    AND
                    \"tipoTeste\" LIKE '%TESTE RÁPIDO - ANTICORPO%'"
                )->count();
            }

        }

        $resultados = new stdClass;
        $resultados->positivos = $testePositivos;
        $resultados->negativos = $testeNegativos;
        $resultados->categorias = $categories;

        return $resultados;

    }

    public static function dataChartCboAntigeno()
    {
        $cbos = CovidDatasus::select(
            DB::raw('"cbo" AS name')
        )->whereRaw(
            '"cbo" IS NOT NULL
            AND
            cbo <> \'\''
        )->groupBy(
            'cbo'
        )->orderBy('cbo')->get();

        $testePositivos = new stdClass;
        $testePositivos->name = 'Positivo';

        $testeNegativos = new stdClass;
        $testeNegativos->name = 'Negativo';

        $categories =  array();

        for ($i=0; $i < count($cbos); $i++) {
            if (strcmp($cbos[$i]->name, '2237 - Nutrisionista') == 0) {
                continue;
            }
            $categories[] = $cbos[$i]->name;
            if (strcmp($cbos[$i]->name, '2237 - Nutricionista') == 0) {
                $negativo = CovidDatasus::whereRaw(
                    "(cbo LIKE '%Nutricionista%'
                    OR
                    cbo LIKE '%Nutrisionista%')
                    AND
                    \"resultadoTeste\" LIKE 'Negativo'
                    AND
                    \"tipoTeste\" LIKE '%TESTE RÁPIDO - ANTÍGENO%'"
                )->count();
                $testeNegativos->data[] = $negativo * -1;
                $testePositivos->data[] = CovidDatasus::whereRaw(
                    "(cbo LIKE '%Nutricionista%'
                    OR
                    cbo LIKE '%Nutrisionista%')
                    AND
                    \"resultadoTeste\" LIKE 'Positivo'
                    AND
                    \"tipoTeste\" LIKE '%TESTE RÁPIDO - ANTÍGENO%'"
                )->count();
            } else {
                $negativo = CovidDatasus::whereRaw(
                    "(cbo LIKE '%{$cbos[$i]->name}%')
                    AND
                    \"resultadoTeste\" LIKE 'Negativo'
                    AND
                    \"tipoTeste\" LIKE '%TESTE RÁPIDO - ANTÍGENO%'"
                )->count();
                $testeNegativos->data[] = $negativo * -1;
                $testePositivos->data[] = CovidDatasus::whereRaw(
                    "(cbo LIKE '%{$cbos[$i]->name}%')
                    AND
                    \"resultadoTeste\" LIKE 'Positivo'
                    AND
                    \"tipoTeste\" LIKE '%TESTE RÁPIDO - ANTÍGENO%'"
                )->count();
            }

        }

        $resultados = new stdClass;
        $resultados->positivos = $testePositivos;
        $resultados->negativos = $testeNegativos;
        $resultados->categorias = $categories;

        return $resultados;

    }

    public function outrasInfo()
    {
        $unidadesNotificadoras = CovidTheController::dataChartUnidadesNotificadoras();
        $resultados = CovidTheController::dataChartTiposTeste();
        $cboRtPcr = CovidTheController::dataChartCboRtPcr();
        $cboAnticorpo = CovidTheController::dataChartCboAnticorpo();
        $cboAntigeno = CovidTheController::dataChartCboAntigeno();
        $cbo = CovidTheController::dataChartCbo();
        $options['titulo'] = self::$resouceFullName;
        $options['session'] = self::$resouce;
        $options['area'] = 'outras-info';
        $routeBefore = array_merge(self::$routeBefore, []);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('outras-info-covid', compact(
            'unidadesNotificadoras',
            'resultados',
            'cboRtPcr',
            'cboAnticorpo',
            'cboAntigeno',
            'cbo',
            'urls',
            'options'
        ));
    }

    public function mapDataTable($data, $typeMap)
    {
        $bairros = Bairro::where('gid', '>', '0')->orderBy('name')->get();
        $dataLimite = new DateTime($data);

        if (strcmp($typeMap, 'is-casos') == 0) {
            $tableName = "Casos";

            for ($i=0; $i < count($bairros); $i++) {

                $bairros[$i]->casos = CovidDatasus::join(
                    'bairroalias', 'coviddatasus.bairroalias_id', '=', 'bairroalias.id'
                )->join(
                    'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
                )->where(
                    [
                        ['municipio', 'Teresina'],
                        ['resultadoTeste', 'Positivo'],
                        ['dataInicioSintomas','<=', $dataLimite->format('Y-m-d H:i:s')],
                        ['bairros.id', $bairros[$i]->id]
                    ]
                )->where(function ($q) {
                    $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
                })->where(function ($q) {
                    $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
                })->count();


                $bairros[$i]->casos += DB::table('sg2020')->join(
                    'bairroalias', 'sg2020.bairroalias_id', '=', 'bairroalias.id'
                )->join(
                    'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
                )->where(
                    [
                        ['PCR_SARS2', 1],
                        ['DT_PRISINT','<=', $dataLimite->format('Y-m-d')],
                        ['bairros.id', $bairros[$i]->id]
                    ]
                )->count();

                $bairros[$i]->casos += DB::table('srag2020')->join(
                    'bairroalias', 'srag2020.bairroalias_id', '=', 'bairroalias.id'
                )->join(
                    'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
                )->where(
                    [
                        ['CLASSI_FIN', 5],
                        ['DT_SIN_PRI','<=', $dataLimite->format('Y-m-d')],
                        ['bairros.id', $bairros[$i]->id]
                    ]
                )->count();

            }

        } elseif (strcmp($typeMap, 'is-obitos') == 0) {

            $tableName = "Óbitos";
            for ($i=0; $i < count($bairros); $i++) {

                $bairros[$i]->casos = DB::table('srag2020')->join(
                    'bairroalias', 'srag2020.bairroalias_id', '=', 'bairroalias.id'
                )->join(
                    'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
                )->where(
                    [
                        ['CLASSI_FIN', 5],
                        ['EVOLUCAO', 2],
                        ['DT_EVOLUCA','<=', $dataLimite->format('Y-m-d')],
                        ['bairros.id', $bairros[$i]->id]
                    ]
                )->count();

            }

        } else {

            $tableName = "Incidência";
            for ($i=0; $i < count($bairros); $i++) {

                $bairros[$i]->casos = CovidDatasus::join(
                    'bairroalias', 'coviddatasus.bairroalias_id', '=', 'bairroalias.id'
                )->join(
                    'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
                )->where(
                    [
                        ['municipio', 'Teresina'],
                        ['resultadoTeste', 'Positivo'],
                        ['dataInicioSintomas','<=', $dataLimite->format('Y-m-d H:i:s')],
                        ['bairros.id', $bairros[$i]->id]
                    ]
                )->where(
                    function ($q) {
                        $q->whereNull('evolucaoCaso')->orWhere('evolucaoCaso', '!=', 'Cancelado');
                    }
                )->where(
                    function ($q) {
                        $q->whereNull('classificacaoFinal')->orWhere('classificacaoFinal', '!=', 'Descartado');
                    }
                )->count();


                $bairros[$i]->casos += DB::table('sg2020')->join(
                    'bairroalias', 'sg2020.bairroalias_id', '=', 'bairroalias.id'
                )->join(
                    'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
                )->where(
                    [
                        ['PCR_SARS2', 1],
                        ['DT_PRISINT','<=', $dataLimite->format('Y-m-d')],
                        ['bairros.id', $bairros[$i]->id]
                    ]
                )->count();

                $bairros[$i]->casos += DB::table('srag2020')->join(
                    'bairroalias', 'srag2020.bairroalias_id', '=', 'bairroalias.id'
                )->join(
                    'bairros', 'bairroalias.bairro_id', '=', 'bairros.id'
                )->where(
                    [
                        ['CLASSI_FIN', 5],
                        ['DT_SIN_PRI','<=', $dataLimite->format('Y-m-d')],
                        ['bairros.id', $bairros[$i]->id]
                    ]
                )->count();

                $bairros[$i]->casos = round(($bairros[$i]->casos/$bairros[$i]->populacao)*1000, 3);
            }

        }
        $bairros = (array)$bairros->getIterator();

        usort(
            $bairros,
            function ($a, $b) {
                if ($a->casos == $b->casos) return 0;
                return ( ( $a->casos > $b->casos ) ? -1 : 1 );
            }
        );


        $routeBefore = array_merge(self::$routeBefore, []);
        $urls = Controller::createUrl(self::$resouce, $routeBefore, self::$routeAfter);
        return view('map-covid-table', compact(
            'tableName',
            'bairros',
            'data',
            'typeMap'
        ));
    }
}
