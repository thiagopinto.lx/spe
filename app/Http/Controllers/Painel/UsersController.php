<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\User;


class UsersController extends Controller
{
    public static $resouce= 'users';

    public function index()
    {
        $options['titulo'] = "Tabelas de Usuários";
        $options['descricao'] = "Lista de Usuários";
        $options['getindex'] = route('painel.'.self::$resouce.'.getindex');
        return view('painel.users', compact('options'));
    }

    public function getIndex(Request $request)
    {
        $tabela = 'users';
        $data = User::toDataTables($request, $tabela);

        $data = User::addColumn(
            $data,
            'delete',
            function($item){
                $id = $item->id;
                return '<form class="deleteUser"  action="'.route('painel.users.destroy', compact('id')).'" method="POST">'
                        .csrf_field()
                        .method_field('DELETE')
                        .'<button type="submit" class="btn btn-danger">Delete</button>
                        </form>';
            }
        );

        $data = User::addColumn(
            $data,
            'reset',
            function($item){
                $id = $item->id;
                return '<form class="deleteUser"  action="'.route('password.email').'" method="POST">'
                          .csrf_field()
                          .method_field('POST')
                          .'<input id="email" type="hidden" name="email" value="'.$item->email.'">
                            <button type="submit" class="btn btn-primary">Reset Senha</button>
                        </form>';
            }
        );

        return $data;
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return true;
    }

}
