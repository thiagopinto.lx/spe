<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\ImunoSies;
use App\Models\ImunoSipni;
use App\Utilities\GoogleMaps;
use Predis\Configuration\Options;
use PhpParser\Node\Scalar\MagicConst\Class_;


class ImunoSiesController extends Controller
{
    public static $resouce = 'imuno-sies';
    public static $tableName = 'imuno_sies';

    public function index()
    {
        $options['titulo'] = "Tabelas de Imuno do Sies";
        $options['descricao'] = "Lista Imunos do Sies";
        $options['getindex'] = route('painel.'.self::$resouce.'.getindex');
        return view('painel.imuno-sies', compact('options'));
    }

    public function getIndex(Request $request)
    {
      $tabela = self::$tableName;
      $data = ImunoSies::toDataTables($request, $tabela);

    $data = ImunoSies::addColumn(
      $data,
      'perda',
          function($item) use ($tabela){
              $imunoSies = ImunoSies::find($item->id);
                return '<div class="form-group">
                      <input type="text" class="form-control perdas" placeholder="Perda técnica" name="imuno-perda" value="'.$imunoSies->perda.'">
                      <input type="hidden" class="siesId" name="imunoId" value="'.$item->id.'">
                    </div>';
              
          }
    );

    $data = ImunoSies::addColumn(
        $data,
        'sipni',
            function($item) use ($tabela){
                $imunoSies = ImunoSies::find($item->id);
                if($imunoSies->imuno_sipni_id == null){
                  return '<div class="form-group multiple-datasets">
                        <input type="text" class="form-control typeahead" placeholder="Nome CNES" name="cnes-0">
                        <input type="hidden" class="siesId" name="siesId" value="'.$item->id.'">
                      </div>';
                } else {
                  $imunoSipni = ImunoSipni::find($imunoSies->imuno_sipni_id );
                  return '<div class="form-group multiple-datasets">
                        <input type="text" class="form-control typeahead" placeholder="Nome CNES" value="'.$imunoSipni->nome.'">
                        <input type="hidden" class="siesId" name="siesId" value="'.$item->id.'">
                      </div>';
                }
                
            }
    );

      $data = ImunoSies::addColumn(
              $data,
              'edit',
                  function($item) use ($tabela){
                      return '<a class="btn btn-primary" href="" role="button"><i class="far fa-edit"></i></a>';
                  }
      );

      return $data;
    }

    public function relationship($idSies, $idSipni){
      $imuno = ImunoSies::find($idSies);
      $imuno->imuno_sipni_id = $idSipni;
      $imuno->save();
    }

    public function percatecnica($idImunoSies, $perdaImuno){
      $imuno = ImunoSies::find($idImunoSies);
      $perdaImuno = intval($perdaImuno);
      $imuno->perda = $perdaImuno;
      $imuno->save(); 
    }

}
