<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\LoadSipni;
use App\Models\DosesAplicadasSipni;
use App\Models\ImunoSipni;
use App\Utilities\GoogleMaps;
use Predis\Configuration\Options;
use PhpParser\Node\Scalar\MagicConst\Class_;


class DosesAplicadasSipniController extends Controller
{
    public static $resouce = 'doses-aplicadas-sipni';
    public static $tableName = 'da_sipni';

    public function index($load_sipni_id)
    {
      $options['titulo'] = "Tabelas de Doses Aplicadas Sipni";
      $options['descricao'] = "Lista Doses Aplicadas Sipni";
      $options['getindex'] = route('painel.'.self::$resouce.'.getindex', compact('load_sipni_id'));
      return view('painel.doses-aplicadas-sipni', compact('options'));
    }

    public function getIndex(Request $request, $load_sipni_id)
    {
      $tabela = self::$tableName;
      $loadSies = LoadSipni::find($load_sipni_id);
      $tabela = $tabela.'_'.strtolower($loadSies->imuno()->nome).'_'.$loadSies->ano;
      $data = DosesAplicadasSipni::toDataTables($request, $tabela, $load_sipni_id);

      return $data;
    }

}
