<?php

namespace App\Http\Controllers\Painel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LoadTrend;
use App\Models\DadosTrend;
use Predis\Configuration\Options;
use PhpParser\Node\Scalar\MagicConst\Class_;


class DadosTrendController extends Controller
{
    public static $resouce = 'dados-trend';
    public static $tableName = 'trend';

    public function index($load_trend_id)
    {
      $options['titulo'] = "Tabelas de Dados Trends Google";
      $options['descricao'] = "Lista Dados Trends Google";
      $options['getindex'] = route('painel.'.self::$resouce.'.getindex', compact('load_trend_id'));
      return view('painel.dados-trend', compact('options'));
    }

    public function getIndex(Request $request, $load_trend_id)
    {
      $tabela = self::$tableName;
      $loadTrend = LoadTrend::find($load_trend_id);
      $tabela = $tabela.'_'.$loadTrend->ano;
      $data = DadosTrend::toDataTables($request, $tabela, $load_trend_id);

      return $data;
    }

}
