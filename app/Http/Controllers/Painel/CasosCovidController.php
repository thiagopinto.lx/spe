<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\CasosCovid;
use App\Models\Estado;
use App\Utilities\GoogleMaps;
use App\Utilities\BrasilIo;

class CasosCovidController extends Controller
{
    public static $resouce = 'casoscovid';
    public static $tableName = 'casoscovid';

    public function index()
    {
        $options['titulo'] = "Tabelas de caosos COVID-19 por Cidades";
        $options['descricao'] = "Lista caosos COVID-19 por Cidade";
        $options['store'] = route('painel.' . self::$resouce . '.store');
        $options['getindex'] = route('painel.' . self::$resouce . '.getindex');

        $latestcaso = CasosCovid::select('data')->latest('data')->first();

        $options['lastUpdate'] = $latestcaso->data;

        return view('painel.casoscovid', compact('options'));
    }

    public function getIndex(Request $request)
    {
        $tabela = self::$tableName;
        $data = CasosCovid::toDataTables($request, $tabela);

        return $data;
    }

    public function store(Request $request)
    {
        $fileCsv = $request->file('fileCsv');
        $tableName = "casoscovid";

        // Define o valor default para a variável que contém o nome da imagem
        $nameFile = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('fileCsv') && $request->file('fileCsv')->isValid()) {

            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = 'casoscovid';

            // Recupera a extensão do arquivo
            $extension = $fileCsv->getClientOriginalExtension();

            // Define finalmente o nome
            $nameFile = "{$name}.{$extension}";

            try {
                Storage::delete('basecsv/' . $nameFile);

                $upload = $fileCsv->storeAs('basecsv', $nameFile);
            } catch (\Throwable $th) {
                return $th;
            }
        }

        if (CasosCovid::csvLoad($tableName, $upload)) {
            //return $upload;
            return "ok;";
        };
    }

    public function updateByApi(Request $request)
    {
      $tableName = "casoscovid";

        if ($request->has('date')) {
          //$estados = Estado::where('uf', '<>', 'PI')->get();
          $token = "f00a4ea94b7e1beeb8bc433bed932974bce53240";

          $casos = array();
          $casos = BrasilIo::getRegisterCovid($request->input('date'), $token, $casos);

          if (CasosCovid::apiLoad($tableName, $casos, "PI")) {
            //return $upload;
            return "ok;";
        };



          /* $casosCapitais = array();
          $tokenCapitais = "6bdf490de339232b83f9519b492f47f498eed79b";
          $tokenState = "f00a4ea94b7e1beeb8bc433bed932974bce53240";
          $tokenCity = "b004f2479c9c3e3df15937f0637d1da18ac80bad";

          foreach ($estados as $estado) {
            $casosCapitais[] = BrasilIo::getRegisterCovidStateCapital($request->input('date'), $estado->co_ibge_capital, $tokenCapitais);
            dd($casosCapitais);
          }




            $casosEstados = BrasilIo::getRegisterCovidState($request->input('date'), $tokenState);
            //$cities = BrasilIo::getRegisterCovidCity($request->input('date'), "PI" $tokenCity);
            dd($states);
 */
        }

        return "date";
    }
}
