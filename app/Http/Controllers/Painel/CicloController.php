<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use App\Models\LoadData;
use App\Models\Ciclo;
use App\Models\Bairro;
use App\Models\CicloBairro;
use PHPUnit\Framework\Constraint\Exception;

class CicloController extends Controller
{
    public static $route = 'ciclos';
    public static $tableName = 'ciclos';

    public function index()
    {
        $cores = self::$cores;
        $options['titulo'] = "Ciclos";
        $options['descricao'] = "Ciclos de avaliação LIRAA";
        $options['store'] = route('painel.'.self::$route.'.store');
        $options['getindex'] = route('painel.'.self::$route.'.getindex');
        return view('painel.ciclos', compact('cores', 'options'));
    }

    public function getIndex(Request $request)
    {
        $tabela = self::$tableName;
        $data = Ciclo::toDataTables($request, $tabela);
        $data = Ciclo::addColumn(
            $data,
            'view',
            function($item){
                $id = $item->id;
                return '<a class="btn btn-primary" href="'.route('painel.ciclos.bairros', compact('id')).'" role="button">Ver lista bairros <i class="far fa-eye"></i></a>';
            }
        );

        return $data;
    }

    public function store(Request $request)
    {
      $delimitador = ';';
      $cerca = '"';
      $ciclo = new Ciclo;
      $ciclo->ano = $request->get('ano');
      $ciclo->nu_ciclo = $request->get('nu_ciclo');
      $ciclo->save();

      if ($request->has('fileCSV')) {

        $fileCSV = $request->file('fileCSV');
        $tableName = self::$tableName;

        // Define o valor default para a variável que contém o nome da imagem
        $nameFile = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('fileCSV') && $request->file('fileCSV')->isValid()) {

            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));

            // Recupera a extensão do arquivo
            $extension = $fileCSV->getClientOriginalExtension();

            // Define finalmente o nome
            $nameFile = "{$name}.{$extension}";

            // Faz o upload:
            $upload = $fileCSV->storeAs('filecsv', $nameFile);
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();
        }

        try{
          $bairros = Array();
          $file = fopen(storage_path('app/'.$upload), 'r');

          if ($request->has('is_cabecalho')) {
            $cabecalho = fgetcsv($file, null, $delimitador, $cerca);
          }

          while (($line = fgetcsv($file, null, $delimitador, $cerca)) !== false)
          {
            $line[1] = str_replace(',', '.', $line[1]);
            $iplas[] = $line;
          }
          fclose($file);
          DB::statement('CREATE EXTENSION IF NOT EXISTS unaccent');
          $naoLocalizado = array();
          $cicloBairroSalvos = array();
          foreach ($iplas as $ipla) {
            $ipla[0] = trim(strip_tags($ipla[0]));
            $ipla[1] = trim($ipla[1]);
            if(floatval($ipla[1]) > 0){

              $options = 'Any-Latin; NFKD; [:Punctuation:] Remove; [^\\u0000-\\u007E] Remove';
              $bairroName = transliterator_transliterate($options, $ipla[0]);
              $bairro = Bairro::whereRaw("unaccent(name) ILIKE unaccent('%{$bairroName}%')")->first();
              if($bairro != null){
                if(in_array($bairro->name, $cicloBairroSalvos)){
                  $cicloBairro = CicloBairro::where('bairro_id', $bairro->id)
                                            ->where('ciclo_id', $ciclo->id)
                                            ->first();
                  $cicloBairro->ipla = ($cicloBairro->ipla+$ipla[1])/2;
                }else{
                  $cicloBairro = new CicloBairro;
                  $cicloBairro->bairro_id = $bairro->id;
                  $cicloBairro->ipla = $ipla[1];
                  if($ciclo->cicloBairros()->save($cicloBairro)){
                    $cicloBairroSalvos[] = $bairro->name;
                  }
                }
              }else{
                $naoLocalizado[] = $ipla;
              }

            }
          }
        }catch(Exception $e){
            dd("Arquivo mau formatado");
        }
      }
        if (count($naoLocalizado) > 0) {
          $retorno ='
          <table class="table table-dark table-sm">
            <thead>
              <tr>
                <th scope="col">Bairros não Localizados!</th>
                <th scope="col">ipla</th>
              </tr>
            </thead>
            <tbody>';
          for ($i=0; $i < count($naoLocalizado) ; $i++) {
            $retorno .=
            '<tr class="bg-danger">
              <td>'.$naoLocalizado[$i][0].'</td>
              <td>'.$naoLocalizado[$i][1].'</td>
            </tr>';
          }
          $retorno .=
          ' </tbody>
          </table>';
        }else {
          $retorno = '<div class="alert alert-success" role="alert">
                        Salvo com Sucesso!
                      </div>';
        }

        return $retorno;
    }

    public function bairros($id)
    {
      $ciclo = Ciclo::find($id);
      $ciclo->load('bairros');
      $cores = self::$cores;
      $options['titulo'] = "Bairros";
      $options['descricao'] = "Lista de bairros do Ciclos {$ciclo->nu_ciclo} do ano de {$ciclo->ano}";
      $options['getindex'] = route('painel.'.self::$route.'.getindexbairros', compact('id'));
      $options['storebairrociclo'] = route('painel.'.self::$route.'.storebairrociclo', compact('id'));
      return view('painel.ciclobairros', compact('cores', 'options'));
    }

    public function getIndexBairros(Request $request, $id)
    {
      $ciclo = Ciclo::find($id);
      $data = $ciclo->bairrosToDataTables($request);
      $data = Ciclo::addColumn(
          $data,
          'view',
          function($item){
              $id = $item->id;
              return '<a class="btn btn-primary" href="'.route('painel.bairros.map', compact('id')).'" role="button">Ver no mapa <i class="far fa-eye"></i></a>';
          }
      );
      $data = Ciclo::addColumn(
          $data,
          'delete',
          function($item){
              $id = $item->id_ipla;
              return
              '<form class="deleteBairro"  action="'.route('painel.ciclos.bairro.destroy', compact('id')).'" method="POST">'
                  .csrf_field()
                  .method_field('POST')
                  .'<button type="submit" class="btn btn-danger">Delete</button>
              </form>';

          }
      );

        return $data;
    }

    public function storeBairroCiclo(Request $request, $id)
    {
        $cicloBairro = CicloBairro::where('bairro_id', $request->get('idBairro'))
                                  ->where('ciclo_id', $id)
                                  ->first();
        $ipla = trim(str_replace(',', '.', $request->get('ipla')));
        if($cicloBairro){
          $cicloBairro->ipla = ($cicloBairro->ipla+$ipla)/2;
          $cicloBairro->save();
        }else{
          $cicloBairro = new CicloBairro;
          $cicloBairro->bairro_id = $request->get('idBairro');
          $cicloBairro->ciclo_id = $id;
          $cicloBairro->ipla = $ipla;
          $cicloBairro->save();
        }

      if ($cicloBairro->id != null) {
        return '<div class="alert alert-success" role="alert">
                ipla salvo id: '.$cicloBairro->id.'
              </div>';
      }else {
        return '<div class="alert alert-danger" role="alert">
                Tempos problemas ipla não salvo
              </div>';
      }
    }

    public function destroyBairros($id)
    {
      $cicloBairro = CicloBairro::find($id);
      $cicloBairro->delete();
    }

}
