<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\ImunoSies;
use App\Models\ImunoSipni;
use App\Utilities\GoogleMaps;
use Predis\Configuration\Options;
use PhpParser\Node\Scalar\MagicConst\Class_;


class ImunoSipniController extends Controller
{
    public static $resouce = 'imuno-sipni';
    public static $tableName = 'imuno_sipnis';

    public function index()
    {
        $options['titulo'] = "Tabelas de Imuno do Sipni";
        $options['descricao'] = "Lista Imunos do Sipni";
        $options['getindex'] = route('painel.'.self::$resouce.'.getindex');
        return view('painel.imuno-sipni', compact('options'));
    }

    public function getIndex(Request $request)
    {
      $tabela = self::$tableName;
      $data = ImunoSies::toDataTables($request, $tabela);

      return $data;
    }


    public function autocompleteNome(Request $request)
    {
      $q= trim($request->input('q'));
      $options = 'Any-Latin; NFKD; [:Punctuation:] Remove; [^\\u0000-\\u007E] Remove';
      $q = transliterator_transliterate($options, $q);
      $data = ImunoSipni::select('id','nome')->whereRaw("unaccent(nome) ILIKE unaccent('%{$q}%')")->get();
  
      return response()->json($data);
  
    }

    public function autocompleteSigla(Request $request)
    {
      $q= trim($request->input('q'));
      $options = 'Any-Latin; NFKD; [:Punctuation:] Remove; [^\\u0000-\\u007E] Remove';
      $q = transliterator_transliterate($options, $q);
      $data = ImunoSipni::select('id','sigla')->whereRaw("unaccent(sigla) ILIKE unaccent('%{$q}%')")->get();
  
      return response()->json($data);
  
    }

}
