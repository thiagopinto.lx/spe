<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\LoadData;
use App\Models\Regional;
use App\Models\RegionalGeocode;
use PHPUnit\Framework\Constraint\Exception;

class RegionalController extends Controller
{
    public static $route = 'regionais';
    public static $tableName = 'regionals';

    public function index()
    {
        $cores = self::$cores;
        $options['titulo'] = "Regional";
        $options['descricao'] = "Geocodes para renderização dos limite dos reginonais";
        $options['store'] = route('painel.'.self::$route.'.store');
        $options['getindex'] = route('painel.'.self::$route.'.getindex');
        return view('painel.regionais', compact('cores', 'options'));
    }

    public function getIndex(Request $request)
    {
        $tabela = self::$tableName;
        $data = Regional::toDataTables($request, $tabela);
        $data = Regional::addColumn(
            $data,
            'view',
            function($item){
                $id = $item->id;
                return '<a class="btn btn-primary" href="'.route('painel.regionais.map', compact('id')).'" role="button">Ver no mapa <i class="far fa-eye"></i></a>';
            }
        );
        return $data;
    }

    public function store(Request $request)
    {
        $fileKml = $request->file('fileKml');
        $tableName = self::$tableName;

        // Define o valor default para a variável que contém o nome da imagem
        $nameFile = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('fileKml') && $request->file('fileKml')->isValid()) {

            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));

            // Recupera a extensão do arquivo
            $extension = $fileKml->getClientOriginalExtension();

            // Define finalmente o nome
            $nameFile = "{$name}.{$extension}";

            // Faz o upload:
            $upload = $fileKml->storeAs('kmlfiles', $nameFile);
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if ( !$upload )
                return redirect()
                            ->back()
                            ->with('error', 'Falha ao fazer upload')
                            ->withInput();
        }

        try{
            $xmlRegional = simplexml_load_file(storage_path('app/'.$upload));
            $regionals = $xmlRegional->Document->Folder->children();

            foreach ($regionals->Placemark as $placemark) {
                $dataRegional = $placemark->ExtendedData->SchemaData->SimpleData;
                $coordinates = explode(" ", $placemark->MultiGeometry->Polygon->outerBoundaryIs->LinearRing->coordinates);

                $regional = new Regional;
                $regional->name = $dataRegional[1];
                $regional->description = $dataRegional[1];
                $regional->gid = $dataRegional[0];

                $regional->save();

                foreach ($coordinates as $coordinate) {
                    $latLng = explode(",", $coordinate);
                    $regionalGeocode = new RegionalGeocode;
                    $regionalGeocode->lat = $latLng[1];
                    $regionalGeocode->lng = $latLng[0];
                    $regional->geocodes()->save($regionalGeocode);
                }
            }

        }catch(Exception $e){
            dd("Arquivo mau formatado");
        }
        return "ok;";
    }

    public function map($id)
    {
      $options['titulo'] = "Mapa view";
      $regional = Regional::find($id);
      $regional->load('geocodes');

      return view('painel.regional-map', compact('regional', 'options'));
    }

}
