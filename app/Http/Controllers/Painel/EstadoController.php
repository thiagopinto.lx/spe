<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Estado;
use App\Models\EstadoGeocode;
use PHPUnit\Framework\Constraint\Exception;

class EstadoController extends Controller
{
    public static $route = 'estados';
    public static $tableName = 'estados';

    public function index()
    {
        $cores = self::$cores;
        $options['titulo'] = "Cidades";
        $options['descricao'] = "Geocodes para renderização dos limite dos bairos";
        $options['store'] = route('painel.' . self::$route . '.store');
        $options['getindex'] = route('painel.' . self::$route . '.getindex');
        return view('painel.cidades', compact('cores', 'options'));
    }

    public function getIndex(Request $request)
    {
        $tabela = self::$tableName;
        $data = Estado::toDataTables($request, $tabela);
        $data = Estado::addColumn(
            $data,
            'view',
            function ($item) {
                $id = $item->id;
                return '<a class="btn btn-primary" href="' . route('painel.estados.map', compact('id')) . '" role="button">Ver no mapa <i class="far fa-eye"></i></a>';
            }
        );
        return $data;
    }

    public function store(Request $request)
    {
        $fileKml = $request->file('fileKml');
        $tableName = self::$tableName;

        // Define o valor default para a variável que contém o nome da imagem
        $nameFile = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('fileKml') && $request->file('fileKml')->isValid()) {
            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));

            // Recupera a extensão do arquivo
            $extension = $fileKml->getClientOriginalExtension();

            // Define finalmente o nome
            $nameFile = "{$name}.{$extension}";

            // Faz o upload:
            $upload = $fileKml->storeAs('fileskml', $nameFile);
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if (!$upload) {
                return redirect()
                    ->back()
                    ->with('error', 'Falha ao fazer upload')
                    ->withInput();
            }
        }

        try {
            $xmlEstados = simplexml_load_file(storage_path('app/' . $upload));
            $estados = $xmlEstados->Document->Folder->children();
            $valuesSqlUpSert = "";

            $countPlacemark = count($estados->Placemark);

            for ($i = 0; $i < $countPlacemark; $i++) {
                $placemark = $estados->Placemark[$i];
                $dataEstado = $placemark->ExtendedData->SchemaData->SimpleData;
                $coordinates = explode(
                    " ",
                    $placemark
                        ->MultiGeometry
                        ->Polygon
                        ->outerBoundaryIs
                        ->LinearRing
                        ->coordinates
                );

                $coordinates = array_values(array_unique($coordinates));

                $estado = Estado::where('co_ibge', intval($dataEstado[4]))->first();

                $countCoordinates = count($coordinates);

                for ($j = 0; $j < $countCoordinates; $j++) {
                    $latLng = explode(",", $coordinates[$j]);

                    $value =
                        "(
                            {$estado->id},
                            {$latLng[1]},
                            {$latLng[0]}
                        )";

                    $valuesSqlUpSert .= "{$value}, ";
                }

                if ($i < ($countPlacemark - 1)) {
                    $valuesSqlUpSert .= "{$value}, ";
                } else {
                    $valuesSqlUpSert .= "{$value}";
                }
            }

            $SqlUpSert = "
            INSERT INTO public.estados_geocodes(estado_id, lat, lng)
            VALUES {$valuesSqlUpSert};";

            try {
                DB::statement($SqlUpSert);
            } catch (\Throwable $th) {
                echo ($th);
                DB::rollback();
            }
        } catch (Exception $e) {
            dd("Arquivo mau formatado");
        }
        return "ok;";
    }

    public function map($id)
    {
        $options['titulo'] = "Mapa view";
        $cidade = Estado::find($id);
        $cidade->load('geocodes');

        return view('painel.cidade-map', compact('cidade', 'options'));
    }

    public function autocomplete(Request $request)
    {
        $q = trim($request->input('q'));
        $options = 'Any-Latin; NFKD; [:Punctuation:] Remove; [^\\u0000-\\u007E] Remove';
        $q = transliterator_transliterate($options, $q);
        $data = Estado::select('id', 'nome')->whereRaw("unaccent(name) ILIKE unaccent('%{$q}%')")->get();

        return response()->json($data);
    }
}
