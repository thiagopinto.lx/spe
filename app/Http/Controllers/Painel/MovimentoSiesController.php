<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Models\MovimentoSies;
use App\Models\LoadSies;
use App\Models\EstabelecimentoCnes;
use App\Utilities\GoogleMaps;
use Predis\Configuration\Options;
use PhpParser\Node\Scalar\MagicConst\Class_;


class MovimentoSiesController extends Controller
{
    public static $resouce = 'movimento-sies';
    public static $tableName = 'movimento_sies';

    public function index($load_sies_id)
    {
        $options['titulo'] = "Tabelas de Carga do Sies";
        $options['descricao'] = "Lista de Carga do Sies";
        $options['getindex'] = route('painel.'.self::$resouce.'.getindex', compact('load_sies_id'));
        return view('painel.movimento-sies', compact('options'));
    }

    public function getIndex(Request $request, $load_sies_id)
    {
      $tabela = self::$tableName;
      $loadSies = LoadSies::find($load_sies_id);
      $tabela = $tabela.$loadSies->ano;
      $data = MovimentoSies::toDataTables($request, $tabela, $load_sies_id);

      $data = MovimentoSies::addColumn(
              $data,
              'edit',
                  function($item) use ($tabela){
                      return '<a class="btn btn-primary" href="" role="button"><i class="fas fa-list"></i></a>';
                  }
              );

      return $data;
    }

}
