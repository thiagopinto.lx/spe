<?php

namespace App\Http\Controllers\Painel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LoadInmet;
use App\Models\DadosInmet;
use Predis\Configuration\Options;
use PhpParser\Node\Scalar\MagicConst\Class_;


class DadosInmetController extends Controller
{
    public static $resouce = 'dados-inmet';
    public static $tableName = 'inmet';

    public function index($load_inmet_id)
    {
      $options['titulo'] = "Tabelas de Dados INMET";
      $options['descricao'] = "Lista Dados INMET";
      $options['getindex'] = route('painel.'.self::$resouce.'.getindex', compact('load_inmet_id'));
      return view('painel.dados-inmet', compact('options'));
    }

    public function getIndex(Request $request, $load_inmet_id)
    {
      $tabela = self::$tableName;
      $loadInmet = LoadInmet::find($load_inmet_id);
      $tabela = $tabela.'_'.$loadInmet->ano;
      $data = DadosInmet::toDataTables($request, $tabela, $load_inmet_id);

      return $data;
    }

}
