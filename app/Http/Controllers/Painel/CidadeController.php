<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Cidade;
use App\Models\CidadeGeocode;
use PHPUnit\Framework\Constraint\Exception;

class CidadeController extends Controller
{
    public static $route = 'cidades';
    public static $tableName = 'cidades';

    public function index()
    {
        $cores = self::$cores;
        $options['titulo'] = "Cidades";
        $options['descricao'] = "Geocodes para renderização dos limite dos cidades";
        $options['store'] = route('painel.' . self::$route . '.store');
        $options['getindex'] = route('painel.' . self::$route . '.getindex');
        return view('painel.cidades', compact('cores', 'options'));
    }

    public function getIndex(Request $request)
    {
        $tabela = self::$tableName;
        $data = Cidade::toDataTables($request, $tabela);
        $data = Cidade::addColumn(
            $data,
            'view',
            function ($item) {
                $id = $item->id;
                return '<a class="btn btn-primary" href="' . route('painel.cidades.map', compact('id')) . '" role="button">Ver no mapa <i class="far fa-eye"></i></a>';
            }
        );
        return $data;
    }

    public function store(Request $request)
    {
        $fileKml = $request->file('fileKml');
        $tableName = self::$tableName;

        // Define o valor default para a variável que contém o nome da imagem
        $nameFile = null;

        // Verifica se informou o arquivo e se é válido
        if ($request->hasFile('fileKml') && $request->file('fileKml')->isValid()) {
            // Define um aleatório para o arquivo baseado no timestamps atual
            $name = uniqid(date('HisYmd'));

            // Recupera a extensão do arquivo
            $extension = $fileKml->getClientOriginalExtension();

            // Define finalmente o nome
            $nameFile = "{$name}.{$extension}";

            // Faz o upload:
            $upload = $fileKml->storeAs('fileskml', $nameFile);
            // Se tiver funcionado o arquivo foi armazenado em storage/app/public/categories/nomedinamicoarquivo.extensao

            // Verifica se NÃO deu certo o upload (Redireciona de volta)
            if (!$upload) {
                return redirect()
                    ->back()
                    ->with('error', 'Falha ao fazer upload')
                    ->withInput();
            }
        }

        try {
            $xmlCidades = simplexml_load_file(storage_path('app/' . $upload));
            $Cidades = $xmlCidades->Document->Folder->children();

            foreach ($Cidades->Placemark as $placemark) {
                $dataCidade = $placemark->ExtendedData->SchemaData->SimpleData;
                $coordinates = explode(
                    " ",
                    $placemark->MultiGeometry->Polygon->outerBoundaryIs->LinearRing->coordinates
                );

                $Cidade = new Cidade;
                $Cidade->nome = $dataCidade[2];
                $Cidade->co_ibge = $dataCidade[1];

                $Cidade->save();

                foreach ($coordinates as $coordinate) {
                    $latLng = explode(",", $coordinate);
                    $CidadeGeocode = new CidadeGeocode;
                    $CidadeGeocode->lat = $latLng[1];
                    $CidadeGeocode->lng = $latLng[0];
                    $Cidade->geocodes()->save($CidadeGeocode);
                }
            }
        } catch (Exception $e) {
            dd("Arquivo mau formatado");
        }
        return "ok;";
    }

    public function map($id)
    {
        $options['titulo'] = "Mapa view";
        $cidade = Cidade::find($id);
        $cidade->load('geocodes');

        return view('painel.cidade-map', compact('cidade', 'options'));
    }

    public function autocomplete(Request $request)
    {
        $q = trim($request->input('q'));
        $options = 'Any-Latin; NFKD; [:Punctuation:] Remove; [^\\u0000-\\u007E] Remove';
        $q = transliterator_transliterate($options, $q);
        $data = Cidade::select('id', 'nome')->whereRaw("unaccent(name) ILIKE unaccent('%{$q}%')")->get();

        return response()->json($data);
    }
}
