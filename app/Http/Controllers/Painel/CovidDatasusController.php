<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\CovidDatasus;
use App\Models\BairroAlias;
use App\Models\Bairro;
use App\Utilities\GoogleMaps;
use App\Utilities\OpenDatasus;
use DateTime;

class CovidDatasusController extends Controller
{
    public static $resouce = 'covid-datasus';
    public static $tableName = 'coviddatasus';

    public function index()
    {
        $lastCovideDatasus = CovidDatasus::select('created_at')->orderBy('created_at', 'desc')->first();
        if (isset($lastCovideDatasus)) {
            $lastDateUpdate = $lastCovideDatasus->created_at->format('Y-m-d');
        } else {
            $lastDateUpdate = '2020-01-01';
        }

        //dd($lastDateUpdate);
        $options['titulo'] = "Tabelas de caosos COVID-19 Open Datasus";
        $options['descricao'] = "Lista caosos COVID-19 por Open Datasus";
        $options['getindex'] = route('painel.' . self::$resouce . '.getindex');
        return view('painel.covid-datasus', compact('options', 'lastDateUpdate'));
    }

    public function getIndex(Request $request)
    {
        $tabela = self::$tableName;
        $data = CovidDatasus::toDataTables($request, $tabela);

        return $data;
    }

    /**
     * Undocumented function
     *
     * @param Datetime $date
     * @return void
     */
    public function update($date = null, $now = null)
    {
        $now = new DateTime($now);
        $date = new DateTime($date);
        $count = OpenDatasus::getCountNotificacoes(
            $date->format(DateTime::ATOM),
            $now->format(DateTime::ATOM),
        );
        $from = "";

        $notificacoes = (Array) OpenDatasus::getNotificacoes(
            $date->format(DateTime::ATOM),
            $now->format(DateTime::ATOM),
            0,
            $count
        );

        foreach ($notificacoes as $notificacao) {
            try {
                $covidDatasus = CovidDatasus::updateOrCreate(
                    [
                        'source_id' => $notificacao['_source']['source_id'],
                        '_created_at_datasus' => $notificacao['_source']['_created_at']
                    ]
                );
            } catch (\Throwable $th) {
                dd($notificacao);
            }

            CovidDatasus::saveAs($covidDatasus, $notificacao);
        }
        return 'OK';
    }

    public function indexBairro()
    {
        $options['titulo'] = "Tabela correspondência entre bairros de Teresina e bairros do Data SUS";
        $options['descricao'] = "Tabela correspondência";
        $options['getindex'] = route('painel.bairro-alias.getindex');
        return view('painel.bairro-alias', compact('options'));
    }

    public function getIndexBairro(Request $request)
    {
        $tabela = 'bairroalias';
        $data = BairroAlias::toDataTables($request, $tabela);

        $data = BairroAlias::addColumn(
            $data,
            'bairro',
            function ($item) use ($tabela) {
                $bairroAlias = BairroAlias::find($item->id);
                if ($bairroAlias->bairro_id == null) {
                    return '<div class="form-group multiple-datasets">
                        <input type="text" class="form-control bg-danger typeahead" placeholder="Bairro Teresina" name="bairo-teresina-0">
                        <input type="hidden" class="idbairroAlias" name="idbairroAlias" value="' . $item->id . '">
                        </div>';
                } else {
                    $bairro = Bairro::find($bairroAlias->bairro_id);
                    return '<div class="form-group multiple-datasets">
                        <input type="text" class="form-control typeahead" placeholder="Bairro Teresina" value="' . $bairro->name . '">
                        <input type="hidden" class="idbairroAlias" name="idbairroAlias" value="' . $item->id . '">
                        </div>';
                }
            }
        );

        $data = BairroAlias::addColumn(
            $data,
            'checkbox',
            function ($item) {
                return '<div class="form-check">
                            <input class="form-check-input" name="checkboxs[]" type="checkbox" value="' . $item->id . '" id="defaultCheck1">
                        </div>';
            }
        );

        return $data;
    }

    public function relationship($idbairroAlias, $idBairro)
    {
        $bairroAlias = BairroAlias::find($idbairroAlias);
        $bairroAlias->bairro_id = $idBairro;
        $bairroAlias->save();
    }

    public function getGps(Request $request)
    {
        foreach ($request->checkboxs as $item) {
            $bairroAlias = BairroAlias::find($item);

            if ($bairroAlias->bairro_id == null) {
                $googleBairros = GoogleMaps::searchBairro(
                    $bairroAlias->nome
                );


                foreach ($googleBairros as $googleBairro) {
                    $q = strtoupper(trim($googleBairro->long_name));
                    $options = 'Any-Latin; NFKD; [:Punctuation:] Remove; [^\\u0000-\\u007E] Remove';
                    $bairro = Bairro::whereRaw(
                        "unaccent(name) ILIKE unaccent('%{$q}%')"
                    )->first();

                    //$bairro = Bairro::where(
                    //    'name',
                    //    'LIKE',
                    //    "%{strtoupper($googleBairro->long_name)}%"
                    //)->first();

                    if ($bairro != null) {
                        var_dump($bairro);
                        break;
                    }
                }

                if ($bairro != null) {
                    $bairroAlias->bairro_id = $bairro->id;
                    $bairroAlias->save();
                }
            }
        }
        return json_encode(true);
    }
}