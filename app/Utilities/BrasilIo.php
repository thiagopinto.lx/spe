<?php

/**
 * Classe utilitarias
 *
 * @category Utilities
 * @author   Thiago Pinto Dias <thiagopinto.lx@gmail.com>
 *
 */

namespace App\Utilities;

use GuzzleHttp\Client;

/**
 * Undocumented class
 */
class BrasilIo
{
    public static function getRegisterCovidStateCapital($date, $co_ibge_capital, $tokenCapitais)
    {
        $client = new Client([
    'base_uri' => 'https://api.brasil.io/',
    'timeout' => 30
  ]);

        $response = $client->get(
            "dataset/covid19/caso_full/data/?date={$date}&city_ibge_code={$co_ibge_capital}",
            [
              'headers' => [
                  'Authorization' => "Token {$tokenCapitais}"
              ]
          ]
        );
        return json_decode($response->getBody());
    }
    public static function getRegisterCovidState($date, $tokenState)
    {
        $client = new Client([
      'base_uri' => 'https://api.brasil.io/',
      'timeout' => 30
    ]);

        $response = $client->get(
            "dataset/covid19/caso_full/data/?date={$date}&place_type=state",
            [
                'headers' => [
                    'Authorization' => "Token {$tokenState}"
                ]
            ]
        );
        return json_decode($response->getBody());
    }

    public static function getRegisterCovidCity($date, $state, $tokenCity)
    {
        $client = new Client([
      'base_uri' => 'https://api.brasil.io/',
      'timeout' => 30
    ]);

        $response = $client->get(
            "dataset/covid19/caso_full/data/?date={$date}&state={$state}&place_type=city",
            [
                'headers' => [
                    'Authorization' => "Token {$tokenCity}"
                ]
            ]
        );
        return json_decode($response->getBody());
    }

    public static function getRegisterCovid($date, $token, $registers, $url = NULL)
    {
      if($url) {
        $client = new Client([
          'timeout' => 30
        ]);

        $response = $client->get(
            $url,
            [
                'headers' => [
                    'Authorization' => "Token {$token}"
                ]
            ]
        );
      } else {
        $client = new Client([
          'base_uri' => 'https://api.brasil.io/',
          'timeout' => 30
        ]);

        $response = $client->get(
            "dataset/covid19/caso_full/data/?page_size=10000&date={$date}",
            [
                'headers' => [
                    'Authorization' => "Token {$token}"
                ]
            ]
        );
    
      }
         
        $response = json_decode($response->getBody());

        $registers = array_merge($registers, $response->results);

        if($response->next){
          $registers = BrasilIo::getRegisterCovid($date, $token, $registers, $response->next);
        }

        return $registers;
    }
}
