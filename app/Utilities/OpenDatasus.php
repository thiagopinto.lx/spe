<?php

/**
 * Classe utilitarias
 *
 * @category Utilities
 * @author   Thiago Pinto Dias <thiagopinto.lx@gmail.com>
 *
 */

namespace App\Utilities;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

/**
 * Class para recuperar dados do dataset do datasus
 *
 * @category Utilities
 * @author   Thiago Pinto Dias <thiagopinto.lx@gmail.com>
 */
class OpenDatasus
{
    /**
     * Undocumented function
     *
     * @param String $data
     * @return void
     */
    public static function getCountNotificacoes($data, $now)
    {
        $hosts = [
            [
                'host' => 'elasticsearch-saps.saude.gov.br',
                'port' => '443',
                'scheme' => 'https',
                'path' => '',
                'user' => 'municipio-esusve-pi-teresina',
                'pass' => 'gisahicimo'
            ]
        ];
        $paramsCount = [
            'index' => 'municipio-esusve-pi-teresina',
            'body'  => [
                'query' => [
                    'bool' => [
                        'must' => [
                            ['match' => ['municipio' => 'Teresina']],
                            ['range' => [
                                '_updated_at' => [
                                    'time_zone' => '-03:00',
                                    'gte' => $data,
                                    'lte' => $now
                                ]
                            ]]
                        ]
                    ]
                ]
            ]
        ];

        $client = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();

        $response = $client->count($paramsCount);

        return $response['count'];
    }

    /**
     * Undocumented function
     *
     * @param String $data
     * @return void
     */
    public static function getNotificacoes($data, $now, $from, $size)
    {
        $hosts = [
            [
                'host' => 'elasticsearch-saps.saude.gov.br',
                'port' => '443',
                'scheme' => 'https',
                'path' => '',
                'user' => 'municipio-esusve-pi-teresina',
                'pass' => 'gisahicimo'
            ]
        ];

        $client = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();

        $paramsSearch = [
            'index' => 'municipio-esusve-pi-teresina',
            'from' => $from,
            'size' => $size,
            'body'  => [
                'query' => [
                    'bool' => [
                        'must' => [
                            ['match' => ['municipio' => 'Teresina']],
                            ['range' => [
                                '_updated_at' => [
                                    'time_zone' => '-03:00',
                                    'gte' => $data,
                                    'lte' => $now
                                ]
                            ]]
                        ]
                    ]
                ]
            ]
        ];

        $response = $client->search($paramsSearch);

        return $response["hits"]["hits"];
    }
}
