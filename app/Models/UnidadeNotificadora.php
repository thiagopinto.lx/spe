<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use App\Utilities\Helper;

class UnidadeNotificadora extends Model
{
    //cria tabela dinamicamente
    public static function createTable($tableName, $upload)
    {

        $db = dbase_open(storage_path('app/'.$upload), 0);

        if($db){

            $columns = dbase_get_header_info($db);

            try {
                Schema::create($tableName, function (Blueprint $table) use ($columns){

                    $laravel_type['memo']      = 'text';
                    $laravel_type['character'] = 'string';
                    $laravel_type['number']    = 'integer';
                    $laravel_type['float']     = 'float';
                    $laravel_type['date']      = 'date';

                    $table->increments('id');

                    foreach ($columns as $column) {

                        $prefix = str_split($column['name'], 2);
                        if($column['name'] == "ID_CNS_SUS"){
                            $table->bigInteger($column['name'])->nullable();
                        }
                        if($column['name'] == "NU_CGC_UNI"){
                            $table->bigInteger($column['name'])->nullable();
                        }
                        elseif($column['name'] == "ID_GEO1" || $column['name'] == "ID_GEO2"){
                            $table->double($column['name'])->nullable();
                        }
                        elseif($column['name'] == "NU_NUMERO"){
                            $table->string($column['name'])->nullable();
                        }
                        elseif($column['name'] == "NU_TELEFON"){
                            $table->string($column['name'])->nullable();
                        }
                        elseif($column['name'] == "ID_DIGIT"){
                            $table->string($column['name'])->nullable();
                        }
                        elseif($column['name'] == "ID_EMAIL"){
                            $table->string($column['name'])->nullable();
                        }
                        elseif($column['name'] == "ID_FAX"){
                            $table->string($column['name'])->nullable();
                        }
                        elseif($column['name'] == "NU_ESTAB"){
                            $table->string($column['name'])->nullable();
                        }
                        elseif($column['name'] == "ID_OCUPA_N"){
                            $table->string($column['name'])->nullable();
                        }
                        elseif ($prefix[0] == 'NU' || $prefix[0] == "ID" && $column['name'] != "ID_AGRAVO" && $column['name'] != "IDENT_MICR") {
                            $table->integer($column['name'])->nullable();
                        }
                        elseif ($laravel_type[$column['type']] == 'string') {
                            $table->string($column['name'])->nullable();
                        }
                        elseif ($laravel_type[$column['type']] == 'integer') {
                            $table->integer($column['name'])->nullable();
                        }
                        elseif ($laravel_type[$column['type']] == 'text') {
                            $table->text($column['name'])->nullable();
                        }
                        elseif ($laravel_type[$column['type']] == 'float') {
                            $table->float($column['name'])->nullable();
                        }
                        elseif ($laravel_type[$column['type']] == 'date') {
                            $table->date($column['name'])->nullable();
                        }
                    }

                    $table->timestamps();
                });
                dbase_close($db);
            } catch (Exception $e) {
                dbase_close($db);
                Storage::delete($upload);
                echo "Não foi possivel criar a tabela 03"
                    .$tableName
                    .$e->getMessage();
                return false;
            }

        }
      return true;
    }

    //carga inicial de dados feita via dbf
    public static function dbfLoad($tableName, $upload, $isUpdate)
    {
      $db = dbase_open(storage_path('app/'.$upload), 0);

        if($db){
            $num_rows    = dbase_numrecords($db);

            try {
                DB::beginTransaction();
                # Loop the Dbase records
                for($index=1; $index <= $num_rows; $index ++)
                {
                    # Get one record
                    $record = dbase_get_record_with_names($db, $index);
                    # Ignore deleted fields
                    if (
                        $record["deleted"] != "1"
                    )
                    {
                        # Insert the record
                        foreach ($record as $key=>$field)
                        {
                            if ($key!=='deleted')
                            {
                                $field = str_replace("'", "\'", $field);
                                $field = str_replace("", "SN", $field);
                                $field = trim($field);
                                $field =  utf8_encode($field);

                                $key = str_replace("'", "\'", $key);
                                $key = trim($key);
                                $key =  utf8_encode($key);
                                if($field != ""){
                                    $data[$key] = $field;
                                }
                            }
                        }
                        if($isUpdate){
                            $data['updated_at'] = date('Y-m-d H:i:s');
                            DB::table($tableName)
                                ->updateOrInsert(
                                    ['ID_UNIDADE' => $data['ID_UNIDADE']],
                                    $data
                                );
                        }else{
                            $data['created_at'] = date('Y-m-d H:i:s');
                            $data['updated_at'] = date('Y-m-d H:i:s');
                            DB::table($tableName)->insert($data);
                        }
                    }
                }
                dbase_close($db);
                Storage::delete($upload);
                DB::commit();
            } catch (\Throwable $e) {
                echo "Não foi possível carregar os dados "
                .$tableName
                .$e->getMessage();
                dbase_close($db);
                Storage::delete($upload);
                return false;
            }

        }
      return true;
    }

    public static function toDataTables($request, $tabela, $where = NULL)
    {

        try {
            $columnsName = array();
            $columnsSearchable = array();

            $foreignKeys = null;
            foreach ($request->columns as $column){

                if ($column['orderable'] != 'false') {
                    if (strpos($column['name'], '_id') !== false) {
                        $foreignTable  = explode("_", $column['name']);
                        $foreignTable[0] = $foreignTable[0].'s';
                        $foreignTable[] = $column['name'];
                        $foreignKeys[] = $foreignTable;
                        $columnsName[] = $foreignTable[0].
                        '.name';
                    }else{
                        $columnsName[] = $tabela.'.'.$column['name'];
                    }
                }

                if($column['searchable'] != 'false'){
                    $columnsSearchable[] = $tabela.'.'.$column['name'];
                }

            }
            $columnsNameCont = count($columnsName);
            $columnsSearchableCont = count($columnsSearchable);

            if ($where == NULL) {
                $totalData = DB::table($tabela)->count();
            }else{
                $totalData = DB::table($tabela)->where("agravo", $where)->count();
            }

            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columnsName[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if(empty($request->input('search.value')))
            {
                if ($where == NULL) {
                    $query = DB::table($tabela)
                    ->select($columnsName)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir);
                }else{
                    $query = DB::table($tabela)
                    ->select($columnsName)
                    ->where("agravo", $where)
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir);
                }

                if($foreignKeys !== null){
                    foreach ($foreignKeys as $foreignKey) {
                        $query->join($foreignKey[0],
                            $foreignKey[0].'.'.$foreignKey[1],
                            '=',
                            $tabela.'.'.$foreignKey[2]);
                    }
                }

                $registros = $query->get();
            } else {

                $search = $request->input('search.value');
                $query = DB::table($tabela)
                    ->select($columnsName)
                    ->where(function($q) use ($columnsSearchable, $search){
                        foreach ($columnsSearchable as $columnSearchable) {
                            $q->orWhere($columnSearchable , 'ILIKE', "%$search%");
                            if (isset($where)) {
                                $q->where("agravo", $where);
                            }
                        }

                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir);
                $registros = $query->get();

                $totalFiltered = DB::table($tabela)
                ->where(function($q) use ($columnsSearchable, $search){
                    foreach ($columnsSearchable as $columnSearchable) {
                        $q->orWhere($columnSearchable , 'ILIKE', "%$search%");
                        if (isset($where)) {
                            $q->where("agravo", $where);
                        }
                    }

                })
                ->count();
            }

            $data = array();
            if(!empty($registros))
            {
                foreach ($registros as $registro)
                {
                    if($foreignKeys !== null){
                        $nestedData = (array) $registro;
                        foreach ($foreignKeys as $foreignKey) {
                            $nestedData[$foreignKey[2]] = $nestedData['name'];
                            unset($nestedData['name']);
                        }
                    }else{
                        $nestedData = (array) $registro;
                    }

                    $data[] = $nestedData;

                }

            }

            $datas = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data"            => $data
                );
        } catch (\Throwable $th) {
            $datas = null;
        }

        return json_encode($datas);

    }


}
