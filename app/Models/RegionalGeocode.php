<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use App\Utilities\Helper;
use App\Models\Regional;

class RegionalGeocode extends Model
{
  public function regional()
  {
      return $this->belongsTo('App\Models\Regional');
  }
}
