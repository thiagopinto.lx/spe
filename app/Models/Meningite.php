<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;


class Meningite extends Agravo
{

  public static function createDataDetalhamentoUnidade($mydata, $unidade)
  {
      $data = array();
      $notif[] = "Notificações";
      foreach ($mydata->periodos as $periodo) {
          $notif[] = DB::table($mydata->table_name . $periodo)
            ->where([
            ['ID_UNIDADE', $unidade]
            ])->count();
      }
      $data[] = $notif;

      for ($i = 1; $i <= count($mydata->criterios); $i++) {
          $data[$i][0] = $mydata->criterios[$i - 1][0];
          for ($j = 1; $j <= count($mydata->periodos); $j++) {
              $data[$i][$j] = $mydata->criterios[$i - 1][1]($mydata->table_name . $mydata->periodos[$j - 1]);
          }
      }

      return $data;
  }

  public static function getBySexoUnidade($table_name, $anos, $unidade)
  {
      $mydata = new \stdClass;
      $mydata->table_name = $table_name;

      $mydata->periodos = $anos;

      $mydata->criterios[] = ["Masculino", function ($tabela) use ($unidade) {
          return DB::table($tabela)->where([
            ['CS_SEXO', 'M'],
            ['ID_UNIDADE', '=', $unidade]
          ])->count();
      }];
      $mydata->criterios[] = ["Femenino", function ($tabela) use ($unidade){
          return DB::table($tabela)->where([
            ['CS_SEXO', 'F'],
            ['ID_UNIDADE', '=', $unidade]
          ])->count();
      }];
      $mydata->criterios[] = ["Indefinido", function ($tabela) use ($unidade){
          return DB::table($tabela)->where([
            ['CS_SEXO', 'I'],
            ['ID_UNIDADE', '=', $unidade]
          ])->count();
      }];

      return Meningite::createDataDetalhamentoUnidade($mydata, $unidade);
  }
}
