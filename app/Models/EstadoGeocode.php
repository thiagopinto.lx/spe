<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Estado;

class EstadoGeocode extends Model
{
    protected $table = 'estados_geocodes';

    public function estado()
    {
        return $this->belongsTo('App\Models\Estado');
    }
}
