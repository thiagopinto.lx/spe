<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;

class CasosCovid extends Model
{
    protected $guarded = [];
    protected $table = 'casoscovid';

    public function cidade()
    {
        return $this->belongsTo('App\Models\Cidade');
    }

    public function estado()
    {
        return $this->belongsTo('App\Models\Estado');
    }

    public static function toDataTables($request, $tabela, $where = null)
    {
        try {
            $columnsName = array();
            $columnsSearchable = array();

            $foreignKeys = null;
            foreach ($request->columns as $column) {
                if ($column['orderable'] != 'false') {
                    if (strpos($column['name'], '_id') !== false) {
                        $foreignTable  = explode("_", $column['name']);
                        $foreignTable[0] = $foreignTable[0] . 's';
                        $foreignTable[] = $column['name'];
                        $foreignKeys[] = $foreignTable;
                        $columnsName[] = $foreignTable[0] .
                            '.nome';
                    } else {
                        $columnsName[] = $tabela . '.' . $column['name'];
                    }
                }

                if ($column['searchable'] != 'false') {
                    $columnsSearchable[] = $tabela . '.' . $column['name'];
                }
            }
            $columnsNameCont = count($columnsName);
            $columnsSearchableCont = count($columnsSearchable);

            if ($where == null) {
                $totalData = DB::table($tabela)->count();
            } else {
                $totalData = DB::table($tabela)->where("casos", $where)->count();
            }

            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columnsName[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if (empty($request->input('search.value'))) {
                if ($where == null) {
                    $query = DB::table($tabela)
                        ->select($columnsName)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order, $dir);
                } else {
                    $query = DB::table($tabela)
                        ->select($columnsName)
                        ->where("casos", $where)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order, $dir);
                }

                if ($foreignKeys !== null) {
                    foreach ($foreignKeys as $foreignKey) {
                        $query->join(
                            $foreignKey[0],
                            $foreignKey[0] . '.' . $foreignKey[1],
                            '=',
                            $tabela . '.' . $foreignKey[2]
                        );
                    }
                }

                $registros = $query->get();
            } else {
                $search = $request->input('search.value');
                $query = DB::table($tabela)
                    ->select($columnsName)
                    ->where(function ($q) use ($columnsSearchable, $search, $where) {
                        foreach ($columnsSearchable as $columnSearchable) {
                            $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
                            if (isset($where)) {
                                $q->where("casos", $where);
                            }
                        }
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir);
                $registros = $query->get();

                $totalFiltered = DB::table($tabela)
                    ->where(function ($q) use ($columnsSearchable, $search, $where) {
                        foreach ($columnsSearchable as $columnSearchable) {
                            $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
                            if (isset($where)) {
                                $q->where("casos", $where);
                            }
                        }
                    })
                    ->count();
            }

            $data = array();
            if (!empty($registros)) {
                foreach ($registros as $registro) {
                    if ($foreignKeys !== null) {
                        $nestedData = (array) $registro;
                        foreach ($foreignKeys as $foreignKey) {
                            $nestedData[$foreignKey[2]] = $nestedData['nome'];
                            unset($nestedData['nome']);
                        }
                    } else {
                        $nestedData = (array) $registro;
                    }

                    $data[] = $nestedData;
                }

                $datas = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                );
            } else {
                $datas = null;
            }
        } catch (\Throwable $th) {
            dd($th);
            $datas = null;
        }

        return json_encode($datas);
    }

    //carregar dados
    public static function csvLoad($tableName, $upload)
    {
        $delimitador = ',';
        $cerca = '';

        $file = file_get_contents(storage_path('app/' . $upload));

        if ($file) {
            $header = null;
            $datas = array();
            $numRow = 0;
            $rows = explode("\n", $file);

            foreach ($rows as $row) {
                $item = str_getcsv($row, $delimitador, $cerca, "\n");
                if (!$header) {
                    $header = $item;
                    continue;
                }
                if (count($header) == count($item)) {
                    $datas[] = array_combine($header, $item);
                }
            }

            $piaui = Estado::where('uf', "PI")->first();
            $maranhao = Estado::where('uf', "MA")->first();
            $ceara = Estado::where('uf', "CE")->first();
            $bahia = Estado::where('uf', "BA")->first();
            $pernanbuco = Estado::where('uf', "PE")->first();

            foreach ($datas as $data) {
                if ($data["city_ibge_code"] != "") {
                    if ($data["place_type"] == "city") {
                        if ($data["state"] == "PI") {
                            if ($data["city_ibge_code"] == 22) {
                                $cidade = Cidade::updateOrCreate(
                                    [
                                    'nome' => "CASO SEM LOCALIZAÇÃO DEFINIDA/PI",
                                    'co_ibge' => 22
                                    ]
                                );
                            } else {
                                $cidade = Cidade::where('co_ibge', $data["city_ibge_code"])->first();
                                $cidade->estado_id = $piaui->id;
                                $cidade->populacao = $data["estimated_population_2019"];
                                $cidade->save();
                            }

                            $casosCovid = CasosCovid::updateOrCreate(
                                [
                                'data' => $data['date'],
                                'semana_epidemiologica' => $data['epidemiological_week'],
                                'cidade_id' => $cidade->id,
                                'tipo_de_lugar' => $data["place_type"],
                                'estado' => $data["state"]
                                ]
                            );

                            if (Estado::where('co_ibge_capital', $data["city_ibge_code"])->exists()) {
                                $casosCovid->capital = true;
                                $estado = Estado::where('co_ibge_capital', $data["city_ibge_code"])
                                ->first();
                                $estado->populacao_capital = $data["estimated_population_2019"];
                                $estado->save();
                            }
                            $casosCovid->estado_id = $estado->id;
                            $casosCovid->casos = $data["new_confirmed"];
                            $casosCovid->obitos = $data["new_deaths"];
                            $casosCovid->save();
                        } elseif ($data["state"] == "MA") {
                            if ($data["city_ibge_code"] == 21) {
                                $cidade = Cidade::updateOrCreate(
                                    [
                                    'nome' => "CASO SEM LOCALIZAÇÃO DEFINIDA/PI",
                                    'co_ibge' => 21
                                    ]
                                );
                            } else {
                                $cidade = Cidade::where('co_ibge', $data["city_ibge_code"])->first();
                                $cidade->estado_id = $maranhao->id;
                                $cidade->populacao = $data["estimated_population_2019"];
                                $cidade->save();
                            }

                            $casosCovid = CasosCovid::updateOrCreate(
                                [
                                'data' => $data['date'],
                                'semana_epidemiologica' => $data['epidemiological_week'],
                                'cidade_id' => $cidade->id,
                                'tipo_de_lugar' => $data["place_type"],
                                'estado' => $data["state"]
                                ]
                            );

                            if (Estado::where('co_ibge_capital', $data["city_ibge_code"])->exists()) {
                                $casosCovid->capital = true;
                                $estado = Estado::where('co_ibge_capital', $data["city_ibge_code"])
                                ->first();
                                $estado->populacao_capital = $data["estimated_population_2019"];
                                $estado->save();
                            }
                            $casosCovid->estado_id = $estado->id;
                            $casosCovid->casos = $data["new_confirmed"];
                            $casosCovid->obitos = $data["new_deaths"];
                            $casosCovid->save();
                        } elseif ($data["state"] == "CE") {
                            if ($data["city_ibge_code"] == 23) {
                                $cidade = Cidade::updateOrCreate(
                                    [
                                    'nome' => "CASO SEM LOCALIZAÇÃO DEFINIDA/PI",
                                    'co_ibge' => 23
                                    ]
                                );
                            } else {
                                $cidade = Cidade::where('co_ibge', $data["city_ibge_code"])->first();
                                $cidade->estado_id = $ceara->id;
                                $cidade->populacao = $data["estimated_population_2019"];
                                $cidade->save();
                            }

                            $casosCovid = CasosCovid::updateOrCreate(
                                [
                                'data' => $data['date'],
                                'semana_epidemiologica' => $data['epidemiological_week'],
                                'cidade_id' => $cidade->id,
                                'tipo_de_lugar' => $data["place_type"],
                                'estado' => $data["state"]
                                ]
                            );

                            if (Estado::where('co_ibge_capital', $data["city_ibge_code"])->exists()) {
                                $casosCovid->capital = true;
                                $estado = Estado::where('co_ibge_capital', $data["city_ibge_code"])
                                ->first();
                                $estado->populacao_capital = $data["estimated_population_2019"];
                                $estado->save();
                            }
                            $casosCovid->estado_id = $estado->id;
                            $casosCovid->casos = $data["new_confirmed"];
                            $casosCovid->obitos = $data["new_deaths"];
                            $casosCovid->save();
                        } elseif ($data["state"] == "PE") {
                            if ($data["city_ibge_code"] == 26) {
                                $cidade = Cidade::updateOrCreate(
                                    [
                                    'nome' => "CASO SEM LOCALIZAÇÃO DEFINIDA/PI",
                                    'co_ibge' => 26
                                    ]
                                );
                            } else {
                                $cidade = Cidade::where('co_ibge', $data["city_ibge_code"])->first();
                                $cidade->estado_id = $pernanbuco->id;
                                $cidade->populacao = $data["estimated_population_2019"];
                                $cidade->save();
                            }

                            $casosCovid = CasosCovid::updateOrCreate(
                                [
                                'data' => $data['date'],
                                'semana_epidemiologica' => $data['epidemiological_week'],
                                'cidade_id' => $cidade->id,
                                'tipo_de_lugar' => $data["place_type"],
                                'estado' => $data["state"]
                                ]
                            );

                            if (Estado::where('co_ibge_capital', $data["city_ibge_code"])->exists()) {
                                $casosCovid->capital = true;
                                $estado = Estado::where('co_ibge_capital', $data["city_ibge_code"])
                                ->first();
                                $estado->populacao_capital = $data["estimated_population_2019"];
                                $estado->save();
                            }
                            $casosCovid->estado_id = $estado->id;
                            $casosCovid->casos = $data["new_confirmed"];
                            $casosCovid->obitos = $data["new_deaths"];
                            $casosCovid->save();
                        } elseif ($data["state"] == "BA") {
                            if ($data["city_ibge_code"] == 29) {
                                $cidade = Cidade::updateOrCreate(
                                    [
                                    'nome' => "CASO SEM LOCALIZAÇÃO DEFINIDA/PI",
                                    'co_ibge' => 29
                                    ]
                                );
                            } else {
                                $cidade = Cidade::where('co_ibge', $data["city_ibge_code"])->first();
                                $cidade->estado_id = $bahia->id;
                                $cidade->populacao = $data["estimated_population_2019"];
                                $cidade->save();
                            }

                            $casosCovid = CasosCovid::updateOrCreate(
                                [
                                'data' => $data['date'],
                                'semana_epidemiologica' => $data['epidemiological_week'],
                                'cidade_id' => $cidade->id,
                                'tipo_de_lugar' => $data["place_type"],
                                'estado' => $data["state"]
                                ]
                            );

                            if (Estado::where('co_ibge_capital', $data["city_ibge_code"])->exists()) {
                                $casosCovid->capital = true;
                                $estado = Estado::where('co_ibge_capital', $data["city_ibge_code"])
                                ->first();
                                $estado->populacao_capital = $data["estimated_population_2019"];
                                $estado->save();
                            }
                            $casosCovid->estado_id = $estado->id;
                            $casosCovid->casos = $data["new_confirmed"];
                            $casosCovid->obitos = $data["new_deaths"];
                            $casosCovid->save();
                        } else {
                            if (Estado::where('co_ibge_capital', $data["city_ibge_code"])->exists()) {
                                $estado = Estado::where('co_ibge_capital', $data["city_ibge_code"])
                                ->first();

                                if ($estado->populacao_capital != $data["estimated_population_2019"]) {
                                    $estado->populacao_capital = $data["estimated_population_2019"];
                                    $estado->save();
                                }

                                $casosCovid = CasosCovid::updateOrCreate(
                                    [
                                    'data' => $data['date'],
                                    'semana_epidemiologica' => $data['epidemiological_week'],
                                    'estado_id' => $estado->id,
                                    'tipo_de_lugar' => $data["place_type"],
                                    'estado' => $data["state"]
                                    ]
                                );

                                $casosCovid->capital = true;
                                $casosCovid->casos = $data["new_confirmed"];
                                $casosCovid->obitos = $data["new_deaths"];
                                $casosCovid->save();
                            }
                        }
                    } elseif ($data["place_type"] == "state") {
                        $estado = Estado::where('co_ibge', $data["city_ibge_code"])
                        ->first();
                        if ($estado->populacao != $data["estimated_population_2019"]) {
                            $estado->populacao = $data["estimated_population_2019"];
                            $estado->save();
                        }

                        $casosCovid = CasosCovid::updateOrCreate(
                            [
                            'data' => $data['date'],
                            'semana_epidemiologica' => $data['epidemiological_week'],
                            'estado_id' => $estado->id,
                            'tipo_de_lugar' => $data["place_type"],
                            'estado' => $data["state"]
                            ]
                        );
                        $casosCovid->casos = $data["new_confirmed"];
                        $casosCovid->obitos = $data["new_deaths"];
                        $casosCovid->save();
                    }
                }
            }
        }
        return true;
    }

    //carregar dados
    public static function apiLoad($tableName, $datas, $UF)
    {
      $estadoCorrent = Estado::where('uf', $UF)->first();

      foreach ($datas as $data) {
        if ($data->city_ibge_code != "") {

          if ($data->place_type == "city") {

            if ($data->state == $estadoCorrent->uf) {

              if ($data->city_ibge_code == $estadoCorrent->co_ibge) {
                  $cidade = Cidade::updateOrCreate(
                      [
                  'nome' => "CASO SEM LOCALIZAÇÃO DEFINIDA/{$estadoCorrent->uf}",
                  'co_ibge' => $estadoCorrent->co_ibge
                  ]
                  );
              } else {
                  $cidade = Cidade::where('co_ibge', $data->city_ibge_code)->first();
                  $cidade->estado_id = $estadoCorrent->id;
                  $cidade->populacao = $data->estimated_population_2019;
                  $cidade->save();
              }

              $casosCovid = CasosCovid::updateOrCreate(
                [
                  'data' => $data->date,
                  'semana_epidemiologica' => $data->epidemiological_week,
                  'cidade_id' => $cidade->id,
                  'tipo_de_lugar' => $data->place_type,
                  'estado' => $data->state
                ]
              );

              if ($estadoCorrent->co_ibge_capital == intval($data->city_ibge_code)) {
                $casosCovid->capital = true;
                $estadoCorrent->populacao_capital = $data->estimated_population_2019;
                $estadoCorrent->save();
              }
              $casosCovid->estado_id = $estadoCorrent->id;
              $casosCovid->casos = $data->new_confirmed;
              $casosCovid->obitos = $data->new_deaths;
              $casosCovid->save();
            }else {

              if (Estado::where('co_ibge_capital', $data->city_ibge_code)->exists()) {

                $estado = Estado::where('co_ibge_capital', $data->city_ibge_code)->first();

                if ($estado->populacao_capital != $data->estimated_population_2019) {
                    $estado->populacao_capital = $data->estimated_population_2019;
                    $estado->save();
                }

                $casosCovid = CasosCovid::updateOrCreate(
                  [
                    'data' => $data->date,
                    'semana_epidemiologica' => $data->epidemiological_week,
                    'estado_id' => $estado->id,
                    'tipo_de_lugar' => $data->place_type,
                    'estado' => $data->state
                  ]
                );

                $casosCovid->capital = true;
                $casosCovid->casos = $data->new_confirmed;
                $casosCovid->obitos = $data->new_deaths;
                $casosCovid->save();
              }
            }
          } elseif ($data->place_type == "state") {

            $estado = Estado::where('co_ibge', $data->city_ibge_code)->first();
            if ($estado->populacao != $data->estimated_population_2019) {
                $estado->populacao = $data->estimated_population_2019;
                $estado->save();
            }

            $casosCovid = CasosCovid::updateOrCreate(
              [
                'data' => $data->date,
                'semana_epidemiologica' => $data->epidemiological_week,
                'estado_id' => $estado->id,
                'tipo_de_lugar' => $data->place_type,
                'estado' => $data->state
              ]
            );
            $casosCovid->casos = $data->new_confirmed;
            $casosCovid->obitos = $data->new_deaths;
            $casosCovid->save();
          }

        }

      }

      return true;
    }

    public static function addColumn($itens, $newKey, $function)
    {
        $itens = json_decode($itens);
        $newData = array();
        foreach ($itens->data as $item) {
            $item->$newKey = $function($item);
            $newData[] = $item;
        }

        $itens->data = $newData;

        return json_encode($itens);
    }
}
