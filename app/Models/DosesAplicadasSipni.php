<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;


class DosesAplicadasSipni extends Model
{
  protected $guarded = [];
    
  public static function toDataTables($request, $tabela, $load_sipnis_id, $where = NULL)
  {

    try {
      $columnsName = array();
      $columnsSearchable = array();

      $foreignKeys = false;
      foreach ($request->columns as $column) {

        if ($column['orderable'] != 'false') {
          if (strpos($column['name'], '_id') !== false) {
            
            $foreignTable = null;
            $foreignTable[0] = substr($column['name'], 0, strpos($column['name'], "_id"));
            $foreignTable[1] = substr($column['name'], strpos($column['name'], "_id")+1, 2);
            
            $foreignTable[] = $column['name'];
            
            if ($foreignTable[0] == "estabelecimento_sies") {
              $columnsName[] = $foreignTable[0] .
              '.no_fantasia';
            }else{
              $columnsName[] = $foreignTable[0] .
              '.nome';
            }
            $foreignKeys[] = $foreignTable;
          } else {
            $columnsName[] = $tabela . '.' . $column['name'];
          }
        }

        if ($column['searchable'] != 'false') {
          $columnsSearchable[] = $tabela . '.' . $column['name'];
        }
      }
      $columnsNameCont = count($columnsName);
      $columnsSearchableCont = count($columnsSearchable);

      if ($where == NULL) {
        $totalData = DB::table($tabela)->where("load_sipnis_id", $load_sipnis_id)->count();
      } else {
        $totalData = DB::table($tabela)->where([
          ['agravo', '=', $where],
          ['load_sipni_id', '=', $load_sipnis_id]
        ])->count();
      }

      $totalFiltered = $totalData;
      $limit = $request->input('length');
      $start = $request->input('start');
      $order = $columnsName[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');

      if (empty($request->input('search.value'))) {
        if ($where == NULL) {
          $query = DB::table($tabela)
            ->select($columnsName)
            ->where("load_sipnis_id", $load_sipnis_id)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir);
        } else {
          $query = DB::table($tabela)
            ->select($columnsName)
            ->where([
              ['agravo', '=', $where],
              ['load_sipnis_id', '=', $load_sipnis_id]
            ])
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir);
        }

        if ($foreignKeys !== null) {
          foreach ($foreignKeys as $foreignKey) {
            $query->join(
              $foreignKey[0],
              $foreignKey[0] . '.' . $foreignKey[1],
              '=',
              $tabela . '.' . $foreignKey[2]
            );
          }
        }

        $registros = $query->get();
      } else {

        $search = $request->input('search.value');
        $query = DB::table($tabela)
          ->select($columnsName)
          ->where(function ($q) use ($columnsSearchable, $search) {
            foreach ($columnsSearchable as $columnSearchable) {
              $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
              if (isset($where)) {
                $q->where("agravo", $where);
              }
            }
            $q->where("load_sipnis_id", $load_sipnis_id);
          })
          ->offset($start)
          ->limit($limit)
          ->orderBy($order, $dir);
        $registros = $query->get();

        $totalFiltered = DB::table($tabela)
          ->where(function ($q) use ($columnsSearchable, $search) {
            foreach ($columnsSearchable as $columnSearchable) {
              $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
              if (isset($where)) {
                $q->where("agravo", $where);
              }
            }
            $q->where("load_sipnis_id", $load_sipnis_id);
          })
          ->count();
      }

      $data = array();
      if (!empty($registros)) {
        foreach ($registros as $registro) {
          //dd($foreignKeys);
          if ($foreignKeys !== null) {
            $nestedData = (array) $registro;
            //dd($nestedData);
            //dd($foreignKeys);
            foreach ($foreignKeys as $foreignKey) {
              if($foreignKey[2] == "estabelecimento_sies_id"){
                $nestedData[$foreignKey[2]] = $nestedData['no_fantasia'];
                unset($nestedData['no_fantasia']);
              }else{
                $nestedData[$foreignKey[2]] = $nestedData['nome'];
                unset($nestedData['nome']);
              }
            }
            //dd($nestedData);
          } else {
            $nestedData = (array) $registro;
          }

          $data[] = $nestedData;
        }

        $datas = array(
          "draw"            => intval($request->input('draw')),
          "recordsTotal"    => intval($totalData),
          "recordsFiltered" => intval($totalFiltered),
          "data"            => $data
        );
      } else {
        $datas = null;
      }
    } catch (\Throwable $th) {
      dd($th);
      $datas = null;
    }

    return json_encode($datas);
  }

  public static function addColumn($itens, $newKey, $function)
  {
      $itens = json_decode($itens);
      $newData = array();
      foreach ($itens->data as $item) {
          $item->$newKey = $function($item);
          $newData[] = $item;
      }

      $itens->data = $newData;

      return json_encode($itens);

  }

  public static function tirarAcentos($string){
    $from = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
    $to = "aaaaeeiooouucAAAAEEIOOOUUC";

    return strtr($string, utf8_decode($from), $to);
  }
}
