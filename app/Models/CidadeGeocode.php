<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Bairro;

class CidadeGeocode extends Model
{
    protected $table = 'cidades_geocodes';

    public function cidade()
    {
        return $this->belongsTo('App\Models\Cidade');
    }
}
