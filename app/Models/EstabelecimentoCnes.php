<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;


class EstabelecimentoCnes extends Model
{
  protected $guarded = [];

  public function estabelecimentoSies()
  {
      return $this->hasMany('App\Models\EstabelecimentoSies');
  }
    
  public static function toDataTables($request, $tabela, $where = NULL)
  {

      try {
          $columnsName = array();
          $columnsSearchable = array();

          $foreignKeys = null;
          foreach ($request->columns as $column){

              if ($column['orderable'] != 'false') {
                  if (strpos($column['name'], '_id') !== false) {
                      $foreignTable  = explode("_", $column['name']);
                      $foreignTable[0] = $foreignTable[0].'s';
                      $foreignTable[] = $column['name'];
                      $foreignKeys[] = $foreignTable;
                      $columnsName[] = $foreignTable[0].
                      '.name';
                  }else{
                      $columnsName[] = $tabela.'.'.$column['name'];
                  }
              }

              if($column['searchable'] != 'false'){
                  $columnsSearchable[] = $tabela.'.'.$column['name'];
              }

          }
          $columnsNameCont = count($columnsName);
          $columnsSearchableCont = count($columnsSearchable);

          if ($where == NULL) {
              $totalData = DB::table($tabela)->count();
          }else{
              $totalData = DB::table($tabela)->where("agravo", $where)->count();
          }

          $totalFiltered = $totalData;
          $limit = $request->input('length');
          $start = $request->input('start');
          $order = $columnsName[$request->input('order.0.column')];
          $dir = $request->input('order.0.dir');

          if(empty($request->input('search.value')))
          {
              if ($where == NULL) {
                  $query = DB::table($tabela)
                  ->select($columnsName)
                  ->offset($start)
                  ->limit($limit)
                  ->orderBy($order,$dir);
              }else{
                  $query = DB::table($tabela)
                  ->select($columnsName)
                  ->where("agravo", $where)
                  ->offset($start)
                  ->limit($limit)
                  ->orderBy($order,$dir);
              }

              if($foreignKeys !== null){
                  foreach ($foreignKeys as $foreignKey) {
                      $query->join($foreignKey[0],
                          $foreignKey[0].'.'.$foreignKey[1],
                          '=',
                          $tabela.'.'.$foreignKey[2]);
                  }
              }

              $registros = $query->get();
          } else {

              $search = $request->input('search.value');
              $query = DB::table($tabela)
                  ->select($columnsName)
                  ->where(function($q) use ($columnsSearchable, $search){
                      foreach ($columnsSearchable as $columnSearchable) {
                          $q->orWhere($columnSearchable , 'ILIKE', "%$search%");
                          if (isset($where)) {
                              $q->where("agravo", $where);
                          }
                      }

                  })
                  ->offset($start)
                  ->limit($limit)
                  ->orderBy($order,$dir);
              $registros = $query->get();

              $totalFiltered = DB::table($tabela)
              ->where(function($q) use ($columnsSearchable, $search){
                  foreach ($columnsSearchable as $columnSearchable) {
                      $q->orWhere($columnSearchable , 'ILIKE', "%$search%");
                      if (isset($where)) {
                          $q->where("agravo", $where);
                      }
                  }

              })
              ->count();
          }

          $data = array();
          if(!empty($registros))
          {
              foreach ($registros as $registro)
              {
                  if($foreignKeys !== null){
                      $nestedData = (array) $registro;
                      foreach ($foreignKeys as $foreignKey) {
                          $nestedData[$foreignKey[2]] = $nestedData['name'];
                          unset($nestedData['name']);
                      }
                  }else{
                      $nestedData = (array) $registro;
                  }

                  $data[] = $nestedData;

              }
            
							$datas = array(
								"draw"            => intval($request->input('draw')),
								"recordsTotal"    => intval($totalData),
								"recordsFiltered" => intval($totalFiltered),
								"data"            => $data
								);

          } else {
              $datas = null;
          }

      } catch (\Throwable $th) {
          dd($th);
          $datas = null;
      }

      return json_encode($datas);

  }

  //carregar dados
  public static function csvLoad($tableName, $upload, $co_municipio_gestor)
  {
    $delimitador = ';';
    $cerca = '"';

    $file = file_get_contents(storage_path('app/'.$upload));

      if($file){

        $header = NULL;
        $datas = array();
        $numRow = 0;
        $rows = explode("\n", $file);
				
        foreach ($rows as $row) {
            $item = str_getcsv($row, $delimitador, $cerca, "\n");
            if(!$header){
                $header = $item;
                continue;
            }
            if(count($header) == count($item) && $item[31] == $co_municipio_gestor){
                $datas[] = array_combine($header, $item);
                //echo($numRow);
                //echo(", ");
                //$numRow++;
            }else{
                //var_dump($header);
                //var_dump($row);
            }
        }

        foreach($datas as $data){
          if($data['CO_MUNICIPIO_GESTOR'] == $co_municipio_gestor){
            $estabelecimento = EstabelecimentoCnes::updateOrCreate(['co_cnes' => $data['CO_CNES']]);
            $estabelecimento->co_cnes = $data['CO_CNES'];
            $estabelecimento->nu_cnpj_mantenedora = $data['NU_CNPJ_MANTENEDORA'];
            $estabelecimento->no_razao_social = $data['NO_RAZAO_SOCIAL'];
            $estabelecimento->no_fantasia = $data['NO_FANTASIA'];
            $estabelecimento->no_logradouro = $data['NO_LOGRADOURO'];
            $estabelecimento->nu_endereco = $data['NU_ENDERECO'];
            $estabelecimento->no_complemento = $data['NO_COMPLEMENTO'];
            $estabelecimento->no_bairro = $data['NO_BAIRRO'];
            $estabelecimento->co_cep = $data['CO_CEP'];
            $estabelecimento->nu_telefone = $data['NU_TELEFONE'];
            $estabelecimento->no_email = $data['NO_EMAIL'];
            $estabelecimento->nu_cnpj = $data['NU_CNPJ'];
            $estabelecimento->co_estado_gestor = $data['CO_ESTADO_GESTOR'];
            $estabelecimento->co_municipio_gestor = $data['CO_MUNICIPIO_GESTOR'];
            $estabelecimento->nu_latitude = ($data['NU_LATITUDE'] != "") ? $data['NU_LATITUDE'] : NULL;
            $estabelecimento->nu_longitude = ($data['NU_LONGITUDE'] != "") ? $data['NU_LONGITUDE'] : NULL;
            
            $estabelecimento->save();
          }
        
        }

      }
  return true;
  }

  public static function addColumn($itens, $newKey, $function)
  {
      $itens = json_decode($itens);
      $newData = array();
      foreach ($itens->data as $item) {
          $item->$newKey = $function($item);
          $newData[] = $item;
      }

      $itens->data = $newData;

      return json_encode($itens);

  }

}
