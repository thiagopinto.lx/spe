<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Bairro;

class BairroGeocode extends Model
{
    public function bairro()
    {
        return $this->belongsTo('App\Models\Bairro');
    }
}
