<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use App\Models\EstabelecimentoCnes;
use DateTime;


class LoadInmet extends Model
{
    protected $table = 'load_inmets';

    protected $guarded = [];

    public static function toDataTables($request, $tabela, $where = NULL)
    {

        try {
            $columnsName = array();
            $columnsSearchable = array();

            $foreignKeys = null;
            foreach ($request->columns as $column) {

                if ($column['orderable'] != 'false') {
                    if (strpos($column['name'], '_id') !== false) {

                        $foreignTable[0] = substr($column['name'], 0, strpos($column['name'], "_id"));
                        $foreignTable[1] = substr($column['name'], strpos($column['name'], "_id") + 1, 2);

                        $foreignTable[] = $column['name'];
                        $foreignKeys[] = $foreignTable;
                        $columnsName[] = $foreignTable[0] .
                            '.nome';
                    } else {
                        $columnsName[] = $tabela . '.' . $column['name'];
                    }
                }

                if ($column['searchable'] != 'false') {
                    $columnsSearchable[] = $tabela . '.' . $column['name'];
                }
            }
            $columnsNameCont = count($columnsName);
            $columnsSearchableCont = count($columnsSearchable);

            if ($where == NULL) {
                $totalData = DB::table($tabela)->count();
            } else {
                $totalData = DB::table($tabela)->where("agravo", $where)->count();
            }

            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columnsName[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if (empty($request->input('search.value'))) {
                if ($where == NULL) {
                    $query = DB::table($tabela)
                        ->select($columnsName)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order, $dir);
                } else {
                    $query = DB::table($tabela)
                        ->select($columnsName)
                        ->where("agravo", $where)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order, $dir);
                }

                if ($foreignKeys !== null) {
                    foreach ($foreignKeys as $foreignKey) {
                        $query->join(
                            $foreignKey[0],
                            $foreignKey[0] . '.' . $foreignKey[1],
                            '=',
                            $tabela . '.' . $foreignKey[2]
                        );
                    }
                }

                $registros = $query->get();
            } else {

                $search = $request->input('search.value');
                $query = DB::table($tabela)
                    ->select($columnsName)
                    ->where(function ($q) use ($columnsSearchable, $search, $where) {
                        foreach ($columnsSearchable as $columnSearchable) {
                            $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
                            if (isset($where)) {
                                $q->where("agravo", $where);
                            }
                        }
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir);
                $registros = $query->get();

                $totalFiltered = DB::table($tabela)
                    ->where(function ($q) use ($columnsSearchable, $search, $where) {
                        foreach ($columnsSearchable as $columnSearchable) {
                            $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
                            if (isset($where)) {
                                $q->where("agravo", $where);
                            }
                        }
                    })
                    ->count();
            }

            $data = array();
            if (!empty($registros)) {
                foreach ($registros as $registro) {
                    if ($foreignKeys !== null) {
                        $nestedData = (array) $registro;
                        foreach ($foreignKeys as $foreignKey) {
                            $nestedData[$foreignKey[2]] = $nestedData['nome'];
                            unset($nestedData['nome']);
                        }
                    } else {
                        $nestedData = (array) $registro;
                    }

                    $data[] = $nestedData;
                }

                $datas = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                );
            } else {
                $datas = null;
            }
        } catch (\Throwable $th) {
            dd($th);
            $datas = null;
        }

        return json_encode($datas);
    }


    //carregar dados
    public static function csvLoad($upload, $request)
    {
        $delimitador = ';';
        $cerca = '';

        $file = file_get_contents(storage_path('app/' . $upload));
        /* Teste de performace */

        //$initPro = new \DateTime('now');
        //$resposta = "Inicio:".$initPro->format('H:i:s.u');

        if ($file) {

            $header = NULL;
            $datas = array();
            $numRow = 0;
            //$rows = explode("\n", $file);
            $rows = array_filter(explode("\n", $file));
            $formato = 'd/m/Y';
            $newFormat = 'Y-m-d';
            $currentAno = null;
            $currentLoadInmet = null;
            $valuesSqlUpSert = "";
            $countRows = count($rows);

            //dd($countRows);

            for ($i = 0; $i < $countRows; $i++) {
                $item = str_getcsv($rows[$i], $delimitador, $cerca, "\n");
                if (!$header) {
                    $itensClean = array();

                    $countItem = count($item);
                    for ($j = 0; $j < $countItem; $j++) {
                        $item[$j] = LoadInmet::tirarAcentos($item[$j]);
                        $item[$j] = str_replace(" ", "_", str_replace(".", "", strtolower($item[$j])));
                        $header[] = $item[$j];
                    }
                    continue;
                }


                if (count($header) == count($item)) {

                    $tempRow = null;
                    foreach ($item as $value) {
                        $tempRow[] = mb_convert_encoding($value, 'UTF-8', 'ISO-8859-1');
                    }
                    $data = array_combine($header, $tempRow);
                    $data['data'] = DateTime::createFromFormat($formato, $data['data']);
                    $ano = (int) $data['data']->format('Y');

                    if ($ano != $currentAno) {

                        $tableName = "inmet_" . $ano;

                        $loadInmet = LoadInmet::updateOrCreate(
                            [
                                'ano' => $data['data']->format('Y'),
                            ]
                        );

                        if (!Schema::hasTable($tableName)) {
                            LoadInmet::createTable($tableName);
                        }

                        if (isset($request->color)) {
                            $loadInmet->color = $request->color;
                        }

                        $loadInmet->save();
                        $currentLoadInmet = $loadInmet;
                        $currentAno = $ano;
                        $currentTableName = $tableName;
                    }

                    $data['data'] = $data['data']->format($newFormat);
                    $data['load_inmet_id'] = $currentLoadInmet->id;
                    $data['created_at'] = date('Y-m-d H:i:s');
                    $data['updated_at'] = date('Y-m-d H:i:s');

                    foreach ($data as $key => $value) {
                        $value = trim($value);
                        if ($value == "") {

                            $data[$key]="NULL";
                        }
                    }

                    $value =
                        "(
                            {$data['estacao']},
                            '{$data['data']}',
                            {$data['hora']},
                            {$data['precipitacao']},
                            {$data['tempmaxima']},
                            {$data['tempminima']},
                            {$data['umidade_relativa_media']},
                            {$data['velocidade_do_vento_media']},
                            {$data['load_inmet_id']}
                        )";

                    if ($i < ($countRows - 1)) {
                        $valuesSqlUpSert .= "{$value}, ";
                    } else {
                        $valuesSqlUpSert .= "{$value}";
                    }



                    /*
                DB::table($currentTableName)
                ->updateOrInsert(
                  [
                    'vacinado' => $data['vacinado'],
                    'imuno_sipnis_id' => $data['imuno_sipnis_id'],
                    'dose' => $data['dose'],
                    'estrategia' => $data['estrategia'],
                    'data_de_nascimento' => $data['data_de_nascimento'],
                    'data_de_aplicacao' => $data['data_de_aplicacao'],
                    'estabelecimento_cnes_id' => $data['estabelecimento_cnes_id']
                  ],
                  $data);
                */


                    //$datas[] = $data;

                } else {
                    //var_dump($row);
                }
            }


            $columnSQL = "(
            estacao,
            data,
            hora,
            precipitacao,
            tempmaxima,
            tempminima,
            umidade_relativa_media,
            velocidade_do_vento_media,
            load_inmet_id
        )";

        $setColumnSQL = "
                estacao = excluded.estacao,
                data = excluded.data,
                hora = excluded.hora,
                precipitacao = excluded.precipitacao,
                tempmaxima = excluded.tempmaxima,
                tempminima = excluded.tempminima,
                umidade_relativa_media = excluded.umidade_relativa_media,
                velocidade_do_vento_media = excluded.velocidade_do_vento_media,
                load_inmet_id = excluded.load_inmet_id
        ;";


            $SqlUpSert = "
          INSERT INTO {$currentTableName} {$columnSQL}
          VALUES {$valuesSqlUpSert}
          ON CONFLICT ON CONSTRAINT {$currentTableName}_unique_register DO UPDATE
          SET {$setColumnSQL}";

            try {
                DB::statement($SqlUpSert);
            } catch (\Throwable $th) {
                echo ($th);
                DB::rollback();
            }



            DB::commit();
            //dd($SqlUpSert);

            /* Teste de performace */
            //$finshPro = new \DateTime('now');
            //$resposta .= "\n Fim:" . $finshPro->format('H:i:s.u');
            //Calcula a diferença entre as datas
            //$diff   =   $initPro->diff($finshPro, true);
            //dd($diff);

            echo ("<div class='p-3 mb-2 bg-warning text-dark'>Dados do INMET Ano: {$ano}</div>");

            DB::commit();
        }
        return true;
    }

    public static function addColumn($itens, $newKey, $function)
    {
        $itens = json_decode($itens);
        $newData = array();
        foreach ($itens->data as $item) {
            $item->$newKey = $function($item);
            $newData[] = $item;
        }

        $itens->data = $newData;

        return json_encode($itens);
    }

    public static function createTable($tableName)
    {

        try {

            Schema::create($tableName, function (Blueprint $table) use ($tableName) {
                $table->increments('id');
                $table->unsignedInteger('estacao');
                $table->date('data')->nullable();
                $table->unsignedInteger('hora')->nullable();
                $table->float('precipitacao')->nullable();
                $table->float('tempmaxima')->nullable();
                $table->float('tempminima')->nullable();
                $table->float('umidade_relativa_media')->nullable();
                $table->float('velocidade_do_vento_media')->nullable();
                $table->unsignedInteger('load_inmet_id')->nullable();
                $table->timestamps();
                $table->foreign('load_inmet_id')->references('id')->on('load_inmets');
                $table->unique(['estacao', 'data', 'precipitacao', 'tempmaxima', 'tempminima', 'umidade_relativa_media', 'velocidade_do_vento_media', 'load_inmet_id'], $tableName.'_unique_register');
            });
        } catch (\Exception $e) {

            echo "Desculpe, não consegui criar a tabela: "
                . $tableName
                . $e->getMessage();
            return false;
        }

        return true;
    }

    public static function tirarAcentos($string)
    {
        $from = "áàãâéêíóôõúüçÁÀÃÂÉÊÍÓÔÕÚÜÇ";
        $to = "aaaaeeiooouucAAAAEEIOOOUUC";

        return strtr($string, utf8_decode($from), $to);
    }
}
