<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use App\Utilities\Helper;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function toDataTables($request, $tabela, $where = null)
    {
      $columnsName = array();
      $columnsSearchable = array();
  
      $foreignKeys = null;
      foreach ($request->columns as $column) {
  
        if ($column['orderable'] != 'false') {
          if (strpos($column['name'], '_id') !== false) {
            $foreignTable  = explode("_", $column['name']);
            $foreignTable[0] = $foreignTable[0] . 's';
            $foreignTable[] = $column['name'];
            $foreignKeys[] = $foreignTable;
            $columnsName[] = $foreignTable[0] .
              '.name';
          } else {
            $columnsName[] = $tabela . '.' . $column['name'];
          }
        }
  
        if ($column['searchable'] != 'false') {
          $columnsSearchable[] = $tabela . '.' . $column['name'];
        }
      }
      $columnsNameCont = count($columnsName);
      $columnsSearchableCont = count($columnsSearchable);
  
      if ($where == null) {
        $totalData = DB::table($tabela)->count();
      } else {
        $totalData = DB::table($tabela)->where("name", $where)->count();
      }
  
      $totalFiltered = $totalData;
      $limit = $request->input('length');
      $start = $request->input('start');
      $order = $columnsName[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');
  
      if (empty($request->input('search.value'))) {
        if ($where == null) {
          $query = DB::table($tabela)
            ->select($columnsName)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir);
        } else {
          $query = DB::table($tabela)
            ->select($columnsName)
            ->where("name", $where)
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir);
        }
  
        if ($foreignKeys !== null) {
          foreach ($foreignKeys as $foreignKey) {
            $query->join(
              $foreignKey[0],
              $foreignKey[0] . '.' . $foreignKey[1],
              '=',
              $tabela . '.' . $foreignKey[2]
            );
          }
        }
  
        $registros = $query->get();
      } else {
  
        $search = $request->input('search.value');
        $query = DB::table($tabela)
          ->select($columnsName)
          ->where(function ($q) use ($columnsSearchable, $search) {
            foreach ($columnsSearchable as $columnSearchable) {
              $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
              if (isset($where)) {
                $q->where("name", $where);
              }
            }
          })
          ->offset($start)
          ->limit($limit)
          ->orderBy($order, $dir);
        $registros = $query->get();
  
        $totalFiltered = DB::table($tabela)
          ->where(function ($q) use ($columnsSearchable, $search) {
            foreach ($columnsSearchable as $columnSearchable) {
              $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
              if (isset($where)) {
                $q->where("name", $where);
              }
            }
          })
          ->count();
      }
  
      $data = array();
      if (!empty($registros)) {
        foreach ($registros as $registro) {
          if ($foreignKeys !== null) {
            $nestedData = (array) $registro;
            foreach ($foreignKeys as $foreignKey) {
              $nestedData[$foreignKey[2]] = $nestedData['name'];
              unset($nestedData['name']);
            }
          } else {
            $nestedData = (array) $registro;
          }
  
          $data[] = $nestedData;
        }
  
        $datas = array(
          "draw"            => intval($request->input('draw')),
          "recordsTotal"    => intval($totalData),
          "recordsFiltered" => intval($totalFiltered),
          "data"            => $data
        );
      } else {
        $datas = null;
      }
  
      return json_encode($datas);
    }

  public static function addColumn($itens, $newKey, $function)
  {
    $itens = json_decode($itens);
    $newData = array();
    foreach ($itens->data as $item) {
      $item->$newKey = $function($item);
      $newData[] = $item;
    }

    $itens->data = $newData;

    return json_encode($itens);
  }
  
}
