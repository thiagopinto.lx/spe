<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoadData extends Model
{
    protected $table = 'load_datas';

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
