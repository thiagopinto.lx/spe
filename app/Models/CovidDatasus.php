<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use DateTime;

class CovidDatasus extends Model
{
    protected $guarded = [];
    protected $table = 'coviddatasus';

    public function bairro_datasus()
    {
        return $this->belongsTo('App\Models\BairroAlias', 'bairroalias_id');
    }

    public function bairro()
    {
        return $this->belongsTo('App\Models\BairroAlias', 'bairroalias_id');
    }

    public static function toDataTables($request, $tabela, $where = null)
    {

        try {
            $columnsName = array();
            $columnsSearchable = array();

            $foreignKeys = null;
            foreach ($request->columns as $column) {
                if ($column['orderable'] != 'false') {
                    if (strpos($column['name'], '_id') !== false) {
                        $foreignTable  = explode("_", $column['name']);
                        $foreignTable[0] = $foreignTable[0];
                        $foreignTable[] = $column['name'];
                        $foreignKeys[] = $foreignTable;
                        $columnsName[] = $foreignTable[0] .
                            '.nome';
                    } else {
                        $columnsName[] = $tabela . '.' . $column['name'];
                    }
                }

                if ($column['searchable'] != 'false') {
                    $columnsSearchable[] = $tabela . '.' . $column['name'];
                }
            }
            $columnsNameCont = count($columnsName);
            $columnsSearchableCont = count($columnsSearchable);

            if ($where == null) {
                $totalData = DB::table($tabela)->count();
            } else {
                $totalData = DB::table($tabela)->where("casos", $where)->count();
            }

            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $order = $columnsName[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');

            if (empty($request->input('search.value'))) {
                if ($where == null) {
                    $query = DB::table($tabela)
                        ->select($columnsName)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order, $dir);
                } else {
                    $query = DB::table($tabela)
                        ->select($columnsName)
                        ->where("casos", $where)
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order, $dir);
                }

                if ($foreignKeys !== null) {
                    foreach ($foreignKeys as $foreignKey) {
                        $query->join(
                            $foreignKey[0],
                            $foreignKey[0] . '.' . $foreignKey[1],
                            '=',
                            $tabela . '.' . $foreignKey[2]
                        );
                    }
                }

                $registros = $query->get();
            } else {

                $search = $request->input('search.value');
                $query = DB::table($tabela)
                    ->select($columnsName)
                    ->where(function ($q) use ($columnsSearchable, $search, $where) {
                        foreach ($columnsSearchable as $columnSearchable) {
                            $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
                            if (isset($where)) {
                                $q->where("casos", $where);
                            }
                        }
                    })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir);
                $registros = $query->get();

                $totalFiltered = DB::table($tabela)
                    ->where(function ($q) use ($columnsSearchable, $search, $where) {
                        foreach ($columnsSearchable as $columnSearchable) {
                            $q->orWhere($columnSearchable, 'ILIKE', "%$search%");
                            if (isset($where)) {
                                $q->where("casos", $where);
                            }
                        }
                    })
                    ->count();
            }

            $data = array();
            if (!empty($registros)) {
                foreach ($registros as $registro) {
                    if ($foreignKeys !== null) {
                        $nestedData = (array) $registro;
                        foreach ($foreignKeys as $foreignKey) {
                            $nestedData[$foreignKey[2]] = $nestedData['nome'];
                            unset($nestedData['nome']);
                        }
                    } else {
                        $nestedData = (array) $registro;
                    }

                    $data[] = $nestedData;
                }

                $datas = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                );
            } else {
                $datas = null;
            }
        } catch (\Throwable $th) {
            dd($th);
            $datas = null;
        }

        return json_encode($datas);
    }

    public static function addColumn($itens, $newKey, $function)
    {
        $itens = json_decode($itens);
        $newData = array();
        foreach ($itens->data as $item) {
            $item->$newKey = $function($item);
            $newData[] = $item;
        }

        $itens->data = $newData;

        return json_encode($itens);
    }

    public static function saveAs($covidDatasus, $notificacao)
    {
        try {
            if (isset($notificacao['_source']['bairro'])) {
                if ($notificacao['_source']['bairro']  == "") {
                    $bairroAlias = BairroAlias::updateOrCreate(
                        [
                            'nome' => 'DESCONHECIDO'
                        ]
                    );
                    $bairroAlias->save();
                } elseif (strlen($notificacao['_source']['bairro']) < 4) {
                    $bairroAlias = BairroAlias::updateOrCreate(
                        [
                            'nome' => 'DESCONHECIDO'
                        ]
                    );
                    $bairroAlias->save();
                } else {
                    $nomeBairro = CovidDatasus::tirarAcentos($notificacao['_source']['bairro']);
                    $nomeBairro = strtoupper(trim($nomeBairro));
                    try {
                        $bairroAlias = BairroAlias::updateOrCreate(
                            [
                                'nome' => $nomeBairro
                            ]
                        );
                        $bairroAlias->save();
                    } catch (\Throwable $th) {
                        dd($th);
                    }
                }
            } else {
                $bairroAlias = BairroAlias::updateOrCreate(
                    [
                        'nome' => 'DESCONHECIDO'
                    ]
                );
                $bairroAlias->save();
            }
        } catch (\Throwable $th) {
            dd($notificacao, $th);
        }



        try {
          /*
            if (isset($notificacao['_source']['dataNascimento'])) {
                $date = '01/' . $notificacao['_source']['dataNascimento'];
                $date = str_replace('/', '-', $date);
                $date = date('Y-m-d', strtotime($date));
                $date = new DateTime($date);
                $covidDatasus->dataNascimento = $date->format(DateTime::ATOM);
            }
            */
            if (isset($notificacao['_source']['dataNascimento'])) {
                $covidDatasus->dataNascimento = $notificacao['_source']['dataNascimento'];
            }
            if (isset($notificacao['_source']['dataNotificacao'])) {
                $covidDatasus->dataNotificacao = $notificacao['_source']['dataNotificacao'];
            }
            if (isset($notificacao['_source']['dataInicioSintomas'])) {
                $covidDatasus->dataInicioSintomas = $notificacao['_source']['dataInicioSintomas'];
            }
            if (isset($notificacao['_source']['dataTeste'])) {
                $covidDatasus->dataTeste = $notificacao['_source']['dataTeste'];
            }
            if (isset($notificacao['_source']['_p_usuario'])) {
                $covidDatasus->_p_usuario = $notificacao['_source']['_p_usuario'];
            }
            if (isset($notificacao['_source']['estrangeiro'])) {
                $covidDatasus->estrangeiro = $notificacao['_source']['estrangeiro'];
            }
            if (isset($notificacao['_source']['profissionalSaude'])) {
                $covidDatasus->profissionalSaude = $notificacao['_source']['profissionalSaude'];
            }
            if (isset($notificacao['_source']['profissionalSeguranca'])) {
                $covidDatasus->profissionalSeguranca = $notificacao['_source']['profissionalSeguranca'];
            }
            if (isset($notificacao['_source']['cbo'])) {
                $covidDatasus->cbo = $notificacao['_source']['cbo'];
            }
            if (isset($notificacao['_source']['cpf'])) {
                $covidDatasus->cpf = $notificacao['_source']['cpf'];
            }
            if (isset($notificacao['_source']['cns'])) {
                $covidDatasus->cns = $notificacao['_source']['cns'];
            }
            if (isset($notificacao['_source']['nomeCompleto'])) {
                $covidDatasus->nomeCompleto = $notificacao['_source']['nomeCompleto'];
            }
            if (isset($notificacao['_source']['nomeMae'])) {
                $covidDatasus->nomeMae = $notificacao['_source']['nomeMae'];
            }
            if (isset($notificacao['_source']['sexo'])) {
                $covidDatasus->sexo = $notificacao['_source']['sexo'];
            }
            if (isset($notificacao['_source']['racaCor'])) {
                $covidDatasus->racaCor = $notificacao['_source']['racaCor'];
            }
            if (isset($notificacao['_source']['passaporte'])) {
                $covidDatasus->passaporte = $notificacao['_source']['passaporte'];
            }
            if (isset($notificacao['_source']['cep'])) {
                $covidDatasus->cep = $notificacao['_source']['cep'];
            }
            if (isset($notificacao['_source']['logradouro'])) {
                $covidDatasus->logradouro = $notificacao['_source']['logradouro'];
            }
            if (isset($notificacao['_source']['numero'])) {
                $covidDatasus->numero = $notificacao['_source']['numero'];
            }
            if (isset($notificacao['_source']['complemento'])) {
                $covidDatasus->complemento = $notificacao['_source']['complemento'];
            }
            if (isset($notificacao['_source']['bairro'])) {
                $covidDatasus->bairro = $notificacao['_source']['bairro'];
            }

            $covidDatasus->bairroalias_id = $bairroAlias->id;

            if (isset($notificacao['_source']['estado'])) {
                $covidDatasus->estado = $notificacao['_source']['estado'];
            }
            if (isset($notificacao['_source']['municipio'])) {
                $covidDatasus->municipio = $notificacao['_source']['municipio'];
            }
            if (isset($notificacao['_source']['telefoneContato'])) {
                $covidDatasus->telefoneContato = $notificacao['_source']['telefoneContato'];
            }
            if (isset($notificacao['_source']['telefone'])) {
                $covidDatasus->telefone = $notificacao['_source']['telefone'];
            }
            if (isset($notificacao['_source']['sintomas'])) {
                $covidDatasus->sintomas = $notificacao['_source']['sintomas'];
            }
            if (isset($notificacao['_source']['outrosSintomas'])) {
                $covidDatasus->outrosSintomas = $notificacao['_source']['outrosSintomas'];
            }
            if (isset($notificacao['_source']['condicoes'])) {
                $covidDatasus->condicoes = $notificacao['_source']['condicoes'];
            }
            if (isset($notificacao['_source']['estadoTeste'])) {
                $covidDatasus->estadoTeste = $notificacao['_source']['estadoTeste'];
            }
            if (isset($notificacao['_source']['tipoTeste'])) {
                $covidDatasus->tipoTeste = $notificacao['_source']['tipoTeste'];
            }
            if (isset($notificacao['_source']['resultadoTeste'])) {
                $covidDatasus->resultadoTeste = $notificacao['_source']['resultadoTeste'];
            }
            if (isset($notificacao['_source']['cnes'])) {
                $covidDatasus->cnes = $notificacao['_source']['cnes'];
            }
            if (isset($notificacao['_source']['estadoNotificacao'])) {
                $covidDatasus->estadoNotificacao = $notificacao['_source']['estadoNotificacao'];
            }
            if (isset($notificacao['_source']['municipioNotificacao'])) {
                $covidDatasus->municipioNotificacao = $notificacao['_source']['municipioNotificacao'];
            }
            if (isset($notificacao['_source']['numeroNotificacao'])) {
                $covidDatasus->numeroNotificacao = $notificacao['_source']['numeroNotificacao'];
            }
            $covidDatasus->_created_at_datasus = $notificacao['_source']['_created_at'];
            $covidDatasus->_updated_at_datasus = $notificacao['_source']['_updated_at'];
            if (isset($notificacao['_source']['desnormalizarNome'])) {
                $covidDatasus->desnormalizarNome = $notificacao['_source']['desnormalizarNome'];
            }
            if (isset($notificacao['_source']['nomeCompletoDesnormalizado'])) {
                $covidDatasus->nomeCompletoDesnormalizado = $notificacao['_source']['nomeCompletoDesnormalizado'];
            }
            if (isset($notificacao['_source']['idade'])) {
                $covidDatasus->idade = $notificacao['_source']['idade'];
            }
            if (isset($notificacao['_source']['dataEncerramento'])) {
                $covidDatasus->dataEncerramento = $notificacao['_source']['dataEncerramento'];
            }
            if (isset($notificacao['_source']['evolucaoCaso'])) {
                $covidDatasus->evolucaoCaso = $notificacao['_source']['evolucaoCaso'];
            }
            if (isset($notificacao['_source']['classificacaoFinal'])) {
                $covidDatasus->classificacaoFinal = $notificacao['_source']['classificacaoFinal'];
            }
            if (isset($notificacao['_source']['paisOrigem'])) {
                $covidDatasus->paisOrigem = $notificacao['_source']['paisOrigem'];
            }
            if (isset($notificacao['_source']['origem'])) {
                $covidDatasus->origem = $notificacao['_source']['origem'];
            }
            if (isset($notificacao['_source']['estadoIBGE'])) {
                $covidDatasus->estadoIBGE = $notificacao['_source']['estadoIBGE'];
            }
            if (isset($notificacao['_source']['estadoNotificacaoIBGE'])) {
                $covidDatasus->estadoNotificacaoIBGE = $notificacao['_source']['estadoNotificacaoIBGE'];
            }
            if (isset($notificacao['_source']['municipioIBGE'])) {
                $covidDatasus->municipioIBGE = $notificacao['_source']['municipioIBGE'];
            }
            if (isset($notificacao['_source']['municipioCapital'])) {
                $covidDatasus->municipioCapital = $notificacao['_source']['municipioCapital'];
            }
            if (isset($notificacao['_source']['municipioNotificacaoIBGE'])) {
                $covidDatasus->municipioNotificacaoIBGE = $notificacao['_source']['municipioNotificacaoIBGE'];
            }
            if (isset($notificacao['_source']['municipioNotificacaoCapital'])) {
                $covidDatasus->municipioNotificacaoCapital = $notificacao['_source']['municipioNotificacaoCapital'];
            }
            if (isset($notificacao['_source']['excluido'])) {
                $covidDatasus->excluido = $notificacao['_source']['excluido'];
            }
            $covidDatasus->save();
        } catch (\Throwable $th) {
            dd($notificacao, $th);
        }
    }

    public static function tirarAcentos($string)
    {
        $comAcentos = array('/', '.', '. ', '-', 'à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ü', 'Ú');
        $semAcentos = array(' ', '', '', '', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U');
        $string = str_replace($comAcentos, $semAcentos, $string);
        $vogaisRepetidas = array('  ', 'AA', 'EE', 'II', 'OO', 'UU', 'aa', 'ee', 'ii', 'oo', 'uu');
        $vogaisUnicas = array(' ', 'A', 'E', 'I', 'O', 'U', 'a', 'e', 'i', 'o', 'u');

        return str_replace($vogaisRepetidas, $vogaisUnicas, $string);
    }
}
